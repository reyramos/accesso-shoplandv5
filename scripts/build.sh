#!/bin/bash
clear
cat <<'EOF'
                                                                      ,----,.
                                                                    ,'   ,' |
  .--.--.     ,---,                                               ,'   .'   |
 /  /    '. ,--.' |              ,-.----.                 ,---. ,----.'    .'
|  :  /`. / |  |  :       ,---.  \    /  \               /__./| |    |   .'
;  |  |--`  :  :  :      '   ,'\ |   :    |         ,---.;  ; | :    :  |--,
|  :  ;_    :  |  |,--. /   /   ||   | .\ :        /___/ \  | | :    |  ;.' \
 \  \    `. |  :  '   |.   ; ,. :.   : |: |        \   ;  \ ' | |    |      |
  `----.   \|  |   /' :'   | |: :|   |  \ :         \   \  \: | `----'.'\   ;
  __ \  \  |'  :  | | |'   | .; :|   : .  |          ;   \  ' .   __  \  .  |
 /  /`--'  /|  |  ' | :|   :    |:     |`-'           \   \   ' /   /\/  /  :
'--'.     / |  :  :_:,' \   \  / :   : :               \   `  ;/ ,,/  ',-   .
  `--'---'  |  | ,'      `----'  |   | :                :   \ |\ ''\       ;
            `--''                `---'.|                 '---"  \   \    .'
                                   `---`                         `--`-,-'
--------------------------------------------------------
   Shopland V5 Testing/Staging Build Script (of Doom)
--------------------------------------------------------
EOF

formatSeconds() {
 ((h=${1}/3600))
  ((m=(${1}%3600)/60))
   ((s=${1}%60))
    printf "%02d:%02d:%02d\n" $h $m $s
}

LOGFILE="/home/accesso/logs/build"

declare -A FAILED_HASHES

while sleep 5; do

    SOURCE_DIR="/home/accesso/server/node"
    cd $SOURCE_DIR

    git clean -f -d
    REMOTE_HASH=$(git ls-remote origin -h refs/heads/develop | sed 's/\srefs.*$//g')
    LOCAL_HASH=$(git rev-list --max-count=1 develop)
    if [ "$REMOTE_HASH" != "$LOCAL_HASH" ]; then
        printf "\nProject: node-api\n" | tee -a $LOGFILE.log
        echo "Last remote hash: \"$REMOTE_HASH\"" | tee -a $LOGFILE.log
        echo "Last local hash: \"$LOCAL_HASH\"" | tee -a $LOGFILE.log
        echo -n "Pulling..." | tee -a $LOGFILE.log
        git pull --tags origin develop >> $LOGFILE.node-api.log 2>&1
        echo "done" | tee -a $LOGFILE.log
        killall node
        echo "--------------------------------------------------------" | tee -a $LOGFILE.log
    fi

    for project in 'store' 'passbook'; do
    
        SOURCE_DIR="/home/accesso/$project-source"
        DEST_DIR="/home/accesso/$project"
    
        cd $SOURCE_DIR
        git clean -f -d
        BRANCHES="$(git branch | grep -o '[0-9A-Za-z]\+')"
        BUILD_TASKS="clean:dist ngtemplates requirejs concat:dist uglify
        spritesheet less:dist imagemin template:dist htmlmin copy:dist copy:dist2
        compress"
        LATEST_TAG=$(git describe --tags `git rev-list --tags --max-count=1`)
        PREV_LATEST_TAG=$LATEST_TAG
        git fetch --tags >> $LOGFILE.$ref.log 2>&1
        LATEST_TAG=$(git describe --tags `git rev-list --tags --max-count=1`)
        TAGS="$(git tag -l)"
   
        function build () {
            if [ "$1" != "$2" ] || [ ! "$(ls -A $DEST_DIR/$ref )" ]; then

    
                printf "\nProject: $project\n" | tee -a $LOGFILE.log
                echo "Tag/Branch: $ref" | tee -a $LOGFILE.log
                echo "Last remote hash: \"$1\"" | tee -a $LOGFILE.log
                echo "Last local hash: \"$2\"" | tee -a $LOGFILE.log

                BOWER_UPDATE=false
                NPM_UPDATE=false
                START_TIME="$(date +%s)"
    
                rm $LOGFILE.$ref.log
                echo -n "Pulling..." | tee -a $LOGFILE.log
                git pull --tags origin $ref >> $LOGFILE.$ref.log 2>&1
                echo "done" | tee -a $LOGFILE.log
                
                COMMIT_LOG=$(git log $2..$1)
                CHANGE_LOG=$(git diff $2..$1)
    
                CHANGED_FILES=$(git diff --name-only $1 $2 )
                for file in $CHANGED_FILES; do
                    if [[ "$file" == *package.json* ]]; then
                        NPM_UPDATE=true
                    fi
                    if [[ "$file" == *bower.json* ]]; then
                        BOWER_UPDATE=true
                    fi
                done
                if [ ! "$(ls -A $SOURCE_DIR/app/lib )" ]; then
                    BOWER_UPDATE=true
                fi
                if [ ! "$(ls -A $SOURCE_DIR/node_modules )" ]; then
                    NPM_UPDATE=true
                fi
    
                if $NPM_UPDATE; then
                    echo -n "Installing Build Dependencies..." | tee -a $LOGFILE.log
                    rm -rf node_modules
                    npm install  >> $LOGFILE.$ref.log 2>&1
                    echo "done" | tee -a $LOGFILE.log
                fi
    
                BUILD_SUCCESS=true

                echo "Building..." | tee -a $LOGFILE.log
                    
                if $BOWER_UPDATE; then
                    echo -n "  - bower..." | tee -a $LOGFILE.log
                    if [ -d "app/lib" ]; then
                        rm -rf app/lib >> $LOGFILE.$ref.log 2>&1
                    fi
                    if [ -d "components" ]; then
                        rm -rf components >> $LOGFILE.$ref.log 2>&1
                    fi
                    bower cache-clean >> $LOGFILE.$ref.log 2>&1
                    bower install >> $LOGFILE.$ref.log 2>&1
                    if [ $? -ne 0 ]; then
                        echo "Build failed on grunt task: bower" | tee -a $LOGFILE.log
                        BUILD_SUCCESS=false

                        if [[ !${FAILED_HASHES[$2]} ]]; then
                            FAILED_HASHES["$2"]=""
                            echo "sending failure email!!!!" 
                        fi

                        break
                    fi
                    BOWER_UPDATE=false
                    echo "done" | tee -a $LOGFILE.log
                fi

                for task in $BUILD_TASKS; do
                    echo -n "  - $task..." | tee -a $LOGFILE.log
    
                    TASK_START_TIME="$(date +%s)"
                    grunt --buildVersion="$ref" $task --force >> $LOGFILE.$ref.log 2>&1
    
                    if [ $? -ne 0 ]; then
                        echo "Build failed on grunt task: $task" | tee -a $LOGFILE.log
                        BUILD_SUCCESS=false

                        if [[ !${FAILED_HASHES[$2]} ]]; then
                            FAILED_HASHES["$2"]=""
                            echo -n "Sending email notification to team..." | tee -a $LOGFILE.log
                            printf "V5 Test Server failed building: $ref\n
Build failed on task: $task\n
----------------------------------------------------------------------------
  Commit Log:
----------------------------------------------------------------------------
\n$COMMIT_LOG\n
----------------------------------------------------------------------------
  Change Log:
----------------------------------------------------------------------------
\n$CHANGE_LOG\n
" | mail -s  "Build Failed: $ref" frontend-js@accesso.com
                            echo "done"
 
                        fi

                        break
                    fi
                    TASK_ELAPSED_TIME="$(($(date +%s)-$TASK_START_TIME))"
                    echo "done in $( formatSeconds $TASK_ELAPSED_TIME)" | tee -a $LOGFILE.log
                done
                
                if $BUILD_SUCCESS; then
    
                    echo -n "Syncing to $DEST_DIR..." | tee -a $LOGFILE.log
                    rm -rf $DEST_DIR/$ref
                    mkdir -p $DEST_DIR/$ref
                    if [ -a $SOURCE_DIR/dist ]; then
                        rsync -Pav $SOURCE_DIR/dist/ $DEST_DIR/$ref/ >> $LOGFILE.$ref.log 2>&1
                    else
                        rsync -Pav $SOURCE_DIR/ $DEST_DIR/$ref/ >> $LOGFILE.$ref.log 2>&1
                    fi
                    echo "done" | tee -a $LOGFILE.log

                    if [ "$project" == "store" ]; then
                         if [ "$LATEST_TAG" != "$PREV_LATEST_TAG" ]; then
                            echo -n "Updating merchant symlinks to $LATEST_TAG..." | tee -a $LOGFILE.log
                             MERCHANTS=$(find $DEST_DIR -type l -maxdepth 1)
                             for merchant in $MERCHANTS; do
                                 rm $merchant
                                 ln -sf $DEST_DIR/$LATEST_TAG $merchant
                                 #ls -lah $merchant
                             done
                            echo "done" | tee -a $LOGFILE.log
                         fi
                     fi


                    ELAPSED_TIME="$(($(date +%s)-$START_TIME))"
                    INDEX_SIZE_RAW=$(($(ls -l $DEST_DIR/$ref/index.html | awk '{ print $5 }')))
                    SPRITE_SIZE_RAW=$(($(ls -l $DEST_DIR/$ref/img/sprite.png | awk '{ print $5 }')))
                    INDEX_SIZE=$(echo ${INDEX_SIZE_RAW} | sed 's/...$/.&/')
                    SPRITE_SIZE=$(echo ${SPRITE_SIZE_RAW} | sed 's/...$/.&/')
                    TOTAL_SIZE=$( echo $(($SPRITE_SIZE_RAW + $INDEX_SIZE_RAW)) | sed 's/...$/.&/')
                    BUILD_TIME=$( formatSeconds $ELAPSED_TIME)

                    # TODO: Find a way to make this indent-able and less ugly
                    echo -n "Sending email notification to team..." | tee -a $LOGFILE.log
                    printf "V5 Test Server just completed building: $ref\n
Access this build directly via: https://store.ceiris.com/SF-BA/?v=$ref\n
----------------------------------------------------------------------------
  Build Stats:
----------------------------------------------------------------------------
\nSprite Size: ${SPRITE_SIZE}K
\nIndex Size: ${INDEX_SIZE}K
\nTotal Size: ${TOTAL_SIZE}K
\nTotal Build Time: ${BUILD_TIME}\n
----------------------------------------------------------------------------
  Commit Log:
----------------------------------------------------------------------------
\n$COMMIT_LOG\n
----------------------------------------------------------------------------
  Change Log:
----------------------------------------------------------------------------
\n$CHANGE_LOG\n
" | mail -s  "Build Complete: $ref" frontend-js@accesso.com
                    echo "done"
  
                    printf "\nSprite Size: ${SPRITE_SIZE}K" | tee -a $LOGFILE.log
                    printf "\nIndex Size: ${INDEX_SIZE}K" | tee -a $LOGFILE.log
                    printf "\nTotal Size: ${TOTAL_SIZE}K" | tee -a $LOGFILE.log
                    printf "\nTotal Build Time: ${BUILD_TIME}\n\n" | tee -a $LOGFILE.log
                    echo "--------------------------------------------------------" | tee -a $LOGFILE.log
        
                    fi 
                fi
    
        }
    
        for ref in $TAGS; do
            mkdir -p $DEST_DIR/$ref
            if [ ! "$(ls -A $DEST_DIR/$ref )" ]; then
                git checkout $ref >> $LOGFILE.$ref.log 2>&1
                if [ $? -eq 0 ]; then
                    #printf "\nInspecting tag: $ref\n"
                    LAST_TAG=$(git describe --tags $(git rev-list --tags --max-count=2| tail -n1))
                	REMOTE_HASH=$(git ls-remote origin -h refs/tags/$ref | sed 's/\srefs.*$//g')
                	LOCAL_HASH=$(git show-ref --tags $LAST_TAG | sed 's/\srefs.*$//g')
        	        build $REMOTE_HASH $LOCAL_HASH
                fi
            fi
        done
    
        for ref in $BRANCHES; do
            git checkout $ref >> $LOGFILE.$ref.log 2>&1
            if [ $? -eq 0 ]; then
                #printf "\nInspecting branch: $ref\n"
            	REMOTE_HASH=$(git ls-remote origin -h refs/heads/$ref | sed 's/\srefs.*$//g')
            	LOCAL_HASH=$(git rev-list --max-count=1 $ref)
        	    build $REMOTE_HASH $LOCAL_HASH
            fi
        done
    
    
    done

done

