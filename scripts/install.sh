#!/bin/bash

echo "Shop V5 Installation Script"
echo "---------------------------"
echo ""

printf "Network username: "
read USER

printf "\nSystem password: "
read -s SYS_PASS
sudo -k
echo "$SYS_PASS" | sudo -v -S > /dev/null 2>&1
printf "\n\n"

printf "Checking for Git... "
if command -v git &>/dev/null; then
    echo "Found"
else
    echo "Not Found"
    echo "Please install using steps found here:"
    echo "http://git-scm.com/book/en/Getting-Started-Installing-Git"
    exit
fi

printf "Checking for GCC... "
if command -v git &>/dev/null; then
    echo "Found"
else
    echo "Not Found"
    echo "Mac Users should install Xcode and optional CLI Tools"
    echo "http://git-scm.com/book/en/Getting-Started-Installing-Git"
    echo ""
    echo "Debian based Linux Users install using:"
    echo "sudo apt-get install build-essential"
    echo ""
    echo "RPM based Linux Users install using:"
    echo "sudo yum groupinstall 'Development Tools'"
    exit
fi


printf "Checking for Node-API... "
if [ ! "$(ls -A api)" ]; then
    echo "Not Found"
    printf "Cloning Node-API... "
    rm -rf api
    git clone -b develop ssh://$USER@gerrit.accesso.office:2222/node-api.git api
    echo "Done"
else
    echo "Found"
fi

printf "Checking for ImageMagick... "
if command -v convert &>/dev/null; then
    echo "Found"
else
    echo "Not Found"
    echo "Please install using steps found here:"
    echo "http://www.imagemagick.org/script/binary-releases.php"
    exit
fi

printf "Checking for NodeJS... "
if command -v npm &>/dev/null; then
    echo "Found $(node --version)"
    printf "Installing Global System Deps... "
    sudo npm install -g grunt-cli bower phantomjs > /dev/null 2>&1
    echo "Done"
    printf "Installing Shopland NodeJS Deps... "
    npm install > /dev/null 2>&1
    echo "Done"
    printf "Installing node-api NodeJS Deps... "
		cd api
    npm install > /dev/null 2>&1
		cd ..
    echo "Done"
else
    echo "Not Found"
    echo "Please install using steps found here:"
    echo "https://github.com/joyent/node/wiki/Installation"
    exit
fi

printf "Checking for Bower... "
if command -v bower &>/dev/null; then
    echo "Found $(bower --version)"
    printf "Installing Client Dependencies... "
    bower install > /dev/null 2>&1
    echo "Done"
else
    echo "Bower not detected"
    echo "Please attempt to verify installation via:"
    echo "sudo npm install -g bower"
    exit
fi


