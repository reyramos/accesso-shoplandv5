# Shopland V4 #

## About ##
Aims to supply an HTML5/JS mobile application that supports all the core
ticket-service features of the Adobe Flex Shopland V3 application to users
initially on mobile devices, and to eventually fully replace Shopland V3 on
desktops as well.

## Current Features ##

  * Initial scaffolding and documentation!
  * Oh right, features? None of those yet...

## Requirements ##

  * Node.js <http://nodejs.org/>
  * Redis <http://redis.io/>
  * hiredis <http://github.com/redis/hiredis/>
  * ImageMagick <http://www.imagemagick.org/>
  	* Version 6.8 or higher is needed. This is specifically for the sprite generator. 
  	* CentOS users may need to do a few steps before. Please view the notes below. 
  * AngularJS <http://angularjs.com/>
  * ExpressJS <http://expressjs.com/>
  * Less.js <http://lesscss.org/>
  * RequireJS <http://requirejs.org/>
  * Bower <http://twitter.github.com/bower/>
  * Grunt <http://gruntjs.com/>

## Development Environment Setup ##




  1. Install Node.JS, ImageMagick, Redis, hiredis, fontforge, ttfautohint
	
	Use your system package manager (brew,port,apt-get,yum etc)
	
	NOTE: Installing ImageMagick on CentOS requires some work. 

		- Install the following packages:
	
		yum install bzip2-devel freetype-devel libjpeg-devel libpng-devel \
		libtiff-devel giflib-devel zlib-devel ghostscript-devel djvulibre-devel \
		libwmf-devel jasper-devel libtool-ltdl-devel libX11-devel libXext-devel \
		libXt-devel lcms-devel libxml2-devel librsvg2-devel OpenEXR-devel
	
		# CentOS/RedHat, install rpm-build, and make sure you have the necessary files for compiling. 
		yum install gcc gcc-c++ make rpm-build
		
		# Get the file.
		# To get the latest version
		wget http://www.imagemagick.org/download/linux/SRPMS/ImageMagick.src.rpm
		# To get a specific version, check out the directory of the url, and find the appropriate file. 
		wget http://www.imagemagick.org/download/linux/SRPMS/ImageMagick-6.8.5-3.src.rpm
		
	
		# Build the package of the file. This will take about 5-15 minutes depending on processor and other factors (ie. Virtual Machine)
		rpmbuild --rebuild ImageMagick-6.8.5-3.src.rpm
			-or-
		rpmbuild --rebuild ImageMagick.src.rpm
		
		If all is good, you will have a file in ./rpmbuild/RPMS/x86_64

		#On Linux installation for webfonts
		sudo apt-get install fontforge ttfautohint

		#On OS X
		brew install fontforge ttfautohint
		
	Installing on Windows
	
		Method 1: under construction
		
		Method 2: using http://chocolatey.org/
		
			Follow the instructions to install chocolatey onto your Windows machine
			
				http://chocolatey.org/
			Install the following Packages
			
				cinst nodejs.install -Version 0.8.22
		
			http://chocolatey.org/packages?q=node+js
			http://chocolatey.org/packages?q=imagemagick	
	 

  2. Install global Bower, Grunt and PhantomJS commands

	  ``bash
		sudo npm install -g grunt-cli bower phantomjs
	  ``

  3. Install core Node.js dependencies with NPM

	  ``bash
	  npm install
	  ``

  4. Install client-facing JS libraries with Bower

	  ``bash
	  bower install
	  ``

## Documentation Generation ##
  
	1. Generate documentation with grunt

	  ``bash
	  grunt dox
	  ``

## Development Work-flow ##

  1. Launch development server

	  ``bash
	  grunt server
	  ``

  2. Make changes (reflected live in browser)

  3. Run unit tests

	  ``bash
	  grunt test
	  ``

  4. Commit changes

	  ``bash
	  git add somefile.json someotherfile.js
	  git commit -m 'I made some changes to somefile and someotherfile'
	  git push
	  ``

## Deployment ##

  1. Run build scripts

	  ``bash
	  grunt build
		``
  2. Serve compiled, production-ready files from the 'dist' directory

	  ``bash
	  grunt server:dist
		``

## General Notes ##

	* Any top-level directories added/changed under app/ must be reflected in
	  Gruntfile.coffee under the livereload modRewrite section.

  * All Javascript code should be formatted according to the NPM style guide
		- https://npmjs.org/doc/coding-style.html

	* All code editors should be equipped with an editorConfig plugin to ensure
	  all code is spaced/formatted identically across editors
		- http://editorconfig.org/

	* Please commit and push as often as possible. The more frequently you commit
	  the easier it is to revert or cherry pick changes when things break.
	
	* When commiting experimental or incomplete features/experiments please do
	  so in a branch, and merge develop into your branch frequently to avoid 
		long merge conflicts later.

	* Please only commit code that is yours. If you did not write it, it probably
	  belongs in component.json or package.json to be handeled and version
		controlled via bower or npm. If it exists, it is probably in the npm or
		bower repos and even if it does not, bower and npm both support linking in
		and tracking remote git repositories.

  * When making changes to dependencies (npm/bower) please do so in a separate
		commit to call attention to it so other team members know to do a new npm
		install or bower install.

	* Please use the Angular $log functions instead of console.log to allow more
	  granular logging controls without polluting the global unit test and
		production logs

	* Wherever possible please conform all CSS/HTML by Googles Style guide
		with the addition of making use of less mixins and nesting where possible.
		- http://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml

## Localization Notes (i18N) ##

	Locales will be store in the database in the languages table. Unfortunately, that table isn't normalized sufficiently so bare with it or get chewed.

	Naming Convention

		Category or Section
		Please categorize (section) locales in order to allow for duplicate target (if needed) but also for self documentation. Ensure that the section or category you are creating makes the most sense. Determine if the locale entry you are creating can be placed in a more generalized section. 

			Ex: Instead of creating a BillingForm section consider a FormSection. It may be that locale entries in the billing form can be used in the shipping form (e.g. first name, last name, etc).

		Use a more specific category name when you are certain at the time of development that locale entries will reside only in that section or view of the application.

			Ex: PacakageListView.pageTitle, PackageListView.pageDescription

		Category or section names must be in the form of CamelCaps.

			Bad Examples:
				mysectionname, mySectionName, my_section_name, my-section-name, MYSECTIONNAME, MY-SECTION-NAME, MY_SECTION_NAME

			Valid Examples:
				MySectionName, MyCategoryName, Forms, BillingView

		Target
		Target must be in the form of camelCase.

			Bad Examples:
				mytargetname, MyTargetName, my_target_name, my-target-name, MYTARGETNAME, MY-TARGET-NAME, MY_TARGET_NAME

			Valid Examples:
				myTargetName, minQtyError, invalidEmail, lessThanMinQtyError

		Use error codes for errors but use descriptive target names for non-errors.

			Bad Examples:
				For first name label: fn, fnLbl, fName, first

			Valid Examples:
				For first name label: firstName, firstNameLabel, firstNameLbl
				
				In this particular case, firstName is the recommended approach.

		Dynamic Values
			At times we will need to customize the error message with information unknown until runtime. 

			Example Requirement: Display the difference of items attempted to be added to the cart and package's min quantity (i.e. if package has a min qty of 10 and user attempts to add 8, error message should say you have to add at least 2 more). Ensure that words are singular or plural based on context.

			Please note that this is just an example and I don't think we would ever display the minQtyError as below.

			Section: Alert
			Target: lessThanMinQtyError
			Value: This package has a minimum quantity of @@pkgMinQty @@[pkgMinQty, item, items]. You are attempting to add @@qtyAttempted @@[qtyAttempted, item, items] to your cart. Please add at least @@difRemaining more @@[difRemaining item, items] in order to proceed.

			The application should swap out variables starting with @@someVarName with values derived at run-time. The result should be as follows:

			This package has a minimum quantity of 10 items. You are attempting to add 8 items to your cart. Please add at least 2 more items in order to proceed.

			Or if it were 10 min qty and adding 9
			This package has a minimum quantity of 10 items. You are attempting to add 9 items to your cart. Please add at least 1 more item in order to proceed.

			NOTE: item is plural in first example and singular in second.
			Re: @@[value, item, items] - Singular and plural options. Select the more sensible option based on values derived elsewhere in the string.


		Other Notes
			Add your locale entries in the language service as default values.

			To verify the application is localized in its entirety, comment out the default values and ensure all text entries are not set to undefined.
			
			The entire application is wrapped in ApplicationController and i18n is defined in ApplicationController scope Simply refer to your locale entries as {{i18n.Alert.addDonationErrorMsg}} for instance, in your views or business logic. In other words, no need to import language service in any controllers other than the ApplicationController.

			Upon completion of the requirement, please write and execute the SQL required to add the entries to the locale table. 
			
			Save all your SQL. It will be used for updating staging and prod database.

URL Paramaters
v (version)
- Specifies a specific GIT branch or tag  name or to serve from and over-rides the merchant paramater if provided.

d (develop)
- Can be true/false. False by default
- If enabled, this enables the global development flag in the application which currently enables logging as well as testing form data
- Note: logging is enabled by default on everything but production.

e (environment)
- Allows a specific backend to be specified
- Allowed values are production, staging, development

u (UID override)
- Allows a custom ID used for tracking source of logging (e.g. ?u=ipadMini)

Code Formatting Examples

No
$scope.decrementQty = function (data) {}

Yes
$scope.decrementQty = function(data){}

No
//this is a random comment

Yes (note space after slashes, propper case, and period)
// This is a random comment.

No
if (detail == $scope.buttonFlags.YES) {}
if ( detail == $scope.buttonFlags.YES ) {}

Yes
if(detail == $scope.buttonFlags.YES){}

No
$scope.showModal(
	{ title: $scope.i18n.Alert.failedPurchaseTitle, description: $scope.i18n.Alert.failedPurchaseMsg, flags: [$scope.buttonFlags.YES, $scope.buttonFlags.NO], callback: function (detail) {
		if (detail == $scope.buttonFlags.YES) {
			$scope.completeOrder()
		}
	}
	}
)

Yes
$scope.showModal(
{ title: $scope.i18n.Alert.failedPurchaseTitle
, description: $scope.i18n.Alert.failedPurchaseMsg
, flags: [$scope.buttonFlags.YES
, $scope.buttonFlags.NO]
, callback: function(detail){
		if(detail == $scope.buttonFlags.YES){
			$scope.completeOrder()
		}
	}
})

No
var customer =
{ f_name: savedBilling.firstName, m_name: '', l_name: savedBilling.lastName, phone: savedBilling.phone, mobile_phone: '', email: savedBilling.email
}

Yes
var customer =
{ f_name: savedBilling.firstName
, m_name: ''
, l_name: savedBilling.lastName
, phone: savedBilling.phone
, mobile_phone: ''
, email: savedBilling.email
}

No
if(){
	
}else if(){
	
}else{
	
}

Yes
if(){
	
} else if(){
	
} else {}

No
if(){
	
}
if(){
	
}
function foo(){
	
}
for(var i = 0; i < 10; i++){
	
}

Yes (they are different statements - treat it as such)
if(){
	
}

if(){
	
}

function foo(){
	
}

for(var i = 0; i < 10; i++){
	
}

No
for ( var i = 0; i < 10; i++ ) {
	
}

if () {
	
}

function foo () {
	
}

Yes
for(var i = 0; i < 10; i++){
	
}

if(){
	
}

function foo(){
	
}

No
var someVar = (someValue==someBool)?someOtherValue:yetSomeOtherValue

Yes
var someVar = (someValue == someBool) ? someOtherValue : yetSomeOtherValue

No
var getObject = function (name) {
	switch (name[0]) {
		case '#':
			var value = value1
			break;
		case '.':
			var value = value2
			break;
		default:
			var value = value3
			break;
	}
	return value;
}

Yes
var getObject = function(name){
	switch(name[0]){
		case '#':
			return value1
		case '.':
			return value2
		default:
			return value3
	}

	return null
}

OR 
var getObject = function(name){
	var value
	switch(name[0]){
		case '#':
			value = value1
			break
		case '.':
			value = value2
			break
		default:
			value = value3
			break
	}

	return value
}

No
$scope.showModal
({ title: $scope.i18n.updateMultipleLocaleKeysInArray
		(['qtyAdded']
			, [qtyAdded]
			, $scope.i18n.replaceAllMultiple
			(['@@qtyAdded']
				, [qtyAdded]
				, $scope.i18n.Alert.cartOrContinueShoppingTitle
			)
		), description: '', flags: [ $scope.buttonFlags.CONTINUE_SHOPPING
		, $scope.buttonFlags.PROCEED_TO_CHECKOUT
	], callback: function(detail){
		if(detail == $scope.buttonFlags.CONTINUE_SHOPPING){
			$scope.routeToPackageList();
		} else if(detail == $scope.buttonFlags.PROCEED_TO_CHECKOUT){
			$scope.routeTo($scope.routes.CART)
		}
	}
	}
)

Yes (although more verbose, it's more legible)
var title = $scope.i18n.replaceAllMultiple(['@@qtyAdded'], [qtyAdded], $scope.i18n.Alert.cartOrContinueShoppingTitle)
title = $scope.i18n.updateMultipleLocaleKeysInArray(['qtyAdded'], [qtyAdded], title)
$scope.showModal
(
	{ title: title
	, description: ''
	, flags:
		[ $scope.buttonFlags.CONTINUE_SHOPPING
		, $scope.buttonFlags.PROCEED_TO_CHECKOUT
		]
	, callback: function(detail){
			if(detail == $scope.buttonFlags.CONTINUE_SHOPPING){
				$scope.routeToPackageList();
			} else if(detail == $scope.buttonFlags.PROCEED_TO_CHECKOUT){
				$scope.routeTo($scope.routes.CART)
			}
		}
	}
)

No
packagesData.funcName = function(){

	$log.log('log something')


}

Yes (What is the deal with all the line breaks?)
packagesData.funcName = function(){
	$log.log('log something')
}

No
packagesData.funcName = function(){
	$log.log('log something')
	$log.log('************************ log something else ****************************************')
	$log.log('log something again')
	
	$log.log('SOME FUNCTION STARTS HERE')
	$log.log('THIS IS THE MIDDLE OF THE FUNCTION')
	$log.log('FUNCTION IS ABOUT TO END')
	$log.log('WAIT FOR IT')
	$log.log('WAIT FOR IT')
	$log.log('WAIT FOR IT')
	$log.log('FUNCTION ENDED')
	
	$log.log('<<<<<<<<<<<<<<<<<<<<<<<<<< I love logging >>>>>>>>>>>>>>>>>>>>>>>>>>>')
	
	$log.log('########################################################################')
	$log.log('######### THIS IS REALLY IMPORTANT - I LOVE CUTE ANIMALS ###############')
	$log.log('########################################################################')
}

Yes
packagesData.funcName = function(){
	// Remove excessive logging - it makes the code confusing.
	// Use logging as a temporary means but delete it when you're done with it. It should be used sparingly.
	// Excessive logging makes logging irrelevant and makes necessary logging hard to read.
	// Use Chrome's debugger and add breakpoints.
}















