# Gruntfile
#
# Commands available:
#
#       grunt server
#           - spawns web server for both static files and socket
#
#       grunt dox
#           - generates documentation
#
#       grunt test
#           - runs karma which reads and runs all jasmine unit tests
#
#       grunt
#           - compiles all static files into production-ready bundle in /dist

proxySnippet = require("grunt-connect-proxy/lib/utils").proxyRequest
lrSnippet = require("grunt-contrib-livereload/lib/utils").livereloadSnippet
modRewrite = require("connect-modrewrite")
mountFolder = (connect, dir) ->
	connect.static require("path").resolve(dir)


module.exports = (grunt) ->

	# load all grunt tasks
	require("matchdep").filter("grunt-*")
	.concat(["gruntacular",'node-spritesheet','grunt-contrib-compress'])
	.forEach grunt.loadNpmTasks

	grunt.initConfig
		yeoman:
			app: "app"
			dist: "dist"
		watch:
			livereload:
				files: [
					"<%= yeoman.app %>/*.html"
					"<%= yeoman.app %>/*.template"
					"{.tmp,<%= yeoman.app %>}/css/*.css"
					"{.tmp,<%= yeoman.app %>}/css/*.less"
					"{.tmp,<%= yeoman.app %>}/js/*.js"
					"{.tmp,<%= yeoman.app %>}/js/*/*.js"
					"{.tmp,<%= yeoman.app %>}/js/*/*/*.js"
					"{.tmp,<%= yeoman.app %>}/views/*.css"
					"<%= yeoman.app %>/images/*.{png,jpg,jpeg}"
				]
				tasks: [ "livereload", "template:dev"]

			less:
				files: [ "{.tmp,<%= yeoman.app %>}/css/*.less" ]
				tasks: [
					"less:dev"
					"livereload"
				]

		curl:
			'test/selenium/selenium.jar':'https://selenium.googlecode.com/files/selenium-server-standalone-2.35.0.jar'
			'test/selenium/chromedriver.zip':'https://chromedriver.googlecode.com/files/chromedriver_linux64_2.3.zip'

		unzip:
			'test/selenium/':'test/selenium/chromedriver.zip'

		exec:
			protractor:
				stdout: true
				stderr: true
				command: 'chmod +x test/selenium/chromedriver; protractor test/protractor.js'

		less:
			dev:
				options:
					paths: ["<%= yeoman.app %>/css"]
					compress: true
					dumpLineNumbers: false
					ieCompat: true
					syncImport: true
					relativeUrls: false
					yuicompress: false

				files:
					".tmp/css/main.css": "<%= yeoman.app %>/css/styles.less"

			dist:
				options:
					paths: [
						"<%= yeoman.app %>/css",
						".tmp/css",
					]
					compress: true
					dumpLineNumbers: false
					ieCompat: true
					syncImport: true
					relativeUrls: false
					yuicompress: false

				files:
					".tmp/styles.css": "<%= yeoman.app %>/css/styles.less"

		spritesheet:
			compile:
				options:
					outputCss: "css/sprite.less"
					selector: ".sprite"
					downsampling: "LanczosSharp"
					output:
						legacy:
							pixelRatio: 1
							outputImage: 'img/sprite.png'
						retina:
							pixelRatio: 2
							outputImage: 'img/sprite2x.png'

				files:
					".tmp": "<%= yeoman.app %>/img/icons/*"


		connect:
			options:
				hostname: "0.0.0.0"
				port: 9998
			proxies: [
				context: "/api"
				host: "0.0.0.0"
				port: 8888
				https: false
				changeOrigin: true
			,
				context: "/debug"
				host: "0.0.0.0"
				port: 9001
				https: false
				changeOrigin: true
			]
			livereload:
				options:
					middleware: (connect) -> [
						lrSnippet
						proxySnippet
						modRewrite([
							"!(png|jpg|gif|css|js|less|html|otf|ttf)$ /index.html [L]"
							"^/[^/]+/([^/]+/.*(png|otf|ttf|css)) /$1 [L]"
						])
						mountFolder(connect, ".tmp")
						mountFolder(connect, "app")
					]

			test:
				options:
					middleware: (connect) ->
						[mountFolder(connect, ".tmp"), mountFolder(connect, "app")]

			dist:
				options:
					middleware: (connect) -> [
						proxySnippet
						modRewrite([
							"!png|jpg|gif|css|js|less|html|otf|ttf$ /index.html [L]"
							"^/[^/]+/([^/]+/.*(png|otf|ttf|css)) /$1 [L]"
						])
						(req, res, next) ->
							if (req.url.split('.').pop() == 'html')
								res.setHeader('Content-Encoding', 'gzip')
							if (req.url.split('.').pop() == 'js')
								res.setHeader('Content-Encoding', 'gzip')
							next()
						mountFolder(connect, "dist")
					]

		clean:
			dist: [".tmp", "<%= yeoman.dist %>/*"]
			server: ".tmp"

		jshint:
			options:
				jshintrc: ".jshintrc"

			all: ["Gruntfile.js", "<%= yeoman.app %>/js/*.js", "app/test/spec/*.js"]

		testacular:
			unit:
				configFile: "karma.coffee"
				singleRun: true

		dox:
			options:
				title: "Shopland v5 Code Documentation"

			files:
				src: ["<%= yeoman.app %>/js"]
				dest: "<%= yeoman.app %>/docs"

		ngtemplates:
			app:
				options:
					base: 'app/'
					prepend: '/'
				src: ["app/views/**.html","app/views/*/*.html"]
				dest: '.tmp/js/views.js'

		requirejs:
			dist:
				options:
					baseUrl: "app/js"
					findNestedDependencies: true
					generateSourceMaps: true
					logLevel: 0
					mainConfigFile: "app/js/main.js"
					name: 'main'
					include: ['../../.tmp/js/views.js']
					onBuildWrite: (moduleName, path, contents) ->
						modulesToExclude = ['main','bootstrap']
						shouldExcludeModule = modulesToExclude.indexOf(moduleName) >= 0
						return '' if shouldExcludeModule
						contents
					optimize: "uglify2"
					uglify2:
						warnings: true
						mangle: false
						no_mangle: true
						output:
							beautify      : false # beautify output?
							indent_start  : 0     # start indentation on every line (only when `beautify`)
							indent_level  : 4     # indentation level (only when `beautify`)
							width         : 80    # informative maximum line width (for beautified output)
							max_line_len  : 32000 # maximum line length (for non-beautified output)
							quote_keys    : false # quote all keys in object literals?
							space_colon   : false # add a space after colon signs?
							ascii_only    : false # output ASCII-safe? (encodes Unicode characters as ASCII)
							ie_proof      : false # output IE-safe code?
							inline_script : true  # escape "</script"?
							bracketize    : false # use brackets every time?
							comments      : false # output comments?
							semicolons    : true  # use semicolons to separate statements? (otherwise newlines)
						compress:
							sequences     : true # join consecutive statemets with the 'comma operator'
							properties    : true # optimize property access: a["foo"] -> a.foo
							dead_code     : true # discard unreachable code
							drop_debugger : true # discard 'debugger' statements
							unsafe        : true # some unsafe optimizations (see docs)
							conditionals  : true # optimize if-s and conditional expressions
							comparisons   : true # optimize comparisons
							evaluate      : true # evaluate constant expressions
							booleans      : true # optimize boolean expressions
							loops         : true # optimize loops
							unused        : true # drop unused variables/functions
							hoist_funs    : true # hoist function declarations
							hoist_vars    : true # hoist variable declarations
							if_return     : true # optimize if-s followed by return/continue
							join_vars     : true # join var declarations
							cascade       : true # try to cascade `right` into `left` in sequences
							side_effects  : true # drop side-effect-free statements
							warnings      : true  # warn about potentially dangerous optimizations/code
							global_defs   : {}    # global definitions
					out: ".tmp/js/scripts.js"
					preserveLicenseComments: false
					skipModuleInsertion: true

		concat:
			dist:
				src: ['.tmp/js/scripts.js','.tmp/js/views.js','app/js/bootstrap.js']
				dest: ".tmp/js/scripts-concat.js"

		htmlmin:
			templates:
				options:
					collapseBooleanAttributes: false
					collapseWhitespace: false
					removeAttributeQuotes: false
					removeComments: false
					removeCommentsFromCDATA: false
					removeCDATASectionsFromCDATA: false
					removeEmptyAttributes: false
					removeEmptyElements: false
					removeOptionalTags: false
					removeRedundantAttributes: false
					useShortDoctype: false

				files: [
					expand: true
					src: ["app/views/**/*.html"]
					dest: '.tmp/'
				]
			dist:
				options:
					collapseBooleanAttributes: false
					collapseWhitespace: false
					removeAttributeQuotes: false
					removeComments: false
					removeCommentsFromCDATA: false
					removeCDATASectionsFromCDATA: false
					removeEmptyAttributes: false
					removeEmptyElements: false
					removeOptionalTags: false
					removeRedundantAttributes: false
					useShortDoctype: false

				files: [
					".tmp/index.html": ".tmp/index-concat.html"
				]

		compress:
			html:
				options:
					mode: 'gzip'
					pretty: true
				src: ['.tmp/index-concat.html']
				dest: 'dist/index.html'
				ext:'.html'
			scripts:
				options:
					mode: 'gzip'
					pretty: true
				src: ['.tmp/js/scripts-concat.js']
				dest: 'dist/js/scripts.js'
				ext:'.js'

		imagemin:
			dist:
				files: [
					expand: true
					cwd: "<%= yeoman.app %>/images"
					src: "*.{png,jpg,jpeg}"
					dest: "<%= yeoman.dist %>/images"
				]

		copy:
			dist:
				files: [
					expand: true
					dot: true
					cwd: "<%= yeoman.app %>"
					dest: "<%= yeoman.dist %>"
					src: [
						"*.html"
						"img/*.{png,gif,ico,txt}"
						"fonts/*.otf"
						"fonts/*.ttf"
						"fonts/*.eot"
						"fonts/*.svg"
						"fonts/*.woff"
						"fonts/*/*.otf"
						"fonts/*/*.ttf"
						"fonts/*/*.eot"
						"fonts/*/*.svg"
						"fonts/*/*.woff"
						"css/merchant/*.css"
						"js/**/*.js"
					]
				]
			dist2:
				files: [
					expand: true
					dot: true
					cwd: ".tmp"
					dest: "<%= yeoman.dist %>"
					src: [
						"img/*.png"
						"js/*.map"
					]
				]

			server:
				files: [
					expand: true
					dot: true
					cwd: "<%= yeoman.dist %>"
					dest: ".tmp"
					src: [
						"css/sprite.less"
						"img/sprite.png|ttf"
						"img/sprite2x.png"
					]
				]

		template:
			dev:
				files:
					".tmp/index.html": "<%= yeoman.app %>/index.template"
				environment: "dev"
			dist:
				files:
					".tmp/index-concat.html": "<%= yeoman.app %>/index.template"
			#"dist/index.html": "<%= yeoman.app %>/index.template"
				environment: "dist"
				css_sources: '<%= grunt.file.read(".tmp/styles.css") %>'
				buildVersion: grunt.option('buildVersion')
	#js_sources: '<%= grunt.file.read(".tmp/scripts.js") %>'

		bower:
			install:
				options:
					targetDir: './app/lib'
					verbose: true
					install: true
					cleanup: true

		webfont:
			icons:
				src: './app/img/svg/*.svg'
				dest: './app/fonts/fontcustom'
				destCss: './app/css'
				options:
					font: 'fontcustom'
					stylesheet: 'less'
					relativeFontPath: 'fonts/fontcustom'
					htmlDemo: true


	grunt.renameTask "regarde", "watch"

	grunt.registerTask "apiServer", ->
		require("./api/server.js")

	grunt.registerTask "logServer", ->
		require("./api/logger.js")

	grunt.registerTask "server", (target) ->
		if target is "dist"
			grunt.task.run [
				"apiServer"
				"logServer"
				"configureProxies"
				"livereload-start"
				"connect:dist:keepalive"
			]
		else
			grunt.task.run [
				#"clean:server"
				"copy"
				"apiServer"
				"logServer"
				"configureProxies"
				"livereload-start"
				"template:dev"
				"connect:livereload"
				"watch"
			]


	grunt.registerTask "server-only", (target) ->
		grunt.task.run [
			"apiServer"
			"logServer"
			"configureProxies"
			"watch"
		]


	grunt.registerTask "test", (target) ->
		if (grunt.file.exists('test/selenium/chromedriver'))
			grunt.task.run [
				"exec:protractor"
			]
		else
			grunt.task.run [
				"curl"
				"unzip"
				"exec:protractor"
			]

	grunt.registerTask "build", [
		"clean:dist"
		#"bower:install"
		#"testacular"
		"htmlmin:templates"
		"ngtemplates"
		"requirejs"
		"concat:dist"
		#"uglify"
		"spritesheet"
		"less:dist"
		"webfont"
		"imagemin"
		"template:dist"
		"htmlmin:dist"
		"copy:dist"
		"copy:dist2"
		"compress:html"
		"compress:scripts"
	]

	grunt.registerTask "default", ["build"]
