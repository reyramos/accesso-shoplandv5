var lrSnippet, modRewrite, mountFolder, proxySnippet;

lrSnippet = require("grunt-contrib-livereload/lib/utils").livereloadSnippet;

proxySnippet = require("grunt-connect-proxy/lib/utils").proxyRequest;

modRewrite = require("connect-modrewrite");

mountFolder = function(connect, dir) {
	return connect["static"](require("path").resolve(dir));
};

module.exports = function(grunt) {
	require("matchdep").filter("grunt-*").concat(["gruntacular", 'node-spritesheet', 'grunt-contrib-compress']).forEach(grunt.loadNpmTasks);
	grunt.initConfig({
		yeoman: {
			app: "app",
			dist: "dist"
		},
		watch: {
			livereload: {
				files: [
					"<%= yeoman.app %>/*.html"
					, "<%= yeoman.app %>/*.template"
					, "{.tmp,<%= yeoman.app %>}/css/*.css"
					, "{.tmp,<%= yeoman.app %>}/css/*.less"
					, "{.tmp,<%= yeoman.app %>}/js/*.js"
					, "{.tmp,<%= yeoman.app %>}/js/*/*.js"
					, "{.tmp,<%= yeoman.app %>}/js/*/*/*.js"
					, "{.tmp,<%= yeoman.app %>}/views/*.css"
					, "<%= yeoman.app %>/images/*.{png,jpg,jpeg}"],
				tasks: ["livereload", "template:dev"]
			},
			less: {
				files: ["{.tmp,<%= yeoman.app %>}/css/*.less"],
				tasks: ["less:dev", "livereload"]
			}
		},
		less: {
			dev: {
				options: {
					paths: ["<%= yeoman.app %>/css"],
					compress: true,
					dumpLineNumbers: false,
					ieCompat: true,
					syncImport: true,
					relativeUrls: false,
					yuicompress: false
				},
				files: {
					".tmp/css/main.css": "<%= yeoman.app %>/css/styles.less"
				}
			},
			dist: {
				options: {
					paths: ["<%= yeoman.app %>/css", ".tmp/css"],
					compress: true,
					dumpLineNumbers: false,
					ieCompat: true,
					syncImport: true,
					relativeUrls: false,
					yuicompress: false
				},
				files: {
					".tmp/styles.css": "<%= yeoman.app %>/css/styles.less"
				}
			}
		},
		spritesheet: {
			compile: {
				options: {
					outputCss: "css/sprite.less",
					selector: ".sprite",
					downsampling: "LanczosSharp",
					output: {
						legacy: {
							pixelRatio: 1,
							outputImage: 'img/sprite.png'
						},
						retina: {
							pixelRatio: 2,
							outputImage: 'img/sprite2x.png'
						}
					}
				},
				files: {
					".tmp": "<%= yeoman.app %>/img/icons/*"
				}
			}
		},
		connect: {
			options: {
				hostname: "0.0.0.0",
				port: 9998
			},
			proxies: [
				{
					context: "/api",
					host: "0.0.0.0",
					port: 8888,
					https: false,
					changeOrigin: true
				}, {
					context: "/debug",
					host: "0.0.0.0",
					port: 9001,
					https: false,
					changeOrigin: true
				}
			],
			livereload: {
				options: {
					middleware: function(connect) {
						return [
							lrSnippet
							, proxySnippet
							, modRewrite([
								"!(png|jpg|gif|css|js|less|html|otf|ttf)$ /index.html [L]"
								, "^/[^/]+/([^/]+/.*(png|otf|ttf|css)) /$1 [L]"])
							, mountFolder(connect, ".tmp"), mountFolder(connect, "app")];
					}
				}
			},
			test: {
				options: {
					middleware: function(connect) {
						return [mountFolder(connect, ".tmp"), mountFolder(connect, "app")];
					}
				}
			},
			dist: {
				options: {
					middleware: function(connect) {
						return [
							proxySnippet, modRewrite([
								"!png|jpg|gif|css|js|less|html|otf|ttf$ /index.html [L]"
								, "^/[^/]+/([^/]+/.*(png|otf|ttf|css)) /$1 [L]"])
							, function(req, res, next) {
								if (req.url.split('.').pop() === 'html') {
									res.setHeader('Content-Encoding', 'gzip');
								}
								if (req.url.split('.').pop() === 'js') {
									res.setHeader('Content-Encoding', 'gzip');
								}
								return next();
							}, mountFolder(connect, "dist")
						];
					}
				}
			}
		},
		clean: {
			dist: [".tmp", "<%= yeoman.dist %>/*"],
			server: ".tmp"
		},
		jshint: {
			options: {
				jshintrc: ".jshintrc"
			},
			all: ["Gruntfile.js", "<%= yeoman.app %>/js/*.js", "app/test/spec/*.js"]
		},
		testacular: {
			unit: {
				configFile: "karma.coffee",
				singleRun: true
			}
		},
		dox: {
			options: {
				title: "Shopland v5 Code Documentation"
			},
			files: {
				src: ["<%= yeoman.app %>/js"],
				dest: "<%= yeoman.app %>/docs"
			}
		},
		ngtemplates: {
			app: {
				options: {
					base: 'app/',
					prepend: '/'
				},
				src: ["app/views/**.html", "app/views/*/*.html"],
				dest: '.tmp/js/views.js'
			}
		},
		requirejs: {
			dist: {
				options: {
					baseUrl: "app/js",
					findNestedDependencies: true,
					generateSourceMaps: true,
					logLevel: 0,
					mainConfigFile: "app/js/main.js",
					name: 'main',
					include: ['../../.tmp/js/views.js'],
					onBuildWrite: function(moduleName, path, contents) {
						var modulesToExclude, shouldExcludeModule;
						modulesToExclude = ['main', 'bootstrap'];
						shouldExcludeModule = modulesToExclude.indexOf(moduleName) >= 0;
						if (shouldExcludeModule) {
							return '';
						}
						return contents;
					},
					optimize: "uglify2",
					uglify2: {
						warnings: true,
						mangle: false,
						no_mangle: true,
						output: {
							beautify: false,
							indent_start: 0,
							indent_level: 4,
							width: 80,
							max_line_len: 32000,
							quote_keys: false,
							space_colon: false,
							ascii_only: false,
							ie_proof: false,
							inline_script: true,
							bracketize: false,
							comments: false,
							semicolons: true
						},
						compress: {
							sequences: true,
							properties: true,
							dead_code: true,
							drop_debugger: true,
							unsafe: true,
							conditionals: true,
							comparisons: true,
							evaluate: true,
							booleans: true,
							loops: true,
							unused: true,
							hoist_funs: true,
							hoist_vars: true,
							if_return: true,
							join_vars: true,
							cascade: true,
							side_effects: true,
							warnings: true,
							global_defs: {}
						}
					},
					out: ".tmp/js/scripts.js",
					preserveLicenseComments: false,
					skipModuleInsertion: true
				}
			}
		},
		concat: {
			dist: {
				src: ['.tmp/js/scripts.js', '.tmp/js/views.js', 'app/js/bootstrap.js'],
				dest: ".tmp/js/scripts-concat.js"
			}
		},
		htmlmin: {
			templates: {
				options: {
					collapseBooleanAttributes: false,
					collapseWhitespace: false,
					removeAttributeQuotes: false,
					removeComments: false,
					removeCommentsFromCDATA: false,
					removeCDATASectionsFromCDATA: false,
					removeEmptyAttributes: false,
					removeEmptyElements: false,
					removeOptionalTags: false,
					removeRedundantAttributes: false,
					useShortDoctype: false
				},
				files: [
					{
						expand: true,
						src: ["app/views/**/*.html"],
						dest: '.tmp/'
					}
				]
			},
			dist: {
				options: {
					collapseBooleanAttributes: false,
					collapseWhitespace: false,
					removeAttributeQuotes: false,
					removeComments: false,
					removeCommentsFromCDATA: false,
					removeCDATASectionsFromCDATA: false,
					removeEmptyAttributes: false,
					removeEmptyElements: false,
					removeOptionalTags: false,
					removeRedundantAttributes: false,
					useShortDoctype: false
				},
				files: [
					{
						".tmp/index.html": ".tmp/index-concat.html"
					}
				]
			}
		},
		compress: {
			html: {
				options: {
					mode: 'gzip',
					pretty: true
				},
				src: ['.tmp/index-concat.html'],
				dest: 'dist/index.html',
				ext: '.html'
			},
			scripts: {
				options: {
					mode: 'gzip',
					pretty: true
				},
				src: ['.tmp/js/scripts-concat.js'],
				dest: 'dist/js/scripts.js',
				ext: '.js'
			}
		},
		imagemin: {
			dist: {
				files: [
					{
						expand: true,
						cwd: "<%= yeoman.app %>/images",
						src: "*.{png,jpg,jpeg}",
						dest: "<%= yeoman.dist %>/images"
					}
				]
			}
		},
		copy: {
			dist: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: "<%= yeoman.app %>",
						dest: "<%= yeoman.dist %>",
						src: [
							"*.html", "img/*.{png,gif,ico,txt}"
							, "fonts/*.otf"
							, "fonts/*.ttf"
							, "fonts/*.eot"
							, "fonts/*.svg"
							, "fonts/*.woff"
							, "fonts/*/*.otf"
							, "fonts/*/*.ttf"
							, "fonts/*/*.eot"
							, "fonts/*/*.svg"
							, "fonts/*/*.woff"
							, "css/merchant/*.css"
							, "js/**/*.js"]
					}
				]
			},
			dist2: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: ".tmp",
						dest: "<%= yeoman.dist %>",
						src: ["img/*.png", "js/*.map"]
					}
				]
			},
			server: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: "<%= yeoman.dist %>",
						dest: ".tmp",
						src: [
							"css/sprite.less"
							, "img/sprite.png|ttf"
							, "img/sprite2x.png"
						]
					}
				]
			}
		},
		template: {
			dev: {
				files: {
					".tmp/index.html": "<%= yeoman.app %>/index.template"
				},
				environment: "dev"
			},
			dist: {
				files: {
					".tmp/index-concat.html": "<%= yeoman.app %>/index.template"
				},
				environment: "dist",
				css_sources: '<%= grunt.file.read(".tmp/styles.css") %>',
				buildVersion: grunt.option('buildVersion')
			}
		},
		bower: {
			install: {
				options: {
					targetDir: './app/lib',
					verbose: true,
					install: true,
					cleanup: true
				}
			}
		},
		webfont: {
			icons: {
				src: './app/img/svg/*.svg',
				dest: './app/fonts/fontcustom',
				destCss: './app/css',
				options: {
					font: 'fontcustom',
					stylesheet: 'less',
					relativeFontPath: 'fonts/fontcustom',
					htmlDemo: true
				}
			}
		}
	});
	grunt.renameTask("regarde", "watch");
	grunt.registerTask("apiServer", function() {
		return require("./api/server.js");
	});
	grunt.registerTask("logServer", function() {
		return require("./api/logger.js");
	});
	grunt.registerTask("server", function(target) {
		if (target === "dist") {
			return grunt.task.run([
				"apiServer"
				, "logServer"
				, "configureProxies"
				, "livereload-start"
				, "connect:dist:keepalive"
			]);
		} else {
			return grunt.task.run([
				"copy"
				, "apiServer"
				, "logServer"
				, "configureProxies"
				, "livereload-start"
				, "template:dev"
				, "connect:livereload"
				, "watch"
			]);
		}
	});
	grunt.registerTask("server-only", function(target) {
		return grunt.task.run([
			"apiServer"
			, "logServer"
			, "configureProxies"
			, "watch"
		]);
	});
	grunt.registerTask("test", ["clean:server", "testacular"]);
	grunt.registerTask("build", [
		"clean:dist"
		, "htmlmin:templates"
		, "ngtemplates"
		, "requirejs"
		, "concat:dist"
		, "spritesheet"
		, "less:dist"
		, "webfont"
		, "imagemin"
		, "template:dist"
		, "htmlmin:dist"
		, "copy:dist"
		, "copy:dist2"
		, "compress:html"
		, "compress:scripts"
	]);
	return grunt.registerTask("default", ["build"]);
};