describe 'Controllers', ->
	beforeEach module('app')

	describe "BillingController", ->
		controller = {}
		beforeEach inject ($rootScope, $controller) ->
			controller = $controller "BillingController", $scope: $rootScope.$new()

		it "should be available", ->
			expect(controller).toBeDefined()


	describe "CartController", ->
		controller = {}
		beforeEach inject ($rootScope, $controller) ->
			controller = $controller "CartController", $scope: $rootScope.$new()

		it "should be available", ->
			expect(controller).toBeDefined()


	describe "PackageDetailsController", ->
		controller = {}
		beforeEach inject ($rootScope, $controller) ->
			controller = $controller "PackageDetailsController", $scope: $rootScope.$new()

		it "should be available", ->
			expect(controller).toBeDefined()


	describe "PackageListController", ->
		controller = {}
		beforeEach inject ($rootScope, $controller) ->
			controller = $controller "PackageListController", $scope: $rootScope.$new()

		it "should be available", ->
			expect(controller).toBeDefined()


	describe "SplashController", ->
		controller = {}
		beforeEach inject ($rootScope, $controller) ->
			controller = $controller "SplashController", $scope: $rootScope.$new()

		it "should be available", ->
			expect(controller).toBeDefined()


	describe "ShippingController", ->
		controller = {}
		beforeEach inject ($rootScope, $controller) ->
			controller = $controller "ShippingController", $scope: $rootScope.$new()

		it "should be available", ->
			expect(controller).toBeDefined()

	describe "MainMenuController", ->
		scope = {}
		controller = {}
		enums = {}
		beforeEach inject ($rootScope, enumsService, $controller) ->
			scope = $rootScope
			enums = enumsService.socket
			controller = $controller "MainMenuController", $scope: $rootScope

		it "should be available", ->
			expect(controller).toBeDefined()

		it "can be populated", ->
			scope.$broadcast enums.receive + enums.getApplicationConsolidated,
				'status':'OK'
				'TREE':
					'MENUITEM':
						'0':
							'label': 'test_label1'
							'keywords': 'test keywords 1'
						'1':
							'label': 'test_label2'
							'keywords': 'test keywords 2'

			expect(scope.menuItems[0]['0'].label).toBe('test_label1')
			expect(scope.menuItems[0]['0'].keywords).toBe('test keywords 1')
			expect(scope.menuItems[0]['1'].label).toBe('test_label2')
			expect(scope.menuItems[0]['1'].keywords).toBe('test keywords 2')
