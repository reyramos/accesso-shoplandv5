var util = require('util');

describe('Shop V5', function() {
  var ptor;

  ptor = protractor.getInstance();

  ptor.get('http://store.ceiris.com/DEMO');

  it('should render packageList with prices', function() {

    ptor.sleep(2000);

    var packagePrice =
      ptor.findElement
        ( protractor.By.repeater("package in packageList")
            .row(1)
            .column('startRate')
        )
        
    expect(packagePrice.isDisplayed())

  },5000);


  it('should navigate to packageDetails when package is clicked', function() {

    ptor.findElement
      ( protractor.By.repeater("package in packageList")
            .row(1)
      ).click();

    ptor.sleep(2000);


    var name = ptor.findElement(protractor.By.binding("packageDetails.name"));
    expect(name.getText()).toBeDefined();

    var headline = ptor.findElement(protractor.By.binding("packageDetails.headline"));
    expect(headline.getText()).toBeDefined();

  },5000);

});

