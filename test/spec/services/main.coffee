describe 'Services', ->
	beforeEach module('app')


	describe "clientInfoService", ->
		service = {}
		beforeEach inject (clientInfoService) ->
			service = clientInfoService

		it "should be available", ->
			expect(service).toBeDefined()

		it "should be populated", ->
			expect(service.browserName).toBeDefined()
			expect(service.deviceType).toBeDefined()
			expect(service.document_size).toBeDefined()
			expect(service.viewport_size).toBeDefined()
			expect(service.language).toBeDefined()
			expect(service.animations).toBeDefined()
			expect(service.orientation).toBeDefined()
			expect(service.merchant).toBeDefined()


	describe "enumsService", ->
		service = {}
		beforeEach inject (enumsService) ->
			service = enumsService

		it "should be available", ->
			expect(service).toBeDefined()

		it "should be populated", ->
			expect(service.socket).toBeDefined()
			expect(service.socket.send).toBeDefined()
			expect(service.socket.receive).toBeDefined()


	describe "storageService", ->
		service = {}
		beforeEach inject (storageService) ->
			service = storageService

		it "should be available", ->
			expect(service).toBeDefined()

		it "strings can be set and recalled", ->
			service.set('test_key','test_value')
			expect(service.get('test_key')).toBe('test_value')

		it "objects can be set and recalled", ->
			service.set('test_key',{'key':'value'})
			expect(service.get('test_key').key).toBe('value')


	describe "languageService", ->
		service = {}
		enums = {}
		scope = {}
		beforeEach inject ($rootScope, languageService, enumsService) ->
			scope = $rootScope
			service = languageService
			enums = enumsService.socket

		it "should be available", ->
			expect(service).toBeDefined()

		it "can be populated", ->
			scope.$broadcast enums.receive + enums.getApplicationConsolidated,
				'LANGUAGE':
					'someGroup':
						'LT':
							'0':
								'target': 'test_target1'
								'value': 'test value text 1'
							'1':
								'target': 'test_target2'
								'value': 'test value text 2'

			expect(service.someGroup.test_target1).toBe('test value text 1')
			expect(service.someGroup.test_target2).toBe('test value text 2')


	describe "socketService", ->
		service = {}
		beforeEach inject (socketService) ->
			service = socketService

		it "should be available", ->
			expect(service).toBeDefined()

		it "should have methods", ->
			expect(service.onopen).toBeDefined()
			expect(service.onmessage).toBeDefined()
			expect(service.onclose).toBeDefined()
