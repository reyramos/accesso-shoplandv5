<?php
header('Content-Type: text/xml');

// config file to use as a template
$config_template = "config-FID.xml";

if(file_exists($config_template)){
	
	$xml = simplexml_load_file($config_template);	
	foreach ($_REQUEST as $name => $value) {
		//$_REQUEST[$name] = mysql_escape_string($value);
		//${$name} = $_REQUEST[$name];
		$attrs = $xml->SETTINGS->attributes();
		if( isset($attrs[$name]) ){
				$attrs[$name] = $value;						
		}else{
				$xml->SETTINGS->addAttribute($name, $value);
		}
	}	
	
	echo($xml->asXML());
	
}else{
	die("<msg>failed to open config.xml</msg>");
}

?>

