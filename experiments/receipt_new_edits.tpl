{config_load file=main.conf section=$section|default:'generic'}
{assign var=version value='4.11'}

<html>
	<head>
<meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">

<SCRIPT  type="text/javascript" src="../ext_functions.js"></SCRIPT>
{if $merchant_id == "3001" or $merchant_id == "3002" or $merchant_id == "3003" or $merchant_id == "3004" or $merchant_id == "3005" or $merchant_id == "3006" or $merchant_id == "3007" or $merchant_id == "3008" or $merchant_id == "3009" or $merchant_id == "3010" or $merchant_id == "3011"}
{literal}
<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' 	type='text/javascript'%3E%3C/script%3E"));
</script> 

<script type="text/javascript">
	try{
		var pageTracker = _gat._getTracker("UA-1277361-1");
		pageTracker._setAllowHash(false);
		pageTracker._setDomainName("none");
		pageTracker._setAllowLinker(true);
		pageTracker._trackPageview();
		{/literal}
		pageTracker._addTrans("{$confirmation}", "{if $merchant_id == "3001"}Fitchburg{/if}{if $merchant_id == "3002"}Newark{/if}{if $merchant_id == "3003"}Rockford{/if}{if $merchant_id == "3004"}Omaha{/if}{if $merchant_id == "3005"}Waterbury{/if}{if $merchant_id == "3006"}Mt. Laurel{/if}{if $merchant_id == "3007"}Cincinnati{/if}{if $merchant_id == "3008"}Arlingtom Heights{/if}{if $merchant_id == "3009"}Danvers{/if}{if $merchant_id == "3010"}Kansas City{/if}{if $merchant_id == "3011"}Orlando{/if}", "{$totals.current_total}", "{$totals.total_original_tax}", "{$totals.total_original_shipping}", "{$billing_address.city}", "{$billing_address.state}", "{$billing_address.country}");
		
		{foreach from=$packages key=k item=item}
		pageTracker._addItem("{$confirmation}", "{$item.package_id}", "{$item.package_name|utf}", "{$item.customer_type_name|utf}", "{$item.retail}", "{$item.qty}");
		{/foreach}
		{literal}
		pageTracker._trackTrans();
	} catch(err) {}
</script>

{/literal}

{/if}

							{if $title == "Cedar Fair"}
								{if $merchant_id == "1602" or $merchant_id == "2" or $order.parent_econsignment_merchant_id == "200"}
									<title>Canada's Wonderland Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1601" or $merchant_id == "1" or $order.parent_econsignment_merchant_id == "110"}
									<title>Carowinds Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1609" or  $merchant_id == "9" or $order.parent_econsignment_merchant_id == "900"}
									<title>Cedar Point Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1612" or  $merchant_id == "12" or $order.parent_econsignment_merchant_id == "1200"}
									<title>Dorney Park Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "6" or $order.parent_econsignment_merchant_id == "600"}
									<title>Gilroy Gardens Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1603" or $merchant_id == "3" or $order.parent_econsignment_merchant_id == "300"}
									<title>Great America Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1604" or  $merchant_id == "4" or $order.parent_econsignment_merchant_id == "400"}
									<title>Kings Dominion Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1605" or $merchant_id == "5" or $order.parent_econsignment_merchant_id == "500"}
									<title>Kings Island Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1610" or  $merchant_id == "10" or $order.parent_econsignment_merchant_id == "1010"}
									<title>Knott's Berry Farm Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1615" or $merchant_id == "15" or $order.parent_econsignment_merchant_id == "1500"}
									<title>Michigan's Adventure Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "20"}
									<title>Soak City Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "17"}
									<title>Soak City Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "18"}
									<title>Soak City Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "19"}
									<title>Soak City Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1611" or  $merchant_id == "11"  or $order.parent_econsignment_merchant_id == "1100"}
									<title>Valleyfair Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "14" or $order.parent_econsignment_merchant_id == "1400"}
									<title>Wildwater Kingdom Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "1613" or $merchant_id == "13" or $order.parent_econsignment_merchant_id == "1300"}
									<title>Worlds of Fun Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "21"}
									<title>Oceans Of Fun Purchase Receipt - Confirmation# {$confirmation}</title>
								{else}
									<title>{$title} Purchase Receipt - Confirmation# {$confirmation}</title>
								{/if}

							{elseif $title == "Palace"}
								{if $merchant_id == "8000" or $merchant_id == "8200" or $order.parent_econsignment_merchant_id == "8300"}
									<title>Big Kahunas Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8018" or $merchant_id == "8218" or $order.parent_econsignment_merchant_id == "8318"}
									<title>Noah's Ark Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8001" or $merchant_id == "8201" or $order.parent_econsignment_merchant_id == "8301"}
									<title>Castle Park Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8002" or $merchant_id == "8202" or $order.parent_econsignment_merchant_id == "8302"}
									<title>Dutch Wonderland Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8004" or $merchant_id == "8204" or $order.parent_econsignment_merchant_id == "8303"}
									<title>Idlewild Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8005" or $merchant_id == "8205" or $order.parent_econsignment_merchant_id == "8304"}
									<title>Kennywood Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8006" or $merchant_id == "8206" or $order.parent_econsignment_merchant_id == "8305"}
									<title>Lake Compounce Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8007"}
									<title>Mountain Creek Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8008" or $merchant_id == "8208" or $order.parent_econsignment_merchant_id == "8306"}
									<title>Raging Waters Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8009" or $merchant_id == "8209" or $order.parent_econsignment_merchant_id == "8307"}
									<title>Raging Waters Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8010" or $merchant_id == "8210" or $order.parent_econsignment_merchant_id == "8308"}
									<title>Raging Water Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8011" or $merchant_id == "8211" or $order.parent_econsignment_merchant_id == "8309"}
									<title>Sandcastle Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8012" or $merchant_id == "8212" or $order.parent_econsignment_merchant_id == "8310"}
									<title>Silver Springs Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8013" or $merchant_id == "8213" or $order.parent_econsignment_merchant_id == "8311"}
									<title>Splish Splash Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8014" or $merchant_id == "8214" or $order.parent_econsignment_merchant_id == "8312"}
									<title>Story Land Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8015" or $merchant_id == "8215" or $order.parent_econsignment_merchant_id == "8315"}
									<title>Wet and Wild Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8016" or $merchant_id == "8216" or $order.parent_econsignment_merchant_id == "8313"}
									<title>Water Country Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "9003" or $merchant_id == "9203" or $order.parent_econsignment_merchant_id == "10003"}
									<title>Water World Concord Purchase Receipt - Confirmation# {$confirmation}</title>
								{elseif $merchant_id == "8017" or $merchant_id == "8217" or $order.parent_econsignment_merchant_id == "8316"}
									<title>Wild Waters Purchase Receipt - Confirmation# {$confirmation}</title>
								{/if}
							{elseif $useDynamicReceipt == "truetrue"}
								{php}
   									global $merchant_id;
   									$varName = 'dynamicTitle_'.$merchant_id;
  									$this->assign('dynamicValue',$varName);
								{/php}								

								<title>{$dynamicValue} Purchase Receipt - Confirmation# {$confirmation}</title>
							{elseif $title == "Herschend"}
								<title>Purchase Receipt - Confirmation# {$confirmation}</title>
							{else}
								<title>{$title} Purchase Receipt - Confirmation# {$confirmation}</title>
							{/if}
							

		{literal}
		<STYLE>
	body{margin:0;}
	 .headerTop { background-color:#FFCC66; border-top:0px solid #000000; border-bottom:1px solid #FFFFFF; text-align:center; }
	 .adminText { font-size:10px; color:#996600; line-height:200%; font-family:verdana; text-decoration:none; }
	 .headerBar { background-color:#FFFFFF; border-top:0px solid #333333; border-bottom:10px solid #FFFFFF; }
	 .title { font-size:20px; font-weight:bold; color:#CC6600; font-family:arial; line-height:110%; }
	 .subTitle { font-size:11px; font-weight:normal; color:#666666; font-style:italic; font-family:arial; }
	 td { font-size:12px; color:#000000; line-height:150%; font-family:trebuchet ms; }
	 .sideColumn { background-color:#FFFFFF; border-left:1px dashed #CCCCCC; text-align:left; }
	 .sideColumnText { font-size:11px; font-weight:normal; color:#999999; font-family:arial; line-height:150%; }
	 .sideColumnTitle { font-size:15px; font-weight:bold; color:#333333; font-family:arial; line-height:150%; }
	 .footerRow { background-color:#FFFFCC; border-top:10px solid #FFFFFF; }
	 .footerText { font-size:10px; color:#996600; line-height:100%; font-family:verdana; }
	 a { color:#e27000;}
	</STYLE>
	{/literal}
	</head>
<body >
	
	
<table width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor='{$background_color}' style="background-color:{$background_color};" >
	<tr>

		<td valign="top" align="center" >
			<table width="600" cellpadding="0" cellspacing="0" bgcolor='#ffffff' style="background-color:#ffffff;">
				
				<!-- HEADER -->
				<table width="600" cellpadding="0" cellspacing="0" bgcolor='#ffffff' style="background-color:#ffffff;">
					<tr>
						<td align="left" valign="middle" style="background-color:#FFFFFF;border-top:0px solid #333333;">
							{if $title == "Cedar Fair"}
								{if $merchant_id == "1602" or $merchant_id == "2" or $order.parent_econsignment_merchant_id == "200"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/canadaswonderland.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1601" or $merchant_id == "1" or $order.parent_econsignment_merchant_id == "110"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/carowinds.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1609" or $merchant_id == "9" or $order.parent_econsignment_merchant_id == "900"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/cedarpoint.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1612" or $merchant_id == "12" or $order.parent_econsignment_merchant_id == "1200"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/dorneypark.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "6" or $order.parent_econsignment_merchant_id == "600"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/gilroygardens.png" BORDER="0" alt="" align="center" width="600"></center>	
								{elseif $merchant_id == "1603" or  $merchant_id == "3" or $order.parent_econsignment_merchant_id == "300"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/greatamerica.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1604" or  $merchant_id == "4" or $order.parent_econsignment_merchant_id == "400"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/kingsdominion.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1605" or $merchant_id == "5" or $order.parent_econsignment_merchant_id == "500"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/kingsisland.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1610" or $merchant_id == "10" or $order.parent_econsignment_merchant_id == "1010"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/knottsberryfarm.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1615" or $merchant_id == "15" or $order.parent_econsignment_merchant_id == "1500"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/michigansadventure.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "20"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/soakcitycp.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "17"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/soakcityknottsoc.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "18"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/soakcityknottsps.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "19"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/soakcityknottssd.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1611" or $merchant_id == "11" or $order.parent_econsignment_merchant_id == "1100"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/valleyfair.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "14" or $order.parent_econsignment_merchant_id == "1400"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/wildwaterkingdom.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1613" or $merchant_id == "13" or $order.parent_econsignment_merchant_id == "1300"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/worldsoffun.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "21"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/oceansoffun.png" BORDER="0" alt="" align="center" width="600"></center>
								{/if}
							{elseif $title == "Palace"}
								{if $merchant_id == "8000" or $merchant_id == "8200" or $order.parent_econsignment_merchant_id == "8300"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/kahuna_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8001" or $merchant_id == "8201" or $order.parent_econsignment_merchant_id == "8301"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/castle_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8018" or $merchant_id == "8218" or $order.parent_econsignment_merchant_id == "8318"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/noah_logo.jpg" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8002" or $merchant_id == "8202" or $order.parent_econsignment_merchant_id == "8302"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/dutch_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8004" or $merchant_id == "8204" or $order.parent_econsignment_merchant_id == "8303"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/idlewild_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8005" or $merchant_id == "8205" or $order.parent_econsignment_merchant_id == "8304"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/kennywood_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8006" or $merchant_id == "8206" or $order.parent_econsignment_merchant_id == "8305"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/compounce_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8007"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/default_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8008" or $merchant_id == "8208" or $order.parent_econsignment_merchant_id == "8306"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/rw_sc_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8009" or $merchant_id == "8209" or $order.parent_econsignment_merchant_id == "8307"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/rw_sd_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8010" or $merchant_id == "8210" or $order.parent_econsignment_merchant_id == "8308"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/rw_sj_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8011" or $merchant_id == "8211" or $order.parent_econsignment_merchant_id == "8309"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/sandcastle_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8012" or $merchant_id == "8212" or $order.parent_econsignment_merchant_id == "8310"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/silversprings_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8013" or $merchant_id == "8213" or $order.parent_econsignment_merchant_id == "8311"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/splish_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8014" or $merchant_id == "8214" or $order.parent_econsignment_merchant_id == "8312"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/storyland_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8015" or $merchant_id == "8215" or $order.parent_econsignment_merchant_id == "8315"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/wild_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8016" or $merchant_id == "8216" or $order.parent_econsignment_merchant_id == "8313"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/watercountry_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "9003" or $merchant_id == "9203" or $order.parent_econsignment_merchant_id == "10003"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/receipt_logoWWUSA.jpg" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "8017" or $merchant_id == "8217" or $order.parent_econsignment_merchant_id == "8316"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/wildwaters_logo.png" BORDER="0" alt="" align="center" width="600"></center>
								{/if}
							{elseif $title == "Herschend"}
								{if $merchant_id == "1001"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/receipt_logo_AA.jpg" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1002"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/receipt_logo_SD.jpg" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1003"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/receipt_logo_SM.jpg" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1004"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/receipt_logo_WA.jpg" BORDER="0" alt="" align="center" width="600"></center>
								{elseif $merchant_id == "1005"}
									<center><IMG id=editableImg1 SRC="{$receipt_path}images/receipt_logo_DW.jpg" BORDER="0" alt="" align="center" width="600"></center>
								{/if}
							{else}
								<center><IMG id=editableImg1 SRC="{$receipt_path}images/{$receipt_image}" BORDER="0" alt="" align="center" width="600"></center>
							{/if}
						</td>
					</tr>
<tr>
						<td style="border-bottom:2px solid #FFFFFF;">
							<IMG id=editableImg1 SRC="{$images_folder}notticket.jpg" BORDER="0" alt="" align="center" width="600">
						</td>
					</tr>

				</table>
			<table width="600" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="background-color:#ffffff;">
				<tr>
					<td width="10"></td>
					<!-- LEFT SIDE TABLE -->
					<td bgcolor="#FFFFFF" valign="top" width="572" style="font-size:;color:#000000;line-height:150%;font-family:arial;">
						<table width="572">
							<tr>
								<td width="560" bgcolor="#FFE3A9" style="border:solid #cccccc 1px;text-align:left;padding-left:5px;font-size:1em;">
									<b>{if $alterConfirmation != ""}{$alterConfirmation}{else}Confirmation#{/if}: <span style="color:#FF0000">{$confirmation}{if $appendConfirmation != ""}{$appendConfirmation}{else}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{/if}</b></span>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									
									<b>Order Date: {$order_date|date_format:"%m/%d/%Y"}</b><br>
									<table width="560" align="center">
									<tr><td width="560" style="text-align:center;">
									{if $smarty.request.merchant}
									<a href="#" onClick="window.print();return false;"><IMG SRC="{$images_folder}printReceiptOrange.png" BORDER="0" alt="" align="center" width="177"></a>
									{/if}
									</td></tr>
									</table>

								</td>
							</tr>
						</table>
						{if $receipt_add_text !=""}
						<table width="572">
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:center;padding-left:5px;font-size:1em;">
									{$receipt_add_text}
								</td>
							</tr>
						</table>
						{/if}

<!-- PASSBOOK : use_passbook   -->

					{if $order.use_passbook=="1" and $use_passbook == "true"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												iOS 6 users can add their tickets to Passbook on iPhone and iPod touch.
												{if $order.software == "ShopV5"}
													<a href="https://store.accesso.com/{if $order.merchant_uid == "AA-MOB"}HFE-AA/{/if}{if $order.merchant_uid == "AMP-MS"}AMP-MS/{/if}{if $order.merchant_uid == "BK"}PE-BK/{/if}{if $order.merchant_uid == "CF-CA"}CF-CA/{/if}{if $order.merchant_uid == "CF-CAP"}CF-CA/{/if}{if $order.merchant_uid == "CF-CP"}CF-CP/{/if}{if $order.merchant_uid == "CF-CW"}CF-CW/{/if}{if $order.merchant_uid == "CF-CWBPCAA"}CF-CW/{/if}{if $order.merchant_uid == "CF-CWP"}CF-CW/{/if}{if $order.merchant_uid == "CF-DP"}CF-DP/{/if}{if $order.merchant_uid == "CF-DPP"}CF-DP/{/if}{if $order.merchant_uid == "CF-GA"}CF-GA/{/if}{if $order.merchant_uid == "CF-GAP"}CF-GA/{/if}{if $order.merchant_uid == "CF-KBF"}CF-KBF/{/if}{if $order.merchant_uid == "CF-KBFP"}CF-KBF/{/if}{if $order.merchant_uid == "CF-KD"}CF-KD/{/if}{if $order.merchant_uid == "CF-KDP"}CF-KD/{/if}{if $order.merchant_uid == "CF-KI"}CF-KI/{/if}{if $order.merchant_uid == "CF-KIP"}CF-KI/{/if}{if $order.merchant_uid == "CF-MA"}CF-MA/{/if}{if $order.merchant_uid == "CF-MAP"}CF-MA/{/if}{if $order.merchant_uid == "CF-PP"}CF-CP/{/if}{if $order.merchant_uid == "CF-VF"}CF-VF/{/if}{if $order.merchant_uid == "CF-WF"}CF-WF/{/if}{if $order.merchant_uid == "CF-WFP"}CF-WF/{/if}{if $order.merchant_uid == "CP-MOB"}CF-CP/{/if}{if $order.merchant_uid == "CP"}PE-CP/{/if}{if $order.merchant_uid == "CW-MOB"}CF-CA/{/if}{if $order.merchant_uid == "CWO-MOB"}CF-CW/{/if}{if $order.merchant_uid == "DL-MOB"}HFE-DL/{/if}{if $order.merchant_uid == "DP-MOB"}CF-DP/{/if}{if $order.merchant_uid == "DW-MOB"}HFE-DW/{/if}{if $order.merchant_uid == "DW"}PE-DW/{/if}{if $order.merchant_uid == "EC-BK"}PE-BK/{/if}{if $order.merchant_uid == "EC-CP"}PE-CP/{/if}{if $order.merchant_uid == "EC-DW"}PE-DW/{/if}{if $order.merchant_uid == "EC-IW"}PE-IW/{/if}{if $order.merchant_uid == "EC-KW"}PE-KW/{/if}{if $order.merchant_uid == "EC-LC"}PE-LC/{/if}{if $order.merchant_uid == "EC-NA"}PE-NA/{/if}{if $order.merchant_uid == "EC-RWSC"}PE-RWSC/{/if}{if $order.merchant_uid == "EC-RWSJ"}PE-RWSJ/{/if}{if $order.merchant_uid == "EC-SC"}PE-SC/{/if}{if $order.merchant_uid == "EC-SPS"}PE-SPS/{/if}{if $order.merchant_uid == "EC-SVS"}PE-SVS/{/if}{if $order.merchant_uid == "EC-WAW"}PE-WAW/{/if}{if $order.merchant_uid == "EC-WLW"}PE-WLW/{/if}{if $order.merchant_uid == "EG-MOB"}HFE-EG/{/if}{if $order.merchant_uid == "GA-MOB"}CF-GA/{/if}{if $order.merchant_uid == "HFE-DL"}HFE-DL/{/if}{if $order.merchant_uid == "HFE-DW"}HFE-DW/{/if}{if $order.merchant_uid == "HFE-EG"}HFE-EG/{/if}{if $order.merchant_uid == "HFE-SDC"}HFE-SDC/{/if}{if $order.merchant_uid == "HFE-WA"}HFE-WA/{/if}{if $order.merchant_uid == "iTicket-CH"}CF-GA/{/if}{if $order.merchant_uid == "iTicket-LA"}SF-LA/{/if}{if $order.merchant_uid == "IW"}PE-IW/{/if}{if $order.merchant_uid == "KB-MOB"}CF-KBF/{/if}{if $order.merchant_uid == "KD-MOB"}CF-KD/{/if}{if $order.merchant_uid == "KI-MOB"}CF-KI/{/if}{if $order.merchant_uid == "KW"}PE-KW/{/if}{if $order.merchant_uid == "LC"}PE-LC/{/if}{if $order.merchant_uid == "MA-MOB"}CF-MA/{/if}{if $order.merchant_uid == "MPS-MS"}AMP-MS/{/if}{if $order.merchant_uid == "NA"}PE-NA/{/if}{if $order.merchant_uid == "NS-MBM"}PRC-NSSC/{/if}{if $order.merchant_uid == "PC-DL"}HFE-DL/{/if}{if $order.merchant_uid == "PC-EG"}HFE-EG/{/if}{if $order.merchant_uid == "PC-MS"}AMP-MS/{/if}{if $order.merchant_uid == "PC-WWUSA"}PE-WWCA/{/if}{if $order.merchant_uid == "PE-BK"}PE-BK/{/if}{if $order.merchant_uid == "PE-CP"}PE-CP/{/if}{if $order.merchant_uid == "PE-DW"}PE-DW/{/if}{if $order.merchant_uid == "PE-IW"}PE-IW/{/if}{if $order.merchant_uid == "PE-KW"}PE-KW/{/if}{if $order.merchant_uid == "PE-LC"}PE-LC/{/if}{if $order.merchant_uid == "PE-NA"}PE-NA/{/if}{if $order.merchant_uid == "PE-RWSC"}PE-RWSC/{/if}{if $order.merchant_uid == "PE-RWSD"}PE-RWSD/{/if}{if $order.merchant_uid == "PE-RWSJ"}PE-RWSJ/{/if}{if $order.merchant_uid == "PE-SC"}PE-SC/{/if}{if $order.merchant_uid == "PE-SIS"}PE-SVS/{/if}{if $order.merchant_uid == "PE-SL"}PE-SL/{/if}{if $order.merchant_uid == "PE-SPS"}PE-SPS/{/if}{if $order.merchant_uid == "PE-SS"}PE-SPS/{/if}{if $order.merchant_uid == "PE-SVS"}PE-SVS/{/if}{if $order.merchant_uid == "PE-WAW"}PE-WAW/{/if}{if $order.merchant_uid == "PE-WC"}PE-WC/{/if}{if $order.merchant_uid == "PE-WLW"}PE-WLW/{/if}{if $order.merchant_uid == "PE-WW"}PE-WWCA/{/if}{if $order.merchant_uid == "PE-WWCA"}PE-WWCA/{/if}{if $order.merchant_uid == "PE-WWEP"}PE-WAW/{/if}{if $order.merchant_uid == "PE-WWS"}PE-WLW/{/if}{if $order.merchant_uid == "RWSC"}PE-RWSC/{/if}{if $order.merchant_uid == "RWSD-MOB"}PE-RWSD/{/if}{if $order.merchant_uid == "RWSJ"}PE-RWSJ/{/if}{if $order.merchant_uid == "SC"}PE-SC/{/if}{if $order.merchant_uid == "SDC-MOB"}HFE-SDC/{/if}{if $order.merchant_uid == "SF-CH"}CF-GA/{/if}{if $order.merchant_uid == "SF-LA"}SF-LA/{/if}{if $order.merchant_uid == "SF-MOB"}CF-GA/{/if}{if $order.merchant_uid == "SFM-MOB"}SF-LA/{/if}{if $order.merchant_uid == "SP-MOB"}SW-SP/{/if}{if $order.merchant_uid == "SPS"}PE-SPS/{/if}{if $order.merchant_uid == "SVS"}PE-SVS/{/if}{if $order.merchant_uid == "SW-MSP"}SW-SP/{/if}{if $order.merchant_uid == "SW-SP"}SW-SP/{/if}{if $order.merchant_uid == "VF-MOB"}CF-VF/{/if}{if $order.merchant_uid == "WA-MOB"}HFE-WA/{/if}{if $order.merchant_uid == "WAW"}PE-WAW/{/if}{if $order.merchant_uid == "WLW"}PE-WLW/{/if}{if $order.merchant_uid == "WOF-MOB"}CF-WF/{/if}{if $order.merchant_uid == "ww-MOBILE"}NOR-WW/{/if}orderView/{$payment.card_number|replace:'X':''}/{$phone}/{$confirmation}" target='_blank'><img src="{$images_folder}addtopassbook.png" alt="<Add To Passbook!>" style="border:none;"></a>
												{else}
													<a href="{$passbook_url}?order_id={$order_id}&island={$passbook_island}" target="_blank"><img src="{$images_folder}addtopassbook.png" alt="<Add To Passbook!>" style="border:none;"></a>
												{/if}
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					
					{/if}

<!-- DELIVERY SECTION -->
						
					{if $mobile_barcode and $mobile_barcode != ""}

						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												You can view your tickets and present them for scanning below.
												<a href="{$mobile_barcode}{$confirmation}" target="_blank"><img src="{$images_folder}viewtickets.png" alt="<View Tickets button!>" style="border:none;"></a> 
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					{elseif $ship_method == "K" and $alternative_k_shipping_desc and $alternative_k_shipping_desc != ""}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												{$alternative_k_shipping_desc}
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					{elseif $ship_method == "M"}
						<table width="572">
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1em;">
												<br/>
												
												{if $order.software == "ShopV5"}
													<b>You can view your mobile tickets on your phone for scanning or print them at home!</b><br>
													<a href="https://store.accesso.com/{if $order.merchant_uid == "AA-MOB"}HFE-AA/{/if}{if $order.merchant_uid == "AMP-MS"}AMP-MS/{/if}{if $order.merchant_uid == "BK"}PE-BK/{/if}{if $order.merchant_uid == "CF-CA"}CF-CA/{/if}{if $order.merchant_uid == "CF-CAP"}CF-CA/{/if}{if $order.merchant_uid == "CF-CP"}CF-CP/{/if}{if $order.merchant_uid == "CF-CW"}CF-CW/{/if}{if $order.merchant_uid == "CF-CWBPCAA"}CF-CW/{/if}{if $order.merchant_uid == "CF-CWP"}CF-CW/{/if}{if $order.merchant_uid == "CF-DP"}CF-DP/{/if}{if $order.merchant_uid == "CF-DPP"}CF-DP/{/if}{if $order.merchant_uid == "CF-GA"}CF-GA/{/if}{if $order.merchant_uid == "CF-GAP"}CF-GA/{/if}{if $order.merchant_uid == "CF-KBF"}CF-KBF/{/if}{if $order.merchant_uid == "CF-KBFP"}CF-KBF/{/if}{if $order.merchant_uid == "CF-KD"}CF-KD/{/if}{if $order.merchant_uid == "CF-KDP"}CF-KD/{/if}{if $order.merchant_uid == "CF-KI"}CF-KI/{/if}{if $order.merchant_uid == "CF-KIP"}CF-KI/{/if}{if $order.merchant_uid == "CF-MA"}CF-MA/{/if}{if $order.merchant_uid == "CF-MAP"}CF-MA/{/if}{if $order.merchant_uid == "CF-PP"}CF-CP/{/if}{if $order.merchant_uid == "CF-VF"}CF-VF/{/if}{if $order.merchant_uid == "CF-WF"}CF-WF/{/if}{if $order.merchant_uid == "CF-WFP"}CF-WF/{/if}{if $order.merchant_uid == "CP-MOB"}CF-CP/{/if}{if $order.merchant_uid == "CP"}PE-CP/{/if}{if $order.merchant_uid == "CW-MOB"}CF-CA/{/if}{if $order.merchant_uid == "CWO-MOB"}CF-CW/{/if}{if $order.merchant_uid == "DL-MOB"}HFE-DL/{/if}{if $order.merchant_uid == "DP-MOB"}CF-DP/{/if}{if $order.merchant_uid == "DW-MOB"}HFE-DW/{/if}{if $order.merchant_uid == "DW"}PE-DW/{/if}{if $order.merchant_uid == "EC-BK"}PE-BK/{/if}{if $order.merchant_uid == "EC-CP"}PE-CP/{/if}{if $order.merchant_uid == "EC-DW"}PE-DW/{/if}{if $order.merchant_uid == "EC-IW"}PE-IW/{/if}{if $order.merchant_uid == "EC-KW"}PE-KW/{/if}{if $order.merchant_uid == "EC-LC"}PE-LC/{/if}{if $order.merchant_uid == "EC-NA"}PE-NA/{/if}{if $order.merchant_uid == "EC-RWSC"}PE-RWSC/{/if}{if $order.merchant_uid == "EC-RWSJ"}PE-RWSJ/{/if}{if $order.merchant_uid == "EC-SC"}PE-SC/{/if}{if $order.merchant_uid == "EC-SPS"}PE-SPS/{/if}{if $order.merchant_uid == "EC-SVS"}PE-SVS/{/if}{if $order.merchant_uid == "EC-WAW"}PE-WAW/{/if}{if $order.merchant_uid == "EC-WLW"}PE-WLW/{/if}{if $order.merchant_uid == "EG-MOB"}HFE-EG/{/if}{if $order.merchant_uid == "GA-MOB"}CF-GA/{/if}{if $order.merchant_uid == "HFE-DL"}HFE-DL/{/if}{if $order.merchant_uid == "HFE-DW"}HFE-DW/{/if}{if $order.merchant_uid == "HFE-EG"}HFE-EG/{/if}{if $order.merchant_uid == "HFE-SDC"}HFE-SDC/{/if}{if $order.merchant_uid == "HFE-WA"}HFE-WA/{/if}{if $order.merchant_uid == "iTicket-CH"}CF-GA/{/if}{if $order.merchant_uid == "iTicket-LA"}SF-LA/{/if}{if $order.merchant_uid == "IW"}PE-IW/{/if}{if $order.merchant_uid == "KB-MOB"}CF-KBF/{/if}{if $order.merchant_uid == "KD-MOB"}CF-KD/{/if}{if $order.merchant_uid == "KI-MOB"}CF-KI/{/if}{if $order.merchant_uid == "KW"}PE-KW/{/if}{if $order.merchant_uid == "LC"}PE-LC/{/if}{if $order.merchant_uid == "MA-MOB"}CF-MA/{/if}{if $order.merchant_uid == "MPS-MS"}AMP-MS/{/if}{if $order.merchant_uid == "NA"}PE-NA/{/if}{if $order.merchant_uid == "NS-MBM"}PRC-NSSC/{/if}{if $order.merchant_uid == "PC-DL"}HFE-DL/{/if}{if $order.merchant_uid == "PC-EG"}HFE-EG/{/if}{if $order.merchant_uid == "PC-MS"}AMP-MS/{/if}{if $order.merchant_uid == "PC-WWUSA"}PE-WWCA/{/if}{if $order.merchant_uid == "PE-BK"}PE-BK/{/if}{if $order.merchant_uid == "PE-CP"}PE-CP/{/if}{if $order.merchant_uid == "PE-DW"}PE-DW/{/if}{if $order.merchant_uid == "PE-IW"}PE-IW/{/if}{if $order.merchant_uid == "PE-KW"}PE-KW/{/if}{if $order.merchant_uid == "PE-LC"}PE-LC/{/if}{if $order.merchant_uid == "PE-NA"}PE-NA/{/if}{if $order.merchant_uid == "PE-RWSC"}PE-RWSC/{/if}{if $order.merchant_uid == "PE-RWSD"}PE-RWSD/{/if}{if $order.merchant_uid == "PE-RWSJ"}PE-RWSJ/{/if}{if $order.merchant_uid == "PE-SC"}PE-SC/{/if}{if $order.merchant_uid == "PE-SIS"}PE-SVS/{/if}{if $order.merchant_uid == "PE-SL"}PE-SL/{/if}{if $order.merchant_uid == "PE-SPS"}PE-SPS/{/if}{if $order.merchant_uid == "PE-SS"}PE-SPS/{/if}{if $order.merchant_uid == "PE-SVS"}PE-SVS/{/if}{if $order.merchant_uid == "PE-WAW"}PE-WAW/{/if}{if $order.merchant_uid == "PE-WC"}PE-WC/{/if}{if $order.merchant_uid == "PE-WLW"}PE-WLW/{/if}{if $order.merchant_uid == "PE-WW"}PE-WWCA/{/if}{if $order.merchant_uid == "PE-WWCA"}PE-WWCA/{/if}{if $order.merchant_uid == "PE-WWEP"}PE-WAW/{/if}{if $order.merchant_uid == "PE-WWS"}PE-WLW/{/if}{if $order.merchant_uid == "RWSC"}PE-RWSC/{/if}{if $order.merchant_uid == "RWSD-MOB"}PE-RWSD/{/if}{if $order.merchant_uid == "RWSJ"}PE-RWSJ/{/if}{if $order.merchant_uid == "SC"}PE-SC/{/if}{if $order.merchant_uid == "SDC-MOB"}HFE-SDC/{/if}{if $order.merchant_uid == "SF-CH"}CF-GA/{/if}{if $order.merchant_uid == "SF-LA"}SF-LA/{/if}{if $order.merchant_uid == "SF-MOB"}CF-GA/{/if}{if $order.merchant_uid == "SFM-MOB"}SF-LA/{/if}{if $order.merchant_uid == "SP-MOB"}SW-SP/{/if}{if $order.merchant_uid == "SPS"}PE-SPS/{/if}{if $order.merchant_uid == "SVS"}PE-SVS/{/if}{if $order.merchant_uid == "SW-MSP"}SW-SP/{/if}{if $order.merchant_uid == "SW-SP"}SW-SP/{/if}{if $order.merchant_uid == "VF-MOB"}CF-VF/{/if}{if $order.merchant_uid == "WA-MOB"}HFE-WA/{/if}{if $order.merchant_uid == "WAW"}PE-WAW/{/if}{if $order.merchant_uid == "WLW"}PE-WLW/{/if}{if $order.merchant_uid == "WOF-MOB"}CF-WF/{/if}{if $order.merchant_uid == "ww-MOBILE"}NOR-WW/{/if}orderView/{$payment.card_number|replace:'X':''}/{$phone}/{$confirmation}" target='_blank'><img src="{$images_folder}registerMobileBttn.jpg" alt="<View Your Tickets>" style="border:none;"></a>
													{if $order.merchant_uid != "SF-LA" AND $order.merchant_uid != "iTicket-LA" AND $order.merchant_uid != "SFM-MOB" AND $order.merchant_uid != "AMP-MS" AND $order.merchant_uid != "PC-MS" AND $order.merchant_uid != "MPS-MS"}<a href="https://shop.accesso.com/htmlTicket/{if $order.merchant_uid == "MPS-MS"}accesso16/ticket.php{/if}{if $order.merchant_uid == "ww-MOBILE"}accesso17/ticket.php{/if}{if $order.merchant_uid == "NS-MBM"}parc/nascar.php{/if}{if $order.merchant_uid == "SF-LA"}SF-LA/{/if}{if $order.merchant_uid == "iTicket-LA"}SF-LA/{/if}{if $order.merchant_uid == "SFM-MOB"}SF-LA/{/if}{if $order.merchant_uid == "AMP-MS"}AMP-MS/{/if}{if $order.merchant_uid == "PC-MS"}AMP-MS/{/if}{if $order.merchant_uid == "MPS-MS"}AMP-MS/{/if}{if $order.merchant_uid == "PE-WC"}accesso18/wc.php{/if}{if $order.merchant_uid == "SP-MOB"}sesameplace/ticket.php{/if}{if $order.merchant_uid == "SW-SP"}sesameplace/ticket.php{/if}{if $order.merchant_uid == "SW-MSP"}sesameplace/ticket.php{/if}{if $order.merchant_uid == "SDC-MOB"}accesso14/sdc.php{/if}{if $order.merchant_uid == "HFE-SDC"}accesso14/sdc.php{/if}{if $order.merchant_uid == "HFE-EG"}accesso14/eg.php{/if}{if $order.merchant_uid == "PC-EG"}accesso14/eg.php{/if}{if $order.merchant_uid == "EG-MOB"}accesso14/eg.php{/if}{if $order.merchant_uid == "HFE-WA"}accesso14/wa.php{/if}{if $order.merchant_uid == "WA-MOB"}accesso14/wa.php{/if}{if $order.merchant_uid == "HFE-DL"}accesso14/dl.php{/if}{if $order.merchant_uid == "PC-DL"}accesso14/dl.php{/if}{if $order.merchant_uid == "DL-MOB"}accesso14/dl.php{/if}{if $order.merchant_uid == "HFE-DW"}accesso14/dw.php{/if}{if $order.merchant_uid == "DW-MOB"}accesso14/dw.php{/if}{if $order.merchant_uid == "HFE-AA"}accesso14/aa.php{/if}{if $order.merchant_uid == "AA-MOB"}accesso14/aa.php{/if}{if $order.merchant_uid == "DW"}accesso18/dw.php{/if}{if $order.merchant_uid == "PE-DW"}accesso18/dw.php{/if}{if $order.merchant_uid == "EC-DW"}accesso18/dw.php{/if}{if $order.merchant_uid == "RWSD-MOB"}accesso18/rwsd.php{/if}{if $order.merchant_uid == "PE-RWSD"}accesso18/rwsd.php{/if}{if $order.merchant_uid == "PE-SC"}accesso18/sc.php{/if}{if $order.merchant_uid == "EC-SC"}accesso18/s.php{/if}{if $order.merchant_uid == "SC"}accesso18/sc.php{/if}{if $order.merchant_uid == "EC-RWSJ"}accesso18/rwsj.php{/if}{if $order.merchant_uid == "RWSJ"}accesso18/rwsj.php{/if}{if $order.merchant_uid == "PE-RWSJ"}accesso18/rwsj.php{/if}{if $order.merchant_uid == "PE-RWSC"}accesso18/rwsc.php{/if}{if $order.merchant_uid == "EC-RWSC"}accesso18/rwsc.php{/if}{if $order.merchant_uid == "RWSC"}accesso18/rwsc.php{/if}{if $order.merchant_uid == "PE-NA"}accesso18/na.php{/if}{if $order.merchant_uid == "EC-NA"}accesso18/na.php{/if}{if $order.merchant_uid == "NA"}accesso18/na.php{/if}{if $order.merchant_uid == "PE-IW"}accesso18/iw.php{/if}{if $order.merchant_uid == "EC-IW"}accesso18/iw.php{/if}{if $order.merchant_uid == "IW"}accesso18/iw.php{/if}{if $order.merchant_uid == "PE-KW"}accesso18/kw.php{/if}{if $order.merchant_uid == "EC-KW"}accesso18/kw.php{/if}{if $order.merchant_uid == "KW"}accesso18/kw.php{/if}{if $order.merchant_uid == "PE-LC"}accesso18/lc.php{/if}{if $order.merchant_uid == "EC-LC"}accesso18/lc.php{/if}{if $order.merchant_uid == "LC"}accesso18/lc.php{/if}{if $order.merchant_uid == "PE-SVS"}accesso18/svs.php{/if}{if $order.merchant_uid == "EC-SVS"}accesso18/svs.php{/if}{if $order.merchant_uid == "SVS"}accesso18/svs.php{/if}{if $order.merchant_uid == "PE-SIS"}accesso18/svs.php{/if}{if $order.merchant_uid == "PE-SPS"}accesso18/sps.php{/if}{if $order.merchant_uid == "EC-SPS"}accesso18/sps.php{/if}{if $order.merchant_uid == "SPS"}accesso18/sps.php{/if}{if $order.merchant_uid == "PE-SS"}accesso18/sps.php{/if}{if $order.merchant_uid == "PE-SL"}accesso18/sl.php{/if}{if $order.merchant_uid == "PE-CP"}accesso18/cp.php{/if}{if $order.merchant_uid == "CP"}accesso18/cp.php{/if}{if $order.merchant_uid == "EC-CP"}accesso18/cp.php{/if}{if $order.merchant_uid == "PE-WWCA"}accesso18/wwusa.php{/if}{if $order.merchant_uid == "PC-WWUSA"}accesso18/wwusa.php{/if}{if $order.merchant_uid == "PE-WW"}accesso18/wwusa.php{/if}{if $order.merchant_uid == "PE-WAW"}accesso18/waw.php{/if}{if $order.merchant_uid == "EC-WAW"}accesso18/waw.php{/if}{if $order.merchant_uid == "WAW"}accesso18/waw.php{/if}{if $order.merchant_uid == "PE-WWEP"}accesso18/waw.php{/if}{if $order.merchant_uid == "PE-WLW"}accesso18/ww.php{/if}{if $order.merchant_uid == "EC-WLW"}accesso18/ww.php{/if}{if $order.merchant_uid == "WLW"}accesso18/ww.php{/if}{if $order.merchant_uid == "PE-WWS"}accesso18/ww.php{/if}{if $order.merchant_uid == "PE-BK"}accesso18/bk.php{/if}{if $order.merchant_uid == "BK"}accesso18/bk.php{/if}{if $order.merchant_uid == "EC-BK"}accesso18/bk.php{/if}{if $order.merchant_uid == "CF-KD"}cedarfair/kd.php{/if}{if $order.merchant_uid == "KD-MOB"}cedarfair/kd.php{/if}{if $order.merchant_uid == "CF-KDP"}cedarfair/kd.php{/if}{if $order.merchant_uid == "CF-KI"}cedarfair/ki.php{/if}{if $order.merchant_uid == "KI-MOB"}cedarfair/ki.php{/if}{if $order.merchant_uid == "CF-KIP"}cedarfair/ki.php{/if}{if $order.merchant_uid == "CF-KBF"}cedarfair/kbf.php{/if}{if $order.merchant_uid == "KB-MOB"}cedarfair/kbf.php{/if}{if $order.merchant_uid == "CF-KBFP"}cedarfair/kbf.php{/if}{if $order.merchant_uid == "CF-MA"}cedarfair/ma.php{/if}{if $order.merchant_uid == "MA-MOB"}cedarfair/ma.php{/if}{if $order.merchant_uid == "CF-MAP"}cedarfair/ma.php{/if}{if $order.merchant_uid == "VF-MOB"}cedarfair/vf.php{/if}{if $order.merchant_uid == "CF-VF"}cedarfair/vf.php{/if}{if $order.merchant_uid == "CF-WF"}cedarfair/wof.php{/if}{if $order.merchant_uid == "WOF-MOB"}cedarfair/wof.php{/if}{if $order.merchant_uid == "CF-WFP"}cedarfair/wof.php{/if}{if $order.merchant_uid == "CF-CW"}cedarfair/cw.php{/if}{if $order.merchant_uid == "CF-CWBPCAA"}cedarfair/cw.php{/if}{if $order.merchant_uid == "CWO-MOB"}cedarfair/cw.php{/if}{if $order.merchant_uid == "CF-CWP"}cedarfair/cw.php{/if}{if $order.merchant_uid == "CF-CA"}cedarfair/ca.php{/if}{if $order.merchant_uid == "CW-MOB"}cedarfair/ca.php{/if}{if $order.merchant_uid == "CF-CAP"}cedarfair/ca.php{/if}{if $order.merchant_uid == "CF-CP"}cedarfair/cp.php{/if}{if $order.merchant_uid == "CP-MOB"}cedarfair/cp.php{/if}{if $order.merchant_uid == "CF-PP"}cedarfair/cp.php{/if}{if $order.merchant_uid == "CF-DP"}cedarfair/dp.php{/if}{if $order.merchant_uid == "DP-MOB"}cedarfair/dp.php{/if}{if $order.merchant_uid == "CF-DPP"}cedarfair/dp.php{/if}{if $order.merchant_uid == "CF-GA"}cedarfair/ga.php{/if}{if $order.merchant_uid == "GA-MOB"}cedarfair/ga.php{/if}{if $order.merchant_uid == "CF-GAP"}cedarfair/ga.php{/if}{if $order.merchant_uid == "iTicket-CH"}cedarfair/ga.php{/if}{if $order.merchant_uid == "SF-CH"}cedarfair/ga.php{/if}{if $order.merchant_uid == "SF-MOB"}cedarfair/ga.php{/if}?order_id={$confirmation}&ph={$phone}" target='_blank'><img src="{$images_folder}printMobileBttn.jpg" alt="<View Your Tickets>" style="border:none;"></a>{/if}



												{else}
													<b>You can view your mobile tickets on your phone or at home!</b><br>
													<a href="https://shop.accesso.com/mobile/{if $merchant_id == "101"}src/?m=SP{/if}{if $merchant_id == "405"}src/?m=WW{/if}{if $merchant_id == "8200"}palace/router.php?m=PE-BK{/if}{if $merchant_id == "8201"}palace/router.php?m=PE-CP{/if}{if $merchant_id == "8202"}palace/router.php?m=PE-DW{/if}{if $merchant_id == "8204"}palace/router.php?m=PE-IW{/if}{if $merchant_id == "8205"}palace/router.php?m=PE-KW{/if}{if $merchant_id == "8206"}palace/router.php?m=PE-LC{/if}{if $merchant_id == "8218"}palace/router.php?m=PE-NA{/if}{if $merchant_id == "8208"}palace/router.php?m=PE-RWSC{/if}{if $merchant_id == "8209"}palace/router.php?m=PE-RWSD{/if}{if $merchant_id == "8210"}palace/router.php?m=PE-RWSJ{/if}{if $merchant_id == "8211"}palace/router.php?m=PE-SC{/if}{if $merchant_id == "8212"}palace/router.php?m=PE-SIS{/if}{if $merchant_id == "8213"}palace/router.php?m=PE-SS{/if}{if $merchant_id == "8214"}palace/router.php?m=PE-SL{/if}{if $merchant_id == "8216"}palace/router.php?m=PE-WC{/if}{if $merchant_id == "9203"}palace/router.php?m=PE-WW{/if}{if $merchant_id == "8215"}palace/router.php?m=PE-WWEP{/if}{if $merchant_id == "8217"}palace/router.php?m=PE-WWS{/if}{if $merchant_id == "1007"}hfe/router.php?m=HFE-EG{/if}{if $merchant_id == "1006"}hfe/router.php?m=HFE-DL{/if}{if $merchant_id == "1001"}hfe/router.php?m=HFE-AA{/if}{if $merchant_id == "1002"}hfe/router.php?m=HFE-SDC{/if}{if $merchant_id == "1003"}{/if}{if $merchant_id == "1004"}hfe/router.php?m=HFE-WA{/if}{if $merchant_id == "1005"}hfe/router.php?m=HFE-DW{/if}{if $merchant_id == "1602"}cedarfair/router.php?m=CF-CW{/if}{if $merchant_id == "1601"}cedarfair/router.php?m=CF-CA{/if}{if $merchant_id == "1609"}cedarfair/router.php?m=CF-CP{/if}{if $merchant_id == "1612"}cedarfair/router.php?m=CF-DP{/if}{if $merchant_id == "1603"}cedarfair/router.php?m=CF-GA{/if}{if $merchant_id == "1604"}cedarfair/router.php?m=CF-KD{/if}{if $merchant_id == "1605"}cedarfair/router.php?m=CF-KI{/if}{if $merchant_id == "1610"}cedarfair/router.php?m=CF-KB{/if}{if $merchant_id == "1615"}cedarfair/router.php?m=CF-MA{/if}{if $merchant_id == "1611"}cedarfair/router.php?m=CF-VF{/if}{if $merchant_id == "1613"}cedarfair/router.php?m=CF-WF{/if}{if $merchant_id == "200"}isc/router.php?m=ISC-R66{/if}{if $merchant_id == "301"}src/router.php?m=SWP-GI{/if}{if $merchant_id == "10058"}src/router.php?m=AMP-MS{/if}{if $merchant_id == "302"}src/router.php?m=SWP-SPI{/if}{if $merchant_id == "303"}src/router.php?m=SWP-NB{/if}{if $merchant_id == "304"}src/router.php?m=SWP-KC{/if}{if $merchant_id == "100"}src/router.php?m=WPZ{/if}{if $merchant_id == "300"}columbuszoo/router.php?m=CZ{/if}{if $merchant_id == "155"}src/router.php?m=CGG{/if}{if $merchant_id == "214"}sixflags/router.php?m=SF-BA{/if}{if $merchant_id == "230"}sixflags/router.php?m=SF-DK{/if}{if $merchant_id == "220"}sixflags/router.php?m=SF-FT{/if}{if $merchant_id == "210"}sixflags/router.php?m=SF-GAV{/if}{if $merchant_id == "215"}sixflags/router.php?m=SF-GAM{/if}{if $merchant_id == "257"}sixflags/router.php?m=SF-HHOT{/if}{if $merchant_id == "256"}sixflags/router.php?m=SF-HHLA{/if}{if $merchant_id == "255"}sixflags/router.php?m=SF-HHNJ{/if}{if $merchant_id == "211"}sixflags/router.php?m=SF-MM{/if}{if $merchant_id == "229"}sixflags/router.php?m=SF-NE{/if}{if $merchant_id == "207"}sixflags/router.php?m=SF-OG{/if}{if $merchant_id == "219"}sixflags/router.php?m=SF-OT{/if}{if $merchant_id == "208"}sixflags/router.php?m=SF-TGE{/if}{if $merchant_id == "258"}sixflags/router.php?m=SF-WW{/if}{if $merchant_id == "100"}src/router.php?m=WPZ{/if}{if $merchant_id == "154"}src/router.php?m=SN{/if}&orderID={$confirmation}&link=orderhistorydetails&ph={$phone}&amp;merchant_id={$merchant_id}&amp;lang={$lang}{$print_at_home_vars}" target='_blank'><img src="{$images_folder}registerMobileBttn.jpg" alt="<View Your Tickets>" style="border:none;"></a>
												{/if}
												<br/>

											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					{elseif $ship_method == "D" || $ship_method == "P" || $ship_method == "G" || $ship_method == "N"}
						{include file="include_template_P@H.tpl"}						
					{/if}
						{if $ship_method == "A"}
							<!-- Unused Shipping Method -->
						{/if}
						{if $ship_method == "E"}
							<p><b>Signature is NOT required on delivery by UPS, rerouting of packages will not be authorized.</b></p>
						{/if}
						{if $ship_method == "K"}
							<!-- UPS GROUND - NO MESSAGE -->
						{/if}
						{if $ship_method == "L"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												*You have chosen to have your order sent via {$delivery_method} Shipping. Signature is not required. Re-routing of packages is not allowed.
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						{/if}
						{if $ship_method == "S"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												Signature is NOT required on delivery by UPS, rerouting of packages will not be authorized.																				</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						{/if}
						{if $ship_method == "V"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												Signature is NOT required on delivery by UPS, rerouting of packages will not be authorized.																				</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						{/if}
						{if $ship_method == "U"}
							<!-- UPS FIRST CLASS - NO MESSAGE -->
						{/if}
						{if $ship_method == "W"}
							<!-- NO SHIPPING METHOD - NO MESSAGE -->
						{/if}
						{if $ship_method == "Y"}
							{if $title == "Bay Hill"}
							<table>
								<tr>
									<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
										<table>
											<tr>
												<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
													This is a Will Call order. Please bring receipt and your ID to the Will Call at Edwin Watts, 7501 Turkey Lake Road, Orlando FL 32819 to pick-up your tickets.															</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							{else}
							<table>
								<tr>
									<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
										<table>
											<tr>
												<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
													This is a Will Call order.  Please bring receipt and your ID to the Will Call window to pick-up your tickets.																					</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							{/if}

						{/if}

				<!-- 
				///////////////////////////////////////////////////////////
				/////////////////////////// AAA ///////////////////////////
				///////////////////////////////////////////////////////////
				-->


				{assign var=flResident value=0}
				{foreach from=$ticketcharacs key=k item=item}
					{if $item.willcall_ticket_option and $flResident == 0}
						{if $item.willcall_ticket_option == "1"}
							{math equation="x + y" x=0 y=1 assign='flResident'}
						{else if $item.willcall_ticket_option == "2"}
							{math equation="x + y" x=0 y=2 assign='flResident'}
						{/if}
					{else if $item.willcall_ticket_option and $flResident == 2}
						{if $item.willcall_ticket_option == "1"}
							{math equation="x + y" x=0 y=1 assign='flResident'}
						{/if}
					{/if}
				{/foreach}


				<!-- H : WDW / USO -->
				{if $ship_method == "H"}
					
					<table style="border:solid #cccccc 1px;margin:0 0 0 2px;">
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}wdw_logo.jpg" style="border:none;" title="Walt Disney World">
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
								This confirmation is not valid for admission.<br/>Your e-ticket will be emailed to you within 2 business days.
							</td>
						</tr>
					</table>
					<table  style="border:solid #cccccc 1px;margin:0 0 0 2px;" >
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}uso_logo.jpg" style="border:none;" title="Universal Orlando">
							</td>
						</tr>


						{if $flResident == 1}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								TICKET WINDOW PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and proceed to any front gate ticket window.  --- At the window, you must present a valid photo ID matching the guest's name listed on this confirmation page.  You will also be required to present the external ID# on this confirmation page and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change. <br/><br/>
&#183;FLORIDA RESIDENT TICKETS - An approved form of Florida photo ID (FL driver's license, FL state-issued ID card with FL address, FL voter's registration card with corresponding photo ID or FL college/university photo ID) matching the guest's name listed on this confirmation page is required for ticket purchase, ticket pickup, and usage.  <br/><br/>
&#183;DINING - If you purchased a dining product, a 36 hour advance reservation is required. Please call 407-224-7554 to reserve your seating.  
							</td>
						</tr>

						{else}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								WILL CALL KIOSK TICKET PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and look to your right for the Will Call Kiosks. -- At the kiosk machine, you must swipe a credit card matching the guest's name listed on this confirmation page for identity verification only. You will also be required to enter the external ID# on this confirmation and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Matching valid photo ID required. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change.  
							</td>
						</tr>

						{/if}

					</table>

				{/if}
				<!-- J : P@H / USO -->
				{if $ship_method == "J"}
					{include file="include_template_P@H.tpl"}					
					<table  style="border:solid #cccccc 1px;margin:0 0 0 2px;" >
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}uso_logo.jpg" style="border:none;" title="Universal Orlando">
							</td>
						</tr>


						{if $flResident == 1}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								TICKET WINDOW PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and proceed to any front gate ticket window.  --- At the window, you must present a valid photo ID matching the guest's name listed on this confirmation page.  You will also be required to present the external ID# on this confirmation page and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change. <br/><br/>
&#183;FLORIDA RESIDENT TICKETS - An approved form of Florida photo ID (FL driver's license, FL state-issued ID card with FL address, FL voter's registration card with corresponding photo ID or FL college/university photo ID) matching the guest's name listed on this confirmation page is required for ticket purchase, ticket pickup, and usage.  <br/><br/>
&#183;DINING - If you purchased a dining product, a 36 hour advance reservation is required. Please call 407-224-7554 to reserve your seating.  
							</td>
						</tr>

						{else}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								WILL CALL KIOSK TICKET PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and look to your right for the Will Call Kiosks. -- At the kiosk machine, you must swipe a credit card matching the guest's name listed on this confirmation page for identity verification only. You will also be required to enter the external ID# on this confirmation and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Matching valid photo ID required. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change.  
							</td>
						</tr>

						{/if}

					</table>

				{/if}
				<!-- O : P@H / WDW -->
				{if $ship_method == "O"}
					{include file="include_template_P@H.tpl"}
					<table style="border:solid #cccccc 1px;margin:0 0 0 2px;">
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}wdw_logo.jpg" style="border:none;" title="Walt Disney World">
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
								This confirmation is not valid for admission.<br/>Your e-ticket will be emailed to you within 2 business days.
							</td>
						</tr>
					</table>
				{/if}
				<!-- Q : P@H / USO / WDW -->
				{if $ship_method == "Q"}
					{include file="include_template_P@H.tpl"}
					<table style="border:solid #cccccc 1px;margin:0 0 0 2px;">
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}wdw_logo.jpg" style="border:none;" title="Walt Disney World">
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
								This confirmation is not valid for admission.<br/>Your e-ticket will be emailed to you within 2 business days.
							</td>
						</tr>
					</table>
					<table  style="border:solid #cccccc 1px;margin:0 0 0 2px;" >
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}uso_logo.jpg" style="border:none;" title="Universal Orlando">
							</td>
						</tr>


						{if $flResident == 1}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								TICKET WINDOW PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and proceed to any front gate ticket window.  --- At the window, you must present a valid photo ID matching the guest's name listed on this confirmation page.  You will also be required to present the external ID# on this confirmation page and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change. <br/><br/>
&#183;FLORIDA RESIDENT TICKETS - An approved form of Florida photo ID (FL driver's license, FL state-issued ID card with FL address, FL voter's registration card with corresponding photo ID or FL college/university photo ID) matching the guest's name listed on this confirmation page is required for ticket purchase, ticket pickup, and usage.  <br/><br/>
&#183;DINING - If you purchased a dining product, a 36 hour advance reservation is required. Please call 407-224-7554 to reserve your seating.  
							</td>
						</tr>

						{else}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								WILL CALL KIOSK TICKET PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and look to your right for the Will Call Kiosks. -- At the kiosk machine, you must swipe a credit card matching the guest's name listed on this confirmation page for identity verification only. You will also be required to enter the external ID# on this confirmation and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Matching valid photo ID required. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change.  
							</td>
						</tr>

						{/if}

					</table>

				{/if}
				<!-- R : USO -->
				{if $ship_method == "R"}
					<table  style="border:solid #cccccc 1px;margin:0 0 0 2px;" >
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}uso_logo.jpg" style="border:none;" title="Universal Orlando">
							</td>
						</tr>


						{if $flResident == 1}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								TICKET WINDOW PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and proceed to any front gate ticket window.  --- At the window, you must present a valid photo ID matching the guest's name listed on this confirmation page.  You will also be required to present the external ID# on this confirmation page and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change. <br/><br/>
&#183;FLORIDA RESIDENT TICKETS - An approved form of Florida photo ID (FL driver's license, FL state-issued ID card with FL address, FL voter's registration card with corresponding photo ID or FL college/university photo ID) matching the guest's name listed on this confirmation page is required for ticket purchase, ticket pickup, and usage.  <br/><br/>
&#183;DINING - If you purchased a dining product, a 36 hour advance reservation is required. Please call 407-224-7554 to reserve your seating.  
							</td>
						</tr>

						{else}

						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								WILL CALL KIOSK TICKET PICKUP REQUIRED
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.8em;font-weight:;color:#008ad5;">
Walk over the bridge to either of the Universal Orlando theme parks and look to your right for the Will Call Kiosks. -- At the kiosk machine, you must swipe a credit card matching the guest's name listed on this confirmation page for identity verification only. You will also be required to enter the external ID# on this confirmation and the phone # supplied at the time of your purchase. Tickets do not include parking or admission to separately ticketed events or venues. Subject to capacity. Matching valid photo ID required. Universal Orlando is located at 6000 Universal Blvd, Orlando, FL 32819. Call 407-363-8000 for operating hours which are subject to change.  
							</td>
						</tr>

						{/if}

					</table>
				{/if}
				<!-- T : WDW -->
				{if $ship_method == "T"}
					<table style="border:solid #cccccc 1px;margin:0 0 0 2px;">
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
								<img src="{$images_folder}wdw_logo.jpg" style="border:none;" title="Walt Disney World">
							</td>
						</tr>
						<tr>
							<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
								This confirmation is not valid for admission.<br/>Your e-ticket will be emailed to you within 2 business days.
							</td>
						</tr>
					</table>
				{/if}



						<table width="560px">
							{if $include_template != ""}
								{include file="include_template_API.tpl"}
							{/if}
						</table>
						{assign var=oneChild value=0}
						{foreach from=$packages key=k item=item}
						{if $item.charac_birthday_child and $oneChild == 0}
						{math equation="x + y" x=$oneChild y=1 assign='oneChild'}	
						<table>
							<tr>
								<td width="565" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:center;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
												{$item.package_name}
											</td>
										</tr>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;">
												For <strong style="font-weight:bold;">{$item.charac_birthday_child}</strong>{if $item.charac_birthday_birth_date != "" && $show_birthday != "false"} ({$item.charac_birthday_sex}) born on {$item.charac_birthday_birth_date}{/if}
											</td>
										</tr>

									</table>
								</td>
							</tr>
						</table>
						{/if}
						{/foreach}

<!-- --------------------------------------------------------------  -->						

						{assign var=don value=0}
						{foreach from=$packages key=k item=item}
								{if $item.customer_type_name|strpos:"Donation" > 0}
									{math equation="x + y" x=$don y=1 assign='don'}
								{elseif $item.customer_type_name|strpos:"donation" > 0}
									{math equation="x + y" x=$don y=1 assign='don'}
								{/if}
						{/foreach}

						{if $don > 0}
							<table>
							<tr>
								<td width="565" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:center;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:left;font-size:.9em;font-weight:bold;">
												<IMG id=editableImg1 SRC="{$images_folder}thankyou.jpg" BORDER="0" alt="" align="center" width="560">

												Thank you for your generous contribution to the Zoo! Look for an official receipt to arrive via email. If you have any questions, please contact us at (614) 724-3498.
											</td>
										</tr>
										
									</table>
								</td>
							</tr>
							</table>				
						{/if}
<!-- --------------------------------------------------------------  -->
						
<!-- ORDER SUMMARY -->
						<table>
							<tr>
								<td width="572" style="text-align:center;">
									<span style="font-size:1.4em;font-weight:bold;color:#666666;font-family:arial;text-align:center;">
										Order Summary
									</span>
								</td>
							</tr>
						</table>
						<table width="572">
							<tr>
								<td width="560" bgcolor="#FFE3A9" style="border:solid #cccccc 1px;text-align:left;">
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="271" bgcolor="#FFE3A9" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Product Description</b>
											</td>
											<td width="60" bgcolor="#FFE3A9" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Type</b>
											</td>
											<td width="60" bgcolor="#FFE3A9" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>SKU</b>
											</td>
											<td width="75" bgcolor="#FFE3A9" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{if $redemption == "true"}<b>Reward Points</b>{else}<b>Unit Price</b>{/if}
											</td>
											<td width="30" bgcolor="#FFE3A9" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Qty</b>
											</td>
											<td width="64" bgcolor="#FFE3A9" style="text-align:center;border-bottom:solid #cccccc 1px;">
												{if $redemption == "true"}<b>Reward Points</b>{else}<b>Price</b>{/if}
											</td>
										</tr>
									</table>
						
			<!-- DYNAMIC PURCHASES LOOP -->
			
									{foreach from=$packages key=k item=item}
									{if $item.bundle_parent_ticket_id != ""}
									{if $item.value != "0.00" || $show_bundle_children == "true"}								
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="271" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if $item.prefix != ""}{$item.prefix} - {/if}{$item.package_name|utf}{if isset($item.charac_resource_id) and $show_resources != "false"} ({$item.charac_resource_id}) {/if} {$item.event_start_date} {$item.event_start_time} {if isset($item.charac_row_name)}<br >Row:{$item.charac_row_name} Seat:{$item.charac_seat_name} Section:{$item.charac_section_name}{/if}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.customer_type_name|utf}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.package_id}
											</td>
											<td width="75" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if  $item.value != "0.00"}${$item.retail}{else}Bundle{/if}
											</td>
											<td width="30" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.qty}
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 1px;">
												{if  $item.value != "0.00"}${$item.line_total_price|string_format:"%.2f"}{else}Bundle{/if}
											</td>
										</tr>
									</table>

									{/if}
									{else}
									{if $item.package_name != ""}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="271" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if $item.prefix != ""}{$item.prefix} - {/if}{$item.package_name|utf}{if isset($item.charac_resource_id) and $show_resources != "false"} ({$item.charac_resource_id}) {/if} {$item.event_start_date} {$item.event_start_time} {if isset($item.charac_row_name)}<br >Row:{$item.charac_row_name} Seat:{$item.charac_seat_name} Section:{$item.charac_section_name}{/if}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.ticket_id}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.package_id}
											</td>
											<td width="75" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if $redemption == "true"}{$item.retail*1}{else}${$item.retail}{/if}
											</td>
											<td width="30" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.qty}
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 1px;">
												{if $redemption == "true"}{$item.retail*1}{else}${$item.retail|string_format:"%.2f"}{/if} <!-- line_total_price -->
											</td>
										</tr>
									</table>
									{/if}

									{/if}
									{/foreach}
									<!-- END OF LOOP -->

							<!-- CHECK FOR REDEMPTIONS FIRST -->
							{if $redemption == "true"}


							{else}
							<!-- TAX (CHECKS FOR ADJUSTMENTS FIRST) -->
									{if $totals.total_adjustments == 0}
                                    				<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Tax:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.tax_total|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}

							<!-- FEES -->	
									{if count($refunds) == 0}	
									{foreach from=$fees key=k item=v}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												{$k}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$v|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/foreach} 
									{/if}
						
							<!-- ADJUSTMENTS -->
									{foreach from=$adjustments key=k item=v}
									{if $v.original == 1 and $v.value != "0.00"}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												{if $totals.total_adjustments != 0}Original {/if} {$v.cart_adjustment_name}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$v.value|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}	
									{/foreach}
							
									
								<!-- NO REFUNDS SHOW THIS -->
									<!-- REMOVING SUBTOTALS FOR NOW
									{if count($refunds) <= 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Subtotal:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$total_subtotal|currency|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}
									-->
										
									{if $totals.total_adjustments == 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												{$delivery_method}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.shipping_total|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}									
									
									{if $totals.total_adjustments != 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Original Additional Fees:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_fees|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Original {$delivery_method}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_shipping|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Original Tax:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_tax|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}
									
									{if $totals.total_adjustments != 0}
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
													Original Total:
												</td>
												<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
													{$totals.total_original|string_format:"%.2f"}
												</td>
											</tr>
										</table>
									{/if}
								{/if}<!-- THIS IS THE END OF THE REWARDS CHECK IF STATEMENT -->

			
					<!-- TOTALS -->
									{if count($refunds) == 0}
									<table  cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;">
												{if $redemption == "true"}<b>Total Reward Points:</b>{else}<b>Total:</b>{/if}
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;">
												{if $redemption == "true"}<b>{$totals.current_total*1}</b>{else}<b>${$totals.current_total|string_format:"%.2f"}</b>{/if}
											</td>
										</tr>
									</table>
									{/if}
									
					<!-- SCHEDULED PAYMENTS -->

			
									{if count($scheduled_payments) > 1}
									{if $showUpdateProfileButton == "true"}
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="797" style="text-align:center;">
													<p>
												 		You have chosen to defer your payment(s). A payment schedule is noted below. If you would like to update your credit card information, please click Update Profile.
													</p>
													<p>
														<a href="{if $title == "Palace" }{if $merchant_id == "8000" or $merchant_id == "8200" or $order.parent_econsignment_merchant_id == "8300"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=bigkahunas{/if}{if $merchant_id == "8001" or $merchant_id == "8201" or $order.parent_econsignment_merchant_id == "8301"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=castlepark{/if}{if $merchant_id == "8002" or $merchant_id == "8202" or $order.parent_econsignment_merchant_id == "8302"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=dutchwonderland{/if}{if $merchant_id == "8004" or $merchant_id == "8204" or $order.parent_econsignment_merchant_id == "8303"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=idlewild{/if}{if $merchant_id == "8005" or $merchant_id == "8205" or $order.parent_econsignment_merchant_id == "8304"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=kennywood{/if}{if $merchant_id == "8006" or $merchant_id == "8206" or $order.parent_econsignment_merchant_id == "8305"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=lakecompounce{/if}{if $merchant_id == "8007"}{/if}{if $merchant_id == "8008" or $merchant_id == "8208" or $order.parent_econsignment_merchant_id == "8306"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=rwsacramento{/if}{if $merchant_id == "8009" or $merchant_id == "8209" or $order.parent_econsignment_merchant_id == "8307"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=rwsandimas{/if}{if $merchant_id == "8018" or $merchant_id == "8218" or $order.parent_econsignment_merchant_id == "8318"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=noahsark{/if}{if $merchant_id == "8010" or $merchant_id == "8210" or $order.parent_econsignment_merchant_id == "8308"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=rwsanjose{/if}{if $merchant_id == "8011" or $merchant_id == "8211" or $order.parent_econsignment_merchant_id == "8309"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=sandcastle{/if}{if $merchant_id == "8012" or $merchant_id == "8212" or $order.parent_econsignment_merchant_id == "8310"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=silversprings{/if}{if $merchant_id == "8013" or $merchant_id == "8213" or $order.parent_econsignment_merchant_id == "8311"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=splishsplash{/if}{if $merchant_id == "8014" or $merchant_id == "8214" or $order.parent_econsignment_merchant_id == "8312"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=storyland{/if}{if $merchant_id == "8015" or $merchant_id == "8215" or $order.parent_econsignment_merchant_id == "8315"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=emeraldpointe{/if}{if $merchant_id == "8016" or $merchant_id == "8216" or $order.parent_econsignment_merchant_id == "8313"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=watercountry{/if}{if $merchant_id == "9003" or $merchant_id == "9203" or $order.parent_econsignment_merchant_id == "10003"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=waterworld{/if}{if $merchant_id == "8017" or $merchant_id == "8217" or $order.parent_econsignment_merchant_id == "8316"}https://shop.accesso.com/updateaccount/src/PaymentPortal.swf?c=wildwaters{/if}{/if}" target='_blank'><img src="{$images_folder}profileBttn2.jpg" alt="<Update Profile button!>" style="border:none;"></a>&nbsp;
													</p>

												</td>
											</tr>
									
										</table>
									{/if}
									
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="562" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												<b>Scheduled Payments</b>
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Payment</b>
											</td>
											<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Date</b>
											</td>
											<td width="100" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												<b>Amount</b>
											</td>
										</tr>
									</table>
									{foreach from=$scheduled_payments key=k item=item}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$k+1}
											</td>
											<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$item.date}
											</td>
											<td width="100" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												${$item.amount|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/foreach}
									{/if}


					<!-- YES REFUNDS SHOW THIS -->
									{if count($refunds) > 0}
										
										<table cellspacing="0" cellpadding="0">
											<tr height="10" bgcolor="#999999"><td height="10" bgcolor="#ffffff"></td></tr>
											<tr>
												<td width="568" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
													<b>Adjustments to Your Order</b>
												</td>
											</tr>
										</table>
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
													<b>Product Description</b>
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
													<b>Type</b>
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
													<b>SKU</b>
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;">
													<b>Unit Price</b>
												</td>
												
											</tr>
										</table>
						
										{foreach from=$refunds key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type == "T"}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{if $item.type == "A"}{$item.cart_adjustment_name}{else}{$item.package_name}{/if}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}

										{foreach from=$refunds key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type == "A" and $item.original == 0}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="298" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.cart_adjustment_name}
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}

										{foreach from=$refunds key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type == "S"}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$delivery_method}
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}


										{foreach from=$refund_fees key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type != "T" and $item.original == "0"}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{if $item.type == "S"}Delivery{elseif $item.type == "F" and $item.retail_amount > 0}Remaining Charge{elseif $item.type == "F" and $item.retail_amount < 0}Additional Fees{elseif $item.type == "A"}{$item.cart_adjustment_name}{else}{$item.package_name}{/if}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_amount|string_format:"%.2f"}&nbsp; 
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}
									{/if}
					<!-- end of refunds -->
					
					<!-- ADJUSTMENTS TOTAL -->
									{if $totals.total_adjustments != 0}
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="481" bgcolor="#999999" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
													Adjustments Total:
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
													{$totals.total_adjustments|string_format:"%.2f"}
												</td>
											</tr>
										</table>
									{/if}
								</td>
							</tr>
						</table>
            
            
            <!-- BOTTOM SECTION -->
						<table cellspacing="0" cellpadding="0" style="margin:10px 0 10px 0;">
							<tr>
								<td width="2"></td>
								<td width="175" bgcolor="#ffffff" style="text-align:left;border:solid #cccccc 1px;padding-left:5px;" valign="top">
									<b>Payment Received By:</b><br>
									{$customer.name|utf}<br>
									{$phone}<br>
									{$email}<br>
								</td>
								<td width="11"></td>
								
								 {if $shipping_address.street neq ''}
									<!-- EMPTY FOR NOW -->
								 {/if}
								<td width="175" bgcolor="#ffffff" style="text-align:left;border:solid #cccccc 1px;padding-left:5px;" valign="top">
									<b>Billing Information:</b><br>
									{$billing_address.street|utf} {$billing_address.co_dept}<br>
									{$billing_address.city|utf}, {$billing_address.state} {$billing_address.zip}<br>
									{$billing_address.country}<br>
								</td>
								<td width="11"></td>
								<td width="175" bgcolor="#ffffff" style="text-align:left;border:solid #cccccc 1px;padding-left:5px;" valign="top">
									<b>Payment Method:</b><br>
									{if $payment.billing_type_desc != ""}{if $payment.billing_type_desc == "American Express"}AMEX{else}{$payment.billing_type_desc}{/if} {$payment.card_number|replace:'X':''} @ ${$payment.amount|string_format:"%.2f"}{else}No Payments{/if}<br>
									{if $payment2.billing_type_desc != ""}{if $payment2.billing_type_desc == "American Express"}AMEX{else}{$payment2.billing_type_desc}{/if} {$payment2.card_number|replace:'X':''} @ ${$payment2.amount|string_format:"%.2f"}<br>{/if}
									{if $payment3.billing_type_desc != ""}{if $payment3.billing_type_desc == "American Express"}AMEX{else}{$payment3.billing_type_desc}{/if} {$payment3.card_number|replace:'X':''} @ ${$payment3.amount|string_format:"%.2f"}<br>{/if}
									{if $payment4.billing_type_desc != ""}{if $payment4.billing_type_desc == "American Express"}AMEX{else}{$payment4.billing_type_desc}{/if} {$payment4.card_number|replace:'X':''} @ ${$payment4.amount|string_format:"%.2f"}<br>{/if}
									{if $payment5.billing_type_desc != ""}{if $payment5.billing_type_desc == "American Express"}AMEX{else}{$payment5.billing_type_desc}{/if} {$payment5.card_number|replace:'X':''} @ ${$payment5.amount|string_format:"%.2f"}<br>{/if}
									{if $payment6.billing_type_desc != ""}{if $payment6.billing_type_desc == "American Express"}AMEX{else}{$payment6.billing_type_desc}{/if} {$payment6.card_number|replace:'X':''} @ ${$payment6.amount|string_format:"%.2f"}<br>{/if}
									{if $payment7.billing_type_desc != ""}{if $payment7.billing_type_desc == "American Express"}AMEX{else}{$payment7.billing_type_desc}{/if} {$payment7.card_number|replace:'X':''} @ ${$payment7.amount|string_format:"%.2f"}<br>{/if}
									{if $payment8.billing_type_desc != ""}{if $payment8.billing_type_desc == "American Express"}AMEX{else}{$payment8.billing_type_desc}{/if} {$payment8.card_number|replace:'X':''} @ ${$payment8.amount|string_format:"%.2f"}<br>{/if}
								</td>

							</tr>
						</table>            
						
						{if $show_order_history == "true"}
            					<table width="569" cellspacing="0" cellpadding="0" style="margin:0px 0 10px 2px; border:solid #ccc 1px; padding:10px;">
            						<tr>
              						<td style="font-weight:bold; padding-bottom:10px;" colspan="3">Order History</td>
            						</tr>
            						{foreach from=$past_orders key=k item=item}
            						<tr>
              						<td width="242">{$item.customer_name|utf}</td>
              						<td width="203"><div align="right"><span class="style2">{$item.order_date}</span></div></td>
              						<td width="205"><div align="right"><a href="{$receipt_path}{$receipt_name}?ph={$ph}&order_id={$item.order_id}&user_id={$user_id}&lang={$lang}">View Order #{$item.order_id}</a></div>
              					</td>
            						</tr>
            						{/foreach}
          					</table> 
						{/if}						
					</td> <!-- END OF COLUMN TD-->
				</tr> <!-- END OF COLUMN TR-->
			</table> <!-- END OF COLUMN TABLE-->
			
		
      
     
			
			<table width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
				<table width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>	
						<td width="600">
							<img src="{$images_folder}footer.jpg" width="600">
						</td>
					</tr>
				</table>
				{if $appendConfirmation != "" AND $appendConfirmation== "-AAA"}
				<table width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>	
						<td width="600" style="text-align:center;">
							<span style="font-size:.8em;color:#996600;line-height:200%;font-family:arial;text-decoration:none;">
								*AAA is a resale agent for attraction tickets only and not responsible for any part of the guest experience within the parks.
							</span>
						</td>
					</tr>
				</table>	
				{/if}
				<table width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td width="160" style="text-align:center;">&nbsp;
							
						</td>
						<td width="280" style="text-align:center;">
							{if $customer_support_number != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								{if $customer_support_number_title != ""}{$customer_support_number_title}{else}Technical Support{/if}: {$customer_support_number}
							</span><br>
							{/if}
							{if $customer_support_number_2 != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								{if $customer_support_number_title_2 != ""}{$customer_support_number_title_2}{else}Technical Support{/if}: {$customer_support_number_2}
							</span><br>
							{/if}

							{if $customer_support_link != "" and $customer_support_link_title != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								<a href="{$customer_support_link}" target="_blank">{$customer_support_link_title}</a>
							</span><br>
							{/if}
							{if $customer_support_link != "" and $customer_support_link_title == ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								<a href="{$customer_support_link}" target="_blank">Click here for Technical Support</a>
							</span><br>
							{/if}
							{if $customer_support_link == "" and $customer_support_link_title != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								<a href="
								{if $parent_econsignment_merchant_id == 4000}http://www.cocokeywaterresort.com/locations/rockford/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4001}http://www.cocokeywaterresort.com/locations/fitchburg/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4002}http://www.cocokeywaterresort.com/locations/newarkoh/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4003}http://www.cocokeywaterresort.com/locations/rockford/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4004}http://www.cocokeywaterresort.com/locations/omaha/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4005}http://www.cocokeywaterbury.com/files/629/WaterResortFAQs.pdf{/if}
								{if $parent_econsignment_merchant_id == 4006}http://www.cocokeywaterresort.com/locations/mountlaurel/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4007}http://www.cocokeywaterresort.com/locations/cincinnati/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4008}http://www.cocokeywaterresort.com/locations/rockford/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4009}http://www.cocokeyboston.com/our-hotel/hotel-fact-sheet/ {/if}
								{if $parent_econsignment_merchant_id == 4010}http://www.cocokeywaterresort.com/locations/kansascity/water-park/faq.aspx{/if}
								{if $parent_econsignment_merchant_id == 4011}http://www.cocokeywaterresort.com/locations/orlando/water-park/faq.aspx{/if}

								{if $merchant_id == 3002}http://www.cocokeywaterresort.com/locations/newarkoh/water-park/faq.aspx{/if}
								{if $merchant_id == 3003}http://www.cocokeywaterresort.com/locations/rockford/water-park/faq.aspx{/if}
								{if $merchant_id == 3005}http://www.cocokeywaterbury.com/files/629/WaterResortFAQs.pdf{/if}
								{if $merchant_id == 3009}http://www.cocokeyboston.com/our-hotel/hotel-fact-sheet/ {/if}
								{if $merchant_id == 3011}http://www.cocokeywaterresort.com/locations/orlando/water-park/faq.aspx{/if}
								" target="_blank">{$customer_support_link_title}</a>
							</span><br>
							{/if}

							{php}
								function curPageURL() {
 									$pageURL = 'http';
 									if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 									$pageURL .= "://";
 									if ($_SERVER["SERVER_PORT"] != "80") {
  										$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 									} else {
  										$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 									}
 									return $pageURL;
								}

							{/php}


							<!--span style="font-size:10px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;">
							Email not displaying correctly? <a title="Receipt Version {$version}" href="{$receipt_path}receipt.php?ph={$ph}&order_id={$order_id}&user_id={$user_id}&lang={$lang}{$print_at_home_vars}" target="_blank" style="font-size:10px;color:#996600;font-family:arial;text-decoration:underline;">
							View it in your browser.</a>
							</span-->
							
							<span style="font-size:.8em;color:#996600;line-height:200%;font-family:arial;text-decoration:none;">
		Email not displaying correctly? <a href="{php}echo curPageURL();{/php}" target="_blank" style="font-size:10px;color:#996600;line-height:200%;font-family:arial;text-decoration:underline;">View it in your browser.</a>
	</span>

							
						</td>
						<td width="160" style="text-align:center;">
							<a href="http://www.accesso.com" target='_blank'><img src="{$images_folder}accessoLogo.jpg" style="border:none;" height="45" title="Receipt Version {$version}"></a>
						</td>
						
					</tr>
				</table>
			</table>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
