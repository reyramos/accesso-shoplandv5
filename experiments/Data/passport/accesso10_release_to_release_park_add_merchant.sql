-- Assumption - Application ID will be 302.
-- only for accesso10:Clementon Park

-- Set these variables as necessary
SET @mer1 = '2200';
SET @mer2 = '2300';
SET @mer3 = '2400';   --  Also run as 2200, 2902...oh joy
SET @supportManagerPath       = 'https://shop.accesso.com/supportmanager/SupportManager.swf?c=nsh';
SET @supportManagerConfigPath = 'https://shop.accesso.com/supportmanager/configs/nsh.xml';
SET @tracker                  = "Clementon Park";
SET @park_name                = "Clementon Park";
SET @logo                     = "logo_clementon.jpg";

-- Merchant Characs
INSERT INTO merchant_characs VALUES(@mer1, 'logo_name', @logo) ON DUPLICATE KEY UPDATE VALUE = @logo;
INSERT INTO merchant_characs VALUES(@mer2, 'logo_name', @logo) ON DUPLICATE KEY UPDATE VALUE = @logo;
INSERT INTO merchant_characs VALUES(@mer3, 'logo_name', @logo) ON DUPLICATE KEY UPDATE VALUE = @logo;

-- Application 
-- INSERT INTO application VALUES ('302', 'Passport POS', 'Passport POS', 0, 1, 0, 0) ON DUPLICATE KEY UPDATE 
-- 	name='Passport POS',
-- 	description='Passport POS',
-- 	parent_application_id = 0,
-- 	app_is_a_parent = 1,
-- 	dynamic_config_flag = 0,
-- 	dynamic_locale_flag = 0
-- ;

-- Application Settings
-- INSERT IGNORE INTO application_settings (application_id, NAME, display_order, description) SELECT 302, 'cash_back_drawer_limit', 0, 'Dollar limit for cash back refund' ;
-- INSERT IGNORE INTO application_settings (application_id, NAME, display_order, description) SELECT 302, 'enable_accounts', 0, 'Enable Account Search' ;
-- INSERT IGNORE INTO application_settings (application_id, NAME, display_order, description) SELECT 302, 'enable_groups', 0, 'Enable Group Search' ;
-- INSERT IGNORE INTO application_settings (application_id, NAME, display_order, description) SELECT 302, 'enable_programs', 0, 'Enable Program Search' ;
-- INSERT IGNORE INTO application_settings (application_id, NAME, display_order, description) SELECT 302, 'enable_support_manager', 0, 'Enable Support Manager' ;

-- Application Navigation Tree  
 INSERT INTO application_navigation_tree VALUES ('302', 'en', @mer1, '<TREE/>') ON DUPLICATE KEY UPDATE tree='<TREE/>';
 INSERT INTO application_navigation_tree VALUES ('302', 'en', @mer2, '<TREE/>') ON DUPLICATE KEY UPDATE tree='<TREE/>';
 INSERT INTO application_navigation_tree VALUES ('302', 'en', @mer3, '<TREE/>') ON DUPLICATE KEY UPDATE tree='<TREE/>';

-- Application Config 
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'supportManagerPath',         @supportManagerPath)       ON DUPLICATE KEY UPDATE VALUE=@supportManagerPath;  
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'supportManagerConfigPath',   @supportManagerConfigPath) ON DUPLICATE KEY UPDATE VALUE=@supportManagerConfigPath;
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'tracker',                   '<tracker accessPointId="1" vendorId="1" apId1="1" apName1="@tracker"/>') ON DUPLICATE KEY UPDATE VALUE='<tracker accessPointId="1" vendorId="1" apId1="1" apName1="@tracker"/>';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'allowAccountReservations',  'false')                    ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'cashin',                    'off') ON DUPLICATE KEY UPDATE VALUE='off';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'forceCashIn',               'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'forceCashOut',              'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'adminFeeCustomerName',      'Standard') ON DUPLICATE KEY UPDATE VALUE='Standard';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'adminFeeCustomerType',      '7') ON DUPLICATE KEY UPDATE VALUE='7';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'adminFeePackageId',         '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'adminFeePackageName',       'Admin Fee') ON DUPLICATE KEY UPDATE VALUE='Admin Fee';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'postOrderCharacs',          'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'postOrderCharacsInterval',  '1') ON DUPLICATE KEY UPDATE VALUE='1';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'prorateCustomerName',       'Standard') ON DUPLICATE KEY UPDATE VALUE='Standard';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'prorateCustomerType',       '7') ON DUPLICATE KEY UPDATE VALUE='7';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'proratePackageId',          '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'proratePackageName',        'Prorated') ON DUPLICATE KEY UPDATE VALUE='Prorated';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'requireOfflineSupervisor',  'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'useAlvaradoCmd',            'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'validateTickets',           'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'hotkeys',                   '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'supervisorPassword',        'THEREWILLBECAKE') ON DUPLICATE KEY UPDATE VALUE='THEREWILLBECAKE';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'sendTicketsToBoca',         'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'cashOutPrint',              'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'useNCRDisplay',             'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'ncrDelay',                  '10') ON DUPLICATE KEY UPDATE VALUE='10';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'ncrMessage1',               'Welcome to|Clementon Park.') ON DUPLICATE KEY UPDATE VALUE='Welcome to|Clementon Park.';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'ncrMessage2',               'Ask me about|a Season Pass.') ON DUPLICATE KEY UPDATE VALUE='Ask me about|a Season Pass.';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'enableTracker',             'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'disableSettlementCustInfo', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'disableSettlementAdjustBal', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer1, 'extendedCashOut',            'false') ON DUPLICATE KEY UPDATE VALUE='false';

INSERT INTO application_config VALUES ('302', 'en', @mer2, 'supportManagerPath',         @supportManagerPath)       ON DUPLICATE KEY UPDATE VALUE=@supportManagerPath;  
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'supportManagerConfigPath',   @supportManagerConfigPath) ON DUPLICATE KEY UPDATE VALUE=@supportManagerConfigPath;
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'tracker',                   '<tracker accessPointId="1" vendorId="1" apId1="1" apName1="@tracker"/>') ON DUPLICATE KEY UPDATE VALUE='<tracker accessPointId="1" vendorId="1" apId1="1" apName1="@tracker"/>';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'allowAccountReservations',  'false')                    ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'cashin',                    'off') ON DUPLICATE KEY UPDATE VALUE='off';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'forceCashIn',               'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'forceCashOut',              'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'adminFeeCustomerName',      'Standard') ON DUPLICATE KEY UPDATE VALUE='Standard';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'adminFeeCustomerType',      '7') ON DUPLICATE KEY UPDATE VALUE='7';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'adminFeePackageId',         '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'adminFeePackageName',       'Admin Fee') ON DUPLICATE KEY UPDATE VALUE='Admin Fee';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'postOrderCharacs',          'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'postOrderCharacsInterval',  '1') ON DUPLICATE KEY UPDATE VALUE='1';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'prorateCustomerName',       'Standard') ON DUPLICATE KEY UPDATE VALUE='Standard';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'prorateCustomerType',       '7') ON DUPLICATE KEY UPDATE VALUE='7';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'proratePackageId',          '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'proratePackageName',        'Prorated') ON DUPLICATE KEY UPDATE VALUE='Prorated';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'requireOfflineSupervisor',  'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'useAlvaradoCmd',            'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'validateTickets',           'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'hotkeys',                   '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'supervisorPassword',        'THEREWILLBECAKE') ON DUPLICATE KEY UPDATE VALUE='THEREWILLBECAKE';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'sendTicketsToBoca',         'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'cashOutPrint',              'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'useNCRDisplay',             'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'ncrDelay',                  '10') ON DUPLICATE KEY UPDATE VALUE='10';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'ncrMessage1',               'Welcome to|Clementon Park.') ON DUPLICATE KEY UPDATE VALUE='Welcome to|Clementon Park.';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'ncrMessage2',               'Ask me about|a Season Pass.') ON DUPLICATE KEY UPDATE VALUE='Ask me about|a Season Pass.';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'enableTracker',             'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'disableSettlementCustInfo', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'disableSettlementAdjustBal', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer2, 'extendedCashOut',            'false') ON DUPLICATE KEY UPDATE VALUE='false';

INSERT INTO application_config VALUES ('302', 'en', @mer3, 'supportManagerPath',         @supportManagerPath)       ON DUPLICATE KEY UPDATE VALUE=@supportManagerPath;  
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'supportManagerConfigPath',   @supportManagerConfigPath) ON DUPLICATE KEY UPDATE VALUE=@supportManagerConfigPath;
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'tracker',                   '<tracker accessPointId="1" vendorId="1" apId1="1" apName1="@tracker"/>') ON DUPLICATE KEY UPDATE VALUE='<tracker accessPointId="1" vendorId="1" apId1="1" apName1="@tracker"/>';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'allowAccountReservations',  'false')                    ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'cashin',                    'off') ON DUPLICATE KEY UPDATE VALUE='off';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'forceCashIn',               'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'forceCashOut',              'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'adminFeeCustomerName',      'Standard') ON DUPLICATE KEY UPDATE VALUE='Standard';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'adminFeeCustomerType',      '7') ON DUPLICATE KEY UPDATE VALUE='7';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'adminFeePackageId',         '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'adminFeePackageName',       'Admin Fee') ON DUPLICATE KEY UPDATE VALUE='Admin Fee';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'postOrderCharacs',          'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'postOrderCharacsInterval',  '1') ON DUPLICATE KEY UPDATE VALUE='1';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'prorateCustomerName',       'Standard') ON DUPLICATE KEY UPDATE VALUE='Standard';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'prorateCustomerType',       '7') ON DUPLICATE KEY UPDATE VALUE='7';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'proratePackageId',          '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'proratePackageName',        'Prorated') ON DUPLICATE KEY UPDATE VALUE='Prorated';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'requireOfflineSupervisor',  'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'useAlvaradoCmd',            'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'validateTickets',           'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'hotkeys',                   '') ON DUPLICATE KEY UPDATE VALUE='';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'supervisorPassword',        'THEREWILLBECAKE') ON DUPLICATE KEY UPDATE VALUE='THEREWILLBECAKE';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'sendTicketsToBoca',         'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'cashOutPrint',              'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'useNCRDisplay',             'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'ncrDelay',                  '10') ON DUPLICATE KEY UPDATE VALUE='10';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'ncrMessage1',               'Welcome to|Clementon Park.') ON DUPLICATE KEY UPDATE VALUE='Welcome to|Clementon Park.';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'ncrMessage2',               'Ask me about|a Season Pass.') ON DUPLICATE KEY UPDATE VALUE='Ask me about|a Season Pass.';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'enableTracker',             'false') ON DUPLICATE KEY UPDATE VALUE='false';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'disableSettlementCustInfo', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'disableSettlementAdjustBal', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
INSERT INTO application_config VALUES ('302', 'en', @mer3, 'extendedCashOut',            'false') ON DUPLICATE KEY UPDATE VALUE='false';

-- Enable/Disable top menu control icons (Moved to application_setting)
-- 
-- INSERT INTO application_config VALUES(302, 'en', @mer1, 'enablePointOfSale', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer1, 'enableAccounts', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer1, 'enableGroups', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer1, 'enablePrograms', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer1, 'enableCommandTest', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer1, 'enableSupportManager', 'true') ON DUPLICATE KEY UPDATE value='true';
--
-- INSERT INTO application_config VALUES(302, 'en', @mer2, 'enablePointOfSale', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer2, 'enableAccounts', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer2, 'enableGroups', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer2, 'enablePrograms', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer2, 'enableCommandTest', 'true') ON DUPLICATE KEY UPDATE value='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer2, 'enableSupportManager', 'true') ON DUPLICATE KEY UPDATE value='true';

-- INSERT INTO application_config VALUES(302, 'en', @mer3, 'enablePointOfSale', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer3, 'enableAccounts', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer3, 'enableGroups', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer3, 'enablePrograms', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer3, 'enableCommandTest', 'true') ON DUPLICATE KEY UPDATE VALUE='true';
-- INSERT INTO application_config VALUES(302, 'en', @mer3, 'enableSupportManager', 'true') ON DUPLICATE KEY UPDATE VALUE='true';

-- user_application -- this one is good
-- INSERT IGNORE INTO user_application (user_id, application_id, state_id) SELECT user_id, '302', state_id FROM user_application WHERE application_id = '301';

-- user_merchant -- this one is good
INSERT IGNORE INTO user_merchant VALUES (1, @mer1);
INSERT IGNORE INTO user_merchant VALUES (5, @mer1);
INSERT IGNORE INTO user_merchant VALUES (1, @mer2);
INSERT IGNORE INTO user_merchant VALUES (5, @mer2);
INSERT IGNORE INTO user_merchant VALUES (1, @mer3);
INSERT IGNORE INTO user_merchant VALUES (5, @mer3);

-- session_totals_category_ref
-- INSERT IGNORE INTO session_totals_category_ref VALUES ('I', 'Initial') ON DUPLICATE KEY UPDATE description='Initial';
-- INSERT IGNORE INTO session_totals_category_ref VALUES ('R', 'Reported') ON DUPLICATE KEY UPDATE description='Reported';
-- INSERT IGNORE INTO session_totals_category_ref VALUES ('T', 'Transaction') ON DUPLICATE KEY UPDATE description='Transaction';

-- Must set the merchant_id to add additional billing types.
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Reservation', 'REZD', 'NA', '0', 'REZD', @mer1, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Issue Refund', 'IROD', 'NA', '0', 'IROD', @mer1, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Issue Refund', 'IRUD', 'NA', '0', 'IRUD', @mer1, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Apply Credit', 'AC', 'NA', '0', 'AC', @mer1, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Under Payment - Balanced Owed', 'UPIC', 'NA', '0', 'UPIC', @mer1, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- 
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Reservation', 'REZD', 'NA', '0', 'REZD', @mer2, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Issue Refund', 'IROD', 'NA', '0', 'IROD', @mer2, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Issue Refund', 'IRUD', 'NA', '0', 'IRUD', @mer2, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Apply Credit', 'AC', 'NA', '0', 'AC', @mer2, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Under Payment - Balanced Owed', 'UPIC', 'NA', '0', 'UPIC', @mer2, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- 
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Reservation', 'REZD', 'NA', '0', 'REZD', @mer3, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Issue Refund', 'IROD', 'NA', '0', 'IROD', @mer3, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Issue Refund', 'IRUD', 'NA', '0', 'IRUD', @mer3, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Apply Credit', 'AC', 'NA', '0', 'AC', @mer3, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;
-- INSERT IGNORE INTO billing_type (billing_type_id, billing_type_desc, billing_type, card_type_code, cePay_merchant_id, auth_type, merchant_id, display_order, category_id, search_password, visible) SELECT (SELECT MAX(billing_type_id)+1 FROM billing_type), 'Under Payment - Balanced Owed', 'UPIC', 'NA', '0', 'UPIC', @mer3, 127, 0, 'none', 1  FROM DUAL WHERE 0 = @bt1;

