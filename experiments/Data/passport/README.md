The passport database release_to_release scripts were stored in vault and haven't been switched over.
 
They were stored in the backend repository under a folder "New Express/DatabaseReleaseToRelease"
 
The principle files were
                release_to_release_schema.sql
                <park>_release_to_release.sql  (which had the add merchant details)
and - (although not checked in )
                brevard_release_to_release_park_add_merchant.sql
 
 
 
The schema is mostly database changes.  The key part of it is to CREATE TABLE IF NOT EXISTS …
On straight inserts, we did a lot more INSERT INTO <table> VALUES (…) ON DUPLICATE KEY UPDATE <field>=<value> (,…)