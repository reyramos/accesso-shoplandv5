START TRANSACTION;
INSERT INTO group_sales_category (id, NAME)  SELECT 100, NAME FROM group_sales_category WHERE id = 0;
-- UPDATE group_sales_category 
-- SET id = 100 WHERE id = 0;
UPDATE group_sales SET category_id ="100" WHERE category_id="0";
DELETE FROM group_sales_category WHERE id = "0";
ALTER TABLE group_sales_category
   CHANGE `id` `id` TINYINT(4) NOT NULL AUTO_INCREMENT;
COMMIT;

START TRANSACTION;
UPDATE dependent SET nickname = "" WHERE nickname = "undefined";
UPDATE dependent SET current_grade = "" WHERE current_grade = "undefined";
UPDATE dependent SET entering_grade = "" WHERE entering_grade = "undefined";
UPDATE dependent SET alt_contact_name = "" WHERE alt_contact_name = "undefined";
UPDATE dependent SET alt_contact_prim_phone = "" WHERE alt_contact_prim_phone = "undefined";
UPDATE dependent SET shirtsize = "" WHERE shirtsize = "undefined";
COMMIT;


-- ------------------------------------------------
-- -- ceBANK changes ------------------------------
-- ------------------------------------------------
-- ALTER TABLE account CHANGE `type` `type` VARCHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY(`account_id`, `type`); 
-- ALTER TABLE account ADD INDEX `ndx_account_id` (`account_id`); 
-- ------------------------------------------------

-- RESERVATIONS

START TRANSACTION;
ALTER TABLE merchant_package_ref ADD COLUMN reservable_flag TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0=Not reservable, 1=Reservable, 2=Reservation-only';
ALTER TABLE merchant_package_rate_ref ADD COLUMN rez_deposit DECIMAL(8,2) DEFAULT NULL COMMENT 'Used for reservations. Minimum deposit for a given rate.';
ALTER TABLE ticket_order_header ADD COLUMN rez_id INT(11) NULL COMMENT 'If ticket came from a reservation, this is the original reservation id';
ALTER TABLE cart_item ADD COLUMN rez_flag TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1=this ticket is being reserved, 0=this ticket is being purchased';
ALTER TABLE account_activity
	MODIFY COLUMN trans_ref_id INT(11) NULL COMMENT 'Order ID',
	ADD COLUMN rez_trans_ref_id INT(11) NULL COMMENT 'Reservation ID' AFTER trans_ref_id ;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_status (
  `status` INT(11) NOT NULL,
  `status_desc` VARCHAR(20) NOT NULL,
  `status_category_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`status`),
  KEY `fk_rez_ticket_status` (`status_category_id`),
  CONSTRAINT `fk_rez_ticket_status` FOREIGN KEY (`status_category_id`) REFERENCES `status_category` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
INSERT INTO rez_ticket_status VALUES(0, 'Reserved', 1);
INSERT INTO rez_ticket_status VALUES(1, 'NA', 0);
INSERT INTO rez_ticket_status VALUES(14, 'Testing value', 1);
INSERT INTO rez_ticket_status VALUES(2, 'Converted', 0);
INSERT INTO rez_ticket_status VALUES(3, 'Cancelled', 0);
INSERT INTO rez_ticket_status values(4, 'Refunded', 0);
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_order_status (
  `rez_order_status_id` INT(11) NOT NULL,
  `rez_order_status` VARCHAR(20) NOT NULL,
  `status_category_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`rez_order_status_id`),
  KEY `fk_rez_order_status` (`status_category_id`),
  CONSTRAINT `fk_rez_order_status` FOREIGN KEY (`status_category_id`) REFERENCES `status_category` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
INSERT INTO rez_order_status VALUES(0, 'Reserved', 1);
INSERT INTO rez_order_status VALUES(1, 'Converted', 0);
INSERT INTO rez_order_status VALUES(2, 'Cancelled', 0);
INSERT INTO rez_order_status VALUES(3, 'Refunded', 0);
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_order_header (
  `rez_id` INT(11) NOT NULL AUTO_INCREMENT,
  `customer_id` INT(11) NOT NULL,
  `status` INT(11) NOT NULL DEFAULT '0',
  `rez_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `conversion_date` TIMESTAMP NULL DEFAULT NULL,
  `conv_to_order_id` INT(11) DEFAULT NULL COMMENT 'converted to order_id',
  `merchant_id` INT(11) NOT NULL,
  `sales_machine_id` INT(11) NOT NULL,
  `sales_agent_id` INT(11) NOT NULL,
  `last_updated` TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:00',
  `source_id` VARCHAR(32) DEFAULT NULL,
  `local_rez_date` DATE DEFAULT NULL,
  `parent_order_id` INT(11) DEFAULT NULL,
  `software` VARCHAR(25) DEFAULT NULL,
  PRIMARY KEY (`rez_id`),
  KEY `fk_rez_ticket_order_header1` (`customer_id`),
  KEY `fk_rez_ticket_order_header2` (`status`),
  KEY `fk_rez_ticket_order_header3` (`sales_machine_id`),
  KEY `fk_rez_ticket_order_header4` (`sales_agent_id`),
  KEY `fk_rez_ticket_order_header5` (`parent_order_id`),
  KEY `ndx_rez_ticket_order_header1` (`rez_date`),
  KEY `ndx_rez_ticket_order_header2` (`merchant_id`),
  KEY `ndx_rez_ticket_order_header3` (`local_rez_date`),
  KEY `ndx_rez_conv_to_order_id` (`conv_to_order_id`),
  CONSTRAINT `fk_rez_ticket_order_header1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  CONSTRAINT `fk_rez_ticket_order_header2` FOREIGN KEY (`status`) REFERENCES `rez_ticket_status` (`status`),
  CONSTRAINT `fk_rez_ticket_order_header3` FOREIGN KEY (`sales_machine_id`) REFERENCES `machine` (`machine_id`),
  CONSTRAINT `fk_rez_ticket_order_header4` FOREIGN KEY (`sales_agent_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_rez_ticket_order_header5` FOREIGN KEY (`parent_order_id`) REFERENCES `ticket_order_header` (`master_ticket_id`)
) ENGINE=INNODB AUTO_INCREMENT=986 DEFAULT CHARSET=latin1 COMMENT='Relationship between tickets & reservations';
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_header (
  `ticket_id` INT(11) NOT NULL AUTO_INCREMENT,
  `status` INT(11) NOT NULL,
  `customer_type` INT(11) NOT NULL,
  `rez_id` INT(11) NOT NULL,
  `merchant_id` INT(11) NOT NULL,
  `package_id` INT(11) NOT NULL,
  `referrer` VARCHAR(64) DEFAULT NULL COMMENT 'An identifier used to track the source of the sale. Used for promo_codes, brokers, etc.',
  `retail_amount` DECIMAL(8,2) NOT NULL,
  `fee_amount` DECIMAL(8,2) NOT NULL,
  `tax_amount` DECIMAL(8,2) NOT NULL,
  `dependent_id` INT(11) DEFAULT NULL,
  `bundle_parent_ticket_id` INT(11) DEFAULT NULL,
  `rate_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`),
  KEY `fk_rez_ticket_header1` (`status`),
  KEY `fk_rez_ticket_header2` (`customer_type`),
  KEY `fk_rez_ticket_header3` (`rez_id`),
  KEY `fk_rez_ticket_header4` (`merchant_id`),
  KEY `fk_rez_ticket_header5` (`package_id`),
  KEY `fk_rez_ticket_header6` (`dependent_id`),
  KEY `ndx_rez_ticket_header1` (`referrer`(7)),
  CONSTRAINT `fk_rez_ticket_header1` FOREIGN KEY (`status`) REFERENCES `rez_ticket_status` (`status`),
  CONSTRAINT `fk_rez_ticket_header2` FOREIGN KEY (`customer_type`) REFERENCES `customer_type` (`customer_type`) ,
  CONSTRAINT `fk_rez_ticket_header3` FOREIGN KEY (`rez_id`) REFERENCES `rez_ticket_order_header` (`rez_id`) ,
  CONSTRAINT `fk_rez_ticket_header4` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`merchant_id`) ,
  CONSTRAINT `fk_rez_ticket_header5` FOREIGN KEY (`package_id`) REFERENCES `package_header` (`package_id`) ,
  CONSTRAINT `fk_rez_ticket_header6` FOREIGN KEY (`dependent_id`) REFERENCES `dependent` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1 COMMENT 'Table containing ticket information from reservations';
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_status_history (
  `ticket_id` INT(11) NOT NULL,
  `transaction_id` INT(11) NOT NULL,
  `status` INT(11) NOT NULL,
  `transaction_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `agent_id` INT(11) NOT NULL,
  PRIMARY KEY (`ticket_id`,`transaction_id`),
  KEY `fk_rez_ticket_status_history2` (`status`),
  CONSTRAINT `fk_rez_ticket_status_history1` FOREIGN KEY (`ticket_id`) REFERENCES `rez_ticket_header` (`ticket_id`) ,
  CONSTRAINT `fk_rez_ticket_status_history2` FOREIGN KEY (`status`) REFERENCES `rez_ticket_status` (`status`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_order_status_history (
  `rez_id` INT(11) NOT NULL,
  `transaction_id` INT(11) NOT NULL,
  `status` INT(11) NOT NULL,
  `transaction_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `agent_id` INT(11) NOT NULL,
  PRIMARY KEY (`rez_id`,`transaction_id`),
  KEY `fk_rez_order_status_history1` (`status`),
  KEY `fk_rez_order_status_history3` (`agent_id`),
  CONSTRAINT `fk_rez_order_status_history1` FOREIGN KEY (`status`) REFERENCES `rez_order_status` (`rez_order_status_id`) ,
  CONSTRAINT `fk_rez_order_status_history2` FOREIGN KEY (`rez_id`) REFERENCES `rez_ticket_order_header` (`rez_id`) ,
  CONSTRAINT `fk_rez_order_status_history3` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS  rez_ticket_billing  (
  `rez_id` INT(11) NOT NULL,
  `customer_id` INT(11) NOT NULL,
  `transaction_id` INT(11) NOT NULL,
  `billing_type_id` INT(11) NOT NULL,
  `account_info` VARCHAR(50) DEFAULT NULL,
  `card_expire_date` DATE DEFAULT NULL,
  `card_type_code` VARCHAR(3) DEFAULT NULL,
  `amount` DECIMAL(10,2) NOT NULL,
  `auth_type` VARCHAR(4) NOT NULL,
  `cepay_merchant_id` INT(11) DEFAULT NULL,
  `response_code` VARCHAR(2) DEFAULT NULL,
  `reference_number` VARCHAR(30) DEFAULT NULL,
  `db_cr_ind` CHAR(1) NOT NULL,
  `billing_name` VARCHAR(105) DEFAULT NULL,
  `transaction_date` DATE NOT NULL,
  `transaction_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `machine_id` INT(11) NOT NULL,
  `agent_id` INT(11) NOT NULL,
  `processed_date` DATE DEFAULT NULL,
  `ref_transaction_id` INT(11) DEFAULT NULL COMMENT 'For credit type transactions, this refers to the credited debit transaction',
  `settle_flag` TINYINT(4) NOT NULL DEFAULT '1' COMMENT 'Indicates payment is marked for settlement, defaults to 1',
  `session_key` INT(11) DEFAULT NULL,
  `comp_code` VARCHAR(32) DEFAULT NULL,
  `batch_id` DECIMAL(10,0) DEFAULT NULL,
  `signature_id` INT(11) DEFAULT NULL,
  `local_transaction_time` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`rez_id`,`customer_id`,`transaction_id`),
  KEY `fk_rez_ticket_billing1` (`rez_id`),
  KEY `fk_rez_ticket_billing2` (`billing_type_id`),
  KEY `ndx_rez_ticket_billing` (`local_transaction_time`),
  CONSTRAINT `fk_rez_ticket_billing1` FOREIGN KEY (`rez_id`) REFERENCES `rez_ticket_order_header` (`rez_id`) ,
  CONSTRAINT `fk_rez_ticket_billing2` FOREIGN KEY (`billing_type_id`) REFERENCES `billing_type` (`billing_type_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_group_sales_order_ref  (
  `group_sales_id` INT(11) NOT NULL,
  `rez_id` INT(11) NOT NULL,
  `mailer_code` VARCHAR(16) DEFAULT NULL,
  `mailer_receive_date` DATE DEFAULT NULL,
  `arrival_date` TIMESTAMP NULL DEFAULT NULL,
  `memo` VARCHAR(500) DEFAULT NULL,
 PRIMARY KEY (`group_sales_id`, `rez_id`),
 UNIQUE KEY `group_sales_rez_ref1` (`rez_id`),
 CONSTRAINT `fk_rez_group_sales_order_ref2` FOREIGN KEY (`group_sales_id`) REFERENCES `group_sales` (`id`),
 CONSTRAINT `fk_rez_group_sales_order_ref3` FOREIGN KEY (`rez_id`) REFERENCES `rez_ticket_order_header` (`rez_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_order_call_history (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `rez_id` INT(11) NOT NULL,
  `call_datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `agent_id` INT(11) NOT NULL,
  `call_type` VARCHAR(20) DEFAULT NULL,
  `call_comment` MEDIUMTEXT,
  PRIMARY KEY (`id`),
  KEY `fk_rez_call_history1` (`rez_id`),
  KEY `fk_rez_call_history2` (`agent_id`),
  CONSTRAINT `fk_rez_order_call_history1` FOREIGN KEY (`rez_id`) REFERENCES `rez_ticket_order_header` (`rez_id`) ,
  CONSTRAINT `fk_rez_order_call_history2` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_order_characs (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `value` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`,`name`),
  CONSTRAINT `fk_rez_order_characs1` FOREIGN KEY (`id`) REFERENCES `rez_ticket_order_header` (`rez_id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_order_items (
  `item_id` INT(11) NOT NULL AUTO_INCREMENT,
  `rez_id` INT(11) NOT NULL,
  `ticket_id` INT(11) DEFAULT NULL,
  `action` CHAR(1) NOT NULL COMMENT 'Action performed on this order: A=add, R=remove',
  `type` CHAR(1) NOT NULL COMMENT 'Type of charge: T=ticket, F=fee, S=shipping, A=adjustment',
  `ref_id` INT(11) DEFAULT NULL,
  `item_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `retail_amount` DECIMAL(8,2) NOT NULL,
  `tax_amount` DECIMAL(8,2) NOT NULL,
  `sales_agent_id` INT(11) NOT NULL,
  `refundable` TINYINT(4) NOT NULL,
  `original` TINYINT(4) NOT NULL DEFAULT '0',
  `sales_machine_id` INT(11) NOT NULL,
  `value` DECIMAL(8,2) DEFAULT NULL,
  `payment_index` TINYINT(4) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `fk_rez_order_items1` (`ticket_id`),
  KEY `fk_rez_order_items2` (`rez_id`),
  KEY `fk_rez_order_items3` (`sales_agent_id`),
  KEY `fk_rez_order_items4` (`sales_machine_id`),
  KEY `ndx_rez_items1` (`rez_id`),
  KEY `ndx_rez_items2` (`ref_id`),
  CONSTRAINT `fk_rez_order_items1` FOREIGN KEY (`ticket_id`) REFERENCES `rez_ticket_header` (`ticket_id`) ,
  CONSTRAINT `fk_rez_order_items2` FOREIGN KEY (`rez_id`) REFERENCES `rez_ticket_order_header` (`rez_id`) ,
  CONSTRAINT `fk_rez_order_items3` FOREIGN KEY (`sales_agent_id`) REFERENCES `users` (`id`) ,
  CONSTRAINT `fk_rez_order_items4` FOREIGN KEY (`sales_machine_id`) REFERENCES `machine` (`machine_id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_order_items_gl_codes (
  `item_id` INT(11) NOT NULL,
  `gl_code` VARCHAR(25) NOT NULL COMMENT 'Store GL code itself, not a key, so we have historical data in this table',
  `gl_code_description` VARCHAR(250) DEFAULT NULL,
  `type` CHAR(1) NOT NULL COMMENT 'F=fee, R=retail, S=Shipping, T=tax',
  `amount` DECIMAL(8,2) NOT NULL COMMENT 'This is the dollar amount allocated to this GL code, and it is calculated at purchase time.',
  `item_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'A order may have multiple entries corresponding to actions taken on different dates, such as purchase and refund.',
  KEY `fk_rez_items_gl_codes1` (`item_id`),
  CONSTRAINT `fk_rez_items_gl_codes1` FOREIGN KEY (`item_id`) REFERENCES `rez_order_items` (`item_id`) 
) COMMENT='GL codes for reservation items' ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_add_ons (
  `ticket_id` INT(11) NOT NULL,
  `add_on_ticket_id` INT(11) NOT NULL,
  PRIMARY KEY (`ticket_id`,`add_on_ticket_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_characs (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `value` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`,`name`),
CONSTRAINT `fk_rez_ticket_characs1` FOREIGN KEY (`id`) REFERENCES `rez_ticket_header` (`ticket_id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_event (
  `ticket_id` INT(11) NOT NULL,
  `event_id` INT(11) NOT NULL,
  `start_date` DATE DEFAULT NULL,
  `start_time` TIME DEFAULT NULL,
  `seat_id` INT(11) DEFAULT NULL,
  `section_id` INT(11) DEFAULT NULL,
  `resource_id` INT(11) DEFAULT NULL,
  `duration` INT(11) DEFAULT '0',
  PRIMARY KEY (`ticket_id`,`event_id`),
  KEY `fk_rez_ticket_event1` (`event_id`),
  KEY `ndx_rez_ticket_event1` (`start_date`),
  CONSTRAINT `fk_rez_ticket_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ,
  CONSTRAINT `fk_rez_ticket_event2` FOREIGN KEY (`ticket_id`) REFERENCES `rez_ticket_header` (`ticket_id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_fee (
  `ticket_id` INT(11) NOT NULL,
  `fee_type_id` INT(11) NOT NULL,
  `fee_amount` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`ticket_id`,`fee_type_id`),
  KEY `fk_rez_ticket_fee1` (`ticket_id`),
  KEY `fk_rez_ticket_fee2` (`fee_type_id`),
  CONSTRAINT `fk_rez_ticket_fee1` FOREIGN KEY (`ticket_id`) REFERENCES `rez_ticket_header` (`ticket_id`) ,
  CONSTRAINT `fk_rez_ticket_fee2` FOREIGN KEY (`fee_type_id`) REFERENCES `fee_type` (`fee_type_id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_gift_card (
  `ticket_id` INT(11) NOT NULL,
  `cebank_id` INT(11) NOT NULL,
  `credit_max` DECIMAL(8,2) NOT NULL,
  `spent` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`ticket_id`),
  CONSTRAINT `fk_rez_ticket_gift_card1` FOREIGN KEY (`ticket_id`) REFERENCES `rez_ticket_header` (`ticket_id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;	

START TRANSACTION;
CREATE TABLE IF NOT EXISTS rez_ticket_notes (
  `ticket_id` INT(11) NOT NULL,
  `note_id` INT(11) NOT NULL AUTO_INCREMENT,
  `agent_id` INT(11) NOT NULL,
  `note_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` VARCHAR(1024) NOT NULL COMMENT 'Notes',
  `printable_flag` TINYINT(4) NOT NULL DEFAULT '0' COMMENT 'Indicates if note is printed on the Ticket',
  PRIMARY KEY (`note_id`),
  UNIQUE KEY `unq_rez_ticket_notes` (`ticket_id`,`note_id`),
  CONSTRAINT `fk_rez_ticket_notes1` FOREIGN KEY (`ticket_id`) REFERENCES `rez_ticket_header` (`ticket_id`) 
) ENGINE=INNODB DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;
CREATE TABLE IF NOT EXISTS pos_print_format (
  
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  
  `print_movie` VARCHAR(30) DEFAULT NULL,
  
  `print_format_template` VARCHAR(3000) DEFAULT NULL,
  
  PRIMARY KEY (`id`)

) ENGINE=INNODB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
COMMIT;

START TRANSACTION;

SET @ppf1 = 0;

SELECT (@ppf1 := 1) FROM pos_print_format WHERE print_movie = 'receipt.xml';

INSERT INTO pos_print_format (print_movie, print_format_template) 

SELECT "receipt.xml", '<PRINT_FORMAT debug_single_page="1" debug_text="1" debug_tickets="0" name="BZ 8-up"> 	<PAPER height="800" left_margin="0" legacy="0" name="Receipt" ticket_cols="1" ticket_rows="1" top_margin="0" width="400" /> 	<FONTS> 		<FONT family="Arial" name="Arial6" size="6" /> 		<FONT family="Arial" name="Arial7" size="7" /> 		<FONT family="Arial" name="Arial7B" size="7" style="bold" /> 		<FONT family="Arial" name="Arial8B" size="8" style="bold" /> 		<FONT family="Arial" name="Arial8" size="8" /> 		<FONT default="1" family="Arial" name="Arial10" size="10" /> 		<FONT family="Arial" name="Arial12B" size="12" style="bold" /> 	</FONTS> 	<IMAGE content="logo" height="160" region="header" width="320" x="30" y-offset="20" /> 	<TEXT content="Receipt" font="Arial8B" region="header" x="130" y-offset="10" /> 	<TEXT content=" [%/STATUS_REPLY/@order_date]" font="Arial7" region="header" x="100" y-offset="20" /> 	 	<TEXT content="CONFIRMATION # " font="Arial8B" region="order" x="50" y-offset="20" /> 	<TEXT content="[./ORDER_STATUS_REPLY/@order_id]" font="Arial8" region="order" x="175" y-offset="0" /> 	 	<TEXT content="RESERVATION # " font="Arial8B" region="rez" x="50" y-offset="20" /> 	<TEXT content="[./REZ_STATUS_REPLY/@rez_id]" font="Arial8" region="rez" x="175" y-offset="0" /> 	 	 	<TEXT content="[package_name]" font="Arial7B" region="ticket" x="0" y-offset="25" /> 	<TEXT content="[customer_type_name]" font="Arial7" region="ticket" x="0" y-offset="15" /> 	<TEXT content="$[retail]" font="Arial7" region="ticket" x="245" y-offset="0" /> 	 	 	<TEXT content="Subtotal " font="Arial7B" region="footer" x="200" y-offset="20" /> 	<TEXT content="$[%/ITEMS_REPLY/TOTALS/@sub_total]" font="Arial7" region="footer" x="245" y-offset="0" /> 	<TEXT content="Total Tax" font="Arial7B" region="footer" x="200" y-offset="20" /> 	<TEXT content="$[%/ITEMS_REPLY/TOTALS/@tax_total]" font="Arial7" region="footer" x="245" y-offset="0" /> 	<TEXT content="Total Tickets     [./@count]" font="Arial7B" region="footer" x="0" y-offset="20" />	 	<TEXT content="Total " font="Arial7B" region="footer" x="200" y-offset="0" /> 	<TEXT content="$[%/ITEMS_REPLY/TOTALS/@current_total]" font="Arial7" region="footer" x="245" y-offset="0" /> 	<TEXT content="Payments" font="Arial7B" region="footer" x="0" y-offset="20" /> 	 	 	<TEXT content="[+billing_type_desc]" font="Arial7" region="payment" x="10" y-offset="25" /> 	<TEXT content="$[+amount]" font="Arial7B" region="payment" x="245" y-offset="0" /> 	<TEXT content="[+transaction_date]" font="Arial7" region="payment" x="10" y-offset="15" /> 	 	<TEXT content="Thank you" font="Arial7B" region="footer2" x="130" y-offset="40" /> </PRINT_FORMAT>'

FROM DUAL WHERE 0 = @ppf1;



SET @ppf2 = 0;

SELECT (@ppf2 := 1) FROM pos_print_format WHERE print_movie = 'ccd_receipt.xml';

INSERT INTO pos_print_format (print_movie, print_format_template) 

SELECT "ccd_receipt.xml", '<PRINT_FORMAT debug_single_page="1" debug_text="1" debug_tickets="0" name="BZ 8-up"> 	<PAPER height="800" left_margin="0" legacy="0" name="Receipt" ticket_cols="1" ticket_rows="1" top_margin="0" width="400" /> 	<FONTS> 		<FONT family="Arial" name="Arial6" size="6" /> 		<FONT family="Arial" name="Arial7" size="7" /> 		<FONT family="Arial" name="Arial7B" size="7" style="bold" /> 		<FONT family="Arial" name="Arial8B" size="8" style="bold" /> 		<FONT family="Arial" name="Arial8" size="8" /> 		<FONT default="1" family="Arial" name="Arial10" size="10" /> 		<FONT family="Arial" name="Arial12B" size="12" style="bold" /> 	</FONTS> 	<IMAGE content="logo" height="160" region="header" width="320" x="30" y-offset="20" /> 	<TEXT content="Payment Receipt" font="Arial8B" region="header" x="100" y-offset="10" /> 	<TEXT content=" [%/STATUS_REPLY/@order_date]" font="Arial7" region="header" x="100" y-offset="20" /> 	 	<TEXT content="CONFIRMATION # " font="Arial8B" region="order" x="50" y-offset="20" /> 	<TEXT content="[./ORDER_STATUS_REPLY/@order_id]" font="Arial8" region="order" x="175" y-offset="0" /> 	 	<TEXT content="RESERVATION # " font="Arial8B" region="rez" x="50" y-offset="20" /> 	<TEXT content="[./REZ_STATUS_REPLY/@rez_id]" font="Arial8" region="rez" x="175" y-offset="0" /> 	 	<TEXT content="[+billing_type_desc]" font="Arial7" region="payment" x="10" y-offset="20" /> 	<TEXT content="[+card_number]" font="Arial7" region="payment" x="40" y-offset="0" /> 	<TEXT content="$[+amount]" font="Arial7B" region="payment" x="245" y-offset="0" /> </PRINT_FORMAT>'

FROM DUAL WHERE 0 = @ppf2;



SET @ppf3 = 0;

SELECT (@ppf3 := 1) FROM pos_print_format WHERE print_movie = 'ticket.xml';

INSERT INTO pos_print_format (print_movie, print_format_template) 

SELECT "ticket.xml", '<PRINT_FORMAT debug_single_page="0" debug_text="0" debug_tickets="0" name="BZ 8-up">   <PAPER height="800" left_margin="0" legacy="0" name="Rodrigo Format" ticket_cols="1" ticket_rows="1" top_margin="0" width="400" />   <FONTS>     <FONT family="Arial" name="Arial6" size="6" />     <FONT family="Arial" name="Arial7" size="7" />     <FONT family="Arial" name="Arial7B" size="7" style="bold" />     <FONT family="Arial" name="Arial8B" size="8" style="bold" />     <FONT default="1" family="Arial" name="Arial10" size="10" />     <FONT family="Arial" name="Arial12B" size="12" style="bold" />   </FONTS>   <IMAGE content="logo" height="160" width="320" x="100" y-offset="20" />   <BARCODE content="[barcode]" height="60" width="280" x="0" y-offset="10" />   <DYNATEXT align="center" content="This is your ticket!" font="Arial10" width="280" x="0" y-offset="10" />   <DYNATEXT align="center" content=" [package_name]" font="Arial10" width="280" x="0" y-offset="10" />   <TEXT content=" TYPE " font="Arial7B" x="0" y-offset="10" />   <TEXT content=" ORDER DATE " font="Arial7B" x="215" y-offset="0" />   <TEXT content=" [customer_type_name] " font="Arial7" x="0" y-offset="10" />   <TEXT content=" [./ORDER_STATUS_REPLY/@order_date]" font="Arial7" x="180" y-offset="0" />   <TEXT content=" TICKET " font="Arial7B" x="0" y-offset="20" />   <TEXT content=" PRICE " font="Arial7B" x="250" y-offset="0" />   <TEXT content=" [barcode]" font="Arial7" x="0" y-offset="10" />   <TEXT content=" $[retail]" font="Arial7" x="250" y-offset="0" />   <TEXT content=" CONFIRMATION # " font="Arial7B" x="20" y-offset="20" />   <TEXT content=" [order_id]" font="Arial7B" x="125" y-offset="0" />   <DYNATEXT align="center" content=" Save Time and Money. " font="Arial7B" width="280" x="0" y-offset="10" />   <DYNATEXT align="center" content=" NON REFUNDABLE - NOT FOR RESALE " font="Arial7B" width="280" x="0" y-offset="10" />   <DYNATEXT align="center" content=" Thank you, have a great time!" font="Arial7" width="280" x="0" y-offset="20" /> </PRINT_FORMAT>' 

FROM DUAL WHERE 0 = @ppf3;

COMMIT;










