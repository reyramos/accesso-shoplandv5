###################################################################################
# 
# MERCHANT CONFIGURATION FOR V5
# 
###################################################################################
SET @application_id = '1500';
SET @merchant_id = '8209';
SET @language = 'en';
SET @image_folder = 'accesso18';
SET @island = 'accesso18';
SET @client = 'Palace Entertainment';
SET @merchant_code = 'PE-RWSD';
SET @merchant_name = 'Raging Waters <br> San Dimas, CA';
SET @passbook_symlink = 'accesso18';
SET @security_provider = 'Trustwave';
SET @support_phone_number = '407-956-3528';

###################################################################################
# CHANGE application_config
###################################################################################

DELETE FROM `application_config` WHERE `application_id`=@application_id AND `language`=@language AND `merchant_id`=@merchant_id AND `name`='display_merchant_logo';

UPDATE `application_config` SET `value`='true' WHERE `application_id`=@application_id and `language`=@language and `merchant_id`=@merchant_id and `name`='display_agreement';
UPDATE `application_config` SET `value`='' WHERE `application_id`=@application_id and `language`=@language and `merchant_id`=@merchant_id and `name`='merchant_logo';
UPDATE `application_config` SET `value`=@merchant_name WHERE `application_id`=@application_id and `language`=@language and `merchant_id`=@merchant_id and `name`='merchant_name';
UPDATE `application_config` SET `value`=REPLACE('{"production":"https://passbook.accesso.com/@passbook_symlink","staging":"https://stg-passbook.accesso.com/@passbook_symlink","development":"https://passbook.ceiris.com/@passbook_symlink"}','@passbook_symlink',@passbook_symlink) WHERE `application_id`=@application_id and `language`=@language and `merchant_id`=@merchant_id and `name`='passbook_path';

INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'use_geolocation','false');

###################################################################################
# CHANGE application_locale
###################################################################################

UPDATE `application_locale` SET `value`='You have chosen to purchase an Advance Purchase Ticket. The barcode will be valid from @@date. Please be sure to review the park\'s hours of operations. Would you like to proceed with this ticket?' WHERE `application_id`=@application_id and `language`=@language and `section`='Alert' and `target`='advancePackageMsg';
UPDATE `application_locale` SET `value`='You\'re missing the billing information required to process your order. Please enter required data.' WHERE `application_id`=@application_id and `language`=@language and `section`='Alert' and `target`='billingInfoNotSetMsg';
UPDATE `application_locale` SET `value`='There was a problem loading the help and FAQ\'s. Please try again.' WHERE `application_id`=@application_id and `language`=@language and `section`='Alert' and `target`='loadFAQErrorMsg';
UPDATE `application_locale` SET `value`='You\'re missing the payment information required to process your order. Please enter required data.' WHERE `application_id`=@application_id and `language`=@language and `section`='Alert' and `target`='paymentInfoNotSetMsg';
UPDATE `application_locale` SET `value`='You MUST have a valid ID with voucher for entry. BEWARE of fraud: Vouchers sold by third parties are NOT valid and will not be accepted. Do not buy from ticket "scalpers."' WHERE `application_id`=@application_id and `language`=@language and `section`='CartView' and `target`='agreement';
UPDATE `application_locale` SET `value`='Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again. If you continue to have issues, please contact Online Ticketing Technical Support at @@supportPhoneNumber and inform the Agent that the Package can\'t be found.' WHERE `application_id`=@application_id and `language`=@language and `section`='CheckCartTicketLevelErrors' and `target`='451';
UPDATE `application_locale` SET `value`='Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again. If you continue to have issues, please contact Online Ticketing Technical Support at @@supportPhoneNumber and inform the Agent that the Customer Type can\'t be found.' WHERE `application_id`=@application_id and `language`=@language and `section`='CheckCartTicketLevelErrors' and `target`='452';
UPDATE `application_locale` SET `value`='We\'re sorry, there was an issue with the quantity of items in your cart. Please check your cart to verify there are not duplicate items or a quantity that exceeds the maximum items allowed per order. Please contact our Online Technical Support at @@supportPhoneNumber.' WHERE `application_id`=@application_id and `language`=@language and `section`='CheckCartTicketLevelErrors' and `target`='465';
UPDATE `application_locale` SET `value`='We\'re sorry, there was a problem adding an item to your cart. You have entered an invalid quantity. Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again.' WHERE `application_id`=@application_id and `language`=@language and `section`='CheckCartTicketLevelErrors' and `target`='476';
UPDATE `application_locale` SET `value`='This group package has a minimum quantity of <b>@@minimum</b>, but you\'ve only selected <b>@@found</b>. <br/><br/><b>Adult and Jr/Sr tickets can be combined to reach the minimum quantity of @@minimum. </b><br/><br/> Please add additional tickets to complete your order.' WHERE `application_id`=@application_id and `language`=@language and `section`='CheckCartTicketLevelErrors' and `target`='484';
UPDATE `application_locale` SET `value`='We\'re sorry, there was a problem adding an item to your cart. Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again.' WHERE `application_id`=@application_id and `language`=@language and `section`='CheckCartTicketLevelErrors' and `target`='defaultError';
UPDATE `application_locale` SET `value`='Sorry, your browser is unsupported. We recommend one of the following:' WHERE `application_id`=@application_id and `language`=@language and `section`='ErrorCodes' and `target`='1000';
UPDATE `application_locale` SET `value`='See you at the park!' WHERE `application_id`=@application_id and `language`=@language and `section`='ReceiptView' and `target`='receiptMessage';
UPDATE `application_locale` SET `value`='As our valued customer, the safety and security of your information is of the utmost importance to us.<br /><br /> The security of the online store, operated by <i>accesso</i>, is verified by Verisign. Be assured that all personal and financial data submitted in our store is protected by industry-standard 128-bit SSL encryption, and transmitted securely to servers which adhere to the highest data security standard, as set forth by the credit card industry and verified by independent auditors.' WHERE `application_id`=@application_id and `language`=@language and `section`='Verisign' and `target`='message';

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert','agreeToTermsMsg','Please agree to terms by checking the box below.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert','agreeToTermsTitle','Terms and Conditions');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert','connectionLost','Your connection was lost.\\nAttempting to reconnect...');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels','copyright', '');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels','gettingEventDates', 'Getting Event Dates');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels','details','Details');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels','submitButton','Submit');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView','date','Date:');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView','dateTime','Date\\Time:');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView','time','Time:');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'DateTime','dateOnlyHeading','Select Date Below');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'DateTime','dateTimeHeading','Select Date and Time');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'DateTime','noDatesAvailable','No dates available for selected month.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'DateTime','timeLimitMessage','Select Date Below');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CheckCartTicketLevelErrors','1','There was a problem reserving the selected date. Please try again. If you continue to experience problems, please contact our support line at @@supportPhoneNumber.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CrossSells','heading','Some other items that might interest you...');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'FormLabels','at','at');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'FormLabels','selectTime','Select Time');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'FormValidation','nameRequiredError','Name must be at least 1 character.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'KnowledgeBase','filterInputPlaceHolder','Filter Knowledgebase');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'KnowledgeBase','noKnowledgeBaseArticles','No Results Found');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'TicketView','customerFirstName','First Name');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'TicketView','customerLastName','Last Name');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'TicketView','ticketNameCopy','Copy Last Name');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'TicketView','ticketNameDescription','Please enter a ticket holder name for each of your purchased tickets below.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'TicketView','ticketNameHeading','Ticket Holder Names');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'TicketView','ticketNameSubmit','Register Ticket');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'TicketView','ticketNameTitle','Tickets Must Have Names to be Valid');