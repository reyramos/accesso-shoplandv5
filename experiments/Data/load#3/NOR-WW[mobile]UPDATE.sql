###################################################################################
# 
# MERCHANT CONFIGURATION FOR V5
# 
###################################################################################
SET @application_id = '1500';
SET @merchant_id = '405';
SET @language = 'en';
SET @image_folder = 'accesso17';
SET @island = 'accesso17';
SET @client = 'Norpoint';
SET @merchant_code = 'NOR-WW';
SET @merchant_name = 'Wild Waves';
SET @passbook_symlink = 'accesso17';
SET @security_provider = 'Trustwave';
SET @support_phone_number = '407-956-3549';

###################################################################################
# application_config
###################################################################################

UPDATE `application_config` SET `value`='{"production":"74","staging":"74","development":"74"}' WHERE `application_id`=@application_id and `language`=@language and `merchant_id`=@merchant_id and `name`='knowledgebase_id';