 
SET @application_id = '1500';
SET @language = 'en';
 
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'loadReceiptErrorTitle', 'Failed to Get Receipt Details');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'loadReceiptErrorMsg', 'There was a problem retrieving your receipts. Please try again.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'missingPromoErrorTitle', 'Missing Promotional Code');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'missingPromoErrorMsg', 'Please enter a promotional code before proceeding.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'reloadTickets', 'Reload Tickets');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'reloadReceipt', 'Reload Receipt');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView', 'orderReviewHeading', 'Order Review');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView', 'receiptViewHeading', 'Your Receipt');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ReceiptView', 'problemLoadingReceipt', 'There was a problem loading your receipts.');