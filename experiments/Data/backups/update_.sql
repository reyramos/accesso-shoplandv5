SET @application_id = '1500';
SET @merchant_id = '';
SET @language = 'en';
SET @image_folder = '';
SET @island = '';

#IMAGE PATH
UPDATE `application_config` SET `value`=REPLACE('{"production":"https://assets.accesso.com/@image_folder/images/","staging":"https://assets.accesso.com/@image_folder/images/","development":"https://assets.accesso.com/@image_folder/images/"}','@image_folder',@image_folder) WHERE `application_id`=@application_id AND `language`=@language AND `merchant_id`=@merchant_id AND `name`='assets_path';


#KNOWLEDGE BASE
UPDATE `application_config` SET `value`='{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase"}' WHERE `application_id`=@application_id AND `language`=@language AND `merchant_id`=@merchant_id AND `name`='knowledgebase_gateway';