#SET @application_id = '1500';
#SET @merchant_id = '';
#SET @language = 'en';
#SET @image_folder = 'shared';
#SET @island = 'SharedV3';
#SET @passbook_symlink = 'shared';


#IMAGE PATH
UPDATE `application_config` SET `value`=REPLACE('{"production":"https://assets.accesso.com/@image_folder/images/","staging":"https://assets.accesso.com/@image_folder/images/","development":"https://assets.accesso.com/@image_folder/images/"}','@image_folder',@image_folder) WHERE `application_id`=@application_id AND `language`=@language AND `merchant_id`=@merchant_id AND `name`='assets_path';


#KNOWLEDGE BASE
UPDATE `application_config` SET `value`='{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase"}' WHERE `application_id`=@application_id AND `language`=@language AND `merchant_id`=@merchant_id AND `name`='knowledgebase_gateway';


#UPDATE `application_config` SET `value`=REPLACE('{"production":"https://passbook.accesso.com/@passbook_symlink","staging":"https://passbook.accesso.com/@passbook_symlink","development":"https://passbook.accesso.com/@passbook_symlink"}','@passbook_symlink',@passbook_symlink) WHERE `application_id`=@application_id AND `language`=@language AND `merchant_id`=@merchant_id AND `name`='passbook_path';


#UPDATE `application_config` SET `value`=REPLACE('{"production":"https://gateway.accesso.com/@island","staging":"https://gateway.accesso.com/@island","development":"https://gateway.accesso.com/@island"}','@island',@island) WHERE `application_id`=@application_id AND `language`=@language AND `merchant_id`=@merchant_id AND `name`='gateway';