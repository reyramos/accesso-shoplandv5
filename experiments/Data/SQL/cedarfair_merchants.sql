/*
-- Query: SELECT * FROM application_config WHERE application_id=1500 ORDER BY merchant_id, name
*/
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'merchant','CF-CA');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'merchant_logo','carowinds_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'merchant_name','Carowinds');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1601,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'default_country','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'knowledgebase_id','{"production":"50,63","staging":"50,63","development":"50,63"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'merchant','CF-CW');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'merchant_logo','canadas_wonderland_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'merchant_name','Canada''s Wonderland');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1602,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'merchant','CF-GA');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'merchant_logo','great_america_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'merchant_name','Califorina''s <br> Great America');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1603,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'merchant','CF-KD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'merchant_logo','kings_dominion_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'merchant_name','Kings Dominion');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1604,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'merchant','CF-KI');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'merchant_logo','kings_island_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'merchant_name','Kings Island');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1605,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'merchant','CF-CP');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'merchant_logo','cedar_point_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'merchant_name','Cedar Point');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1609,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'merchant','CF-KBF');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'merchant_logo','knotts_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'merchant_name','Knott''s Berry Farm');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1610,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'merchant','CF-VF');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'merchant_logo','valleyfair_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'merchant_name','Valleyfair');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1611,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'merchant','CF-DP');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'merchant_logo','dorney_park_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'merchant_name','Dorney Park <br> & Wildwater Kingdom');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1612,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'merchant','CF-WF');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'merchant_logo','worlds_of_fun_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'merchant_name','Worlds of Fun / <br> Oceans of Fun');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1613,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'assets_path','{"production":"https://assets.accesso.com/cedarfair/images/","staging":"https://stg-assets.accesso.com/cedarfair/images/","development":"https://assets.ceiris.com/cedarfair/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'client','Cedar Fair Entertainment Company');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-cedarfair","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-cedarfair","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'knowledgebase_id','{"production":"50,62","staging":"50,62","development":"50,62"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'merchant','CF-MA');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'merchant_logo','mi_adventure_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'merchant_name','Michigan''s Adventure');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'passbook_path','{"production":"https://passbook.accesso.com/cedarfair","staging":"https://stg-passbook.accesso.com/cedarfair","development":"https://passbook.ceiris.com/cedarfair"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'support_phone_number','407.261.4291');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1615,'use_geolocation','false');
/*
-- Query: SELECT * FROM application_navigation_tree WHERE application_id=1500 ORDER BY merchant_id
*/
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1601,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1602,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1603,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1604,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1605,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1609,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1610,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1611,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1612,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1613,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1615,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Fast Lane" label="Fast Lane" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Dinosaurs Alive" label="Dinosaurs Alive" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Haunt" label="Haunt" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="8" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="9" isactive="true" keywords="Dinosaurs" label="Dinosaurs" sortedordernumber="9" target="_blank"></MENUITEM></TREE>');
