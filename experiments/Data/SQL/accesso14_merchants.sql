/*
-- Query: SELECT * FROM application_config WHERE application_id=1500 ORDER BY merchant_id, name
*/
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'analytics','{"production":["MO-3967898-22"],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'assets_path','{"production":"https://assets.accesso.com/accesso14/images/","staging":"https://stg-assets.accesso.com/accesso14/images/","development":"https://assets.ceiris.com/accesso14/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'client','Herschend Family Entertainment');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-accesso14","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-accesso14","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'knowledgebase_id','{"production":"50,56","staging":"50,56","development":"50,56"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'merchant','HFE-AA');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'merchant_logo','HFE_AAQ_Logo_blue_150x30.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'merchant_name','Adventure Aquarium');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'passbook_path','{"production":"https://passbook.accesso.com/accesso14","staging":"https://stg-passbook.accesso.com/accesso14","development":"https://passbook.ceiris.com/accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'support_phone_number','407.261.4284');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1001,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'analytics','{"production":["MO-15959957-1"],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'assets_path','{"production":"https://assets.accesso.com/accesso14/images/","staging":"https://stg-assets.accesso.com/accesso14/images/","development":"https://assets.ceiris.com/accesso14/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'client','Herschend Family Entertainment');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-accesso14","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-accesso14","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'knowledgebase_id','{"production":"50,56","staging":"50,56","development":"50,56"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'merchant','HFE-SDC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'merchant_name','Silver Dollar City');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'passbook_path','{"production":"https://passbook.accesso.com/accesso14","staging":"https://stg-passbook.accesso.com/accesso14","development":"https://passbook.ceiris.com/accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'support_phone_number','407.261.4284');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1002,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'analytics','{"production":["MO-7774948-1"],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'assets_path','{"production":"https://assets.accesso.com/accesso14/images/","staging":"https://stg-assets.accesso.com/accesso14/images/","development":"https://assets.ceiris.com/accesso14/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'client','Herschend Family Entertainment');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-accesso14","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-accesso14","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'knowledgebase_id','{"production":"50,56","staging":"50,56","development":"50,56"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'merchant','HFE-WA');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'merchant_logo','HFE_WA_v5_small_logo.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'merchant_name','Wild Adventures');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'passbook_path','{"production":"https://passbook.accesso.com/accesso14","staging":"https://stg-passbook.accesso.com/accesso14","development":"https://passbook.ceiris.com/accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'support_phone_number','407.261.4284');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1004,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'analytics','{"production":["MO-12490965-1"],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'assets_path','{"production":"https://assets.accesso.com/accesso14/images/","staging":"https://stg-assets.accesso.com/accesso14/images/","development":"https://assets.ceiris.com/accesso14/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'client','Herschend Family Entertainment');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-accesso14","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-accesso14","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'knowledgebase_id','{"production":"50,56","staging":"50,56","development":"50,56"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'merchant','HFE-DW');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'merchant_logo','DW-logo-150x30.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'merchant_name','Dollywood');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'passbook_path','{"production":"https://passbook.accesso.com/accesso14","staging":"https://stg-passbook.accesso.com/accesso14","development":"https://passbook.ceiris.com/accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'support_phone_number','407.261.4284');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1005,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'analytics','{"production":["MO-29832479-4"],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'assets_path','{"production":"https://assets.accesso.com/accesso14/images/","staging":"https://stg-assets.accesso.com/accesso14/images/","development":"https://assets.ceiris.com/accesso14/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'client','Herschend Family Entertainment');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-accesso14","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-accesso14","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'knowledgebase_id','{"production":"50,56","staging":"50,56","development":"50,56"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'merchant','HFE-DL');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'merchant_logo','DL_LOGO_30x150.png');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'merchant_name','Darien Lake');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'passbook_path','{"production":"https://passbook.accesso.com/accesso14","staging":"https://stg-passbook.accesso.com/accesso14","development":"https://passbook.ceiris.com/accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'support_phone_number','407.261.4284');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1006,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'analytics','{"production":["MO-20986869-1"],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'assets_path','{"production":"https://assets.accesso.com/accesso14/images/","staging":"https://stg-assets.accesso.com/accesso14/images/","development":"https://assets.ceiris.com/accesso14/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'client','Herschend Family Entertainment');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-accesso14","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-accesso14","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'knowledgebase_id','{"production":"50,56","staging":"50,56","development":"50,56"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'merchant','HFE-EG');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'merchant_name','Elitch Gardens <br> Theme & Water Park');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'passbook_path','{"production":"https://passbook.accesso.com/accesso14","staging":"https://stg-passbook.accesso.com/accesso14","development":"https://passbook.ceiris.com/accesso14"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'support_phone_number','407.261.4284');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',1007,'use_geolocation','false');
/*
-- Query: SELECT * FROM application_navigation_tree WHERE application_id=1500 ORDER BY merchant_id
*/
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1001,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="General Admission" label="General Admission" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Annual Passes" label="Annual Passes" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Theater Tickets" label="Theater Tickets" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Rentals" label="Rentals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Special Offers" label="Special Offers" sortedordernumber="4" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1002,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Silver Dollar City" label="Silver Dollar City" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="White Water" label="White Water" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Splash and Play" label="Splash and Play" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Special Offers" label="Special Offers" sortedordernumber="5" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1004,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Rentals" label="Rentals" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Special Offers" label="Special Offers" sortedordernumber="3" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1005,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Dollywood" label="Dollywood" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Dollywood''s Splash Country" label="Dollywood''s Splash Country" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Splash & Play" label="Splash & Play" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Group Tickets" label="Group Tickets" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Rentals" label="Rentals" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Special Offers" label="Special Offers" sortedordernumber="7" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1006,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="5" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',1007,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Parking" label="Parking" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Meals" label="Meals" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Cabanas" label="Cabanas" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Additional Items" label="Additional Items" sortedordernumber="5" target="_blank"></MENUITEM></TREE>');
