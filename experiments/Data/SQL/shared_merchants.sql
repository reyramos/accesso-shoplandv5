/*
-- Query: SELECT * FROM application_config WHERE application_id=1500 ORDER BY merchant_id, name
*/
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'assets_path','{"production":"https://assets.accesso.com/shared/images/","staging":"https://stg-assets.accesso.com/shared/images/","development":"https://assets.ceiris.com/shared/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'client','Schlitterbahn Waterparks & Resorts');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-SharedV3","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-SharedV3","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-SharedV3"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'knowledgebase_id','{"production":"50","staging":"50","development":"50"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'merchant','SWP-GI');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'merchant_name','Schlitterbahn <br> Galveston Island');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'passbook_path','{"production":"https://passbook.accesso.com/schlitterbahn","staging":"https://stg-passbook.accesso.com/schlitterbahn","development":"https://passbook.ceiris.com/schlitterbahn"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'support_phone_number','409-770-9283');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',301,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'assets_path','{"production":"https://assets.accesso.com/shared/images/","staging":"https://stg-assets.accesso.com/shared/images/","development":"https://assets.ceiris.com/shared/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'client','Schlitterbahn Waterparks & Resorts');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-SharedV3","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-SharedV3","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-SharedV3"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'knowledgebase_id','{"production":"50","staging":"50","development":"50"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'merchant','SWP-SPI');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'merchant_name','Schlitterbahn <br> South Padre Island');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'passbook_path','{"production":"https://passbook.accesso.com/schlitterbahn","staging":"https://stg-passbook.accesso.com/schlitterbahn","development":"https://passbook.ceiris.com/schlitterbahn"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'support_phone_number','956-772-7873');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',302,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'assets_path','{"production":"https://assets.accesso.com/shared/images/","staging":"https://stg-assets.accesso.com/shared/images/","development":"https://assets.ceiris.com/shared/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'client','Schlitterbahn Waterparks & Resorts');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-SharedV3","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-SharedV3","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-SharedV3"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'knowledgebase_id','{"production":"50","staging":"50","development":"50"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'merchant','SWP-NB');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'merchant_name','Schlitterbahn <br> New Braunfels, Texas');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'passbook_path','{"production":"https://passbook.accesso.com/schlitterbahn","staging":"https://stg-passbook.accesso.com/schlitterbahn","development":"https://passbook.ceiris.com/schlitterbahn"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'support_phone_number','830-625-2351');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',303,'use_geolocation','false');




INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'assets_path','{"production":"https://assets.accesso.com/shared/images/","staging":"https://stg-assets.accesso.com/shared/images/","development":"https://assets.ceiris.com/shared/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'client','Schlitterbahn Waterparks & Resorts');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-SharedV3","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-SharedV3","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-SharedV3"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'knowledgebase_id','{"production":"50","staging":"50","development":"50"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'merchant','SWP-KC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'merchant_name','Schlitterbahn <br> Kansas City, Kansas');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'passbook_path','{"production":"https://passbook.accesso.com/schlitterbahn","staging":"https://stg-passbook.accesso.com/schlitterbahn","development":"https://passbook.ceiris.com/schlitterbahn"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'support_phone_number','409-770-9283');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',304,'use_geolocation','false');
/*
-- Query: SELECT * FROM application_navigation_tree WHERE application_id=1500 ORDER BY merchant_id
*/
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',301,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Memberships" label="Memberships" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Parking" label="Parking" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Special Events" label="Special Events" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Adopt" label="Adopt" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Donate" label="Donate" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Tours" label="Tours" sortedordernumber="6" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',302,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Memberships" label="Memberships" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Parking" label="Parking" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Special Events" label="Special Events" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Adopt" label="Adopt" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Donate" label="Donate" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Tours" label="Tours" sortedordernumber="6" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',303,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Memberships" label="Memberships" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Parking" label="Parking" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Special Events" label="Special Events" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Adopt" label="Adopt" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Donate" label="Donate" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Tours" label="Tours" sortedordernumber="6" target="_blank"></MENUITEM></TREE>');
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',304,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Memberships" label="Memberships" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Parking" label="Parking" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="Special Events" label="Special Events" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="Adopt" label="Adopt" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Donate" label="Donate" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Tours" label="Tours" sortedordernumber="6" target="_blank"></MENUITEM></TREE>');
