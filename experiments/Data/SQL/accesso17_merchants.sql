/*
-- Query: SELECT * FROM application_config WHERE application_id=1500 ORDER BY merchant_id, name
*/
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'assets_path','{"production":"https://assets.accesso.com/accesso17/images/","staging":"https://stg-assets.accesso.com/accesso17/images/","development":"https://assets.ceiris.com/accesso17/images/"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'client','Norpoint');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'default_language','en');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-accesso17","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-accesso17","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-accesso17"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'geolocation_options','{enableHighAccuracy:true,maximumAge:0,timeout:1000}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://stg-cegateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://cegateway.ceiris.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'knowledgebase_id','{"production":"74","staging":"74","development":"74"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'merchant','NOR-WW');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'merchant_name','Wild Waves');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'passbook_path','{"production":"https://passbook.accesso.com/accesso17","staging":"https://stg-passbook.accesso.com/accesso17","development":"https://passbook.ceiris.com/accesso17"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'printPLUOverride','transmit_package_id');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'security_provider','Trustwave');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'support_phone_number','407-956-3549');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (1500,'en',405,'use_location','false');
/*
-- Query: SELECT * FROM application_navigation_tree WHERE application_id=1500 ORDER BY merchant_id
*/
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (1500,'en',405,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="May Tickets" label="May Tickets" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="June Tickets" label="June Tickets" sortedordernumber="2" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="3" isactive="true" keywords="June Groups 12+ Weekday Tickets" label="June Groups 12+ Weekday Tickets" sortedordernumber="3" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="4" isactive="true" keywords="July-September Tickets" label="July-September Tickets" sortedordernumber="4" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="5" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="5" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="6" isactive="true" keywords="Fright Fest" label="Fright Fest" sortedordernumber="6" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="7" isactive="true" keywords="Parking" label="Parking" sortedordernumber="7" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="8" isactive="true" keywords="Special Events" label="Special Events" sortedordernumber="8" target="_blank"></MENUITEM></TREE>');
