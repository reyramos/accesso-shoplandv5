SET @application_id = '1500';
SET @language = 'en';

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'errorWithPayPalMsg', 'A problem with creating link, PayPal Checkout is unavailable at this time.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'errorWithPayPalTitle', 'An Error with PayPal');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'loadReceiptErrorMsg', 'There was a problem retrieving your receipts. Please try again.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'loadReceiptErrorTitle', 'Failed to Get Receipt Details');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'missingPromoErrorMsg', 'Please enter a promotional code before proceeding.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'missingPromoErrorTitle', 'Missing Promotional Code');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'paypalCompleteUrlMsg', 'Updating your cart to include your PayPal information.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'paypalCancelledUrlMsg', 'Cancelling Your PayPal Method, Returning you to the Store.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels', 'connectionLost', 'Application Offline. Reconnecting');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels', 'offlineTitle', 'Application Offline');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels', 'offlinePreResponseMsg', 'The application went off line while a request was made to our servers.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels', 'offlinePrePurchaseResponse', 'If you attempted a purchase, please check your email for your receipt before attmepting to purchase again otherwise you may be charged twice.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels', 'offlineRefreshMsg', 'Please refresh the application before attempting to proceed.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ApplicationLabels', 'redirectingToPayPal', 'Redirecting to PayPal');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'details', 'Details');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'noThanks', 'No Thanks');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'proceedToCart', 'Proceed to Cart');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'reloadTickets', 'Reload Tickets');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'reloadReceipt', 'Reload Receipt');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ButtonLabels', 'viewDetails', 'View Details');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView', 'orderReviewHeading', 'Order Review');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView', 'paymentMethodHeading', 'Payment Method');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView', 'paypalAccountLabel', 'PayPal Account');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView', 'receiptViewHeading', 'Your Receipt');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CrossSells', 'heading', 'Other Items of Interest');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'PaymentMethodView', 'creditCardlabel', 'Credit Card');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'PaymentMethodView', 'debitCreditCardlabel', 'Debit / Credit Card');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'PaymentMethodView', 'giftCardlabel', 'Gift Card');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'PaymentMethodView', 'paypalLabel', 'PayPal Checkout');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'PaymentMethodView', 'selectPaymentMethodHeading', 'Select a Payment Method');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ReceiptView', 'problemLoadingReceipt', 'There was a problem loading your receipts.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ReceiptView', 'receiptMessage', 'Thank you for your purchase!');


INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'internationalShippingErrorTitle', 'No International Delivery');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'internationalShippingErrorMsg', 'We currently do not support international delivery. Please chose an alternate delivery method.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'reconnectAttemptsExceededTitle', 'Exceeded Reconnect Attempts');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'reconnectAttemptsExceededMsg', 'The application attempted to reconnect more than the allowed times. For a better user experience, please visit us again when you have a more consistent Internet connection.<br /><br /> Click OK to refresh your browser.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'shippingInfoNotSetTitle', 'Missing Information');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Alert', 'shippingInfoNotSetMsg', 'You\'re missing the delivery information required to process your order. Please enter required data.');

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'BillingView', 'addressSameAsShipping', 'Use my Delivery Address.');

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'CartView', 'agreement', 'To continue your purchase, you must agree to the following: By clicking this box, I agree that all ticket sales are final. There are no refunds or exchanges.');

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'FormLabels', 'deliveryMethod', 'Delivery Method');

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'FormValidation', 'deliveryMethodRequiredError', 'Delivery method is required.');

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Reservations', 'heading', 'Reservations');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Reservations', 'pageTitle', 'Names for Ticket Reservation');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Reservations', 'pageDescription', 'Please enter a ticket holder name for each of your tickets below.');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'Reservations', 'sameLastName', 'Use the same last name for all tickets.');

INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'shippingHeading', 'Shipping Information');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'uDescription', 'First Class Mail');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'uLongDescription', 'Allow 14 Business Days');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'mDescription', 'Mobile');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'mLongDescription', 'Shipping Method for Mobile Stores');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'yDescription', 'Will Call');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'yLongDescription', 'Pick-up your tickets at the gate');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'optin1', 'Yes, I would like to receive the APIMC HTML email newsletter (we respect your privacy).');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'optin2', 'Yes, I would like to receive mobile TEXT messages (tournament week only, no more than 3 per day).');
INSERT INTO `application_locale` (`application_id`,`language`,`section`,`target`,`value`) VALUES (@application_id,@language,'ShippingView', 'optin3', 'Yes, I would like to receive promotional mailings.');