###################################################################################
# 
# MERCHANT CONFIGURATION FOR V5
# 
###################################################################################
SET @application_id = '1500';
SET @merchant_id = '501';
SET @language = 'en';
SET @image_folder = 'demo';
SET @island = 'DEMO';
SET @client = 'accesso';
SET @merchant_code = 'DEMO';
SET @merchant_name = 'Demo Park';
SET @passbook_symlink = 'demo';
SET @security_provider = 'Trustwave';
SET @support_phone_number = '407-333-7311';

###################################################################################
# 
# ADD THE V5 APPLICATION TO THE ISLAND CONFIGURATION (RUN ONLY ONCE PER ISLAND)
# 
###################################################################################
#INSERT INTO `application` (`id`,`name`,`description`,`parent_application_id`,`app_is_a_parent`,`dynamic_config_flag`,`dynamic_locale_flag`) VALUES (@application_id,'Shopland 5','Shopland 5',0,1,1,1);

###################################################################################
# DELETE PREVIOUS V5 CONFIGURATION
###################################################################################
DELETE FROM `application_config` WHERE `application_id` = @application_id AND `language` = @language AND `merchant_id` = @merchant_id;
DELETE FROM `application_navigation_tree` WHERE `application_id` = @application_id AND `language` = @language AND `merchant_id` = @merchant_id;

###################################################################################
# application_config
###################################################################################
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'agent_id','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'analytics','{"production":[],"staging":[],"development":[]}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'assets_path',REPLACE('{"production":"https://assets.accesso.com/@image_folder/images/","staging":"https://assets.accesso.com/@image_folder/images/","development":"https://assets.accesso.com/@image_folder/images/"}','@image_folder',@image_folder));
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'billing_types','EC');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'client',@client);
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'currency_code','USD');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'default_country','US');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'default_keyword','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'default_language',@language);
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'default_order_view','ticketView');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'display_accesso_branding','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'display_agreement','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'display_expanded_discounts','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'display_expanded_fees','false');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'display_knowledge_base','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'display_promo_code','true');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'gateway',REPLACE('{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-@island","staging":"https://gateway.accesso.com/ceGateway/servlet/gateway-@island","development":"https://gateway.accesso.com/ceGateway/servlet/gateway-@island"}','@island',@island));
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'knowledgebase_gateway','{"production":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","staging":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase","development":"https://gateway.accesso.com/ceGateway/servlet/gateway-KnowledgeBase"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'knowledgebase_id','{"production":"50","staging":"50","development":"50"}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'links','{}');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'machine_id','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'merchant',@merchant_code);
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'merchant_logo','');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'merchant_name',@merchant_name);
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'optins','[]');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'passbook_path',REPLACE('{"production":"https://passbook.accesso.com/@passbook_symlink","staging":"https://passbook.accesso.com/@passbook_symlink","development":"https://passbook.accesso.com/@passbook_symlink"}','@passbook_symlink',@passbook_symlink));
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'security_provider',@security_provider);
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'service_purchase_timeout','60');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'service_request_limit','500');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'service_request_timeout','5');
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'support_phone_number',@support_phone_number);
INSERT INTO `application_config` (`application_id`,`language`,`merchant_id`,`name`,`value`) VALUES (@application_id,@language,@merchant_id,'use_location','false');

###################################################################################
# application_navigation_tree
###################################################################################
INSERT INTO `application_navigation_tree` (`application_id`,`language`,`merchant_id`,`tree`) VALUES (@application_id,@language,@merchant_id,'<TREE><MENUITEM alternateModule="" id="0" isactive="true" keywords="Daily Tickets" label="Daily Tickets" sortedordernumber="0" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="1" isactive="true" keywords="Season Passes" label="Season Passes" sortedordernumber="1" target="_blank"></MENUITEM><MENUITEM alternateModule="" id="2" isactive="true" keywords="Parking" label="Parking" sortedordernumber="2" target="_blank"></MENUITEM></TREE>');