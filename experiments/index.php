<?php
	// TODO: Determine the environment based on the _system.xml file
	switch(strtolower($_SERVER['SERVER_NAME'])){
		case 'store.accesso.com':
			$environment = 'production';
			$server_url = 'https://store.accesso.com/';
		break;
		case 'stg-store.accesso.com':
			$environment = 'staging';
			$server_url = 'https://stg-store.accesso.com/';
		break;
		case 'store.ceiris.com':
		case 'localhost':
			$environment = 'development';
			$server_url = 'https://store.ceiris.com/';
		break;
		default:
			$environment = 'unknown';
			$server_url = 'https://'.$_SERVER['HTTP_HOST'].'/';
		break;
	}
	// TODO: Add in device detection
	$device = 'unknown';
	//$CNFS_PATH = '../server/node/cnfs/parks/';
	$CNFS_PATH = '../api/cnfs/parks/';
	try{
		$directory = new DirectoryIterator($CNFS_PATH);
		$proceed = true;
	}
	catch(Exception $exception){
		$proceed = false;
	}
	$client_array = array();
	if($proceed){
		foreach($directory as $file){
			if(!$file->isDot()){
				$filename = $file->getFilename();
				$is_config = strpos($filename, '.xml');
				$is_system_config = strpos($filename, '_system.xml');
				$is_affiliate_config = strpos($filename, '-affiliate.xml');
				if($is_config !== false && $is_system_config === false && $is_affiliate_config === false){
					preg_match('/(.*)\.xml/', $filename, $matches);
					$merchant_code = $matches[1];
					// TODO: Check file exists
					$configuration = simplexml_load_file($CNFS_PATH.$filename);
					// TODO: Check the contents of the file are xml
					$client_name = false;
					$xpath = '//configuration/@client';
					$result = $configuration->xpath($xpath);
					if(count($result)){
						$client_name = (string)$result[0];
					}
					
					$vendor_name = false;
					$xpath = '//configuration/@name';
					$result = $configuration->xpath($xpath);
					if(count($result)){
						$vendor_name = (string)$result[0];
					}
					
					$vendor_url = false;
					$xpath = '//configuration/@url';
					$result = $configuration->xpath($xpath);
					if(count($result)){
						$vendor_url = (string)$result[0];
					}
					
					$island = false;
					$xpath = '//environment/@island';
					$result = $configuration->xpath($xpath);
					if(count($result)){
						$island = (string)$result[0];
					}
					
					$store_url = $server_url.$merchant_code;
					$order_url = 'https://'.$_SERVER['HTTP_HOST'].'/'.$merchant_code.'/orderLookup';
					
					if($client_name !== false && $vendor_name !== false && $island !== false){
						$vendor_array = array(
							'name'=>(string)$vendor_name
							, 'code'=>(string)$merchant_code
							, 'file'=>(string)$filename
							, 'store_url'=>(string)$store_url
							, 'order_url'=>(string)$order_url
							, 'vendor_url'=>(string)$vendor_url
						);
						$found = false;
						foreach($client_array as &$client){
							if($client['name'] == $client_name){
								$found = true;
								array_push($client['vendor'], $vendor_array);
								break;
							}
						}
						unset($client);
						if(!$found){
							$init_array = array();
							array_push($init_array, $vendor_array);
							$add_client = array(
								'name'=>(string)$client_name
								, 'island'=>(string)$island
								, 'vendor'=>$init_array
							);
							array_push($client_array, $add_client);
						}
					}
				}
			}
		}
	}
	function orderByClientName($a, $b){
		return strcmp(strtolower($a['name']), strtolower($b['name']));
	}
	usort($client_array, 'orderByClientName');
?>
<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="initial-scale = 1, maximum-scale=1, user-scalable = 0">
		<title>accesso Portfolio</title>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<?php foreach($client_array as $client){ ?>
			<?php $index = 0; ?>
			<div>
				<h1><?php echo $client['name']; ?></h1>
				<ol>
					<?php foreach($client['vendor'] as $vendor){ ?>
						<?php $index++; ?>
						<li><?php echo $vendor['name']; ?> [ <a href="<?php echo $vendor['vendor_url']; ?>" target="_blank">Website</a> | <a href="<?php echo $vendor['store_url']; ?>" target="_blank">Store</a> ]</li>
					<?php } ?>
				</ol>
			</div>
		<?php } ?>
		<script type="text/javascript" async src="//stats.g.doubleclick.net/dc.js"></script>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			var device = '<?php echo $device; ?>';
			var environment = '<?php echo $environment; ?>';
			var _accounts = { production:['UA-42209183-1'], staging:['UA-26514552-22'], development:['UA-26514552-22'] };
			var account = _accounts[environment];
			_gaq.push(
				['_setAccount', account]
				, ['_setDomainName', 'none']
				, ['_setCustomVar', 1, 'host', window.location.host, 3]
				, ['_setCustomVar', 2, 'environment', environment, 3]
				, ['_setCustomVar', 3, 'device', device, 3]
				//, ['_setCustomVar', 4, 'merchant', merchant, 3]
				//, ['_setCustomVar', 5, 'langauge', langauge, 3]
				, ['_trackPageview']
			);
			function trackEvent(data){
				/*
				 * _trackEvent(category, action, opt_label, opt_value, opt_noninteraction)
				 *
				 * category (required)
				 * The name you supply for the group of objects you want to track.
				 *
				 * action (required)
				 * A string that is uniquely paired with each category, and commonly used to define the type of user interaction for the web object.
				 *
				 * label (optional)
				 * An optional string to provide additional dimensions to the event data.
				 *
				 * value (optional)
				 * An integer that you can use to provide numerical data about the user event.
				 *
				 * non-interaction (optional)
				 * A boolean that when set to true, indicates that the event hit will not be used in bounce-rate calculation.
				 *
				 */
				_gaq.push(
					['_setAccount', account]
					, ['_setDomainName', 'none']
					, ['_trackEvent', data.category, data.action, data.label, data.value]
				);
			}
		</script>
	</body>
</html>