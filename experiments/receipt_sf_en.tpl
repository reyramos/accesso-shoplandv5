{config_load file=main.conf section=$section|default:'generic'}

<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">

<SCRIPT type="text/javascript" src="../ext_functions.js"></SCRIPT>
<title>{$title} Purchase Receipt - Confirmation# {$confirmation}</title>
{literal}
<STYLE type="text/css">
 body{margin:0;}
 .headerTop { background-color:#FFCC66; border-top:0px solid #000000; border-bottom:1px solid #FFFFFF; text-align:center; }
 .adminText { font-size:10px; color:#996600; line-height:200%; font-family:verdana; text-decoration:none; }
 .headerBar { background-color:#FFFFFF; border-top:0px solid #333333; border-bottom:10px solid #FFFFFF; }
 .title { font-size:20px; font-weight:bold; color:#CC6600; font-family:arial; line-height:110%; }
 .subTitle { font-size:11px; font-weight:normal; color:#666666; font-style:italic; font-family:arial; }
 td { font-size:12px; color:#000000; line-height:150%; font-family:trebuchet ms; }
 .sideColumn { background-color:#FFFFFF; border-left:1px dashed #CCCCCC; text-align:left; }
 .sideColumnText { font-size:11px; font-weight:normal; color:#999999; font-family:arial; line-height:150%; }
 .sideColumnTitle { font-size:15px; font-weight:bold; color:#333333; font-family:arial; line-height:150%; }
 .footerRow { background-color:#FFFFCC; border-top:10px solid #FFFFFF; }
 .footerText { font-size:10px; color:#996600; line-height:100%; font-family:verdana; }
 a { color:#FF6600; color:#FF6600; color:#FF6600; }

</STYLE>

{/literal}
</head>
<body>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor='#154a7c' style="background-color:#154a7c;" >
	<tr>
		<td valign="top" align="center" >
<table width="797" cellpadding="0" cellspacing="0" bgcolor='#ffffff' style="background-color:#ffffff;">
			<!-- HEADER -->
			<table width="797" cellpadding="0" cellspacing="0" bgcolor='#ffffff' style="background-color:#ffffff;">
				<tr>
					<td align="left" valign="middle" style="background-color:#FFFFFF;border-top:0px solid #333333;border-bottom:10px solid #FFFFFF;"><center><IMG id=editableImg1 SRC="{$receipt_path}images/receipt_logo_sf_fr_en.jpg" BORDER="0" alt="Sixflags Banner" align="center"></center>
					</td>
				</tr>
			</table>
			
			<table width="797" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="background-color:#ffffff;">
				<tr>
					<td width="10"></td>
					<!-- LEFT SIDE TABLE -->
					<td bgcolor="#FFFFFF" valign="top" width="560" style="font-size:.8em;color:#000000;line-height:150%;font-family:arial;">
						<table>
							<tr>
								<td>
									<span style="font-size:1em;font-weight:bold;color:#666666;font-family:arial;">
										Thank you for purchasing on Sixflags.com!
									</span>
								</td>
							</tr>
						</table>
						<table width="572" cellpadding="0" cellspacing="0">
							<tr>
								<td width="320" bgcolor="#e3f4fa" style="border-left:solid #cccccc 1px;border-top:solid #cccccc 1px;border-bottom:solid #cccccc 1px;text-align:left;padding-left:5px;">
									<b>Confirmation#:</b> <span style="color:#FF0000">{$confirmation}</span><br>
									<b>Order Date:</b> {$order_date}<br>
									<b>Delivery Method:</b> {$delivery_method}<br>
								</td>
								<td width="242" bgcolor="#e3f4fa" style="border-right:solid #cccccc 1px;border-top:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
									{if $smarty.request.merchant}
										<a href="#" onclick="window.print();return false;"><IMG SRC="{$images_folder}printReceipt.png" BORDER="0" alt="" align="center" width="136"></a>
									{else}
										<p></p>
									{/if}
								
								</td>

							</tr>
						</table>
						
						<!-- DYNAMIC DELIVERY SECTION -->
						{if $ship_method == "P"}
							{include file="P.shipping_sf_fr_en.tpl"}
						{else}
							{include file="$ship_method.shipping_sf.tpl"}
						{/if}
						
		<!-- ORDER SUMMARY -->
						<table>
							<tr>
								<td width="572" style="text-align:center;">
									<span style="font-size:1.4em;font-weight:bold;color:#666666;font-family:arial;text-align:center;">
										Order Summary
									</span>
								</td>
							</tr>
						</table>
						<table width="572">
							<tr>
								<td width="560" bgcolor="#e3f4fa" style="border:solid #cccccc 1px;text-align:left;">
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="271" bgcolor="#e3f4fa" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Product Description</b>
											</td>
											<td width="60" bgcolor="#e3f4fa" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Type</b>
											</td>
											<td width="60" bgcolor="#e3f4fa" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>SKU</b>
											</td>
											<td width="75" bgcolor="#e3f4fa" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Unit Price</b>
											</td>
											<td width="30" bgcolor="#e3f4fa" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Qty</b>
											</td>
											<td width="64" bgcolor="#e3f4fa" style="text-align:center;border-bottom:solid #cccccc 1px;">
												<b>Price</b>
											</td>
										</tr>
									</table>
						
			<!-- DYNAMIC PURCHASES LOOP -->
			
									{foreach from=$packages key=k item=item}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="271" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if $item.prefix != ""}{$item.prefix} - {/if}{$item.package_name|utf}{if isset($item.charac_valid_from)} <b style="color:red;">Valid Starting {$item.charac_valid_from|date_format:"%A"}, {assign var=temp value='[-]'|split:$item.charac_valid_from} {$temp.1}/{$temp.2}/{$temp.0}</b>{/if}{if isset($item.charac_resource_id)} (Hut #{$item.charac_resource_id}) {/if}{$item.event_start_date} {$item.event_start_time} {if isset($item.charac_row_name)}<br />Row:{$item.charac_row_name} Seat:{$item.charac_seat_name} Section:{$item.charac_section_name}{/if}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.customer_type_name|utf}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.package_id}
											</td>
											<td width="75" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												${$item.retail}
											</td>
											<td width="30" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.qty}
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 1px;">
												${$item.retail|string_format:"%.2f"}
										</tr>
									</table>
									{/foreach}
									<!-- END OF LOOP -->
							<!-- DISCOVER 5% ADJUSTMENT -->
									
									{foreach from=$adjustments key=k item=v}
									{if $v.original == 1 and $v.cart_adjustment_name == "5% Discover Savings" and $v.value != "0.00"}
									
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
											5% Discover Savings:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$v.value|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}	
									{/foreach}
									

							<!-- TAX (CHECKS FOR ADJUSTMENTS FIRST) -->
									{if $totals.total_adjustments == 0}
                                    				<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Tax:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.tax_total|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}

							<!-- FEES -->	
									{if count($refunds) == 0}	
									{foreach from=$fees key=k item=v}
									
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												{$k}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$v|string_format:"%.2f"}
											</td>
										</tr>
									</table>
								
									{/foreach} 
									{/if}
						
							<!-- ADJUSTMENTS -->
									{foreach from=$adjustments key=k item=v}
									{if $v.original == 1 and $v.cart_adjustment_name != "5% Discover Savings" and $v.value != "0.00"}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												{if $totals.total_adjustments != 0}Original {/if} {if $merchant_id == "7" and $v.cart_adjustment_name == "Processing Fee"}*{/if}{$v.cart_adjustment_name}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$v.value|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}	
									{/foreach}
									
						<!-- NO REFUNDS SHOW THIS -->
									<!--
									{if count($refunds) <= 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Subtotal:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$total_subtotal|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}
									-->

									{if $totals.total_adjustments == 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												{$delivery_method}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.shipping_total|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}									
									
									{if $totals.total_adjustments != 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Original Additional Fees:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_fees|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Original {$delivery_method}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_shipping|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
												Original Tax:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_tax|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}
									
									{if $totals.total_adjustments != 0}
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 0px;">
													Original Total:
												</td>
												<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
													{$totals.total_original|string_format:"%.2f"}
												</td>
											</tr>
										</table>
									{/if}				
					<!-- TOTALS -->
									{if count($refunds) == 0}
									<table  cellspacing="0" cellpadding="0">
										<tr>
											<td width="500" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;">
												<b>Total:</b>
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;">
												<b>${$totals.current_total|string_format:"%.2f"}</b>
											</td>
										</tr>
									</table>
									{/if}
									{if $merchant_id == "7"}
									<table  cellspacing="0" cellpadding="0">
										<tr>
											<td width="564" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 0px;">
												*Processing fee includes 6.0% Georgia sales tax.
											</td>
										</tr>
									</table>
									{/if}


					<!-- SCHEDULED PAYMENTS -->
									{if count($scheduled_payments) > 1}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="797" style="text-align:center;">
												<p>
												You have chosen to defer your payment(s). A payment schedule is noted below. If you would like to update your credit card information or print a copy of your terms and conditions, please click "Launch Payment Portal" below.
												</p>
												<p><a href="http://web1.sixflags.com/payments" target='_blank'><img src="{$receipt_path}images/profileBttn2EN.jpg" alt="<Update Profile button!>" style="border:none;"></a>&nbsp;
												</p>

											</td>
										</tr>
									
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="562" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												<b>Scheduled Payments</b>
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Payment</b>
											</td>
											<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Date</b>
											</td>
											<td width="100" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												<b>Amount</b>
											</td>
										</tr>
									</table>

									{assign var=coo value=0}
									{assign var=sch value=0}
										{if count($refunds) > 0}
											{foreach from=$refunds key=k item=item}
												{if $item.retail_amount != "0.00" and $item.type == "T"}
													{math equation="x + y" x=$coo y=1 assign='coo'}
												{/if}
											{/foreach}
											{foreach from=$scheduled_payments key=k item=item}
												{if $item.status == "Cancelled"}
													{math equation="x + y" x=$sch y=1 assign='sch'}
												{/if}		
											{/foreach}
										{/if}

									{foreach from=$scheduled_payments key=k item=item}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$k+1}
											</td>
											<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$item.date}
											</td>
											<td width="100" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												${$item.amount|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/foreach}
									{/if}

					<!-- YES REFUNDS SHOW THIS -->
									{if count($refunds) > 0}
										{if count($scheduled_payments) > 1 and $sch > 0}
											{if $coo == 0}
												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="568"  style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
															<b>Your Scheduled Payments have been refunded!</b>
														</td>
													</tr>
												</table>
											{else}
												<table cellspacing="0" cellpadding="0">
													<tr height="10" bgcolor="#999999"><td height="10" bgcolor="#ffffff"></td></tr>
													<tr>
														<td width="562" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
															<b>Adjustments to your Scheduled Payments</b>
														</td>
													</tr>
												</table>
												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
															<b>Payment</b>
														</td>
														<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
															<b>Date</b>
														</td>
														<td width="100" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
															<b>Amount</b>
														</td>
													</tr>
												</table>

												{foreach from=$scheduled_payments key=k item=item}
												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
															{$k+1}
														</td>
														<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
															{$item.date}
														</td>
														<td width="100" bgcolor="#999999" style="color:#a31111;text-align:center;border-bottom:solid #cccccc 1px;">
															{if $item.status == "Pending"}CANCELLED{else}-${$item.amount|string_format:"%.2f"}{/if}
														</td>
													</tr>
												</table>
												{/foreach}
											{/if}
										{else}											
											<table cellspacing="0" cellpadding="0">
												<tr height="10" bgcolor="#999999"><td height="10" bgcolor="#ffffff"></td></tr>
													<tr>
														<td width="568" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
															<b>Adjustments to Your Order</b>
														</td>
													</tr>
											</table>
											<table cellspacing="0" cellpadding="0">
												<tr>
													<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
														<b>Product Description</b>
													</td>
													<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
														<b>Type</b>
													</td>
													<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
														<b>SKU</b>
													</td>
													<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;">
														<b>Unit Price</b>
													</td>
												
												</tr>
											</table>
										
											{foreach from=$refunds key=k item=item}
												{if $item.retail_amount != "0.00" and $item.type == "T"}
													{if count($scheduled_payments) > 1 and $item.customer_type == "Season Pass"}{else}
  													<table cellspacing="0" cellpadding="0">
														<tr>
															<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{if $item.type == "A"}{$item.cart_adjustment_name|utf}{else}{$item.package_name|utf}{/if}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.customer_type|utf}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.package_id}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="color:#a31111;text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:.7em;">
																{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
															</td>
														
														</tr>
													</table>
													{/if}
												{/if}
											{/foreach}

											{foreach from=$refunds key=k item=item}
												{if $item.retail_amount != "0.00" and $item.type == "A" and $item.original == 0}
  													<table cellspacing="0" cellpadding="0">
														<tr>
															<td width="298" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.cart_adjustment_name|utf}
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.customer_type|utf}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.package_id}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="color:#a31111;text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:.7em;">
																{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
															</td>
														
														</tr>
													</table>
												{/if}
											{/foreach}

											{foreach from=$refunds key=k item=item}
												{if $item.retail_amount != "0.00" and $item.type == "S"}
  													<table cellspacing="0" cellpadding="0">
														<tr>
															<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$delivery_method}
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.customer_type}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.package_id}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="color:#a31111;text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:.7em;">
																{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
															</td>
														
														</tr>
													</table>
												{/if}
											{/foreach}

											{foreach from=$refund_fees key=k item=item}
												{if $item.retail_amount != "0.00" and $item.type != "T" and $item.original == "0"}
  													<table cellspacing="0" cellpadding="0">
														<tr>
															<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{if $item.type == "S"}Delivery{elseif $item.type == "F" and $item.retail_amount > 0}Remaining Charge{elseif $item.type == "F" and $item.retail_amount < 0}Additional Fees{elseif $item.type == "A"}{$item.cart_adjustment_name}{else}{$item.package_name}{/if}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.customer_type}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:.7em;">
																{$item.package_id}&nbsp;
															</td>
															<td width="90" bgcolor="#999999" style="color:#a31111;text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:.7em;">
																{$item.retail_amount|string_format:"%.2f"}&nbsp; 
															</td>
														
														</tr>
													</table>
												{/if}
											{/foreach}
										{/if}
									{/if}
						<!-- end of refunds -->
						
					




					<!-- ADJUSTMENTS TOTAL -->
									{if $totals.total_adjustments != 0}
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="481" bgcolor="#999999" style="text-align:right;border-right:solid #cccccc 0px;border-bottom:solid #cccccc 1px;">
													Adjustments Total:
												</td>
												<td width="90" bgcolor="#999999" style="color:#a31111;text-align:center;border-bottom:solid #cccccc 1px;">
													{$totals.total_adjustments|string_format:"%.2f"}
												</td>
											</tr>
										</table>
									{/if}
								</td>
							</tr>
						</table>
            
            
            
						<table cellspacing="0" cellpadding="0" style="margin:10px 0 0 0;">
							<tr>
								<td width="2"></td>
								<td width="250" bgcolor="#ffffff" style="text-align:left;border:solid #cccccc 1px;padding-left:5px;">
									<b>Payment Received By:</b><br>
									{$customer.name|utf}<br>
									{$phone}<br>
									{$email}<br>
								</td>
								<td width="54"></td>
								
								 {if $shipping_address.street neq ''}
									<!-- EMPTY FOR NOW -->
								 {/if}
								<td width="250" bgcolor="#ffffff" style="text-align:left;border:solid #cccccc 1px;padding-left:5px;">
									<b>Billing Information:</b><br>
									{$billing_address.street|utf} {$billing_address.co_dept}<br>
									{$billing_address.city|utf}, {$billing_address.state} {$billing_address.zip}<br>
									{$billing_address.country}<br>
									Payment Method: {$payment.billing_type_desc}<br>
								</td>
							</tr>
						</table>            
		{if $show_order_history == "true"}				
            <table cellspacing="0" cellpadding="0" style="margin:10px 0 10px 2px; border:solid #ccc 1px; padding:10px;">
            <tr>
              <td style="font-weight:bold; padding-bottom:10px;" colspan="3">Order History</td>
            </tr>
            {foreach from=$past_orders key=k item=item}
            <tr>
              <td width="242">{$item.customer_name|utf}</td>
              <td width="203"><div align="right"><span class="style2">{$item.order_date}</span></div></td>
              <td width="205"><div align="right"><a href="{$receipt_path}{$receipt_name}?ph={$ph}&order_id={$item.order_id}&user_id={$user_id}&lang={$lang}">View Order #{$item.order_id}</a></div></td>
            </tr>
            {/foreach}
          </table> 	
		{/if}				
						
					</td>
					
					
					<!-- ADS RIGHT SIDE -->
					<td width="225" valign="top" style="background-color:#FFFFFF;text-align:center;">
						
						<span style="font-size:11px;font-weight:normal;color:#999999;font-family:arial;line-height:150%;">
														
						</span> 
							
					</td>
					
				</tr>

				
				

			</table>
			
			<table width="797" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
				<tr>
					<td>
						<table width="797" cellspacing="0" cellpadding="0">
							<tr>
								<td width="10"></td>
								<td width="250" bgcolor="#ffffff" style="text-align:left;border:solid #cccccc 1px;padding-left:5px;">

								</td>
								<td width="48"></td>
								
			
								<td width="250" bgcolor="#ffffff" style="text-align:left;border:solid #cccccc 1px;padding-left:5px;">

								</td>
								<td width="220" style="text-align:center;">
									<a href="http://www.accesso.com" target='_blank'><img src="{$receipt_path}images/accessoLogo.jpg" style="border:none;"alt=""></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
      
     
			
			<table width="797" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
				<table width="797" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>	
						<td width="797">
							<img src="{$receipt_path}images/footer.jpg"alt="">
						</td>
					</tr>
				</table>
				
				<table width="797" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td width="280" style="text-align:center;">
							{if $customer_support_number != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								Customer Support: {$customer_support_number}
							</span><br>
							{/if}
							
							{if $customer_support_link != "" and $customer_support_link_title != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								<a href="{$customer_support_link}" target="_blank">{$customer_support_link_title}</a>
							</span><br>
							{/if}
							{if $customer_support_link != "" and $customer_support_link_title == ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								<a href="{$customer_support_link}" target="_blank">Click here for Technical Support</a>
							</span><br>
							{/if}
						</td>
					</tr>
					<tr>
						<td width="797" style="text-align:center;">
							<span style="font-size:10px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;">
							Email not displaying correctly? <a href="{$receipt_path}laronde.php?ph={$ph}&order_id={$order_id}&user_id={$user_id}&lang={$lang}&merchant=sixflags" target="_blank" style="font-size:10px;color:#996600;font-family:arial;text-decoration:underline;">
							View it in your browser.</a>
							</span>
						</td>
					</tr>
				</table>
				
			</table>
</table>
		</td>
	</tr>
</table>

</body>
</html>
