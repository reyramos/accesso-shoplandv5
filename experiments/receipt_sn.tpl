{config_load file=main.conf section=$section|default:'generic'}
{assign var=version value='4.01'}
<html>
	<head>
<meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
 
<SCRIPT  type="text/javascript" src="../ext_functions.js"></SCRIPT>
{if $merchant_id == "3001" or $merchant_id == "3002" or $merchant_id == "3003" or $merchant_id == "3004" or $merchant_id == "3005" or $merchant_id == "3006" or $merchant_id == "3007" or $merchant_id == "3008" or $merchant_id == "3009" or $merchant_id == "3010" or $merchant_id == "3011"}
{literal}
<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' 	type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
	try{
		var pageTracker = _gat._getTracker("UA-1277361-1");
		pageTracker._setAllowHash(false);
		pageTracker._setDomainName("none");
		pageTracker._setAllowLinker(true);
		pageTracker._trackPageview();
		{/literal}
		pageTracker._addTrans("{$confirmation}", "{if $merchant_id == "3001"}Fitchburg{/if}{if $merchant_id == "3002"}Newark{/if}{if $merchant_id == "3003"}Rockford{/if}{if $merchant_id == "3004"}Omaha{/if}{if $merchant_id == "3005"}Waterbury{/if}{if $merchant_id == "3006"}Mt. Laurel{/if}{if $merchant_id == "3007"}Cincinnati{/if}{if $merchant_id == "3008"}Arlingtom Heights{/if}{if $merchant_id == "3009"}Danvers{/if}{if $merchant_id == "3010"}Kansas City{/if}{if $merchant_id == "3011"}Orlando{/if}", "{$totals.current_total}", "{$totals.total_original_tax}", "{$totals.total_original_shipping}", "{$billing_address.city}", "{$billing_address.state}", "{$billing_address.country}");
		
		{foreach from=$packages key=k item=item}
		pageTracker._addItem("{$confirmation}", "{$item.package_id}", "{$item.package_name|utf}", "{$item.customer_type_name|utf}", "{$item.retail}", "{$item.qty}");
		{/foreach}
		{literal}
		pageTracker._trackTrans();
	} catch(err) {}
</script>
{/literal}   
{/if}

							<title>{$title} Purchase Receipt - Confirmation# {$confirmation}</title>					

		{literal}
		<STYLE>
	body{margin:0;background-color:#ffffff;}
	 .headerTop { background-color:#FFCC66; border-top:0px solid #000000; border-bottom:1px solid #FFFFFF; text-align:center; }
	 .adminText { font-size:10px; color:#996600; line-height:200%; font-family:verdana; text-decoration:none; }
	 .headerBar { background-color:#FFFFFF; border-top:0px solid #333333; border-bottom:10px solid #FFFFFF; }
	 .title { font-size:20px; font-weight:bold; color:#CC6600; font-family:arial; line-height:110%; }
	 .subTitle { font-size:11px; font-weight:normal; color:#666666; font-style:italic; font-family:arial; }
	 td { font-size:12px; color:#000000; line-height:150%; font-family:trebuchet ms; }
	 .sideColumn { background-color:#FFFFFF; border-left:1px dashed #CCCCCC; text-align:left; }
	 .sideColumnText { font-size:11px; font-weight:normal; color:#999999; font-family:arial; line-height:150%; }
	 .sideColumnTitle { font-size:15px; font-weight:bold; color:#333333; font-family:arial; line-height:150%; }
	 .footerRow { background-color:#FFFFCC; border-top:10px solid #FFFFFF; }
	 .footerText { font-size:10px; color:#996600; line-height:100%; font-family:verdana; }
	 a { color:#e27000;}
	</STYLE>
	{/literal}
	</head>
<body >
	
	
<table width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor='#ffffff' style="background-color:#ffffff;" >
	<tr>

		<td valign="top" align="center" >
			<table width="600" cellpadding="0" cellspacing="0" bgcolor='#ffffff' style="background-color:#ffffff;">
				
				<!-- HEADER -->
				<table width="600" cellpadding="0" cellspacing="0" bgcolor='#ffffff' style="background-color:#ffffff;">
					<tr>
						<td align="left" valign="middle" style="background-color:#FFFFFF;border-top:0px solid #333333;">
							<center><IMG id=editableImg1 SRC="{$receipt_path}images/{$receipt_image}" BORDER="0" alt="" align="center" width="600"></center>
						</td>
					</tr>
					<tr>
						<td align="left" valign="middle" style="background-color:#FFFFFF;border-top:0px solid #333333;">
							<br/><center><b>Thank you for your order!</b></center><br/>				</td>	
					</tr>

				</table>
			<table width="600" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="background-color:#ffffff;">
				<tr>
					<td width="10"></td>
					<!-- LEFT SIDE TABLE -->
					<td bgcolor="#FFFFFF" valign="top" width="600" style="font-size:;color:#000000;line-height:150%;font-family:arial;">
						<table width="600">
							<tr>
								<td width="600" bgcolor="" style="border:solid #cccccc 0px;text-align:left;padding-left:5px;font-size:1em;">
									<b>Confirmation#: <span style="color:;">{$confirmation}</b></span>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<b>Order Date: {$order_date}</b><br>
									<table width="600" align="center">
									<tr><td width="600" style="text-align:center;">
									{if $smarty.request.merchant}
									<a href="#" onClick="window.print();return false;"><IMG SRC="{$images_folder}printReceiptOrange.png" BORDER="0" alt="" align="center" width="177"></a>
									{/if}
									</td></tr>
									</table>

								</td>
							</tr>
						</table>

<!-- PASSBOOK : use_passbook   -->

					{if $order.use_passbook=="1" and $use_passbook == "true"}
						<table>
							<tr>
								<td width="600" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="600" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												iOS 6 users can add their tickets to Passbook on iPhone and iPod touch.
												<a href="{$passbook_url}?order_id={$order_id}&island={$passbook_island}" target="_blank"><img src="{$images_folder}addtopassbook.png" alt="<Add To Passbook!>" style="border:none;"></a> 
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					
					{/if}

<!-- DELIVERY SECTION -->
						
					{if $mobile_barcode and $mobile_barcode != ""}

						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												You can view your tickets and present them for scanning below.
												<a href="{$mobile_barcode}{$confirmation}" target="_blank"><img src="{$images_folder}viewticketsSN.png" alt="<View Tickets button!>" style="border:none;"></a> 
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					{elseif $ship_method == "K" and $alternative_k_shipping_desc and $alternative_k_shipping_desc != ""}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												{$alternative_k_shipping_desc}
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					{elseif $ship_method == "M"}
						<table width="600">
							<tr>
								<td width="600" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="600" bgcolor="#FFFFFF" style="text-align:center;font-size:1em;">
												<br/>
												<b>You can view your mobile tickets on your phone or at home!</b><br>
						
												<a href="http://shop.accesso.com/mobile/{if $merchant_id == "155"}src/router.php?m=CGG{/if}{if $merchant_id == "154"}src/router.php?m=SN{/if}&orderID={$confirmation}&link=orderhistorydetails&ph={$phone}&amp;merchant_id={$merchant_id}&amp;lang={$lang}{$print_at_home_vars}" target='_blank'><img src="{$images_folder}registerMobileBttnSN.jpg" alt="<View Your Tickets>" style="border:none;"></a>
						
												<br/>

											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					{elseif $ship_method == "D" || $ship_method == "P" || $ship_method == "G" || $ship_method == "N"}
							<table width="600">
							<tr>
								<td width="600" bgcolor="#FFFFFF" style="border:solid #cccccc 0px;text-align:left;">
									<table cellpadding="0" cellspacing="0">
										<tr>
											<td width="600" bgcolor="#FFFFFF" style="text-align:center;font-size:1em;">
												
												
<a href="{$print_v4}?printv4=true&m={$merchant_id}&order_id={$confirmation}&ph={$phone}&amp;merchant_id={$merchant_id}&lang={$lang}{$print_at_home_vars}" target='_blank'><img src="{$images_folder}registerBttnSN.jpg" alt="<Register and Print Tickets button!>" style="border:none;"></a>								
											<br/><b style="color:grey;">THIS IS NOT A TICKET</b><br>		
											</td>
										</tr>
									</table>
									
									<table>
										<tr>
											<td width="600" bgcolor="#FFFFFF" style="text-align:left;font-size:;">
												<b>To print your tickets:</b><br>
1. <a style="color:#000000;text-decoration:none;" href="{$print_v4}?printv4=true&m={$merchant_id}&order_id={$confirmation}&ph={$phone}&amp;merchant_id={$merchant_id}&lang={$lang}{$print_at_home_vars}" target='_blank'>Click here to register and print your tickets.</a><br/>												
												2. Enter the guest or group name for each ticket.<br>
												3. Click "Print Ticket".<br>
												4. Select the printer you wish to use from the pop up printer dialog box.<br>
												5. Verify that your tickets have printed correctly and click "Yes" to continue.<br>											 										{if $support_url != ""}
<a href="{$support_url}" target="_blank"><img src="{$images_folder}techBttnSN.jpg" alt="<Customer Support button!>" style="border:none;"></a> 
												{/if}
												{if $ship_method == "D"}
													<table>
														<tr>
															<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
																The items not part of Print@Home will be sent via First Class Mail.																								</td>
														</tr>
													</table>	
												{/if}
												{if $ship_method == "G"}
													<table>
														<tr>
															<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
																The items not part of Print@Home will be sent via UPS Expedited Shipping. Signature is not required. Re-routing of packages is not  allowed.
															</td>
														</tr>
													</table>
												{/if}
												{if  $ship_method == "N" and $alternative_n_shipping_desc != ""}
													<table>
														<tr>
															<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
																{$alternative_n_shipping_desc}
															</td>
														</tr>
													</table>
												{elseif $ship_method == "N"}
													<table>
														<tr>
															<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;font-weight:bold;">
																The items not part of Print-N-Go will be sent via UPS Ground Shipping. Signature is not required. Re-routing of packages is not allowed.
															</td>
														</tr>
													</table>
												{/if}

											</td>
										</tr>
									</table>
									
								</td>
							</tr>
						</table>
						{/if}
						{if $ship_method == "A"}
							<!-- Unused Shipping Method -->
						{/if}
						{if $ship_method == "E"}
							<p><b>Signature is NOT required on delivery by UPS, rerouting of packages will not be authorized.</b></p>
						{/if}
						{if $ship_method == "K"}
							<!-- UPS GROUND - NO MESSAGE -->
						{/if}
						{if $ship_method == "L"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												*You have chosen to have your order sent via {$delivery_method} Shipping. Signature is not required. Re-routing of packages is not allowed.
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						{/if}
						{if $ship_method == "S"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												Signature is NOT required on delivery by UPS, rerouting of packages will not be authorized.																				</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						{/if}
						{if $ship_method == "V"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												Signature is NOT required on delivery by UPS, rerouting of packages will not be authorized.																				</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						{/if}
						{if $ship_method == "U"}
							<!-- UPS FIRST CLASS - NO MESSAGE -->
						{/if}
						{if $ship_method == "W"}
							<!-- NO SHIPPING METHOD - NO MESSAGE -->
						{/if}
						{if $ship_method == "Y"}
						<table>
							<tr>
								<td width="560" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:left;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:.9em;">
												This is a Will Call order.  Please bring receipt and your ID to the Will Call at the front gate to pick-up your tickets.																</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						{/if}

						<table>
							{if $include_template != ""}
								{include file="include_template_API.tpl"}
							{/if}
						</table>

						{foreach from=$packages key=k item=item}
						{if $item.charac_birthday_child}	
						<table>
							<tr>
								<td width="565" bgcolor="#FFFFFF" style="border:solid #cccccc 1px;text-align:center;">
									<table>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;font-weight:bold;">
												{$item.package_name}
											</td>
										</tr>
										<tr>
											<td width="560" bgcolor="#FFFFFF" style="text-align:center;font-size:1.1em;">
												For <strong style="font-weight:bold;">{$item.charac_birthday_child}</strong>{if $item.charac_birthday_birth_date != "" && $show_birthday != "false"} ({$item.charac_birthday_sex}) born on {$item.charac_birthday_birth_date}{/if}
											</td>
										</tr>

									</table>
								</td>
							</tr>
						</table>
						{/if}
						{/foreach}
<br/>


						<table width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						
						<td width="600" style="text-align:left;">	
							{assign var=sch value=0}
							{foreach from=$ticketcharacs key=k item=item}
								
								{if $item.charac_group_sales_package_SN == "1"}
									{math equation="x + y" x=$sch y=1 assign='sch'}
								{/if}
							{/foreach}
							{if $sch > 0}
								{if $merchant_id == "12" OR $merchant_id == "22"}
								<span style="font-size:10px;color:#0f5fa8;line-height:200%;font-family:arial;text-decoration:none;">
									Boarding begins 30 minutes prior to the selected Launch Time. To expedite the check-in process on the day of arrival, the lead chaperone or guide should check in at the Group Sales/Will Call window of the admission booth with a copy of this final confirmation. The admission booth is located outside on the north side of the Space Needle. If tickets are in hand and have been printed in advance, proceed to the timed-ticket entry line and bypass the admission booth. Tickets are valid for a single visit. If additional tickets are needed, the Space Needle staff will try to accommodate the request; availability is not guaranteed. Groups have a 30 minute window prior to the selected Launch Time to enter the timed ticket line for elevator boarding. The specified Launch Time is listed on each ticket. If the Launch Time is missed, there is no guarantee that we can fit you in at a later time, as the capacity may be filled. Every guest should present their ticket upon entry for staff to scan. 
  <br/><br/>
All tickets are subject to a non-exempt City of Seattle Admissions Tax of 5%. All ticket purchases are final and non-refundable. 
  <br/><br/>
Everyone in the group is required to comply with the Space Needle safety rules and are cautioned to heed all warnings and instructions from the Space Needle staff. The Space Needle may refuse admission or expel any person whose conduct is objectionable. The Space Needle reserves the right to close in part or whole without notice for special events, holidays, and/or facility upgrades. Questions may be directed to 206-905-2180.<br/><br/>

								</span>

								{else}
								<span style="font-size:10px;color:#0f5fa8;line-height:200%;font-family:arial;text-decoration:none;">
									Please print all group tickets in advance from a home or office printer. Tickets will not be available at Will Call. Upon arrival to the Space Needle, bypass the main admission booth and proceed to the entrance ramp outside on the north side of the building. Enter the timed ticket line according to your selected Launch Time. Boarding begins 30 minutes prior to the Launch Time selected. Every guest should present their ticket upon entry for staff to scan. 
  <br/><br/>
All tickets are subject to a non-exempt City of Seattle Admissions Tax of 5%. All ticket purchases are final and non-refundable. 
  <br/><br/>
Everyone in the group is required to comply with the Space Needle safety rules and are cautioned to heed all warnings and instructions from the Space Needle staff. The Space Needle may refuse admission or expel any person whose conduct is objectionable. The Space Needle reserves the right to close in part or whole without notice for special events, holidays, and/or facility upgrades. Questions may be directed to 206-905-2180. <br/><br/>

								</span>
								{/if}
								<!--span style="font-size:10px;color:#0f5fa8;line-height:200%;font-family:arial;text-decoration:none;">
									ARRIVAL INFORMATION: A copy of this confirmation must be presented at the admissions booth to collect tickets. Group tickets may be collected at the Group Sales / Will Call window of the admissions booth, located on the north side of the Space Needle. Tickets are available for pick-up on the day of your visit at any time during building hours. If you have Print@Home tickets, please proceed past the admissions booth to the entry ramp where your ticket will be scanned. Tickets are valid for single visit only. Elevator wait times vary; please allow time for your group to wait for the elevator, especially during our peak season (May - September). We reserve the right to close in part or whole without notice for special events, holidays, and/or facility upgrades. Questions may be directed to 206-905-2180.
<br/><br/>
PAYMENT INFORMATION: All tickets are subject to a non-exempt City of Seattle admissions tax of 5%. All ticket purchases are final and non-refundable.
<br/><br/>
GUEST CONDUCT: As our guests, you are required to comply with our safety rules and are cautioned to heed all warnings and instructions from Space Needle staff. We may refuse admission or expel any person whose conduct is objectionable.<br/><br/>

								</span-->
							{/if}
						</td>
      					</tr>
			</table>


<br/>



						
<!-- ORDER SUMMARY -->
						<table>
							<tr>
								<td width="600" style="text-align:center;">
									<span style="font-size:1.4em;font-weight:bold;color:#040404;font-family:arial;text-align:center;">
										Order Summary
									</span>
								</td>
							</tr>
						</table>
						<table width="600">
							<tr>
								<td width="600" bgcolor="#0f5fa8" style="border:solid #cccccc 1px;text-align:left;">
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="300" bgcolor="#0f5fa8" style="color:white;text-align:left;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Product Description</b>
											</td>
											<td width="60" bgcolor="#0f5fa8" style="color:white;text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Type</b>
											</td>
											<td width="60" bgcolor="#0f5fa8" style="color:white;text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>SKU</b>
											</td>
											<td width="75" bgcolor="#0f5fa8" style="color:white;text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{if $redemption == "true"}<b>Reward Points</b>{else}<b>Unit Price</b>{/if}
											</td>
											<td width="30" bgcolor="#0f5fa8" style="color:white;text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Qty</b>
											</td>
											<td width="64" bgcolor="#0f5fa8" style="color:white;text-align:center;border-bottom:solid #cccccc 1px;">
												{if $redemption == "true"}<b>Reward Points</b>{else}<b>Price</b>{/if}
											</td>
										</tr>
									</table>
						
			<!-- DYNAMIC PURCHASES LOOP -->
			
									{foreach from=$packages key=k item=item}
									{if $item.bundle_parent_ticket_id != ""}
									{if $item.value != "0.00" || $show_bundle_children == "true"}								
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="300" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if $item.prefix != ""}{$item.prefix} - {/if}{$item.package_name|utf}{if isset($item.charac_resource_id) and $show_resources != "false"} ({$item.charac_resource_id}) {/if} {$item.event_start_date} {$item.event_start_time} {if isset($item.charac_row_name)}<br >Row:{$item.charac_row_name} Seat:{$item.charac_seat_name} Section:{$item.charac_section_name}{/if}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.customer_type_name|utf}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.package_id}
											</td>
											<td width="75" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if  $item.value != "0.00"}${$item.retail}{else}Bundle{/if}
											</td>
											<td width="31" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.qty}
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 1px;">
												{if  $item.value != "0.00"}${$item.line_total_price|string_format:"%.2f"}{else}Bundle{/if}
											</td>
										</tr>
									</table>

									{/if}
									{else}
									{if $item.package_name != ""}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="300" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if $item.prefix != ""}{$item.prefix} - {/if}{$item.package_name|utf}{if isset($item.charac_resource_id) and $show_resources != "false"} ({$item.charac_resource_id}) {/if} {$item.event_start_date} {$item.event_start_time} {if isset($item.charac_row_name)}<br >Row:{$item.charac_row_name} Seat:{$item.charac_seat_name} Section:{$item.charac_section_name}{/if}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.ticket_id}
											</td>
											<td width="60" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.package_id}
											</td>
											<td width="75" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{if $redemption == "true"}{$item.retail*1}{else}${$item.retail}{/if}
											</td>
											<td width="31" bgcolor="#ffffff" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
												{$item.qty}
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 1px;">
												{if $redemption == "true"}{$item.retail*1}{else}${$item.retail|string_format:"%.2f"}{/if} <!-- line_total_price -->
											</td>
										</tr>
									</table>
									{/if}

									{/if}
									{/foreach}
									<!-- END OF LOOP -->

							<!-- CHECK FOR REDEMPTIONS FIRST -->
							{if $redemption == "true"}


							{else}
							<!-- TAX (CHECKS FOR ADJUSTMENTS FIRST) -->
									{if $totals.total_adjustments == 0}
                                    				<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												Tax:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.tax_total|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}

							<!-- FEES -->	
									{if count($refunds) == 0}	
									{foreach from=$fees key=k item=v}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$k}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$v|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/foreach} 
									{/if}
						
							<!-- ADJUSTMENTS -->
									{foreach from=$adjustments key=k item=v}
									{if $v.original == 1 and $v.value != "0.00"}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{if $totals.total_adjustments != 0}Original {/if} {$v.cart_adjustment_name}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$v.value|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}	
									{/foreach}
							
									
								<!-- NO REFUNDS SHOW THIS -->
									<!-- REMOVING SUBTOTALS FOR NOW
									{if count($refunds) <= 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												Subtotal:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$total_subtotal|currency|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}
									-->
										
									{if $totals.total_adjustments == 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$delivery_method}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.shipping_total|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}									
									
									{if $totals.total_adjustments != 0}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												Original Additional Fees:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_fees|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												Original {$delivery_method}:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_shipping|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												Original Tax:
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
												{$totals.total_original_tax|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/if}
									
									{if $totals.total_adjustments != 0}
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
													Original Total:
												</td>
												<td width="64" bgcolor="#ffffff" style="text-align:center;border-bottom:solid #cccccc 0px;">
													{$totals.total_original|string_format:"%.2f"}
												</td>
											</tr>
										</table>
									{/if}
								{/if}<!-- THIS IS THE END OF THE REWARDS CHECK IF STATEMENT -->

			
					<!-- TOTALS -->
									{if count($refunds) == 0}
									<table  cellspacing="0" cellpadding="0">
										<tr>
											<td width="530" bgcolor="#ffffff" style="text-align:right;border-right:solid #cccccc 1px;">
												{if $redemption == "true"}<b>Total Reward Points:</b>{else}<b>Total:</b>{/if}
											</td>
											<td width="64" bgcolor="#ffffff" style="text-align:center;">
												{if $redemption == "true"}<b>{$totals.current_total*1}</b>{else}<b>${$totals.current_total|string_format:"%.2f"}</b>{/if}
											</td>
										</tr>
									</table>
									{/if}
									
					<!-- SCHEDULED PAYMENTS -->
									{if count($scheduled_payments) > 1}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="562" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												<b>Scheduled Payments</b>
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Payment</b>
											</td>
											<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												<b>Date</b>
											</td>
											<td width="100" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												<b>Amount</b>
											</td>
										</tr>
									</table>
									{foreach from=$scheduled_payments key=k item=item}
									<table cellspacing="0" cellpadding="0">
										<tr>
											<td width="60" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$k+1}
											</td>
											<td width="400" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
												{$item.date}
											</td>
											<td width="100" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
												${$item.amount|string_format:"%.2f"}
											</td>
										</tr>
									</table>
									{/foreach}
									{/if}


					<!-- YES REFUNDS SHOW THIS -->
									{if count($refunds) > 0}
										
										<table cellspacing="0" cellpadding="0">
											<tr height="10" bgcolor="#999999"><td height="10" bgcolor="#ffffff"></td></tr>
											<tr>
												<td width="568" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
													<b>Adjustments to Your Order</b>
												</td>
											</tr>
										</table>
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
													<b>Product Description</b>
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
													<b>Type</b>
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
													<b>SKU</b>
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;">
													<b>Unit Price</b>
												</td>
												
											</tr>
										</table>
						
										{foreach from=$refunds key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type == "T"}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{if $item.type == "A"}{$item.cart_adjustment_name}{else}{$item.package_name}{/if}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}

										{foreach from=$refunds key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type == "A" and $item.original == 0}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="298" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.cart_adjustment_name}
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}

										{foreach from=$refunds key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type == "S"}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$delivery_method}
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_plus_tax|string_format:"%.2f"}&nbsp;
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}


										{foreach from=$refund_fees key=k item=item}
											{if $item.retail_amount != "0.00" and $item.type != "T" and $item.original == "0"}
  												<table cellspacing="0" cellpadding="0">
													<tr>
														<td width="299" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{if $item.type == "S"}Delivery{elseif $item.type == "F" and $item.retail_amount > 0}Remaining Charge{elseif $item.type == "F" and $item.retail_amount < 0}Additional Fees{elseif $item.type == "A"}{$item.cart_adjustment_name}{else}{$item.package_name}{/if}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.customer_type}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;font-size:;">
															{$item.package_id}&nbsp;
														</td>
														<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;border-left:solid #cccccc 1px;font-size:;">
															{$item.retail_amount|string_format:"%.2f"}&nbsp; 
														</td>
														
													</tr>
												</table>
											{/if}
										{/foreach}
									{/if}
					<!-- end of refunds -->
					
					<!-- ADJUSTMENTS TOTAL -->
									{if $totals.total_adjustments != 0}
										<table cellspacing="0" cellpadding="0">
											<tr>
												<td width="481" bgcolor="#999999" style="text-align:right;border-right:solid #cccccc 1px;border-bottom:solid #cccccc 1px;">
													Adjustments Total:
												</td>
												<td width="90" bgcolor="#999999" style="text-align:center;border-bottom:solid #cccccc 1px;">
													{$totals.total_adjustments|string_format:"%.2f"}
												</td>
											</tr>
										</table>
									{/if}
								</td>
							</tr>
						</table>
            
            
            <!-- BOTTOM SECTION -->
						<table width="600" cellspacing="0" cellpadding="0" style="margin:10px 0 10px 0;">
							<tr>
								<td width="2"></td>
								<td width="175" bgcolor="#ffffff" style="text-align:center;border:solid #ffffff 0px;padding-left:5px;" valign="top">
									<b>Payment Received By</b><br>
									{$customer.name|utf}<br>
									{$phone}<br>
									{$email}<br>
								</td>
								
								<td width="2" bgcolor="#ffffff" style="text-align:center;border:solid #ffffff 0px;padding-left:0px;" valign="top">
									<img src="{$images_folder}orangeslice.jpg" />
								</td>

								
								
								
								<td width="175" bgcolor="#ffffff" style="text-align:center;border:solid #ffffff 0px;padding-left:5px;" valign="top">
									<b>Billing Information</b><br>
									{$billing_address.street|utf} {$billing_address.co_dept}<br>
									{$billing_address.city|utf}, {$billing_address.state} {$billing_address.zip}<br>
									{$billing_address.country}<br>
								</td>


								<td width="2" bgcolor="#ffffff" style="text-align:center;border:solid #ffffff 0px;padding-left:0px;" valign="top">
									<img src="{$images_folder}orangeslice.jpg" />
								</td>
				
							
								<td width="175" bgcolor="#ffffff" style="text-align:center;border:solid #ffffff 0px;padding-left:5px;" valign="top">
									<b>Payment Method</b><br>
									{if $payment.billing_type_desc != ""}{if $payment.billing_type_desc == "American Express"}AMEX{else}{$payment.billing_type_desc}{/if} {$payment.card_number|replace:'X':''} @ ${$payment.amount|string_format:"%.2f"}{else}No Payments{/if}<br>
									{if $payment2.billing_type_desc != ""}{if $payment2.billing_type_desc == "American Express"}AMEX{else}{$payment2.billing_type_desc}{/if} {$payment2.card_number|replace:'X':''} @ ${$payment2.amount|string_format:"%.2f"}<br>{/if}
									{if $payment3.billing_type_desc != ""}{if $payment3.billing_type_desc == "American Express"}AMEX{else}{$payment3.billing_type_desc}{/if} {$payment3.card_number|replace:'X':''} @ ${$payment3.amount|string_format:"%.2f"}<br>{/if}
									{if $payment4.billing_type_desc != ""}{if $payment4.billing_type_desc == "American Express"}AMEX{else}{$payment4.billing_type_desc}{/if} {$payment4.card_number|replace:'X':''} @ ${$payment4.amount|string_format:"%.2f"}<br>{/if}
									{if $payment5.billing_type_desc != ""}{if $payment5.billing_type_desc == "American Express"}AMEX{else}{$payment5.billing_type_desc}{/if} {$payment5.card_number|replace:'X':''} @ ${$payment5.amount|string_format:"%.2f"}<br>{/if}
									{if $payment6.billing_type_desc != ""}{if $payment6.billing_type_desc == "American Express"}AMEX{else}{$payment6.billing_type_desc}{/if} {$payment6.card_number|replace:'X':''} @ ${$payment6.amount|string_format:"%.2f"}<br>{/if}
									{if $payment7.billing_type_desc != ""}{if $payment7.billing_type_desc == "American Express"}AMEX{else}{$payment7.billing_type_desc}{/if} {$payment7.card_number|replace:'X':''} @ ${$payment7.amount|string_format:"%.2f"}<br>{/if}
									{if $payment8.billing_type_desc != ""}{if $payment8.billing_type_desc == "American Express"}AMEX{else}{$payment8.billing_type_desc}{/if} {$payment8.card_number|replace:'X':''} @ ${$payment8.amount|string_format:"%.2f"}<br>{/if}
								</td>

							</tr>
						</table>            
						
						{if $show_order_history == "true"}
            					<table width="569" cellspacing="0" cellpadding="0" style="margin:0px 0 10px 2px; border:solid #ccc 1px; padding:10px;">
            						<tr>
              						<td style="font-weight:bold; padding-bottom:10px;" colspan="3">Order History</td>
            						</tr>
            						{foreach from=$past_orders key=k item=item}
            						<tr>
              						<td width="242">{$item.customer_name|utf}</td>
              						<td width="203"><div align="right"><span class="style2">{$item.order_date}</span></div></td>
              						<td width="205"><div align="right"><a href="{$receipt_path}{$receipt_name}?ph={$ph}&order_id={$item.order_id}&user_id={$user_id}&lang={$lang}">View Order #{$item.order_id}</a></div>
              					</td>
            						</tr>
            						{/foreach}
          					</table> 
						{/if}						
					</td> <!-- END OF COLUMN TD-->
				</tr> <!-- END OF COLUMN TR-->
			</table> <!-- END OF COLUMN TABLE-->
			
			
     
			
			<table width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
				
				
				<table width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						
						<td width="600" style="text-align:center;">
							{if $customer_support_number != ""}
							<span style="font-size:11px;color:#0f5fa8;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								{if $customer_support_number_title != ""}{$customer_support_number_title}{else}Technical Support{/if} {$customer_support_number}
							</span><br>
							{/if}
							{if $customer_support_number_2 != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								{if $customer_support_number_title_2 != ""}{$customer_support_number_title_2}{else}Technical Support{/if}: {$customer_support_number_2}
							</span><br>
							{/if}

							{if $customer_support_link != "" and $customer_support_link_title != ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								<a href="{$customer_support_link}" target="_blank">{$customer_support_link_title}</a>
							</span><br>
							{/if}
							{if $customer_support_link != "" and $customer_support_link_title == ""}
							<span style="font-size:11px;color:#996600;line-height:200%;font-family:arial;text-decoration:none;font-weight:bold;">
								<a href="{$customer_support_link}" target="_blank">Click here for Technical Support</a>
							</span><br>
							{/if}
							
							<span style="font-size:10px;color:#0f5fa8;line-height:200%;font-family:arial;text-decoration:none;">
							Email not displaying correctly? <a title="Receipt Version {$version}" href="{$receipt_path}receipt.php?ph={$ph}&order_id={$order_id}&user_id={$user_id}&lang={$lang}{$print_at_home_vars}" target="_blank" style="font-size:10px;color:#0f5fa8;font-family:arial;text-decoration:underline;">
							View it in your browser.</a>
							</span>

							
						</td>
					
						
					</tr>
				</table>
			</table>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
