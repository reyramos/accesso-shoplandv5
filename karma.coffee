# Testacular configuration

# base path, that will be used to resolve files and exclude
basePath = ''

# list of files / patterns to load in the browser
files = [
  JASMINE
  JASMINE_ADAPTER
	'app/lib/sockjs/index.js'
	'app/lib/ua-parser-js/src/ua-parser.js'
  'app/lib/requirejs/require.js'
  'app/lib/angular/index.js'
  'app/lib/angular-mobile/index.js'
  'app/lib/angular-mocks/index.js'
  'app/lib/angular-hammer/index.js'
	'app/lib/hammerjs/dist/hammer.js'
	'app/lib/modernizr/modernizr.js'
	'app/js/*.js'
  'app/js/**/*.js'
  'test/mock/**/*.coffee'
  'test/spec/**/*.coffee'
]

# list of files to exclude
exclude = []

# test results reporter to use
# possible values: dots || progress
reporter = 'progress'

# web server port
port = 8081

# cli runner port
runnerPort = 9100

# enable / disable colors in the output (reporters and logs)
colors = true

# level of logging
# possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
logLevel = LOG_INFO

# enable / disable watching file and executing tests whenever any file changes
autoWatch = false

# Start these browsers, currently available:
# - Chrome
# - ChromeCanary
# - Firefox
# - Opera
# - Safari
# - PhantomJS
browsers = ['PhantomJS']

# Continuous Integration mode
# if true, it capture browsers, run tests and exit
singleRun = false
