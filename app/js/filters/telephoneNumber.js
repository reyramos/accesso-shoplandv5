/**
 * # Telephone Number Filter
 *
 * A filter that formats a telephone number.
 */

angular.module('app').filter
	('telephoneNumber',['$log',
	, function($log){
		return function(tel){
			if(!tel){
				return ''
			}

			var value = tel.toString().trim().replace(/^\+/, '')

			if(value.match(/[^0-9]/)){
				return tel
			}

			var areaCode, firstThree, lastFour

			switch(value.length){
				case 10: // +1PPP####### -> C(PPP) ###-####
					areaCode = 1
					firstThree = value.slice(0, 3)
					lastFour = value.slice(3)
					break

				case 11: // +CPPP####### -> CCC(PP) ###-####
					areaCode = value[0]
					firstThree = value.slice(1, 4)
					lastFour = value.slice(4)
					break

				case 12: // +CCCPP####### -> CCC(PP) ###-####
					areaCode = value.slice(0, 3)
					firstThree = value.slice(3, 5)
					lastFour = value.slice(5)
					break

				default:
					return tel
			}

			if(areaCode == 1){
				areaCode = ""
			}

			lastFour = lastFour.slice(0, 3) + '-' + lastFour.slice(3)
			return("(" + firstThree + ") " + lastFour).trim()
			//return(areaCode + "(" + firstThree + ") " + lastFour).trim()
		}
	}])
