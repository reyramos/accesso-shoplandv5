/**
 * # Filters
 *
 * Helpers to be used in templates to format data displayed to a user
 */

angular.module('app').filter
	( 'range'
	, function(){
			return function(arr, lower, upper){
				for (var i = lower; i <= upper; i++) arr.push(i)
				return arr
			}
		}
	)

angular.module('app').filter
	( 'JSONtoString'
	, function(){
			return function(obj){
				try{
					return JSON.stringify(obj);
				}
				catch(error){
					return JSON.stringify(error);
				}
			}
		}
	)

angular.module('app').filter
	( 'truncate',['$window'
	, function ($window){
			return function (text, length, end, ellipsis){
				if(!text || text == undefined)
					return ''

				if(typeof length == 'string'){
					var handler = length.split(":")
					length = $window.innerWidth <= 320 ? Number(handler[0]) : Number(handler[1])
				}

				var str = text
				  , maxChars = length
				  , appendEllipsis = ellipsis || true

				if(str.length > maxChars){
					var arr = str.split(' ') // Array of words.
					  , s = '' // Final string.
					  , lsum = 0 // Local Sum of the string length.
					  , i // Iterator.
					  , ls // Local String in for loop.

					for(i = 0; i < arr.length; i++){
						ls = arr[i]
						lsum += ls.length

						if(lsum <= maxChars){
							if(s != '')
								s += ' '
							
							s += arr[i]
						} else {
							// If we don't have at least one word, set s = to the first word.
							if(s == '')
								s = arr[0]

							break
						}
					}

					// Append ellipsis.
					if(appendEllipsis) s += "..."

					return s
				}

				// Return original string. No formatting applied.
				return str
			}
		}]
	)

angular.module('app').filter
	('rightSubstring'
	, function(){
			return function (value, length){
				string = '' + value
				return string.substring(string.length - length)
			}
		}
	)

angular.module('app').filter
	('leadingZeros'
	, function(){
			return function (value, length){
				string = '' + value
				while (string.length < length) string = '0' + string
				return string
			}
		}
	)

angular.module('app').filter
	( 'camelCase',
		function (){
			return function (text){
				var str = text.toLowerCase();
				var arr = str.split(" ");
				str = arr[0];
				for( i in arr){
					if(i > 0){
						str += arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
					}
				}
				return str
			}
		}
	)