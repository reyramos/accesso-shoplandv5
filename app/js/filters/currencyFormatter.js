angular.module( 'app' ).filter
( 'currencyFormatter'
, [   'iso4217'
	, function( iso4217 ){
		var formatter =
			{ format: function( amount, currencyCode, langCode ){
					return amount;
			  }
			, getOptionByCode: function( formatOptions, currencyCode, langCode ){
					// Get the currency format by currencyCode. If array of objects, then select by langCode.
					var options = formatOptions[ currencyCode.toUpperCase() ];
					if( options ){
						if( angular.isArray( options ) ){
							var optionsObj;

							if( langCode ){
								for( var i = 0; i < options.length; i++ ){
									if( options[ i ][ langCode ] ){
										optionsObj = options[ i ][ langCode ];
										break;
									}
								}
							}

							if( !optionsObj ){
								// No options for this currencyCode + langCode. Should we use USD by default?
								return null;
							} else {
								options = optionsObj;
							}
						}
					} else {
						// No options for this currencyCode. Should we use USD by default?
						return null;
					}

					return options;
			  }
			}

			return function( amount, currencyCode, langCode ){
				// Don't process the undefined values, anything else should get processed.
				if( typeof amount == 'undefined' ){
					return '';
				} else if( isNaN( Number( amount ) ) ){
					// Could be the word 'Free'.
					return amount;
				}

				// CONSTs.
				var LEFT = 'left';
				var RIGHT = 'right';

				// Local properties.
				var i
				  , value
				  , amountArr
				  , wholeAmtStr
				  , finalAmtStr = ''
				  , options
				  , lessThanZero = amount < 0;
					
				if( lessThanZero ){
					amount = amount * -1;
				}

				if( currencyCode ){
					options = formatter.getOptionByCode( iso4217, currencyCode, langCode );
				}

				// Use USD as default.
				if( !options ){
					options = formatter.getOptionByCode( iso4217, 'USD' );
				}

				// Set local references.
				symbol = options.symbol;
				decSeparator = options.decSeparator;
				thousandSeparator = options.thousandSeparator;
				symbolPlacement = options.symbolPlacement;
				precision = options.precision;
				leadingZeros = options.leadingZeros;

				if( symbolPlacement != LEFT ){
					symbolPlacement = RIGHT;
				}

				value = amount.toString().trim().replace( /^\+/, '' );

				var n = Number( value );
				n = n.toFixed( precision );

				amountArr = n.split( '.' );
				wholeAmtStr = amountArr[ 0 ];

				var c = 0;
				for( i = wholeAmtStr.length; i > 0; i-- ){
					if( c % 3 == 0 && c != 0 ){
						// Is thousand separator always in the same place?
						finalAmtStr = thousandSeparator + finalAmtStr;
					}

					finalAmtStr = wholeAmtStr.charAt( i - 1 ) + finalAmtStr;
					c++;
				}

				if( n < 1 ){
					for( i = 0; i < leadingZeros - 1; i++ ){
						finalAmtStr += '0';
					}
				}
				
				if( symbolPlacement == LEFT ){
					finalAmtStr = symbol + finalAmtStr + decSeparator + amountArr[ 1 ];
				} else {
					finalAmtStr = finalAmtStr + decSeparator + amountArr[ 1 ] + symbol;
				}
				
				if( lessThanZero ){
					finalAmtStr = '-' + finalAmtStr;
				}
				
				return finalAmtStr;
			}
	  }
  ]
)
