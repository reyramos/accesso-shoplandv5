/**
 * # Global countries and regions enumerations.
 * 
 * For some reason, the word `native` is causing an error on some browsers.
 * The error message shows as up: "Uncaught SyntaxError: Unexpected token native"  
 */
angular.module('app').factory
	( 'countriesJSon'
	, function() {

			var data =
			{ countries:
				[
					{ code: "CA"
					, name: "Canada"
					, native: "Canada"
					}
				,	{ code: "MX"
					, name: "Mexico"
					, native: "Mexico"
					}
				,	{ code: "US"
					, name: "United States"
					, native: "United States"
					}
				,	{ code: "UK"
					, name: "United Kingdom"
					, native: "United Kingdom"
					}
				/*
					{ code: "AF"
					, name: "Afghanistan"
					, native: "Afghanestan"
					}
				, { code: "AL"
					, name: "Albania"
					, native: "Shqiperia"
					}
				, { code: "DZ"
					, name: "Algeria"
					, native: "Al Jaza\"ir"
					}
				, { code: "AD"
					, name: "Andorra"
					, native: "Andorra"
					}
				, { code: "AO"
					, name: "Angola"
					, native: "Angola"
					}
				, { code: "AG"
					, name: "Antigua and Barbuda"
					, native: "Antigua and Barbuda"
					}
				, { code: "AR"
					, name: "Argentina"
					, native: "Argentina"
					}
				, { code: "AM"
					, name: "Armenia"
					, native: "Hayastan"
					}
				, { code: "AU"
					, name: "Australia"
					, native: "Australia"
					}
				, { code: "AT"
					, name: "Austria"
					, native: "Oesterreich"
					}
				, { code: "AZ"
					, name: "Azerbaijan"
					, native: "Azarbaycan Respublikasi"
					}
				, { code: "BS"
					, name: "Bahamas"
					, native: "The Bahamas"
					}
				, { code: "BH"
					, name: "Bahrain"
					, native: "Al Bahrayn"
					}
				, { code: "BD"
					, name: "Bangladesh"
					, native: "Bangladesh"
					}
				, { code: "BB"
					, name: "Barbados"
					, native: "Barbados"
					}
				, { code: "BY"
					, name: "Belarus"
					, native: "Byelarus"
					}
				, { code: "BE"
					, name: "Belgium"
					, native: "Belgique (French) or Belgie (Flemish)", "fr-BE": "Belgique", "nl-BE": "Belgie"
					}
				, { code: "BZ"
					, name: "Belize"
					, native: "Belice"
					}
				, { code: "BJ"
					, name: "Benin"
					, native: "Benin"
					}
				, { code: "BT"
					, name: "Bhutan"
					, native: "Drukyul"
					}
				, { code: "BO"
					, name: "Bolivia"
					, native: "Bolivia, Plurinational State of"
					}
				, { code: "BQ"
					, name: "Bonaire, Sint Eustatius and Saba"
					}
				, { code: "BA"
					, name: "Bosnia and Herzegovina"
					, native: "Bosna i Hercegovina"
					}
				, { code: "BW"
					, name: "Botswana"
					, native: "Botswana"
					}
				, { code: "BR"
					, name: "Brazil"
					, native: "Brasil"
					}
				, { code: ""
					, name: "Brunei"
					, native: "Brunei"
					}
				, { code: ""
					, name: "Bulgaria"
					, native: "Republika Bulgariya"
					}
				, { code: ""
					, name: "Burkina Faso"
					, native: "Burkina Faso"
					}
				, { code: ""
					, name: "Burundi"
					, native: "Burundi"
					}
				, { code: ""
					, name: "Cambodia"
					, native: "Kampuchea"
					}
				, { code: ""
					, name: "Cameroon"
					, native: "Cameroon or Cameroun (French)", "fr": "Cameroun"
					}
				, { code: ""
					, name: "Cape Verde"
					, native: "Cabo Verde"
					}
				, { code: ""
					, name: "Central African Republic"
					, native: "Republique Centrafricaine"
					}
				, { code: ""
					, name: "Chad"
					, native: "Tchad"
					}
				, { code: ""
					, name: "Chile"
					, native: "Chile"
					}
				, { code: ""
					, name: "China"
					, native: "Zhong Guo"
					}
				, { code: ""
					, name: "Colombia"
					, native: "Colombia"
					}
				, { code: ""
					, name: "Comoros"
					, native: "Comores"
					}
				, { code: ""
					, name: "Congo, Republic of the"
					, native: "Republique du Congo"
					}
				, { code: ""
					, name: "Congo, Democratic Republic of the"
					, native: "Republique Democratique du Congo"
					}
				, { code: ""
					, name: "Costa Rica"
					, native: "Costa Rica"
					}
				, { code: ""
					, name: "Cote d\"Ivoire"
					, native: "Cote d\"Ivoire"
					}
				, { code: ""
					, name: "Croatia"
					, native: "Hrvatska"
					}
				, { code: ""
					, name: "Cuba"
					, native: "Cuba"
					}
				, { code: ""
					, name: "Cyprus"
					, native: "Kypros (Greek) or Kibris (Turkish)", "gr": "Kypros", "tr": "Kibris"
					}
				, { code: ""
					, name: "Czech Republic"
					, native: "Ceska Republika"
					}
				, { code: ""
					, name: "Denmark"
					, native: "Danmark"
					}
				, { code: ""
					, name: "Djibouti"
					, native: "Djibouti"
					}
				, { code: ""
					, name: "Dominica"
					, native: "Dominica"
					}
				, { code: ""
					, name: "Dominican Republic"
					, native: "Republica Dominicana"
					}
				, { code: ""
					, name: "Ecuador"
					, native: "Ecuador"
					}
				, { code: ""
					, name: "Egypt"
					, native: "Misr"
					}
				, { code: ""
					, name: "El Salvador"
					, native: "El Salvador"
					}
				, { code: ""
					, name: "Equatorial Guinea"
					, native: "Guinea Ecuatorial"
					}
				, { code: ""
					, name: "Eritrea"
					, native: "Ertra"
					}
				, { code: ""
					, name: "Estonia"
					, native: "Eesti"
					}
				, { code: ""
					, name: "Ethiopia"
					, native: "YeItyop\"iya"
					}
				, { code: ""
					, name: "Fiji"
					, native: "Fiji"
					}
				, { code: ""
					, name: "Finland"
					, native: "Suomi"
					}
				, { code: ""
					, name: "France"
					, native: "France or Republique Francaise"
					}
				, { code: ""
					, name: "Gabon"
					, native: "Gabon"
					}
				, { code: ""
					, name: "The Gambia"
					, native: "The Gambia"
					}
				, { code: ""
					, name: "Georgia"
					, native: "Sak\"art\"velo"
					}
				, { code: ""
					, name: "Germany"
					, native: "Deutschland"
					}
				, { code: ""
					, name: "Ghana"
					, native: "Ghana"
					}
				, { code: ""
					, name: "Greece"
					, native: "Ellas"
					}
				, { code: ""
					, name: "Grenada"
					, native: "Grenada"
					}
				, { code: ""
					, name: "Guatemala"
					, native: "Guatemala"
					}
				, { code: ""
					, name: "Guinea"
					, native: "Guinee"
					}
				, { code: ""
					, name: "Guinea-Bissau"
					, native: "Guine-Bissau"
					}
				, { code: ""
					, name: "Guyana"
					, native: "Guyana"
					}
				, { code: ""
					, name: "Haiti"
					, native: "Haiti"
					}
				, { code: ""
					, name: "Honduras"
					, native: "Honduras"
					}
				, { code: ""
					, name: "Hungary"
					, native: "Magyarorszag"
					}
				, { code: ""
					, name: "Iceland"
					, native: "Island"
					}
				, { code: ""
					, name: "India"
					, native: "India, Bharat"
					}
				, { code: ""
					, name: "Indonesia"
					, native: "Indonesia"
					}
				, { code: ""
					, name: "Iran"
					, native: "Iran, Persia"
					}
				, { code: ""
					, name: "Iraq"
					, native: "Al Iraq"
					}
				, { code: ""
					, name: "Ireland"
					, native: "Ireland or Eire"
					}
				, { code: ""
					, name: "Israel"
					, native: "Yisra\"el"
					}
				, { code: ""
					, name: "Italy"
					, native: "Italia"
					}
				, { code: ""
					, name: "Jamaica"
					, native: "Jamaica"
					}
				, { code: ""
					, name: "Japan"
					, native: "Nippon"
					}
				, { code: ""
					, name: "Jordan"
					, native: "Al Urdun"
					}
				, { code: ""
					, name: "Kazakhstan"
					, native: "Qazaqstan"
					}
				, { code: ""
					, name: "Kenya"
					, native: "Kenya"
					}
				, { code: ""
					, name: "Kiribati"
					, native: "Kiribati"
					}
				, { code: ""
					, name: "Korea, North"
					, native: "Choson or Choson-minjujuui-inmin-konghwaguk"
					}
				, { code: ""
					, name: "Korea, South"
					, native: "Taehan-min\"guk"
					}
				, { code: ""
					, name: "Kuwait"
					, native: "Al Kuwayt"
					}
				, { code: ""
					, name: "Kyrgyzstan"
					, native: "Kyrgyz Respublikasy"
					}
				, { code: ""
					, name: "Laos"
					, native: "Sathalanalat Paxathipatai Paxaxon Lao"
					}
				, { code: ""
					, name: "Latvia"
					, native: "Latvija"
					}
				, { code: ""
					, name: "Lebanon"
					, native: "Lubnan"
					}
				, { code: ""
					, name: "Lesotho"
					, native: "Lesotho"
					}
				, { code: ""
					, name: "Liberia"
					, native: "Liberia"
					}
				, { code: ""
					, name: "Libya"
					, native: "Libya"
					}
				, { code: ""
					, name: "Liechtenstein"
					, native: "Liechtenstein"
					}
				, { code: ""
					, name: "Lithuania"
					, native: "Lietuva"
					}
				, { code: ""
					, name: "Luxembourg"
					, native: "Luxembourg"
					}
				, { code: ""
					, name: "Macedonia"
					, native: "Makedonija"
					}
				, { code: ""
					, name: "Madagascar"
					, native: "Madagascar"
					}
				, { code: ""
					, name: "Malawi"
					, native: "Malawi"
					}
				, { code: ""
					, name: "Malaysia"
					, native: "Malaysia"
					}
				, { code: ""
					, name: "Maldives"
					, native: "Dhivehi Raajje"
					}
				, { code: ""
					, name: "Mali"
					, native: "Mali"
					}
				, { code: ""
					, name: "Malta"
					, native: "Malta"
					}
				, { code: ""
					, name: "Marshall Islands"
					, native: "Marshall Islands"
					}
				, { code: ""
					, name: "Mauritania"
					, native: "Muritaniyah"
					}
				, { code: ""
					, name: "Mauritius"
					, native: "Mauritius"
					}
				, { code: ""
					, name: "Federated States of Micronesia"
					, native: "Federated States of Micronesia"
					}
				, { code: ""
					, name: "Moldova"
					, native: "Moldova"
					}
				, { code: ""
					, name: "Monaco"
					, native: "Monaco"
					}
				, { code: ""
					, name: "Mongolia"
					, native: "Mongol Uls"
					}
				, { code: ""
					, name: "Morocco"
					, native: "Al Maghrib"
					}
				, { code: ""
					, name: "Mozambique"
					, native: "Mocambique"
					}
				, { code: ""
					, name: "Myanmar (Burma)"
					, native: "Myanma Naingngandaw"
					}
				, { code: ""
					, name: "Namibia"
					, native: "Namibia"
					}
				, { code: ""
					, name: "Nauru"
					, native: "Nauru"
					}
				, { code: ""
					, name: "Nepal"
					, native: "Nepal"
					}
				, { code: ""
					, name: "Netherlands"
					, native: "Nederland"
					}
				, { code: ""
					, name: "New Zealand"
					, native: "New Zealand"
					}
				, { code: ""
					, name: "Nicaragua"
					, native: "Nicaragua"
					}
				, { code: ""
					, name: "Niger"
					, native: "Niger"
					}
				, { code: ""
					, name: "Nigeria"
					, native: "Nigeria"
					}
				, { code: ""
					, name: "Norway"
					, native: "Norge"
					}
				, { code: ""
					, name: "Oman"
					, native: "Uman"
					}
				, { code: ""
					, name: "Pakistan"
					, native: "Pakistan"
					}
				, { code: ""
					, name: "Palau"
					, native: "Belau"
					}
				, { code: ""
					, name: "Panama"
					, native: "Panama"
					}
				, { code: ""
					, name: "Papua New Guinea"
					, native: "Papua New Guinea"
					}
				, { code: ""
					, name: "Paraguay"
					, native: "Paraguay"
					}
				, { code: ""
					, name: "Peru"
					, native: "Peru"
					}
				, { code: ""
					, name: "Philippines"
					, native: "Pilipinas"
					}
				, { code: ""
					, name: "Poland"
					, native: "Polska"
					}
				, { code: ""
					, name: "Portugal"
					, native: "Portugal"
					}
				, { code: ""
					, name: "Qatar"
					, native: "Qatar"
					}
				, { code: ""
					, name: "Romania"
					, native: "Romania"
					}
				, { code: ""
					, name: "Russia"
					, native: "Rossiya"
					}
				, { code: ""
					, name: "Rwanda"
					, native: "Rwanda"
					}
				, { code: ""
					, name: "Saint Kitts and Nevis"
					, native: "Saint Kitts and Nevis"
					}
				, { code: ""
					, name: "Saint Lucia"
					, native: "Saint Lucia"
					}
				, { code: ""
					, name: "Samoa"
					, native: "Samoa"
					}
				, { code: ""
					, name: "San Marino"
					, native: "San Marino"
					}
				, { code: ""
					, name: "Sao Tome and Principe"
					, native: "Sao Tome e Principe"
					}
				, { code: ""
					, name: "Saudi Arabia"
					, native: "Al Arabiyah as Suudiyah"
					}
				, { code: ""
					, name: "Senegal"
					, native: "Senegal"
					}
				, { code: ""
					, name: "Serbia and Montenegro (Federal Republic of Yugoslavia)"
					, native: "Srbija-Crna Gora"
					}
				, { code: ""
					, name: "Seychelles"
					, native: "Seychelles"
					}
				, { code: ""
					, name: "Sierra Leone"
					, native: "Sierra Leone"
					}
				, { code: ""
					, name: "Singapore"
					, native: "Singapore"
					}
				, { code: ""
					, name: "Slovakia"
					, native: "Slovensko"
					}
				, { code: ""
					, name: "Slovenia"
					, native: "Slovenija"
					}
				, { code: ""
					, name: "Solomon Islands"
					, native: "Solomon Islands"
					}
				, { code: ""
					, name: "Somalia"
					, native: "Somalia"
					}
				, { code: ""
					, name: "South Africa"
					, native: "South Africa"
					}
				, { code: ""
					, name: "Spain"
					, native: "Espana"
					}
				, { code: ""
					, name: "Sri Lanka"
					, native: "Sri Lanka"
					}
				, { code: ""
					, name: "Sudan"
					, native: "As-Sudan"
					}
				, { code: ""
					, name: "Suriname"
					, native: "Suriname"
					}
				, { code: ""
					, name: "Swaziland"
					, native: "Swaziland"
					}
				, { code: ""
					, name: "Sweden"
					, native: "Sverige"
					}
				, { code: ""
					, name: "Switzerland"
					, native: "Schweiz (German), Suisse (French), Svizzera (Italian)", "de": "Schweiz", "fr": "Suisse", "it": "Svizzera"
					}
				, { code: ""
					, name: "Syria"
					, native: "Suriyah"
					}
				, { code: ""
					, name: "Taiwan"
					, native: "T\"ai-wan"
					}
				, { code: ""
					, name: "Tajikistan"
					, native: "Jumhurii Tojikistan"
					}
				, { code: ""
					, name: "Tanzania"
					, native: "Tanzania"
					}
				, { code: ""
					, name: "Thailand"
					, native: "Muang Thai"
					}
				, { code: ""
					, name: "Tolo"
					, native: "Togo"
					}
				, { code: ""
					, name: "Tonga"
					, native: "Tonga"
					}
				, { code: ""
					, name: "Trinidad and Tobago"
					, native: "Trinidad and Tobago"
					}
				, { code: ""
					, name: "Tunisia"
					, native: "Tunis"
					}
				, { code: ""
					, name: "Turkey"
					, native: "Turkiye"
					}
				, { code: ""
					, name: "Turkmenistan"
					, native: "Turkmenistan"
					}
				, { code: ""
					, name: "Tuvalu"
					, native: "Tuvalu"
					}
				, { code: ""
					, name: "Uganda"
					, native: "Uganda"
					}
				, { code: ""
					, name: "Ukraine"
					, native: "Ukrayina"
					}
				, { code: ""
					, name: "United Arab Emirates"
					, native: "Al Imarat al Arabiyah al Muttahidah"
					}
				, { code: ""
					, name: "Uruguay"
					, native: "Uruguay"
					}
				, { code: ""
					, name: "Uzbekistan"
					, native: "Uzbekiston Respublikasi"
					}
				, { code: ""
					, name: "Vanuatu"
					, native: "Vanuatu"
					}
				, { code: ""
					, name: "Vatican City (Holy See)"
					, native: "Santa Sede (Citta del Vaticano)"
					}
				, { code: ""
					, name: "Venezuela"
					, native: "Venezuela"
					}
				, { code: ""
					, name: "Vietnam"
					, native: "Viet Nam"
					}
				, { code: ""
					, name: "Yemen"
					, native: "Al Yaman"
					}
				, { code: ""
					, name: "Zambia"
					, native: "Zambia"
					}
				, { code: ""
					, name: "Zimbabwe"
					, native: "Zimbabwe"
					}
				*/
				]
			, regions:
				{ "CA":
					[
						{ code: "AB"
						, name: "Alberta"
						}
					, { code: "BC"
						, name: "British Columbia", "en": "British Columbia", "fr": "Colombie-Britannique"
						}
					, { code: "MB"
						, name: "Manitoba"
						}
					, { code: "NB"
						, name: "New Brunswick", "en": "New Brunswick", "fr": "Nouveau-Brunswick"
						}
					, { code: "NL"
						, name: "Newfoundland and Labrador", "en": "Newfoundland and Labrador", "fr": "Terre-Neuve-et-Labrador"
						}
					, { code: "NT"
						, name: "Northwest Territories", "en": "Northwest Territories", "fr": "Territoires du Nord-Ouest"
						}
					, { code: "NS"
						, name: "Nova Scotia", "en": "Nova Scotia", "fr": "Nouvelle-Écosse"
						}
					, { code: "NU"
						, name: "Nunavut"
						}
					, { code: "ON"
						, name: "Ontario"
						}
					, { code: "PE"
						, name: "Prince Edward Island", "en": "Prince Edward Island", "fr": "Nouvelle-Écosse"
						}
					, { code: "QC"
						, name: "Québec"
						}
					, { code: "SK"
						, name: "Saskatchewan"
						}
					, { code: "YT"
						, name: "Yukon"
						}
					]
					, "MX":
					[
						{ code: "AG"
						, name: "Aguascalientes"
						}
					, { code: "BC"
						, name: "Baja California"
						}
					, { code: "BS"
						, name: "Baja California Sur"
						}
					, { code: "CM"
						, name: "Campeche"
						}
					, { code: "CS"
						, name: "Chiapas"
						}
					, { code: "CH"
						, name: "Chihuahua"
						}
					, { code: "CO"
						, name: "Coahuila"
						}
					, { code: "CL"
						, name: "Colima"
						}
					, { code: "DF"
						, name: "Federal District"
						}
					, { code: "DG"
						, name: "Durango"
						}
					, { code: "GT"
						, name: "Guanajuato"
						}
					, { code: "GR"
						, name: "Guerrero"
						}
					, { code: "HG"
						, name: "Hidalgo"
						}
					, { code: "JA"
						, name: "Jalisco"
						}
					, { code: "ME"
						, name: "Mexico State"
						}
					, { code: "MI"
						, name: "Michoacán"
						}
					, { code: "MO"
						, name: "Morelos"
						}
					, { code: "NA"
						, name: "Nayarit"
						}
					, { code: "NL"
						, name: "Nuevo León"
						}
					, { code: "OA"
						, name: "Oaxaca"
						}
					, { code: "PB"
						, name: "Puebla"
						}
					, { code: "QE"
						, name: "Querétaro"
						}
					, { code: "QR"
						, name: "Quintana Roo"
						}
					, { code: "SL"
						, name: "San Luis Potosí"
						}
					, { code: "SI"
						, name: "Sinaloa"
						}
					, { code: "SO"
						, name: "Sonora"
						}
					, { code: "TB"
						, name: "Tabasco"
						}
					, { code: "TM"
						, name: "Tamaulipas"
						}
					, { code: "TL"
						, name: "Tlaxcala"
						}
					, { code: "VE"
						, name: "Veracruz"
						}
					, { code: "YU"
						, name: "Yucatán"
						}
					, { code: "ZA"
						, name: "Zacatecas"
						}
					]
					,"US":
					[
						{ code: "AL"
						, name: "Alabama"
						}
					, { code: "AK"
						, name: "Alaska"
						}
					, { code: "AZ"
						, name: "Arizona"
						}
					, { code: "AR"
						, name: "Arkansas"
						}
					, { code: "CA"
						, name: "California"
						}
					, { code: "CO"
						, name: "Colorado"
						}
					, { code: "CT"
						, name: "Connecticut"
						}
					, { code: "DC"
						, name: "District of Columbia"
						}
					, { code: "DE"
						, name: "Delaware"
						}
					, { code: "FL"
						, name: "Florida"
						}
					, { code: "GA"
						, name: "Georgia"
						}
					, { code: "HI"
						, name: "Hawaii"
						}
					, { code: "ID"
						, name: "Idaho"
						}
					, { code: "IL"
						, name: "Illinois"
						}
					, { code: "IN"
						, name: "Indiana"
						}
					, { code: "IA"
						, name: "Iowa"
						}
					, { code: "KS"
						, name: "Kansas"
						}
					, { code: "KY"
						, name: "Kentucky"
						}
					, { code: "LA"
						, name: "Louisiana"
						}
					, { code: "ME"
						, name: "Maine"
						}
					, { code: "MD"
						, name: "Maryland"
						}
					, { code: "MA"
						, name: "Massachusetts"
						}
					, { code: "MI"
						, name: "Michigan"
						}
					, { code: "MN"
						, name: "Minnesota"
						}
					, { code: "MS"
						, name: "Mississippi"
						}
					, { code: "MO"
						, name: "Missouri"
						}
					, { code: "MT"
						, name: "Montana"
						}
					, { code: "NE"
						, name: "Nebraska"
						}
					, { code: "NV"
						, name: "Nevada"
						}
					, { code: "NH"
						, name: "New Hampshire"
						}
					, { code: "NJ"
						, name: "New Jersey"
						}
					, { code: "NM"
						, name: "New Mexico"
						}
					, { code: "NY"
						, name: "New York"
						}
					, { code: "NC"
						, name: "North Carolina"
						}
					, { code: "ND"
						, name: "North Dakota"
						}
					, { code: "OH"
						, name: "Ohio"
						}
					, { code: "OK"
						, name: "Oklahoma"
						}
					, { code: "OR"
						, name: "Oregon"
						}
					, { code: "PA"
						, name: "Pennsylvania"
						}
					, { code: "RI"
						, name: "Rhode Island"
						}
					, { code: "SC"
						, name: "South Carolina"
						}
					, { code: "SD"
						, name: "South Dakota"
						}
					, { code: "TN"
						, name: "Tennessee"
						}
					, { code: "TX"
						, name: "Texas"
						}
					, { code: "UT"
						, name: "Utah"
						}
					, { code: "VT"
						, name: "Vermont"
						}
					, { code: "VA"
						, name: "Virginia"
						}
					, { code: "WA"
						, name: "Washington"
						}
					, { code: "WV"
						, name: "West Virginia"
						}
					, { code: "WI"
						, name: "Wisconsin"
						}
					, { code: "WY"
						, name: "Wyoming"
						}
					]
				}
			}

		return data
		}
	)


//, { code: "AX", name: "Åland Islands"}
//, { code: "AS", name: "American Samoa"}
//, { code: "AI", name: "Anguilla"}
//, { code: "AI", name: "Antarctica"}
//, { code: "AW", name: "Aruba"}
//, { code: "BM", name: "Bermuda"}
//, { code: "BV", name: "BOUVET ISLAND"}
