/**
 * # Settings for Extra Flow Views
 *
 */

angular.module('app').factory
	( 'extraFlowSettings'
	, function() {
			var settings =
				{ date_time:
					{ name: 'date_time'
					, view: 'dateTime'
					, showCapacityRemaining: false
					, showTimeList: true
					, doEditMode: true
					, writeSingleCharac: false
					, enabled: true
					, type: 'extraFlowControl'
					}
				, date:
					{ name: "date"
					, view: 'dateTime'
					, showCapacityRemaining: false
					, showTimeList: false
					, doEditMode: true
					, writeSingleCharac: false
					, enabled: true
					, type: 'extraFlowControl'
					}
				, date_group:
					{ name: 'date_group'
					, view: 'dateTime'
					, showCapacityRemaining: false
					, showTimeList: true
					, doEditMode: true
					, writeSingleCharac: false
					, enabled: true
					, type: 'extraFlowControl'
					}
				, camp_date_picker:
					{ name: 'camp_date_picker'
					, view: 'dateTime'
					, showCapacityRemaining: true
					, showTimeList: true
					, doEditMode: true
					, writeSingleCharac: false
					, enabled: true
					, type: 'extraFlowControl'
					}
				, bundle_camp_date_picker:
					{ name: 'bundle_camp_date_picker'
					, view: 'dateTime'
					, showCapacityRemaining: true
					, showTimeList: true
					, doEditMode: true
					, writeSingleCharac: true
					, enabled: true
					, type: 'extraFlowControl'
					}
				, date_time_dolphin:
					{ name: 'date_time_dolphin'
					, view: 'dateTime'
					, showCapacityRemaining: false
					, showTimeList: true
					, doEditMode: true
					, writeSingleCharac: false
					, enabled: true
					, type: 'extraFlowControl'
					}
				, sp_reservation:
					{ name: "sp_reservation"
					, view: "reservations"
					, widgetCharacs: "rv_first_name, rv_last_name"
					, pillDataField: "rv_first_name"
					, doEditMode: true
					, writeSingleCharac: false
					, enabled: true
					, type: 'extraFlowControl'
					}
				, flashpassmodule:
					{ name: "flashpassmodule"
					, view: "flashPassList"
					, doEditMode: true
					, writeSingleCharac: false
					, enabled: true
					, type: 'alternateFlowModule'
					}
				}
			
			settings.CLIENT_XFC_VIEW = 'client_xfc_view';
			settings.CLIENT_XFC_NAME = 'client_xfc_name';
			settings.CLIENT_ALTERNATE_VIEW = 'client_alternate_view';
			settings.CLIENT_ALTERNATE_NAME = 'client_alternate_name';
			
			settings.getEnabledXFCsFromList = function ( list ){
				if( !( list && list.length > 0 ) ){
					return [];
				}
				
				// TESTING ONLY: if( list.indexOf( 'sp_reservation' ) == -1 ) list += ',sp_reservation';
				
				var enabledSettings = [];
				list = list.toLowerCase().split( ' ' ).join().split( ',' );
				
				if( list.length >= 1 ){
					// There are XFCs. Let's ensure we have settings for each one.
					for( var i = 0; i < list.length; i++ ){
						if( !settings[ list[ i ] ] || !settings[ list[ i ] ].enabled ){
							// This XFC does not have settings associated with it. Remove it from the array.
							list.splice( i, 1 );
							i--;
						} else {
							// The XFC has settings associated with it. Create a reference object for it.
							enabledSettings.push
								(
									{ completed: false
									, name: list[ i ]
									, data: null
									}
								);
						}
					}
				}
				
				return enabledSettings;
			}
			
			settings.addReferenceToCartItems = function( value, cartItems, reference ){
				// References:
				// client_xfc_view, client_xfc_name,
				// client_alternate_view, client_alternate_name.
				
				for( var i = 0; i < cartItems.length; i++ ){
					var item = cartItems[ i ];
					item.CHARACS = item.CHARACS || [];
					
					for( var j = 0; j < item.CHARACS.length; j++ ){
						item.CHARACS[ j ] = item.CHARACS[ j ] || {};
						var itemCharac = item.CHARACS[ j ];
						
						// Set value if it doesn't exist else append it.
						itemCharac[ reference ] = itemCharac[ reference ] || '';
						itemCharac[ reference ] =
							( itemCharac[ reference ] == '' )
							? value
							: ( itemCharac[ reference ].indexOf( value ) > -1 )
								? itemCharac[ reference ]
								: itemCharac[ reference ] + ',' + value;
					}
				}
			}
			
			settings.getAlternateFlowView = function( moduleName ){
				for( var key in settings ){
					if( settings[ key ] && settings[ key ].name == moduleName ){
						if( settings[ key ].enabled ){
							return settings[ key ].view;
						}
					}
				}
				
				return '';
			}
			
			settings.isAlternateFlow = function( keyword, menuItems ){
				// TODO: Check associated keywords too.
				for( var i = 0; i < menuItems.length; i++ ){
					if( !menuItems[ i ].isactive ){
						continue;
					}
					
					var alternateFlow = '';
					var kws = menuItems[ i ].keywords.toLowerCase();
					if( kws.indexOf( keyword.toLowerCase() ) > -1 ){
						if( menuItems[ i ].alternateModule ){
							return menuItems[ i ].alternateModule.toLowerCase();
						}
					} else if( menuItems[ i ].MENUITEM ){
						for( var j = 0; j < menuItems[ i ].MENUITEM.length; j++ ){
							if( !menuItems[ i ].MENUITEM[ j ].isactive ){
								continue;
							}
							
							alternateFlow = '';
							kws = menuItems[ i ].MENUITEM[ j ].keywords.toLowerCase();
							
							if( kws.indexOf( keyword.toLowerCase() ) > -1 ){
								if( menuItems[ i ].MENUITEM[ j ].alternateModule ){
									return menuItems[ i ].alternateModule.toLowerCase();
								}
							}
						}
					}
				}
				
				return '';
			}
			
			settings.getXFCEditMdodeStatusForItem = function( item ){
				// Returns true if ALL XFCs are editable.
				var count = 0;
				var keys = [ settings.CLIENT_XFC_NAME, settings.CLIENT_ALTERNATE_NAME ];
				
				for( var i = 0; i < keys.length; i++ ){
					if( item.CHARACS && item.CHARACS[ 0 ][ keys[ i ] ] ){
						var xfcList = item.CHARACS[ 0 ][ keys[ i ] ].split( ',' );
						
						for( var j = 0; j < xfcList.length; j++ ){
							if( settings[ xfcList[ j ] ] &&
								settings[ xfcList[ j ] ].enabled &&
								settings[ xfcList[ j ] ].doEditMode ){
								count++;
							}
						}
						
						// Returns bool(all enabled and editMode is true).
						return ( xfcList.length > 0 && ( xfcList.length == count ) );
					}
				}
				
				return false;
			}
			
			return settings;
		}
	)

