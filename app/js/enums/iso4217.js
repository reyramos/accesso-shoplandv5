/**
 * # Global currency enumerations. Work in progress. Add currency here as required.
 * 	See CAD, fr-CA, en-CA for setting currency format based on currency code and language code.
 */
angular.module('app').factory
	( 'iso4217'
	, function() {

			var data =
			{ CAD:
					[
						{ 'fr-CA':
							{ symbol: '$'
							, decSeparator: ','
							, thousandSeparator: '.'
							, symbolPlacement: 'right'
							, precision: 2
							, leadingZeros: 1
							}
						}
					, { 'en-CA':
							{ symbol: '$'
							, decSeparator: '.'
							, thousandSeparator: ','
							, symbolPlacement: 'left'
							, precision: 2
							, leadingZeros: 1
							}
						}
					]
			, USD:
				{ symbol: '$'
				, decSeparator: '.'
				, thousandSeparator: ','
				, symbolPlacement: 'left'
				, precision: 2
				, leadingZeros: 1
				}
			, MXN:
				{ symbol: '$'
				, decSeparator: '.'
				, thousandSeparator: ','
				, symbolPlacement: 'left'
				, precision: 2
				, leadingZeros: 1
				}
			}

		return data
		}
	)
