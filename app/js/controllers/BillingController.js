angular.module('app')
.controller
('BillingController'
, [   '$scope'
	, '$location'
	, '$routeParams'
	, '$log'
	, 'appDataService'
	, 'consolidatedData'
	, 'connectionService'
	, 'languageService'
	, 'analyticService'
	, 'countriesJSon'
	, 'enumsService'
	, 'cartService'
	, 'utilitiesService'
	, '$filter'
	, '$rootScope'
	, 'cartService'
	, 'merchantDetailsData'
	, 'modalService'
	, 'userInfoService'
	, function
		( $scope
		, $location
		, $routeParams
		, $log
		, appDataService
		, consolidatedData
		, connectionService
		, languageService
		, analyticService
		, countriesJSon
		, enumsService
		, cart
		, utilitiesService
		, $filter
		, $rootScope
		, cartService
		, merchantDetailsData
		, modalService
		, userInfoService
		){
			window.scrollTo( 0, 0 );

			$scope.hideSpinner();
			$scope.zipType = 'tel';
			$scope.useShipAddr = '0';
			$scope.shipping = userInfoService.shipping || {}
			$scope.isNonPhysicalDeliveryMethod = typeof($scope.shipping.isNonPhysicalDeliveryMethod) == 'undefined'?false:!$scope.shipping.isNonPhysicalDeliveryMethod

			//set the min/max of creditcards
			$scope.ccMin = 12
			$scope.ccMax = 19

			var billingCopy = {}
			$scope.$on('ZIPTASTIC',function(event,data){
				$scope.billing.country = data.country
				$scope.billing.city = data.city.toLowerCase()
				$scope.billing.state = data.state
			})


			$scope.optins = [];
			$scope.countries = countriesJSon.countries;
			$scope.regions = countriesJSon.regions;

			/* PaymentController.js*/

			$scope.currentYear = languageService.currentYear;
			$scope.currentMonth = languageService.currentMonth;
			$scope.range = $filter('range')([],$scope.currentYear,$scope.currentYear + 10)

			var select = utilitiesService.getObject('#country-select');
			select.bind('click',function(){
				console.log('select ', select)
			}, false);

			$scope.showBackToShipping =
				(
					typeof( $scope.shipping ) != 'undefined'
						&& typeof( $scope.shipping.shippingAvailable ) != 'undefined'
						&& $scope.shipping.shippingAvailable == true
					);

			$scope.shippingAddressAvailable =
				(
					$scope.showBackToShipping
						&& (
						typeof( $scope.shipping.deliveryMethod ) != 'undefined'
							&& $scope.shipping.deliveryMethod == enumsService.deliveryMethods.FIRST_CLASS_MAIL
						)
					)

			$scope.save = function(data){
				if( $scope.hasItemsInCart()){
					$scope.billingForm.submitted = true
					if($scope.billingForm.$valid){

						// Save the optins into the billing object.
						$scope.billing.optins = $scope.optins
						$scope.billing.useShipAddr = $scope.useShipAddr
						$scope.billing.valid = true

						// Store the Billing Data to App Data.
						userInfoService.set('billing', $scope.billing)

						if (cart.total == "0.00"){

							userInfoService.payment.valid = true
							userInfoService.payment.billingId = "9"
							userInfoService.payment.billingType = "CSH"
							userInfoService.payment.authType = "CS"

							appDataService.fetch
							( $scope.services.UPDATE_CART_CHECKOUT_INFO
								, false
								, { shippingMethod: $scope.shipping.deliveryMethod
								, shippingAmount: $scope.shipping.deliveryAmount
								, billingId: "9"
							}
							).then
							( function(data){
								if(data.status == $scope.status.OK){
									$scope.routeTo(enumsService.routes.ORDER_REVIEW)
								} else if(data.status == $scope.status.FAILED){

									modalService.displayModal({
											title: $scope.i18n.Alert.failedUpdateCheckoutInfoTitle
										, description: $scope.i18n.Alert.failedUpdateCheckoutInfoMsg
										, hideAllButtons: true
										})

								}
							}
							)
						}else{
							validatePayment()
						}
					}
				}
			}
			
			/**
			* validate payment options and values
			* cctype, month, year etc
			*/
			var validatePayment = function (){
				if(typeof(getBillingTypeByCardType($scope.payment.ccType)) != 'undefined'){

					// Reset the payment information.
					appDataService.payment = {}
					userInfoService.payment = {}

					// Determine card type and set relevant information on payment object.
					// Get billing type information.
					var billTypeObj = getBillingTypeByCardType( $scope.payment.ccType )

					// (i.e. ensure that this merchant accepts the type of card entered).
					$scope.payment.billingId = billTypeObj.billing_id
					$scope.payment.billingType = billTypeObj.billing_type
					$scope.payment.authType = billTypeObj.auth_type
					$scope.payment.cepayMerchantId = billTypeObj.cepay_merchant_id
					$scope.payment.card_type_code = billTypeObj.card_type_code

					// Format the expiration date as expected by the server.
					$scope.payment.cardExpirationDate = $scope.payment.year + $scope.payment.month

					// Save this data in the shipping form when we implement it.
					if( typeof( $scope.shipping ) != 'undefined' && $scope.shipping !== null){
						$scope.payment.shipMethod = ( typeof( $scope.shipping.deliveryMethod ) != 'undefined' ) ? $scope.shipping.deliveryMethod : enumsService.deliveryMethods.MOBILE;
						$scope.payment.shipAmount = ( typeof( $scope.shipping.deliveryAmount ) != 'undefined' ) ? $scope.shipping.deliveryAmount : 0;
					} else {
						$scope.payment.shipMethod = enumsService.deliveryMethods.MOBILE;
						$scope.payment.shipAmount = 0;
					}

					$scope.payment.valid = true

					appDataService.payment = {
						valid: true
					}

					//payment information is not to be save in localStorage, it is kept alive as long the user dont refresh
					userInfoService.payment = $scope.payment

					/*
					* This should come in from the selection by the user, or the defaulted value.
					*
					*/
					var myData =
					{
						 shippingMethod: $scope.payment.shipMethod
						, shippingAmount: $scope.payment.shipAmount
						, billingId: $scope.payment.billingId
					}

					// Add reference of request in pendingRequests.
					$rootScope.pendingRequests.push($scope.services.UPDATE_CART_CHECKOUT_INFO)

					// Show the spinner.
					$scope.showSpinner($scope.i18n.ApplicationLabels.updatingPaymentInfoLoadMsg)
					appDataService.fetch
						( $scope.services.UPDATE_CART_CHECKOUT_INFO
							, false
							, myData
						).then
					( function(data){
						// Remove reference of request in pendingRequests.
						$rootScope.removePendingRequest($scope.services.UPDATE_CART_CHECKOUT_INFO)

						if( data.status == $scope.status.OK ) {
							function routeToReview(){
								$scope.routeTo($scope.routes.REVIEW);

								// Hide the spinner.
								$scope.hideSpinner();
							}

							userInfoService.getCartSummary( false, routeToReview );
						} else if( data.status == $scope.status.FAILED){
							// The request to updateCartItem failed.
							modalService.displayModal({
									title: $scope.i18n.Alert.failedUpdateCheckoutInfoTitle
								 , description: $scope.i18n.Alert.failedUpdateCheckoutInfoMsg
								 , hideAllButtons: false
								})
						}
					}
					)
				}
			}

			var getBillingTypeByCardType = function(cardType){

				for(var i = 0; i < merchantDetailsData.BILLINGTYPE.length; i++){
					if( merchantDetailsData.BILLINGTYPE[i].name.toLowerCase() == cardType.toLowerCase()){
						$log.log('BILLING TYPE',merchantDetailsData.BILLINGTYPE)
						return merchantDetailsData.BILLINGTYPE[i]
					}
				}
			}

			$scope.handleChkChanged = function(){

				$scope.billing = $scope.useShipAddr == '1' && typeof( $scope.shipping ) != 'undefined' && $scope.shipping ?utilitiesService.copyObject($scope.shipping):billingCopy
			}

			function initializeController(){

				// Set the billing information to the data stored in appDataService.
				$scope.billing = userInfoService.billing || {};

				//create payment.ccType empty, avoid error within directive
				$scope.payment = {};
				$scope.payment.ccType = typeof($scope.payment.ccType) == 'undefined'?'':$scope.payment.ccType

				//set the checkmark for useShipAddress if available from localStorage
				$scope.useShipAddr = typeof($scope.billing.useShipAddr) == 'undefined'?'0':userInfoService.billing.useShipAddr;

				$scope.optins = [];
				//set the numeric position of the header placement and value, this will determine 1 shipping, 2 billing, 3 review
				$scope.arrowPositioning = typeof($scope.shipping) != 'undefined' && $scope.shipping !== null? (utilitiesService.getBoolean($scope.shipping.shippingAvailable)?2:1):1

				// Set default country.
				if( !$scope.billing.country ){
					$scope.billing.country = appDataService.defaults.country;
					$scope.setTelephone( $scope.billing.country );
				}

				for( var i in appDataService.optins ){
					if(appDataService.optins[i].section == 'BillingView'){
						$scope.optins.push( appDataService.optins[i] );
					}
				}

				// Dev mode only.
				if( $scope.autofill && typeof( $scope.billing.firstName ) == 'undefined' ){
					$scope.billing = utilitiesService.copyObject(userInfoService.userInfo)
				}
				if( $scope.autofill && typeof( $scope.payment.credicard ) == 'undefined' ){
					$scope.payment = utilitiesService.copyObject(userInfoService.payment)
				}

				$log.log('$scope.payment',$scope.payment)


				// Store the localized label as part of the optin object.
				for(var i = 0; i < $scope.optins.length; i++){
					var section = $scope.optins[i].section;
					var target = $scope.optins[i].target;
					$scope.optins[i].label = $scope.i18n[section][target];
				}

				//temp copy of billingInformation
				billingCopy = $scope.billing

			}

			$scope.showSecurityCode = function(){
				// Show image from config if available othersie use the default.
				var imgPath = 'img/'
				// TODO: check to see if settings is still used
				if( appDataService &&
					appDataService.settings &&
					appDataService.settings.security_code_image
					)
				{
					imgPath += appDataService.settings.security_code_image
				} else {
					imgPath += 'cvv.gif"'
				}

				modalService.displayModal({
				title: $scope.i18n.Alert.whatIsCVVTitle
				, headerFlag:'primary'
				, description: '<span class="sprite cvv"></span>'
				, buttonFlags: [$scope.buttonFlags.OK]
			})
			}

			$scope.months = function(){
				var months = []
				for(var i in languageService.months){
					value = $filter('leadingZeros')(parseInt(i)+1,2)
					months.push
					( { value: value
						 , text: value + ' - ' + languageService.months[i]
					 }
					)
				}

				return months
			}()

			$scope.years = function(){
				var years = []
				for(var i in $scope.range){
					years.push
					( { value: $filter('rightSubstring')($scope.range[i], 2)
						 , text: '' + $scope.range[i]
					 }
					)
				}
				return years
			}()

			$scope.setTelephone = function($index){
				if($index == 'US'){
					$scope.zipType = 'tel';
				} else {
					$scope.zipType = 'text';
				}
			}

			$scope.setCardType = function(ccType){
				// TODO: Move this to enumsService.
				if ( typeof ccType != 'undefined' ) {
					ccType = ccType.toLowerCase()
					switch (ccType){
						case "mastercard":
						case "visa":
						case "bankcard":
						case "discover":
						case "discover card":
						case "jcb":
							$scope.ccMax = 16
							break;
						case "amex":
						case "american express":
						case "enroute":
							$scope.ccMax = 15
							break;
						case "dinersclub":
							$scope.ccMax = 14
							break;
						default :
							$scope.ccMax = 19
							break;
					}
				} else {
					// reset the values;.
					$scope.ccMin = 12;
					$scope.ccMax = 19;
				}

				if( typeof (($scope).payment || {}).number != 'undefined' && $scope.payment.number.length > $scope.ccMax){
					$scope.payment.number = $scope.payment.number.substring(0, $scope.ccMax)
				}
			}

			$scope.$on("$routeChangeSuccess", function(event){
					// Set valid to false in order to force user to click continue button.
					var locPath = location.pathname.toLowerCase()
					if(locPath.indexOf($scope.routes.BILLING.toLowerCase()) > -1){
						if(appDataService.billing)
							appDataService.billing.valid = false
						initializeController()
					}

			})
		}])
