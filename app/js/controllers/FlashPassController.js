angular.module( 'app' )
.controller
( 'FlashPassController'
, [   '$location'
	, '$routeParams'
	, '$scope'
	, 'appDataService'
	, 'cartService'
	, 'extraFlowService'
	, 'flashpassService'
	, 'languageService'
	, 'merchantPackageListData'
	, 'packageService'
	, 'storageService'
	, function
		( $location
		, $routeParams
		, $scope
		, appDataService
		, cartService
		, extraFlowService
		, flashpassService
		, languageService
		, merchantPackageListData
		, packageService
		, storageService
		){
			window.scrollTo( 0, 0 );
			
			var LANDING_PAGE = 0
			  , DISCLAIMER = 1
			  , GUEST_SELECTION = 2
			  , GROUP_SELECTION = 3
			  , DATE_SELECTION = 4
			  , PACKAGE_SELECTION = 5
			  , YES = 'yes'
			  , NO = 'no'
			  , MAX_QTY = 60;
			
			// Determine the view.
			var locPath = $location.path().toLowerCase()
			  , viewQtySelector = locPath.indexOf( flashpassService.steps[ GUEST_SELECTION ].toLowerCase() ) > -1
			  , viewGroupSelector = locPath.indexOf( flashpassService.steps[ GROUP_SELECTION ].toLowerCase() ) > -1
			  , viewDateSelector = locPath.indexOf( flashpassService.steps[ DATE_SELECTION ].toLowerCase() ) > -1
			  , viewPackageSelector = locPath.indexOf( flashpassService.steps[ PACKAGE_SELECTION ].toLowerCase() ) > -1;
			
			// List of packages. Filter these by Flash Pass (selected) keyword.
			var packages = ( ( ( merchantPackageListData ).PS || {} ).P || [] );
			var requestListItems = {};
			
			// Different package types.
			var packageTypes =
				{ good: [ 'regular', 'base' ]
				, better: [ 'gold', 'turbo' ]
				, best: [ 'platinum', 'lightning' ]
				};
			
			// TODO: Move this to utilities.
			// Alphabet array.
			var alphabet =
				( function(){
					var a = [];
					for( var i = 65; i <= 90; i++ ){
						a[ a.length ] = String.fromCharCode( i );
					}
					
					return a;
				  }
				)();
			
			// $scope Properties.
			$scope.model = flashpassService;
			$scope.errorMessage = '';
			$scope.noFlashPassFound = false;
			$scope.chkYesVal = false;
			$scope.chkNoVal = false;
			$scope.enableYes = false;
			$scope.enableNo = false;
			$scope.showLightning = ( languageService.FlashPass.better == 'turbo' );
			$scope.minNumGroups = 0;
			$scope.groupSum = 0;
			$scope.selectedDate = null;
			$scope.selectionTotal = 0;
			
			// Callbacks for calendar directive.
			$scope.callbacks =
				{ eventIdChanged: function( newId ){
					eventID = newId;
				  }
				, gettingDates: function(){
					$scope.showNoDateMessage = false;
					$scope.showSpinner( languageService.ApplicationLabels.gettingEventDates );
				  }
				, gotDates: function( data ){
					$scope.hideSpinner();
				  }
				,	showOrHideNoDateMsg: function( val ){
					// If there is not at least one date, show the message.
					$scope.showNoDateMessage = val;
				  }
				};
			
			// Options for calendar dierective.
			$scope.calOptions =
				{ showWeeks: false
				, enableTimeLimit: false
				, autoRequest: false
				, requestObj: null
				, resourcesNeeded: 0
				, selectedDateString: ''
				};
			
			/***-----------------------------------------------------------------------
			 * PRIVATE
			 *------------------------------------------------------------------------**/
			
			function initializeController(){
				// Hide the spinner.
				$scope.hideSpinner();
				
				// Get and validate keyword from URL params and store in service.
				$scope.noKeywordErrorMsg = '';
				
				// Get the current step from the URL.
				var pthNum = Number( $location.path().toLowerCase().split( '/' )[ 2 ].split( '-' )[ 2 ] );
				if( !isNaN( pthNum ) ){
					flashpassService.currentStep = pthNum;
				}
				
				if( isNaN( pthNum ) || pthNum <= 1){
					flashpassService.reset();
				}
				
				if( $routeParams.keyword ){
					// Try getting the keyword from the URL.
					flashpassService.keyword = $routeParams.keyword;
				}
				
				if( !flashpassService.keyword ){
					// Try getting the keyword from the local storage.
					flashpassService.keyword = storageService.get( 'flashpassServiceKeyword' );
				}
				
				if( !flashpassService.keyword || ( !packages || packages.length == 0 ) ){
					// Hmm... No keyword or no packages. Route back to landing page.
					$scope.noKeywordErrorMsg = languageService.FlashPass.noKeywordErrorMsg;
					
					if( flashpassService.currentStep != LANDING_PAGE )
						$scope.routeToStep( LANDING_PAGE );
					
					return;
				}
				
				// Save the keyword to local storage.
				storageService.set( 'flashpassServiceKeyword', flashpassService.keyword );
				
				// Init the qty (NOTE: resets as user bounces from view to view).
				$scope.groupSum = getTotalGroupSum();
				
				// Initialize the packages data.
				requestListItems = getRequestListItems( flashpassService.keyword );
				flashpassService.products = requestListItems.packages;
				
				// Number of guests are bound directly to view.
				if( viewQtySelector ){
					initQtySelectorView();
				}
				
				if( viewGroupSelector ){
					initGroupSelector();
				}
				
				if( viewDateSelector ){
					initDateSelector();
				}
				
				if( viewPackageSelector ){
					initPackageSelector();
				}
			}
			
			/***-----------------------------------------------------------------------
			 * VIEW INITIALIZATION.
			 *------------------------------------------------------------------------**/
			
			function initQtySelectorView(){
				// Default the checkbox.
				if( viewQtySelector && flashpassService.stayingTogether == YES ){
					$scope.chkYesVal = true;
					$scope.chkNoVal = false;
					toggleCheckboxes();
				} else if( viewQtySelector && flashpassService.numGuests > 0 ){
					$scope.chkYesVal = false;
					$scope.chkNoVal = true;
					toggleCheckboxes();
				}
			}
			
			function initGroupSelector(){
				if( !validateForSection( GROUP_SELECTION ) ){
					return;
				}
				
				// Generate the groups.
				$scope.minNumGroups = Math.floor( flashpassService.numGuests / 6 );
				$scope.minNumGroups += ( flashpassService.numGuests % 6 > 0 ) ? 1 : 0;
				
				flashpassService.groups = flashpassService.groups || [];
				for( var i = 0; i < $scope.minNumGroups; i++ ){
					flashpassService.groups[ i ] = flashpassService.groups[ i ] || { qty: 0 };
				}
				
				cleanGroupQtyAndName( false );
				$scope.groupSum = getTotalGroupSum();
			}
			
			function cleanGroupQtyAndName( removeZeroQty ){
				for( var i = 0; i < flashpassService.groups.length; i++ ){
					// Always retain the name.
					flashpassService.groups[ i ].name = languageService.FlashPass.groupLbl + ' ' + alphabet[ i ];
					
					// Remove groups with 0 qty.
					if( removeZeroQty && flashpassService.groups[ i ].qty == 0 ){
						flashpassService.groups.splice( i, 1 );
						i--;
						
						continue;
					}
				}
			}
			
			function initDateSelector(){
				if( !validateForSection( DATE_SELECTION ) ){
					return;
				}
				
				// Filter pacakges by keyword.
				var reqObj =
					{ minCapacity: flashpassService.numGuests
					, customerType: requestListItems.customerTypeIDs
					, version: '2' // TODO: Hardcoded. Verify this is OK.
					, eventID: requestListItems.eventIDs
					, packageID: requestListItems.packageIDs
					, extraMovie: ''
					};
				
				// Set the calendarDirective requestObj option to reqObj.
				// This will trigger watch in calendarDirective that will force the request.
				$scope.calOptions.resourcesNeeded = flashpassService.numGuests;
				$scope.calOptions.requestObj = reqObj;
			}

			function getRequestListItems( keyword ){
				// Get a list of all the packageIDs, eventIDs, and customerTypeIDs.
				var listCntr =
					{ eventIDs: ''
					, customerTypeIDs: ''
					, packageIDs: ''
					, packages: []
					};
				
				for( var i = 0; i < packages.length; i++ ){
					var item = packages[ i ];
					var kw = item.keyword.split(', ').join().split(',');

					for( var j = 0; j < kw.length; j++ ){
						if( keyword == kw[ j ] ){
							// Make E (events) array.
							item.E = item.E || [];
							item.E = angular.isArray( item.E ) ? item.E : [ item.E ];
							
							// Make CT (customer tpes) array.
							item.CT = item.CT || [];
							item.CT = angular.isArray( item.CT ) ? item.CT : [ item.CT ];
							
							// Verify there is an Events and Cutomer Types.
							if( item.E.length > 0 && item.CT.length > 0 ){
								listCntr.packages.push( item );
								
								// Event IDs.
								for( var k = 0; k < item.E.length; k++ ){
									if( listCntr.eventIDs.indexOf( item.E[ k ].id ) == -1 ){
										if( listCntr.eventIDs )
											listCntr.eventIDs += ',';
									
										listCntr.eventIDs += item.E[ k ].id;
									}
								}
								
								// Customer Type IDs.
								for( var l = 0; l < item.CT.length; l++ ){
									if( listCntr.customerTypeIDs.indexOf( item.CT[ l ].id ) == -1 ){
										if( listCntr.customerTypeIDs )
											listCntr.customerTypeIDs += ',';
										
										listCntr.customerTypeIDs += item.CT[ l ].id;
									}
								}
								
								// Package IDs
								if( listCntr.packageIDs.indexOf( item.id ) == -1 ){
									if( listCntr.packageIDs )
										listCntr.packageIDs += ',';
									
									listCntr.packageIDs += item.id;
								}
							}
						}
					}
				}
				
				return listCntr;
			}
			
			function initPackageSelector(){
				if( !validateForSection( PACKAGE_SELECTION ) ){
					return;
				}
				
				var c = 0;
				resetGroupCategories();
				
				// For each product in flashpassService.products...
				for( var i = 0; i < flashpassService.products.length; i++ ){
					// For each group in flashpassService.groups
					for( var j = 0; j < flashpassService.groups.length; j++ ){
						// For each CT in current iteration of product.
						for( var k = 0; k < flashpassService.products[ i ].CT.length; k++ ){
							// Get the appropriate CT object. Select it by the name.
							// CT.name = 'QTY Person THE FLASH Pass...'
							// Ex: CT.name = '1 Person THE FLASH Pass...'
							// Ex: CT.name = '4 Person THE FLASH Pass...' etc.
							var pkg = flashpassService.products[ i ];
							var grp = flashpassService.groups[ j ];
							var ctObj = flashpassService.products[ i ].CT[ k ];
							var ctName = Number( ctObj.name.substring( 0, 1 ) );
							
							if( ctName == grp.qty ){
								grp.categories = grp.categories || [];
								grp.categories.push
									(
										{ name: pkg.name
										, price: ctObj.retail_amount
										, selected: false
										, id: c
										, groupName: grp.name.toLowerCase().split(' ').join('-')
										, customerTypeID: ctObj.id
										, packageID: pkg.id
										, eventID: pkg.E[ 0 ].id
										, pkgDetails: pkg
										}
									);
								
								grp.categories.sort( sortByPrice );
								c++;
								
								break;
							}
						}
					}
				}
			}
			
			function sortByPrice( item1, item2 ){
				return Number( item1.price ) < Number( item2.price );
			}
			
			function resetGroupCategories(){
				for( var i = 0; i < flashpassService.groups.length; i++ ){
					var grp = flashpassService.groups[ i ];
					grp.categories = [];
				}
			}
			
			function validateForSection( section ){
				if( section > GUEST_SELECTION && !validateQtyScreen() ){
					// Ensure we have a quantity selected.
					$scope.routeToStep( GUEST_SELECTION );
					return false;
				}
				
				if( section > GROUP_SELECTION && !validateGroupSelectionScreen() ){
					// Ensure each group has quantity and sum matches numGuests.
					$scope.routeToStep( GROUP_SELECTION );
					return false;
				}
				
				if( section > DATE_SELECTION && !validateDateScreen() ){
					// Ensure we have a date selected.
					$scope.routeToStep( DATE_SELECTION );
					return false;
				}
				
				return true;
			}
			
			/***-----------------------------------------------------------------------
			 * VIEW VALIDATION.
			 *------------------------------------------------------------------------**/
			
			function validateQtyScreen(){
				if( flashpassService.numGuests == 0 ){
					$scope.errorMessage = languageService.FlashPass.pleaseSelectQty;
					return false;
				} else if( flashpassService.numGuests == 1 && $scope.chkNoVal == '1'){
					// User wants to separate a group of one individual into multiple groups.
					// This should never happen.
					$scope.errorMessage = languageService.FlashPass.guestSelectionErrorMsg;
					return false;
				} else if( flashpassService.numGuests > 1 &&
						   flashpassService.numGuests < 7 &&
						   $scope.chkNoVal == '0' &&
						   $scope.chkYesVal == '0' &&
						   flashpassService.stayingTogether == ''){
					// User must select an option of staying together or breaking the group apart.
					$scope.errorMessage = languageService.FlashPass.guestSelectionErrorMsg;
					return false;
				} else if( flashpassService.numGuests > 7 && $scope.chkNoVal == '0' && flashpassService.stayingTogether == '' ){
					// User wants to keep group together but has exceeded max group qty.
					$scope.errorMessage = languageService.FlashPass.guestSelectionErrorMsg;
					return false;
				}
				
				return true;
			}
			
			function validateGroupSelectionScreen(){
				if( flashpassService.stayingTogether == NO && $scope.groupSum != flashpassService.numGuests ){
					$scope.errorMessage = languageService.FlashPass.groupTotalErrorMsg;
					return false;
				} else {
					$scope.errorMessage = '';
				}
				
				cleanGroupQtyAndName( true );
				
				return true;
			}
			
			function validateDateScreen(){
				return ( flashpassService.selectedDate != null );
			}
			
			function toggleCheckboxes(){
				if( flashpassService.numGuests == 0 ){
					$scope.chkYesVal = false;
					$scope.chkNoVal = false;
					$scope.enableYes = false;
					$scope.enableNo = false;
				} else if( flashpassService.numGuests == 1 ){
					$scope.chkYesVal = true;
					$scope.chkNoVal = false;
					$scope.enableYes = false;
					$scope.enableNo = false;
				} else if( flashpassService.numGuests > 1 && flashpassService.numGuests < 7 ){
					$scope.enableYes = true;
					$scope.enableNo = true;
				} else if( flashpassService.numGuests > 6 ){
					$scope.chkYesVal = false;
					$scope.chkNoVal = true;
					$scope.enableYes = false;
					$scope.enableNo = false;
				}
			}
			
			function getTotalGroupSum(){
				var sum = 0;
				for( var i = 0; i < flashpassService.groups.length; i++ ){
					sum += flashpassService.groups[ i ].qty;
				}
				
				return sum;
			}
			
			function updateGroupErrorMsg(){
				$scope.groupSum = getTotalGroupSum();
				if( $scope.groupSum > flashpassService.numGuests ){
					$scope.errorMessage = languageService.FlashPass.exceededNumberOfGuestErrorMsg;
				} else {
					$scope.errorMessage = '';
				}
			}
			
			function enableCategory( group, category ){
				for( var i = 0; i < group.categories.length; i++ ){
					var cat = group.categories[ i ];
					cat.selected = false;
				}
				
				category.selected = true;
			}
			
			function getSelectionTotal(){
				var sum = 0;
				for( var i = 0; i < flashpassService.groups.length; i++ ){
					for( var j = 0; j < flashpassService.groups[ i ].categories.length; j++ ){
						var cat = flashpassService.groups[ i ].categories[ j ];
						if( cat.selected ){
							sum += Number( cat.price );
						}
					}
				}
				
				return sum;
			}
			
			/***-----------------------------------------------------------------------
			 * PUBLIC
			 *------------------------------------------------------------------------**/
			
			$scope.routeToStep = function( stepTo ){
				if( flashpassService.currentStep > stepTo ){
					if( stepTo == GROUP_SELECTION && flashpassService.stayingTogether == YES ){
						// Account for skipping the group selection screen.
						stepTo = GUEST_SELECTION;
					}
					
					// Going backwards, just route.
					flashpassService.currentStep = stepTo;
					
					$scope.routeTo( flashpassService.steps[ stepTo ] + '/' + flashpassService.keyword );
					return;
				}
				
				// Going forward.
				var sectionValid = true;
				switch( stepTo ){
					case LANDING_PAGE:
					case DISCLAIMER:
					case GUEST_SELECTION:
						// 0 No validation needed.
						$scope.animateOut
							(
								function(){
									$scope.routeTo( flashpassService.steps[ stepTo ] + '/' + flashpassService.keyword );
								}
							);
						break;
					case GROUP_SELECTION:
						// 2-3 Validate user selected quantity.
						// validate correct option was selected.
						flashpassService.stayingTogether = ( $scope.chkNoVal == '0' ) ? YES : NO;
						
						// If staying together, ensure there is that one group.
						if( flashpassService.stayingTogether == YES ){
							flashpassService.groups =
								[
									{ name: languageService.FlashPass.groupLbl + ' ' + 'A'
									, qty: flashpassService.numGuests
									}
								];
						}
						
						sectionValid = validateQtyScreen();
						break;
					case DATE_SELECTION:
						// 3-4 Validate group selection.
						sectionValid = validateGroupSelectionScreen();
						break;
					case PACKAGE_SELECTION:
						// 4-5 Validate Calendar selection.
						flashpassService.selectedDate = $scope.selectedDate;
						flashpassService.selectedDateString = $scope.calOptions.selectedDateString;
						sectionValid = validateDateScreen();
						break;
					default:
						break;
				}
				
				if( sectionValid ){
					flashpassService.currentStep = stepTo;
					
					// If navigating to group selection, ensure user opted to break party into groups.
					if( stepTo == GROUP_SELECTION && flashpassService.stayingTogether == YES ){
						stepTo = DATE_SELECTION;
					}
					
					$scope.animateOut
						(
							function(){
								$scope.routeTo( flashpassService.steps[ stepTo ] + '/' + flashpassService.keyword );
							}
						);
				}
			}
			
			/***-----------------------------------------------------------------------
			 * QUANTITY SELECTOR VIEW FUNCTIONALITY.
			 *------------------------------------------------------------------------**/
			
			$scope.incrementQty = function( data ){
				flashpassService.numGuests++;
				if( flashpassService.numGuests > MAX_QTY ){
					flashpassService.numGuests = MAX_QTY;
				}
				
				toggleCheckboxes();
			}
			
			$scope.decrementQty = function(){
				flashpassService.numGuests--;
				if( flashpassService.numGuests < 0 ){
					flashpassService.numGuests = 0;
				}
				
				toggleCheckboxes();
			}
			
			$scope.onQtyChange = function(){
				flashpassService.numGuests = Number( $scope.model.numGuests );
				if( flashpassService.numGuests > MAX_QTY ){
					flashpassService.numGuests = MAX_QTY;
				}
				toggleCheckboxes();
			}
			
			$scope.handleChkChanged = function( chk ){
				if( chk == YES ){
					if( $scope.enableYes ){
						if( $scope.chkYesVal == '1' || $scope.chkYesVal == true ){
							$scope.chkYesVal = true;
							$scope.chkNoVal = false;
						} else {
							$scope.chkYesVal = false;
							$scope.chkNoVal = true;
						}
					}
				} else if( chk == NO ){
					if( $scope.enableNo ){
						if( $scope.chkNoVal == '1' || $scope.chkNoVal == true ){
							$scope.chkNoVal = true;
							$scope.chkYesVal = false;
						} else {
							$scope.chkNoVal = false;
							$scope.chkYesVal = true;
						}
					}
				}
			}
			
			/***-----------------------------------------------------------------------
			 * GROUP VIEW FUNCTIONALITY.
			 *------------------------------------------------------------------------**/
			
			$scope.addGroup = function(){
				// Ensure number of groups does not exceed number of guests and is less than 10.
				if( flashpassService.groups.length < flashpassService.numGuests && flashpassService.groups.length < 10 ){
					flashpassService.groups.push
						(
							{ name: languageService.FlashPass.groupLbl + ' ' + alphabet[ flashpassService.groups.length ]
							, qty: 0
							, showDeleteBtn: true
							}
						);
				}
			}
			
			$scope.deleteGroup = function( index ){
				if( flashpassService.groups.length - 1 >= $scope.minNumGroups ){
					$scope.groupSum -= flashpassService.groups[ index ].qty;
					flashpassService.groups.splice( index, 1 );
					updateGroupErrorMsg();
					cleanGroupQtyAndName( false );
				}
			}
			
			$scope.incrementGroupQty = function( index ){
				var itemObj = flashpassService.groups[ index ];
				$scope.groupSum = getTotalGroupSum();
				
				if( itemObj.qty < 6 ){
					if( $scope.groupSum < flashpassService.numGuests ){
						$scope.groupSum++;
						itemObj.qty++;
					} else {
						// Alert the user he is attempting to add more than the qty selected.
					}
				} else {
					itemObj.qty = 6;
				}
				
				updateGroupErrorMsg();
			}
			
			$scope.decrementGroupQty = function( index ){
				var itemObj = flashpassService.groups[ index ];
				
				if( itemObj.qty > 0 ){
					itemObj.qty--;
					$scope.groupSum = getTotalGroupSum();
				}
				
				updateGroupErrorMsg();
			}
			
			$scope.onGroupQtyChange = function( index ){
				var itemObj = flashpassService.groups[ index ];
				
				if( itemObj.qty == '' || isNaN( itemObj.qty ) ){
					return;
				}
				
				var qtyEntered = Number( itemObj.qty );
				if( qtyEntered > 0 && qtyEntered <= 6 ){
					itemObj.qty = Number( qtyEntered );
				}
				
				if( qtyEntered < 0 ){
					qtyEntered = 0;
				} else if( qtyEntered > 6 ){
					qtyEntered = 6;
				}
				
				itemObj.qty = qtyEntered;
				$scope.groupSum = getTotalGroupSum();
				
				if( $scope.groupSum > flashpassService.numGuests ){
					$scope.errorMessage = languageService.FlashPass.exceededNumberOfGuestErrorMsg;
				} else{
					updateGroupErrorMsg();
				}
			}
			
			/***-----------------------------------------------------------------------
			 * DATE VIEW FUNCTIONALITY.
			 *------------------------------------------------------------------------**/
			
			/***-----------------------------------------------------------------------
			 * PACKAGE VIEW FUNCTIONALITY.
			 *------------------------------------------------------------------------**/
			
			$scope.handleCategoryChanged = function( group, category ){
				enableCategory( group, category );
				$scope.selectionTotal = getSelectionTotal();
			}
			
			$scope.addToCart = function(){
				// Validate each category in each group were selected.
				var categoriesSelected = 0;
				for( var i = 0; i < flashpassService.groups.length; i++ ){
					for( var j = 0; j < flashpassService.groups[ i ].categories.length; j++ ){
						var cat = flashpassService.groups[ i ].categories[ j ];
						categoriesSelected = ( cat.selected ) ? categoriesSelected + 1 : categoriesSelected;
					}
				}
				
				if( categoriesSelected == flashpassService.groups.length ){
					// Valid.
					
					var itemsToBeAdded = [];
					var pkgDetails = {};
					for( var i = 0; i < flashpassService.groups.length; i++ ){
						for( var j = 0; j < flashpassService.groups[ i ].categories.length; j++ ){
							var cat = flashpassService.groups[ i ].categories[ j ];
							if( cat.selected ){
								pkgDetails = pkgDetails || cat.pkgDetails;
								var item =
									{ qty: 1 // Package specifies qty (i.e. 1 User Flash Pass, 2 User Flash Pass, 6 User Flash Pass) 
									, package_name: cat.name
									, customer_type: cat.customerTypeID
									, package_id: cat.packageID
									, cusTypeName: "General"
									, cusTypeRetail: cat.price
									, extra_movie: cat.pkgDetails.extra_movie || ''
									, promo_code: cat.pkgDetails.promo_code || ''
									, cart_limit_override: cat.pkgDetails.cart_limit_override || ''
									}
									
								for( var k = 0; k < item.qty; k++ ){
									item.CHARACS = item.CHARACS || [];
									item.CHARACS[ k ] = item.CHARACS[ k ] || {};
									item.CHARACS[ k ].event_id = cat.eventID;
									item.CHARACS[ k ].start_date = flashpassService.selectedDateString;
									item.CHARACS[ k ].start_time = '';
								}
								
								itemsToBeAdded.push( item );
							}
						}
					}
					
					// Add reference of XFC to CHARACs.
					// TODO: String values should come from settings somehow.
					extraFlowService.addReferenceToCartItems( 'flashPassList', itemsToBeAdded, extraFlowService.CLIENT_ALTERNATE_VIEW );
					extraFlowService.addReferenceToCartItems( 'flashpassmodule', itemsToBeAdded, extraFlowService.CLIENT_ALTERNATE_NAME );
					
					cartService.addToCart( itemsToBeAdded, pkgDetails );
				} else {
					// Invalid. Display error message - please select a Flash Pass for each group.
				}
			}
			
			$scope.showAvailableRides = function(){
				// TODO: Build as per spec.
				// TODO: Localize data.
				var modalContent = {
					title: "Available Rides"
					, headerFlag:'flashpass-availableRides'
					, description:
						"<h3>Gain Access to all the following rides with the Flass Pass</h3>"
							+ "<ul>"
							+ "<li>The Dark Knight</li>"
							+ "<li>Batman The Ride</li>"
							+ "<li>The Big Wheel</li>"
							+ "<li>Congo Rapids</li>"
							+ "</ul>"
					, buttonFlags: []
				}
				
				modalService.displayModal(modalContent)
			}
			
			$scope.showFPInfo = function( type ){
				var pckInfo = null;
				for( var key in packageTypes ){
					var arr = packageTypes[ key ];
					if( pckInfo ){
						break;
					}
					
					for( var i = 0; i < arr.length; i++ ){
						if( pckInfo ){
							break;
						}
						
						if( type == arr[ i ] ){
							// Loop through all the packages and select the one wich matches.
							for( var j = 0; j < flashpassService.products.length; j++ ){
								if( pckInfo ){
									break;
								}
								
								var pkg = flashpassService.products[ j ];
								var pckName = flashpassService.products[ j ].name.toLowerCase();
								if( pckName.indexOf( type ) > -1 ){
									pckInfo =
										{ title: pkg.name
										, description: pkg.desc
										}
								}
							}
						}
					}
				}
				
				if( pckInfo ){
					var modalContent = {
						title: pckInfo.title
						, headerFlag:'flashpass-pkg'
						, description: '<span class="text-left">'+pckInfo.description+'</span>'
						, buttonFlags: []
					}
					
					modalService.displayModal(modalContent)
				}
			}
			
			initializeController();
		 }
  ]
)
