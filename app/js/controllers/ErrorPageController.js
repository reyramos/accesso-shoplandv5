/**
 * # ErrorPageController
 *
 * Controller that manages the larger error pages.

 @todo: DO NOT USE LOADING SERVICE FOR KNOWLEDGEBASE UNTIL THE API CODE IS UP TO THE 3.5 IMPLEMENTATION.
 */
angular.module('app').controller
	( 'ErrorPageController',[
		'$scope'
		, '$log'
		, '$route'
		, '$routeParams'
		, '$location'
		, 'connectionService'
		, 'storageService'
		, 'navigationService'
		, 'clientInfoService'
		, 'languageService'
		, 'analyticService'
	, function(
		$scope
		, $log
		, $route
		, $routeParams
		, $location
		, connectionService
		, storageService
		, navigationService
		, clientInfoService
		, languageService
		, analyticService
	) {
		window.scrollTo( 0, 0 );
		$scope.hideSpinner()

		$scope.errorCode = false;
		$scope.errorMsg  = '';
		$scope.errorTest  = 'qweqweqwe';
		$scope.clientInfoService = clientInfoService;
		
		/**
		 * Might switch over to this in the future.
		 */
		$scope.goHome = function() {
			if ( angular.isDefined($routeParams.merchant) ) {
				// Need the following line to get rid of the querystring data that gets left behind.
				$location.search( 'unknownPage', null );
				// TODO: Use routeTo()
				$location.path('/' + $routeParams.merchant + '/');
			} else {

			}
		}

		function resetScopeVars() {
			$scope.errorCode = false
			$scope.errorMsg  = ''
			$scope.showAndroidBrowserLinks = false
			$scope.showBackToStoreLink = false
		}

		function initializeController(){
			
			resetScopeVars()
			
			if ( angular.isDefined($routeParams.errorCode) ) {
				
				$scope.errorCode = $routeParams.errorCode
				$scope.clientInfoService = clientInfoService;
				
				if ( angular.isDefined( $scope.i18n.ErrorCodes[$scope.errorCode] ) ) {
					
					$scope.errorMsg = $scope.i18n.ErrorCodes[$scope.errorCode]
					
					if ( $scope.errorCode == 1000 ) {
						$scope.showBackToStoreLink = false
					}
					
					if ( $scope.errorCode == 1004 ) {
						$scope.showBackToStoreLink = true
					}
					
				} else {
					$scope.errorMsg = 'undefined'
					$scope.errorCode = false
				}
			} else {

			}
		}
		initializeController()
	}]);
