angular.module( 'app' )
.controller
( 'PackageListController'
, [   '$scope'
	, '$log'
	, '$routeParams'
	, '$location'
	, '$filter'
	, '$timeout'
	, 'appDataService'
	, 'utilitiesService'
	, 'analyticService'
	, 'clientInfoService'
	, 'languageService'
	, 'extraFlowService'
	, 'enumsService'
	, 'consolidatedData'
	, 'merchantPackageListData'
	, function
		( $scope
		, $log
		, $routeParams
		, $location
		, $filter
		, $timeout
		, appDataService
		, utilitiesService
		, analyticService
		, clientInfoService
		, languageService
		, extraFlowService
		, enumsService
		, consolidatedData
		, merchantPackageListData
		){
			window.scrollTo( 0, 0 );
			var appDataResult = appDataService.setData(consolidatedData);

			var menuItems = appDataService.menuItems;
			var swapArray = appDataService.storageService.get( '_geolocationSwap', true );
			var requestPending = false;
			var geoPending = false;
			var geoTimeout = null;
			
			$scope.showNoPkgMsg = false;
			$scope.showSubMenu = true;
			$scope.packages = ( ( merchantPackageListData || {} ).PS || {} ).P || [];
			$scope.packageList = [];

			// @TODO: Consider using alternative to watch.
			$scope.$watch( function(){
					return appDataService.keywordMenuItems;
				}, function( keywordMenuItems ){
					// Update the local property with the passed in service value.
					$scope.subMenuItems = keywordMenuItems;
				}
			 )

			$scope.packageDetails = function( $package_id ){
				// Route the package by packageId.
				$scope.routeTo( 'packageDetails/' + $package_id );
			}

			$scope.onKeywordMenuItemClick = function( $event, data ){
				// Don't let event bubble up.
				stopEventPropagation( $event );
				// Reset selected flag on all items.
				for( var i = 0; i < appDataService.keywordMenuItems.length; i++ ){
					if( data.subMenuItem != appDataService.keywordMenuItems[ i ] ){
						appDataService.keywordMenuItems[ i ].selected = false;
					}
				}

				// Flag this keyword as the selected item.
				data.subMenuItem.selected = true;

				// @TODO: add click action.
				$scope.routeTo( 'packageList/keyword/' + data.subMenuItem.keywords );

				// Set selected flag on item clicked.
				$scope.showAllItems = !$scope.showAllItems;
			}
			
			/***-----------------------------------------------------------------------
			 * GEO LOCATION
			 * TODO: Move code out of this controller.
			 *------------------------------------------------------------------------**/

			function geolocationSuccess( position ){
				if( geoTimeOut ){
					$timeout.cancel( geoTimeOut );
					geoTimeOut = null;
				}
				
				appDataService.storageService.set( '_geolocationPosition' , position, true );
				appDataService.storageService.set( '_geolocationError' , {}, true );
				appDataService.storageService.set( '_geolocationSwap' , [], true );
				fetchGeolocationPackage( position );
				geoPending = false;
			}

			function geolocationError( error ){
				try{
					error.timestamp = new Date().getTime();
					if( geoTimeOut ){
						$timeout.cancel( geoTimeOut );
						geoTimeOut = null;
					}
				} catch( e ){}
				
				appDataService.storageService.set( '_geolocationPosition' , {}, true );
				appDataService.storageService.set( '_geolocationError' , error, true );
				appDataService.storageService.set( '_geolocationSwap' , [], true );
				geoPending = false;
				
				if( !requestPending && !geoPending ){
					$scope.hideSpinner();
				}
			}

			function fetchGeolocationPackage( position ){
				$scope.showSpinner( languageService.ApplicationLabels.loadingPackage );
				
				requestPending = true;
				appDataService.fetch
					( $scope.services.GET_MERCHANT_PACKAGE_LIST
					, false //true
					, { latitude: ( ( position ).coords || {} ).latitude
					  , longitude: ( ( position ).coords || {} ).longitude
					  , accuracy: ( ( position ).coords || {} ).accuracy
					  , _skip_cache: true
					  , _no_cache: true
					  , show_details: '1'
					  , long_desc_flag: '1'
					  , keyword: ( merchantKeywordsData || {} ).keywords || ''
					  }
					).then
						(
							function( data ){
								var swappedCurrentKeyword = false;
								requestPending = false;
								position = appDataService.storageService.get( '_geolocationPosition', true );
								if( data.status == $scope.status.OK && utilitiesService.getBoolean( data.geoLoc_override ) && typeof data.KEYWORDSWAP != 'undefined' && position !== null ){
									if( data.KEYWORDSWAP.type == 'geolocation' && typeof ( ( data ).KEYWORDSWAP || {} ).SWAP != 'undefined' ){
										data.KEYWORDSWAP.SWAP = angular.isArray( data.KEYWORDSWAP.SWAP ) ? data.KEYWORDSWAP.SWAP : [ data.KEYWORDSWAP.SWAP ];
										appDataService.storageService.set( '_geolocationSwap' , data.KEYWORDSWAP.SWAP, true );
										
										for( var i in data.KEYWORDSWAP.SWAP ){
											if( data.KEYWORDSWAP.SWAP[ i ].keyword && data.KEYWORDSWAP.SWAP[ i ].swap_keyword ){
												for( var j in appDataService.menuItems ){
													var mItem = appDataService.menuItems[ j ];
													
													if( mItem.keywords == data.KEYWORDSWAP.SWAP[ i ].keyword ){
														mItem._keywords = mItem.keywords;
														mItem.keywords = data.KEYWORDSWAP.SWAP[ i ].swap_keyword;
													}
													
													if( typeof mItem.MENUITEM != 'undefined' ){
														for( var k in mItem.MENUITEM ){
															var mSubItem = mItem.MENUITEM[ k ];
															if( mSubItem.keywords == data.KEYWORDSWAP.SWAP[ i ].keyword ){
																mSubItem._keywords = mSubItem.keywords;
																mSubItem.keywords = data.KEYWORDSWAP.SWAP[ i ].swap_keyword;
															}
														}
													}
												}
												
												if( typeof appDataService.lastKeyword == 'string' && appDataService.lastKeyword == data.KEYWORDSWAP.SWAP[ i ].keyword && !swappedCurrentKeyword ){
													appDataService.lastKeyword = data.KEYWORDSWAP.SWAP[ i ].swap_keyword;
													swappedCurrentKeyword = true;
												}
											}
										}
									} else if( data.KEYWORDSWAP.type == 'zipcode' && typeof ( ( data ).KEYWORDSWAP || {} ).SWAP != 'undefined' ){
										// TODO: Add Zip Code functionality
									}
								}
								
								if( swappedCurrentKeyword ){
									if( appDataService.lastKeyword ){
										$scope.routeTo( 'packageList/keyword/' + appDataService.lastKeyword )
									} else {
										$scope.routeTo()
									}
								}
								
								if( !requestPending && !geoPending ){
									$scope.hideSpinner();
								}
							}
						);
			}

			function gatherGeolocation(){
				if( 'geolocation' in navigator ){
					geoPending = true;
					$scope.showSpinner( languageService.ApplicationLabels.useLocation );
					navigator.geolocation.getCurrentPosition( geolocationSuccess, geolocationError, appDataService.geolocationOptions );
					geoTimeOut =
						$timeout
							( function(){
								geoPending = false;
								position = appDataService.storageService.get( '_geolocationPosition', true );
								error = appDataService.storageService.get( '_geolocationError', true );
								if(position === null && error === null){
									geolocationError( {"message":"Skipped Geolocation","code":-1} );
								}
							  }
							, 10000
							);
				} else {
					// TODO: Use Zipcode Location
				}
			}
			
			/***-----------------------------------------------------------------------
			 * GEO LOCATION - End
			 * TODO: Move code out of this controller.
			 *------------------------------------------------------------------------**/

			function initializeController(){
				// TODO: Make sure the Promo Code & Keyword matching is case insensitive
				if( angular.isDefined( $routeParams.promocode ) ){
					// Fetch the promocode.
					fetchPromoCodePackage();
				} else if( angular.isDefined( $routeParams.crosssells ) ){
					getPackageKeywordList( $routeParams.crosssells );
					$scope.showSubMenu = false;
				} else if( angular.isDefined( $routeParams.keyword ) ){
					selectedKeyword = $routeParams.keyword;
					
					var alternateFlow = extraFlowService.isAlternateFlow( selectedKeyword, consolidatedData.TREE.MENUITEM )
					if( alternateFlow != '' ){
						var view = extraFlowService.getAlternateFlowView( alternateFlow );
						if( view ){
							$scope.routeTo( view + '/' + selectedKeyword )
							return;
						}
					}
					
					if( angular.isArray( swapArray ) ){
						for( var i in swapArray ){
							if( swapArray[ i ].keyword && swapArray[ i ].swap_keyword && swapArray[ i ].keyword == selectedKeyword ){
								selectedKeyword = swapArray[ i ].swap_keyword;
								break;
							}
						}
					}
					
					if( isKeywordActive( selectedKeyword ) ){
						getPackageKeywordList( selectedKeyword );
						// Supporting return keyword.
						appDataService.lastKeyword = selectedKeyword;
						$scope.subMenuItems = appDataService.keywordMenuItems || getKeywordMenuByKeyword( selectedKeyword );
					}
				} else {
					var kw = '';
					if( typeof appDataService.defaults.keyword != 'undefined' && appDataService.defaults.keyword != '' ){
						for( var i in menuItems ){
							if( appDataService.defaults.keyword.toLowerCase() == menuItems[ i ].keywords.toLowerCase() ){
								kw = appDataService.defaults.keyword;
								break;
							}

							if( typeof menuItems[ i ].MENUITEM != 'undefined' ){
								for( var j in menuItems[ i ].MENUITEM ){
									if( appDataService.defaults.keyword.toLowerCase() == menuItems[ i ].MENUITEM[ j ].keywords.toLowerCase() ){
										kw = appDataService.defaults.keyword;
										break;
									}
								}
							}
						}
					}

					if( kw == '' ){
						kw = getFirstKeywordFromMenu();
					}

					getPackageKeywordList( kw );
					appDataService.lastKeyword = kw;
					$scope.subMenuItems = appDataService.keywordMenuItems || getKeywordMenuByKeyword( kw );
				}

				if( $scope.packageList.length == 1 ){
					// Auto route to the package details screen.
					// @TODO: Temporarily disabled for now as user may be re-routed to pkg list when hitting continue shopping.
					//$scope.routeTo( $scope.routes.DETAILS + '/' + $scope.packageList[ 0 ].id )
				}

				cleanPackages();

				if( appDataService.useLocation && ( !angular.isArray( swapArray ) || ( angular.isArray( swapArray ) && swapArray.length == 0 ) ) ){
					$timeout( function(){
						gatherGeolocation();
					}, 0 );
				}
				
				if( !requestPending && !geoPending ){
					$scope.hideSpinner();
				}
				
				for(var i in appDataService.menuItems ) {
					if(appDataService.lastKeyword && appDataService.lastKeyword.toLowerCase() == appDataService.menuItems[i].keywords.toLowerCase()){
						appDataService.menuItems[i].selected = true
						if(typeof appDataService.menuItems[i].MENUITEM != 'undefined' ) {
							appDataService.menuItems[i].showSubMenu = true
						}
					}
				}
			}
			
			function stopEventPropagation( event ){
				event.preventDefault();

				if( event && event.stopPropagation ){
					event.stopPropagation();
				} else {
					var e = window.event;
					e.cancelBubble = true;
				}
			}

			function getPackageKeywordList( keyword ){
				var keyword = keyword || '';
				$scope.packageList = [];

				// Ensure packages is in array format.
				$scope.packages = angular.isArray( $scope.packages ) ? $scope.packages : [ $scope.packages ]

				for( var i = 0; i < $scope.packages.length; i++ ){
					// Check if Keyoword, promo_code, or assoc_keywords matches.
					if( addPackageDataByProperty( $scope.packages[ i ], 'keyword', keyword, true ) ||
						(
							angular.isDefined( $routeParams.promocode ) &&
							addPackageDataByProperty( $scope.packages[ i ], 'promo_code', keyword, false )
						) ||
						addPackageDataByProperty( $scope.packages[ i ], 'assoc_keywords', keyword, true ) ){
						// OK. Package was added if any criteria matched.
						replaceDateWithValidFrom( $scope.packages[ i ] );
					}
				}
				
				addStartRateToAllPackages();
				$scope.showNoPkgMsg = ( $scope.packageList.length == 0 );
			}
			
			function replaceDateWithValidFrom( packageData ){
				// TODO: Refactor. Should be in service since this is not DRY.
				// This was occuring every view to this controller hence packageData.validFromUpdated.
				// Replace @@date in advanced ticket with valid-from date.
				if( packageData.CHARACS && packageData.CHARACS.valid_from && !packageData.validFromUpdated){
					packageData.headline =
						$scope.i18n.replaceAllMultiple
							( [ '@@date' ]
								, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
								, $scope.i18n.replaceAllMultiple
								( [ '@@Date' ]
									, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
									, packageData.headline
								 )
							 );
					
					packageData.validFromUpdated = true;
				}
			}
			
			function addPackageDataByProperty( packageData, property, filter, hidePromo ){
				// Determines if the property exists and if it matches the filter.
				// If so, it adds this package to $scope to be rendered.
				var propValue = '';
				if ( typeof packageData[ property ] != 'undefined' ){
					propValue = packageData[ property ].toLowerCase();
				}
				
				if( propValue.length > 0 ){
					propValue = propValue.split( ', ' ).join().split( ',' );
				}
				
				for( var j = 0; j < propValue.length; j++ ){
					if( filter.toLowerCase() == propValue[ j ] ){
						var notExist = !packageExists( packageData.id );
						var isPromo = packageData.isPromo;
						var isPromoAndPkg = packageData.promoAndPackage;

						if(
							( !isPromo && notExist ) ||
							( isPromo && isPromoAndPkg && notExist ) ||
							( isPromo && !hidePromo && notExist )
						){
							$scope.packageList.push( packageData )
							return true;
						}
						
						// TODO: The refactoring above was not tested.
						// if( !packageData.isPromo && !packageExists( packageData.id ) ){
						// 	$scope.packageList.push( packageData )
						// 	return true;
						// } else if( packageData.isPromo && packageData.promoAndPackage && !packageExists( packageData.id ) ){
						// 	$scope.packageList.push( packageData )
						// 	return true;
						// } else if( packageData.isPromo && !hidePromo && !packageExists( packageData.id ) ){
						// 	$scope.packageList.push( packageData )
						// 	return true;
						// }
					}
				}
				
				return false;
			}
			
			function packageExists( packageID ){
				for( var i = 0; i < $scope.packageList.length; i++ ){
					if( $scope.packageList[ i ].id == packageID ){
						return true;
					}
				}
				
				return false;
			}
			
			function addStartRateToAllPackages(){
				// Determine if we have a starting price.
				for( var i = 0; i < $scope.packageList.length; i++ ){
					var p = $scope.packageList[ i ];
					p.CT = ( angular.isArray( p.CT ) ) ? p.CT : [ p.CT ];

					// Check if we have a price label override, if so, use it, otherwise, determine a starting price.
					if( angular.isDefined( p.CHARACS ) && angular.isDefined( p.CHARACS.price_label_override ) ){
						p.startRate = p.CHARACS.price_label_override;
					
					} else {
						var startRate = 0
						  , currentRate = 0;
						
						for( var j = 0; j < p.CT.length; j++ ){
							currentRate = Number( p.CT[ j ].retail_amount );
							if( startRate == 0 && currentRate > 0 ){
								startRate = currentRate;
							}
							
							startRate = ( currentRate < startRate && currentRate > 0 ) ? currentRate : startRate;
						}
						
						// Final formatting of data before the presentation.
						p.startRate = $filter( 'currencyFormatter' )( startRate , $scope.currencyCode );
					}
				}
			}
			
			function getFirstKeywordFromMenu(){
				var keyword = '';

				for( var i = 0; i < menuItems.length; i++ ){
					// Skip items that are not active.
					if( !menuItems[ i ].isactive ){
						continue;
					}

					// If it has a sub-keyword, use that keyword if it's active.
					// @TODO: Mark the selected keyword as selected.
					if( menuItems[ i ].MENUITEM ){
						for( var j = 0; j <  menuItems[ i ].MENUITEM.length; j++ ){
							if( menuItems[ i ].MENUITEM[ j ].isactive ){
								keyword = menuItems[ i ].MENUITEM[ j ].keyword;
								break;
							}

							if( keyword != '' ){
								break;
							}
						}
					} else {
						keyword = menuItems[ i ].keywords;
						break;
					}
				}

				return keyword;
			}

			function isKeywordActive( keyword ){
				// Iterate through menu collection in appDataService and validate keyword is active.
				// Reference to menu tree.
				for( var i = 0; i < menuItems.length; i++ ){
					if( menuItems[ i ].keywords.toLowerCase() == keyword.toLowerCase() ){
						// TODO: This does not resolve for an inactive subKeyword with an active parent keyword of same value.
						// EG: parent keyword 'Daily Tickets' isactive = true
						//     sub keyword 'Daily Tickets' isactive = false.
						return menuItems[ i ].isactive;
					}

					if( menuItems[ i ].MENUITEM ){
						for( var j = 0; j < menuItems[ i ].MENUITEM.length; j++ ){
							if( menuItems[ i ].MENUITEM[ j ].keywords.toLowerCase() == keyword.toLowerCase() ){
								return menuItems[ i ].MENUITEM[ j ].isactive;
							}
						}
					}
				}

				return false
			}

			function getKeywordMenuByKeyword( keyword ){
				var kw = '';
				var broke = false;

				for( var i = 0; i < menuItems.length; i++ ){
					if( broke ){
						break;
					}

					// First check if this is a parent menu and if so check its subkeywords.
					if( menuItems[ i ].MENUITEM ){
						if( menuItems[ i ].keywords == keyword ){
							menuItems[ i ].selected = true;
							menuItems[ i ].showSubMenu = true;
							menuItems[ i ].MENUITEM[ 0 ].selected = true;
							appDataService.keywordMenuItems = menuItems[ i ].MENUITEM;
							break;
						} else {
							for( var j = 0; j < menuItems[ i ].MENUITEM.length; j++ ){
								if( menuItems[ i ].MENUITEM[ j ].keywords == keyword ){
									menuItems[ i ].selected = true;
									menuItems[ i ].showSubMenu = true;
									menuItems[ i ].MENUITEM[ j ].selected = true;
									appDataService.keywordMenuItems = menuItems[ i ].MENUITEM;
									broke = true;
									break;
								}
							}
						}
					} else if( menuItems[ i ].keywords == keyword ){
						// If it's not a prent menu item, check the keyword for this item.
						menuItems[ i ].selected = true;
						appDataService.keywordMenuItems = [ menuItems[ i ] ];
						break;
					}

					if( broke ){
						break;
					}
				}
				return appDataService.keywordMenuItems;
			}

			function cleanPackages(){
				// Single items are coming back as objects where multiple items come back in array.
				// If it's single, store it in an array.
				if( $scope.packages ){
					if( !angular.isArray( $scope.packages ) ){
						$scope.packages = [ $scope.packages ];
					}
				}

				// Ensures that CT object is also an array.
				for( var i = 0; i < $scope.packages.length; i++ ){
					if( $scope.packages[ i ].CT && !angular.isArray( $scope.packages[ i ].CT ) ){
						$scope.packages[ i ].CT = [ $scope.packages[ i ].CT ];
					}
				}
			}

			function fetchPromoCodePackage(){
				// Check the packages for the promocode before making request.
				var promoPkgs = getPkgsWithPromocode( $routeParams.promocode )
				if( promoPkgs ){
					handlePackageListUpdates( promoPkgs, $routeParams.promocode );
					return;
				}
				
				$scope.showSpinner( $scope.i18n.ApplicationLabels.searchingForPromoMsg );
				requestPending = true;
				appDataService.fetch
					( $scope.services.GET_MERCHANT_PACKAGE_LIST
					, false
					, { promos_only: 'Y'
					  , promo_codes: $routeParams.promocode
					  , show_details: '1'
					  , long_desc_flag: '1'
					  }
					 ).then( 
						function( data ){
							if( data.status == $scope.status.OK ){
								handlePackageListUpdates( data, $routeParams.promocode );
							} else if( data.status == $scope.status.FAILED ){
								// The request failed.
								$scope.showNoPkgMsg = true;

								// Hide the keyword nav if no packages are loaded.
								// @TODO: Should we just use one flag for both?
								$scope.showSubMenu = false;
							}
							
							requestPending = false;
							if( !requestPending && !geoPending ){
								$scope.hideSpinner();
							}
						}
					 )
			}
			
			function getPkgsWithPromocode( promocode ){
				var packList = { PS: { P: [] } }
				for( var i = 0; i < merchantPackageListData.PS.P.length; i++ ){
					var pkgPromo = merchantPackageListData.PS.P[ i ].promo_code || '';
					
					if( pkgPromo.toLowerCase() == promocode.toLowerCase() ){
						packList.PS.P.push( merchantPackageListData.PS.P[ i ] );
					}
				}
				
				if( packList.PS.P.length > 0 ){
					return packList;
				}
				
				return null;
			}

			function handlePackageListUpdates( data, filter ){
				// Handle package list update. This happens when we get a promocode request.
				if( !angular.isArray( data.PS.P ) ){
					data.PS.P = [ data.PS.P ];
				}

				for( var i = 0; i < data.PS.P.length; i++ ){
					var hasItem = false;
					for( var j = 0; j < $scope.packages.length; j++ ){
						hasItem = ( $scope.packages[ j ].id == data.PS.P[ i ].id );

						if( hasItem ){
							addPromoDetails( $scope.packages[ j ] )
							break;
						}
					}

					if( !hasItem ){
						$scope.packages.push( data.PS.P[ i ] );
						// Append the code to the system.
						if ( typeof data.PS.P[ i ].promo_code != 'undefined' ){
							appendPromoCodeToStorage( data.PS.P[ i ].promo_code.toString() );
							appendPromoPackageToPackageData( data );
						}
					}
				}

				if( filter ){
					getPackageKeywordList( filter );
					$scope.showSubMenu = false;
				}
			}
			
			function addPromoDetails( packageData ){
				// Some promo packages don't contain the promo_code property and that can be problematic.
				// This block adds the promo_code property to the package if it doesn't already exist and 
				// only if its applicable (i.e. promocode exists in the URL).
				var defInURL = angular.isDefined( $routeParams.promocode );
				var defInProp = angular.isDefined( packageData.promo_code );
				var indexOfProp =
					(
						defInProp &&
						( packageData.promo_code.toLowerCase().indexOf( $routeParams.promocode.toLowerCase() ) > -1 )
					);
				
				if( defInURL && ( !defInProp || !indexOfProp ) ){
					// If promo is in the URL and promo code does not exist in the package property, add it.
					packageData.promo_code = packageData.promo_code || '';
					if( packageData.promo_code.length > 0 ){
						packageData.promo_code += ',';
					}
					
					if( !packageData.isPromo ){
						packageData.promoAndPackage = true;
					}
					
					packageData.promo_code += $routeParams.promocode;
					packageData.isPromo = true;
				}
			}

			function appendPromoCodeToStorage( pendingPromoCode ){
				if ( appDataService.promoCodes.indexOf( pendingPromoCode ) == -1 ){
					// We have a promo_code, NOT in the pending_promo_codes list, let's add it.
					appDataService.promoCodes.push( pendingPromoCode );
					// update the LocalStorage.
					appDataService.storageService.set( 'promo_codes' , appDataService.promoCodes, true );
				}
			}
			
			function appendPromoPackageToPackageData( data ){
				// We've received a promocode package and need to append it to the packagelist data.
				var replaced = false;
				var packageListData = merchantPackageListData;
				for( var i = 0; i < data.PS.P.length; i++ ){
					var pItem = data.PS.P[ i ];
					pItem.isPromo = true;
					
					if( ( !angular.isDefined( packageListData ) || packageListData.status == enumsService.status.FAILED ) && !replaced ){
						// This accounts for situations where no packages are defined and user loaded a promocode.
						// The packages data will have a status of FAILED. Overwrite the response on appDataService with data.
						// Do this exactly once.
						merchantPackageListData = data;
						packageListData = data;
						replaced = true;
					} else {
						// Append the response with the packages from data.
						if( !packageExists( pItem.id ) ){
							// Added conversion to an Array because this is throwing an error otherwise
							packageListData.PS.P = angular.isArray(packageListData.PS.P) ? packageListData.PS.P : [packageListData.PS.P];
							packageListData.PS.P.push( pItem );
						}
					}
				}
			}
			
			initializeController();
		 }
  ]
)
