angular.module( 'app' )
.controller
( 'PackageDetailsController'
, [   '$scope'
	, '$routeParams'
	, '$rootScope'
	, '$filter'
	, 'cartService'
	, 'packageService'
	, 'appDataService'
	, 'merchantPackageListData'
	, function
		( $scope
		, $routeParams
		, $rootScope
		, $filter
		, cartService
		, packageService
		, appDataService
		, merchantPackageListData
		){
			window.scrollTo( 0, 0 );
			var qtyAdded = 0;
			var imageSource = '';
			var assetsPath = appDataService.assets.url;
			var packageID = $routeParams.packageId;

			$scope.customerTypes = [];

			// TODO: Determine these settings based on config.
			// TODO: Is this in the new design?
			$scope.showPassbookEnabled = false;
			$scope.showFaceBookButton = false;

			/**
			 * Handles the social icon button click event.
			 * @return undefined
			 */
			$scope.onSocialButtonClick = function(){
				// TODO: Handle social icon click event.
			}

			/**
			 * Handles back button click event. Routes back to the packagelist.
			 * @return undefined
			 */
			$scope.routeToPackageList = function(){
				// Route back to package list.
				if( appDataService && appDataService.lastKeyword ){
					$scope.routeTo( $scope.routes.LISTKEYWORD + '/' + appDataService.lastKeyword );
				} else {
					$scope.routeTo( $scope.routes.LIST );
				}
			}

			$scope.decrementQty = function( data ){
				packageService.decrementQty( data, false );
			}

			$scope.incrementQty = function( data ){
				packageService.incrementQty( data, false );
			}

			$scope.onQtyChange = function( data ){
				packageService.onQtyChange( data, false );
			}

			$scope.onAddToCart = function(){
				cartService.addToCart( $scope.customerTypes, $scope.packageDetails );
			}
			
			/***-----------------------------------------------------------------------
			 * PRIVATE
			 *------------------------------------------------------------------------**/
			
			function initializeController(){
				// Reset the advaced ticket flags.
				packageService.isAdvancedTicket = false;
				packageService.advanceTicketDate = null;
				packageService.cartLimit = merchantPackageListData.cart_limit;
					
				// If packageID is not defined route back to packageList.
				if( !packageID ){
					$scope.routeToPackageList();
					return;
				}
				
				$scope.packageDetails = getPackageDetails( packageID );
				setPackageDetails( $scope.packageDetails );
				$scope.hideSpinner();
			}
			
			function getPackageDetails(){
				var packageArray = ( ( merchantPackageListData ).PS || {} ).P || [];
				var pkgData = null;
				packageArray = angular.isArray( packageArray ) ? packageArray : [ packageArray ]
				
				if( packageArray.length == 0 ){
					// No details for this package. Something went wrong.
					return;
				}

				for( var i in packageArray ){
					if ( packageArray[ i ].id == packageID ){
						pkgData = packageArray[ i ];
						break;
					}
				}
				
				return pkgData;
			}
			
			function setPackageDetails( pkgData ){
				if( hasValidFrom( pkgData ) ){
					updateAdvancedTicketDesciption( pkgData );
				}
				
				$scope.customerTypes = setCustomerType( pkgData );
				
				if( typeof pkgData.shop_image_v4 == 'string' && pkgData.shop_image_v4 != '' ){
					$scope.packageImage = assetsPath + pkgData.shop_image_v4;
				}
			}
			
			function setCustomerType( packageData ){
				var customerTypes = [];
				packageData.CT = ( angular.isArray( packageData.CT ) ) ? packageData.CT : [ packageData.CT ];

				// Iterate through all CTs in this package and store references to
				// relevant information in $scope.customerTypes.
				// TODO: Can a pacakge come back with either a min/max qty undefined. If so, account for that.
				for( var i in packageData.CT ){
					var itemRef =
						{ package_id: packageData.id
						, max_quantity: Number( packageData.max_quantity )
						, min_quantity: Number( packageData.min_quantity )
						, extra_movie: packageData.extra_movies
						, customer_type: packageData.CT[ i ].id
						, cusTypeName: packageData.CT[ i ].name
						, cusTypeRetail: Number( packageData.CT[ i ].retail_amount )
						, qty: 0
						, promo_code: ( typeof packageData.promo_code != 'undefined' ) ? packageData.promo_code : '' 
						, cart_limit_override: packageData.cart_limit_override
						, CHARACS:
							[ 
								{ shop_rate_name: packageData.CT[ i ].name
								, shop_rate_id: ''
								, shop_google_sales_class_title: ''
								}
							 ]
						};
					
					if( packageService.isAdvancedTicket ){
						itemRef.CHARACS[ 0 ].valid_from = packageService.advanceTicketDate;
					}
					
					customerTypes.push( itemRef );
				}
				// If there is only on rate type, set the qty to min qty.
				if( customerTypes.length == 1 ){
					customerTypes[ 0 ].qty = Number( customerTypes[ 0 ].min_quantity );
				}
				
				packageService.customerTypes = customerTypes;

				return customerTypes;
			}
			
			/**
			 * Checks if the object is an advance ticket ticket
			 * @param {object} packageData
			 * @return {boolean} true if the packageData contains CHARACS for advance date
			 *
			 */
			function hasValidFrom( packageData ){
				if( packageData.CHARACS && packageData.CHARACS.valid_from ){
					packageService.isAdvancedTicket = true;

					// Set the flag.
					packageService.advanceTicketDate = packageData.CHARACS.valid_from
				} else {
					packageService.isAdvancedTicket = false;
					packageService.advanceTicketDate = null;
				}

				return packageService.isAdvancedTicket;
			}
			
			/**
			 * The advance data parameters can be found within the packageDetails CHARACS or
			 * within the rates CHARACS
			 * If data is undefined it will check advance data parameter within the packageDetails
			 *
			 * @param {object} packageData
			 *
			 */
			function updateAdvancedTicketDesciption( packageData ){
				// Set the advanceTicketDate identifier.
				packageService.advanceTicketDate = packageData.CHARACS.valid_from;

				packageData.desc =
					$rootScope.i18n.replaceAllMultiple
						( [ '@@date' ]
							, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
							, $rootScope.i18n.replaceAllMultiple
							( [ '@@Date' ]
								, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
								, packageData.desc
							 )
						 );

				packageData.headline =
					$rootScope.i18n.replaceAllMultiple
						( [ '@@date' ]
							, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
							, $rootScope.i18n.replaceAllMultiple
							( [ '@@Date' ]
								, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
								, packageData.headline
							 )
						 );

				packageData.more_info =
					$rootScope.i18n.replaceAllMultiple
						( [ '@@date' ]
							, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
							, $rootScope.i18n.replaceAllMultiple
							( [ '@@Date' ]
								, [ $filter( 'date' )( packageData.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
								, packageData.more_info
							 )
						 )	;
			}
			
			$scope.$on
				( '$routeChangeStart'
				, function( angularEvent, current, previous ){
					// Reset the advaced ticket flags.
					packageService.isAdvancedTicket = false;
					packageService.advanceTicketDate = null;
				  }
				)
			
			initializeController();
		 }
  ]
)
