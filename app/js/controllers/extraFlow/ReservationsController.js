angular.module('app')
.controller
( 'ReservationsController'
, [   '$scope'
	, '$location'
	, '$routeParams'
	, '$log'
	, 'packageService'
	, 'appDataService'
	, 'analyticService'
	, 'extraFlowService'
	, function
		( $scope
		, $location
		, $routeParams
		, $log
		, packageService
		, appDataService
		, analyticService
		, extraFlowService
		){
	
			window.scrollTo( 0, 0 );
			$scope.reservations = [];
			$scope.packageName = '';
			$scope.sameLastNameChk = 0;
			$scope.smLastName = '0';
			
			/***-----------------------------------------------------------------------
			 * PUBLIC
			 *------------------------------------------------------------------------**/
			
			$scope.save = function( data ){
				$scope.theForm.submitted = true;
				
				if( $scope.theForm.$valid ){
					for( var i = 0; i < packageService.itemsToBeAdded.length; i++ ){
						var item = packageService.itemsToBeAdded[i];
						item.CHARACS = item.CHARACS || [];
						
						for( var j = 0; j < item.qty; j++ ){
							item.CHARACS[j] = item.CHARACS[j] || {};
							
							var itemCharac = item.CHARACS[j];
							itemCharac.rv_first_name = $scope.reservations[j].firstName;
							itemCharac.rv_last_name = $scope.reservations[j].lastName;
						}
					}
				}
				
				// Add reference of XFC to CHARACs.
				extraFlowService.addReferenceToCartItems( packageService.settings.view, packageService.itemsToBeAdded, extraFlowService.CLIENT_XFC_VIEW );
				extraFlowService.addReferenceToCartItems( packageService.settings.name, packageService.itemsToBeAdded, extraFlowService.CLIENT_XFC_NAME );
						
				// Navigate to the next XFC.
				packageService.nextXFC();
			}
			
			$scope.cancel = function(data){
				// Handle the cancel button clicked.
				packageService.cancelXFC();
			}
			
			$scope.handleTxtChanged = function(index){
				if( index == 0 && $scope.smLastName == '1' ){
					copyLastName();
				}
			}
			
			$scope.handleChkChanged = function(){
				if( $scope.smLastName == '1' ){
					copyLastName();
				}
			}
			
			/***-----------------------------------------------------------------------
			 * PRIVATE
			 *------------------------------------------------------------------------**/
			
			function initializeController(){
				// Ensure we have packages to add. This would not be true if user refreshed the browser.
				if( !packageService.initXFC() )
					return;
				
				// Generate a model reference for each item being added.
				$scope.reservations = [];
				
				for( var i = 0; i < packageService.itemsToBeAdded.length; i++ ){
					for( var j = 0; j < packageService.itemsToBeAdded[i].qty; j++ ){
						var ctName = packageService.getCTNameByID( packageService.itemsToBeAdded[i].customer_type );
						$scope.reservations.push
							(
								{ customerTypeName: ctName
								, firstName: ''
								, lastName: ''
								}
							);
					}
				}
				
				// Get name of package.
				$scope.packageName = packageService.packageDetails.name;
			}
			
			function copyLastName(){
				for( var i = 0; i < $scope.reservations.length; i++ ){
					if( i == 0 ){
						continue;
					}
					
					$scope.reservations[i].lastName = $scope.reservations[0].lastName;
				}
				
				try{
					if(!$rootScope.$$phase){
						$rootScope.$apply();
					}
				} catch(e){}
			}
			
			$scope.hideSpinner()
			initializeController()
		 }
  ]
)
