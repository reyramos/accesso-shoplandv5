angular.module( 'app' )
.controller
( 'DateTimeController'
, [   '$scope'
	, '$filter'
	, 'packageService'
	, 'languageService'
	, 'extraFlowService'
	, function
		( $scope
		, $filter
		, packageService
		, languageService
		, extraFlowService
		){
			window.scrollTo( 0, 0 );
			var eventID = ''
			  , calendarType = -1;
			
			// Init first before executing remainder of the code:
			// Ensure we have packages to add. This would not be true if user refreshed the browser.
			if( !packageService.initXFC() )
				return;
			
			// Parameters
			$scope.heading = '';
			$scope.timeSelection = '';
			$scope.showNoDateMessage = false;
			$scope.showTimeList = false;
			$scope.timeList = [];
			$scope.selectedDate = null;
			
			// Hide the spinner.
			$scope.hideSpinner();
			
			// Callback for calendar directive.
			$scope.callbacks =
				{ eventIdChanged: function( newId ){
					eventID = newId;
				  }
				, typeChanged: function( type ){
					// Set the appropriate heading locale.
					$scope.heading = ( type == '1' ) ? languageService.DateTime.dateTimeHeading : languageService.DateTime.dateOnlyHeading;
					
					// Show (type == 1) / hide (type == 2 (or anything else)) the timeList based on type. 
					$scope.showTimeList = ( type == '1' );
					calendarType = type;
				  }
				, gettingDates: function(){
					$scope.showNoDateMessage = false;
					$scope.showSpinner( languageService.ApplicationLabels.gettingEventDates );
				  }
				, gotDates: function(){
					$scope.hideSpinner();
				  }
				, timeListChanged: function( timeList ){
					$scope.timeList = timeList;
					$scope.showTimeList = ( timeList && timeList.length > 0 );

					if(timeList[0].time == ''){
						$scope.showTimeList = false
					}
					
					// Override timelist visibility.
					// Show (type == 1) / hide (type == 2 (or anything else)) the timeList based on type. 
					$scope.showTimeList = ( calendarType == '1' );
				  }
				, timeSelectionChanged: function( timeSelection ){
					// If there is not at least one date, show the message.
					if( !timeSelection ){
						$scope.showNoDateMessage = true;
					}
						
					$scope.timeSelection = timeSelection;
				  }
				,	showOrHideNoDateMsg: function( val ){
					// If there is not at least one date, show the message.
					$scope.showNoDateMessage = val;
				  }
				};
			
			// Options for calendar dierective.
			$scope.calOptions =
				{ showWeeks: false
				, enableTimeLimit: false
				, resourcesNeeded: false
				, autoRequest: true
				, selectedDateString: ''
				};
				
			$scope.minCapacity =
				( function getMinCapacity(){
					var qty = 0
					  , i = 0;
					
					// TODO: Should probably be looking at packageService.itemsToBeAdded instead.
					for( i = 0; i < packageService.customerTypes.length; i++){
						qty += packageService.customerTypes[ i ].qty;
					}
					
					return qty;
				  }
				)();
				
			$scope.customerTypeList =
				( function getCustomerTypesList(){
					var ctsId = []
					  , i = 0
					  , item;
					  
					for( i = 0; i < packageService.itemsToBeAdded.length; i++ ){
						item = packageService.itemsToBeAdded[ i ];
						
						if( item.qty > 0 && item.package_id == packageService.packageDetails.id ){
							ctsId.push( item.customer_type );
						}
					}
					
					return ctsId.join( "," );
				  }
				)();
			
			$scope.save = function( data ){
				// By now, the calendarDirective has taken care of everything 
				// except saving and canceling.
				
				// Ensure date is selected.
				// If applicable, ensure time is selected.
				// Write the values to the data object.
				// Pass ownership to ExtraFlowService.
				if( !$scope.selectedDate ){
					// Date not selected. User is notified by displayed message.
					return;
				} else if( $scope.timeList && $scope.timeList.length > 0 && !$scope.timeSelection ){
					// Time required and not selected. User is notified by displayed message.
					return;
				}
				
				for( var i = 0; i < packageService.itemsToBeAdded.length; i++ ){
					var item = packageService.itemsToBeAdded[ i ];
					item.CHARACS = item.CHARACS || [];
					
					for( var j = 0; j < item.qty; j++ ){
						item.CHARACS[j] = item.CHARACS[ j ] || {};
						
						var itemCharac = item.CHARACS[ j ];
						itemCharac.event_id = eventID;
						itemCharac.start_date = $filter( 'date' )( $scope.selectedDate, 'yyyy-MM-dd' );
						itemCharac.start_time = $scope.timeSelection.time == ""?"":$scope.timeSelection.time;
						itemCharac.timeblock = $scope.timeSelection.timeblock;
						itemCharac.description = $scope.timeSelection.description;
					}
				}
				
				// Add reference of XFC to CHARACs.
				extraFlowService.addReferenceToCartItems( packageService.settings.view, packageService.itemsToBeAdded, extraFlowService.CLIENT_XFC_VIEW );
				extraFlowService.addReferenceToCartItems( packageService.settings.name, packageService.itemsToBeAdded, extraFlowService.CLIENT_XFC_NAME );
				
				// Navigate to the next XFC.
				packageService.nextXFC();
			}
			
			$scope.cancel = function(){
				// Handle the cancel button clicked.
				packageService.cancelXFC();
			}
	  }
  ]
)
