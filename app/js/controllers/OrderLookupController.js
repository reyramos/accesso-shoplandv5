/**
 * # OrderLookupController
 *
 * Controller that manages Order look up related screens.
 */
angular.module('app').controller
	( 'OrderLookupController',
		[ '$scope'
		, '$log'
		, '$location'
		, '$routeParams'
		, 'appDataService'
		, 'connectionService'
		, 'enumsService'
		, 'orderHistoryService'
		, 'languageService'
		, 'analyticService'
		, 'modalService'
	, function
		( $scope
		, $log
		, $location
		, $routeParams
		, appDataService
		, connectionService
		, enumsService
		, orderHistoryService
		, languageService
		, analyticService
		, modalService
		){
			window.scrollTo( 0, 0 );
			$scope.hideSpinner()
			
			$scope.showNoOrdersFoundMsg = false
			$scope.orders = orderHistoryService.orders;
			$scope.lastFourDigits = orderHistoryService.lastFourDigits;
			$scope.phoneNumber = orderHistoryService.phoneNumber;
			$scope.emailAddress = orderHistoryService.emailAddress;
			
			$scope.showErrorMessage = false;
			$scope.errorMessageText = '';
			
			if ( angular.isDefined($routeParams.err) 
				&& angular.isDefined(languageService.OrderLookupErrorCodes[$routeParams.err]) 
			) {
				$scope.showErrorMessage = true;
				$scope.errorMessageText = languageService.OrderLookupErrorCodes[$routeParams.err];
			}
			
			if( typeof( $scope.emailAddress ) == undefined || $scope.emailAddress == '' ){
				$scope.routeTo( enumsService.routes.ORDER_SEARCH );
			}
			
			// Dev mode only.
			if( $scope.autofill ){
				$scope.orderLookup = {};
				$scope.orderLookup.phoneNumber = '1234567890';
				$scope.orderLookup.emailAddress = 'frontend-js@accesso.com';
			}
			
			function getTimeStr( orderItem ){
				var timeStr = '';
				
				try{
					// This assumes time always comes back in military time.
					var timeArr = orderItem.order_date.split( ' ' )[ 1 ].split( '.' )[ 0 ].split( ':' );
					if( timeArr.length == 3 ){
						// Remove seconds and anything after that.
						timeArr = timeArr.splice( 0, 2 );
					}
					
					if( Number( timeArr[ 0 ] ) > 12 ){
						timeArr[ 1 ] += ' PM';
						timeArr[ 0 ] = String( Number( timeArr[ 0 ] ) - 12 );
						if( timeArr[ 0 ].length == 1){
							timeArr[ 0 ] = '0' + timeArr[ 0 ];
						}
					} else if( Number( timeArr[ 0 ] ) == 12 ){
						timeArr[ 1 ] += ' PM';
					} else if( Number( timeArr[ 0 ] ) == 0 ){
						timeArr[ 0 ] = 12;
						timeArr[ 1 ] += ' AM';
					} else {
						// If time is not military time, this will not work. All time will be AM.
						timeArr[ 1 ] += ' AM';
					}
					
					timeStr = timeArr.join( ':' );
				} catch( e ){}
				
				return timeStr;
			}
			
			$scope.process = function( data ){	
				$scope.orderLookupForm.submitted = true;
				
				if ($scope.orderLookupForm.$valid){
					orderHistoryService.orders = [];
					orderHistoryService.lastFourDigits = data.lastFourDigits;
					orderHistoryService.phoneNumber = data.phoneNumber;
					orderHistoryService.emailAddress = data.emailAddress;
					
					$scope.lastFourDigits = orderHistoryService.lastFourDigits;
					$scope.phoneNumber = orderHistoryService.phoneNumber;
					$scope.emailAddress = orderHistoryService.emailAddress;
					
					// Show the spinner.
					$scope.showSpinner( languageService.ApplicationLabels.orderSearchLoadMsg );
					
					appDataService.fetch
						( enumsService.services.ORDER_SEARCH
						, false
						, data
						).then
							(
								function( data ){
									// Hide the spinner.
									$scope.hideSpinner();
									$scope.orderLookupForm.submitted = false;
									

									if( data.status == $scope.status.OK ){
										var _orders = angular.isArray( data.MATCH ) ? data.MATCH : [ data.MATCH ] ;

										for( var i = 0; i < _orders.length; i++ ){
											var orderItem = _orders[ i ];
											if( typeof( orderItem.order_date ) != 'string' ){
												// If the order_date was already converted to date object...
												continue;
											}
											
											var dateObj = null;
											var timeStr = getTimeStr( orderItem );
											
											try{
												dateObj = orderItem.order_date.split( ' ' )[ 0 ].split( '-' );
											} catch( e ){}
											
											if( dateObj ){
												orderItem.order_date = new Date( Number( dateObj[ 0 ] ), Number( dateObj[ 1 ] ) - 1, Number( dateObj[ 2 ] ) );
											}
											
											orderItem.order_time = timeStr;
											orderItem.order_link = 'orderView/' + $scope.emailAddress + '/' + $scope.phoneNumber + '/' + orderItem.order_id;
										}
										
										orderHistoryService.orders = _orders;
										$scope.orders = _orders;
										$scope.routeTo( enumsService.routes.ORDER_SEARCH );
									} else if( data.status == $scope.status.FAILED ){
										modalService.displayModal({
											title: $scope.i18n.Alert.noOrdersFoundTitle
											, headerFlag: modalService.DANGER
											, description: $scope.i18n.Alert.noOrdersFoundMsg
											, buttonFlags: [$scope.buttonFlags.OK]
											})
									}
									
								}
							);
				}
			}
		}
	]);
