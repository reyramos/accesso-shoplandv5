angular.module( 'app' )
.controller
( 'ShippingController'
, [   '$scope'
	, '$routeParams'
	, '$rootScope'
	, '$log'
	, '$location'
	, '$filter'
	, 'analyticService'
	, 'appDataService'
	, 'cartService'
	, 'connectionService'
	, 'consolidatedData'
	, 'countriesJSon'
	, 'enumsService'
	, 'languageService'
	, 'userInfoService'
	, 'utilitiesService'
	, function
		( $scope
		, $routeParams
		, $rootScope
		, $log
		, $location
		, $filter
		, analyticService
		, appDataService
		, cartService
		, connectionService
		, consolidatedData
		, countries
		, enums
		, i18n
		, userInfoService
		, utilitiesService
		){
			window.scrollTo( 0, 0 );
			// TODO: We'll probably need one of these for international shipping.
			// TODO: May also need config to enable/disbale international shipping.
			// TODO: BH may want to disable international shipping after a date based on config.
			
			// Add all non physical delivery methods here. Default values are overwritten by config.
			var nonPhysicalDeliveryMethods =
					[ 'M' // Mobile
					, 'P' // Print at Home
					, 'Y' // Will Call
					];


			$scope.hideSpinner();
			
			$scope.zipType = 'tel';
			$scope.dataFormEnabled = false;
			$scope.formInited = false;
			$scope.selectedShipMethDescription = '';
			$scope.countries = countries.countries;
			$scope.regions = countries.regions;
			$scope.optins = [];
			$scope.deliveryMethods = [];

			$scope.$on('ZIPTASTIC',function(event,data){
				$scope.shipping.country = data.country
				$scope.shipping.city = data.city.toLowerCase()
				$scope.shipping.state = data.state
			})


			$scope.checkItem = function(index){
				$scope.deliveryMethods[index].selected = true;

				for(i in $scope.deliveryMethods){
					if(index != i){
						delete $scope.deliveryMethods[i].selected;
					}
				}

				$scope.shipping.deliveryAmount = getDeliveryAmountByDeliveryType( $scope.deliveryMethod );
				handleDeliveryMethodChanged();
			}
			
			$scope.save = function(data){
				if($scope.hasItemsInCart()){
					$scope.dataForm.submitted = true

					if( verifyInternationDelivery() )
						return;

					if( $scope.dataForm.$valid ) {
						// Save the optins into the billing object.
						$scope.shipping.optins = $scope.optins;

						// Store the Shipping Data in userInfoService.
						$scope.shipping.deliveryAmount = getDeliveryAmountByDeliveryType( $scope.shipping.deliveryMethod );
						$scope.shipping.isNonPhysicalDeliveryMethod = isNonPhysicalDeliveryMethod( $scope.shipping.deliveryMethod );
						$scope.shipping.shippingAvailable = true;
						$scope.shipping.valid = true;

						//save the userData in the userInfoService
						userInfoService.set ('shipping',$scope.shipping)

						//appDataService.shipping = $scope.shipping;
						appDataService.shipping = {
							valid : true
						}

						$log.log('####### setting userInfoService.payment.shipMethod = ',$scope.shipping.deliveryMethod);
						$log.log('####### userInfoService.payment.shipAmount = ',$scope.shipping.deliveryAmount);

						/*	--------------------------------------------------------
						 *	START :: PayPal / Shipping Method Integration
						 *	--------------------------------------------------------
						 */
						if( appDataService.billing.valid ) {
							// Need the following to disable crossSells from appearing on the return.
								appDataService.crossSells = false;
							// Apply the PayPal information to the necessary data points for purchase.

							// @TODO: need to figure out a way to pass in the data I need for the updateCartCheckoutInfo.
							// @TODO: Hardcode shipping method until we have shipping form implemented.
							// This should come in from the selection by the user, or the defaulted value.
							// Save the final data before redirecting to orderReview.
							// commenting out for now. may not need it.
							// @TODO: Save this data in the shipping form when we implement it.

							var myUpdateCartCheckoutInfoData =
							{ 	  shippingMethod : $scope.shipping.deliveryMethod
								, shippingAmount : $scope.shipping.deliveryAmount
								, billingId      : appDataService.payment.billingId
								, skip_requests  : 'SetExpressCheckout'
							};

							cartService.callUpdateCartCheckoutInfo(
								myUpdateCartCheckoutInfoData
								//, merchantDetailsData
								, function(){
									cartService.updatePayPalSessionInfo();

									cartService.fetchGetCartSummary(
										function() { $scope.routeTo( $scope.routes.ORDER_REVIEW ) }
										, function() {

											//save the keys from myUpdateCartCheckoutInfoData to payment scope
											Object.keys(myUpdateCartCheckoutInfoData).forEach(function(key) {
												$scope.payment[key] = myUpdateCartCheckoutInfoData[key]
											});

											//save the current data from shipping into localStorage for later use
											userInfoService.set ('payment',$scope.payment)

											$scope.animateOut
											(
												function(){
													$scope.routeTo( $scope.routes.ORDER_REVIEW );
												}
											)

											$log.error('issue with updating cart. ');
										}
									)
								}
								, function() {

									$scope.animateOut
									(
										function(){
											$scope.routeTo( $scope.routes.ORDER_REVIEW );
										}
									)

									$log.error('issue with updating cart. ');
								}
							);

						}else{
							$scope.animateOut
							(
								function(){
									$scope.routeTo($scope.routes.BILLING);
								}
							)
						}
						/*	--------------------------------------------------------
						 *	END :: PayPal / Shipping Method Integration
						 *	--------------------------------------------------------
						 */
					}
				}
			}
			
			$scope.handleCountriesChanged = function(){
				verifyInternationDelivery();
				$scope.dataForm.submitted = false;
			}
			
			function handleDeliveryMethodChanged(){

				verifyInternationDelivery();
				$scope.dataForm.submitted = false;
				
				if( typeof( $scope.shipping.deliveryMethod ) != 'undefined' ){
					$scope.selectedShipMethDescription = getDeliveryDescriptionByType( $scope.shipping.deliveryMethod );
				} else {
					$scope.selectedShipMethDescription = '';
					$scope.dataFormEnabled = false;
				}
			}

			$scope.setTelephone = function($index){
				if($index == 'US'){
					$scope.zipType = 'tel';
				} else {
					$scope.zipType = 'text';
				}
			}

			
			function initializeController(){

				// Set the shipping information to the data stored in userInfoService.
				$scope.shipping = userInfoService.shipping

				if( typeof( consolidatedData.SETTINGS.nonPhysicalDeliveryMethods ) != 'undefined' ){
					consolidatedData.SETTINGS.nonPhysicalDeliveryMethods = consolidatedData.SETTINGS.non_physical_delivery_methods.split(', ').join(',').split(',');
					nonPhysicalDeliveryMethods = consolidatedData.SETTINGS.nonPhysicalDeliveryMethods;
				}


				var shipReq = userInfoService.shippingRequired( { skipRequests : 'SetExpressCheckout' }, initializeController );

				if( shipReq == 'gettingCartSummary' ){
					return;
				}
				
				// Get reference to delivery methods.
				$scope.deliveryMethods = userInfoService.deliveryMethods;

				if( angular.isArray( $scope.deliveryMethods ) ){
					//select the first value
					$scope.deliveryMethods[0].selected = true

					for(i in $scope.deliveryMethods){
						$scope.priceLabel = Number($scope.deliveryMethods[i].price) == 0?$scope.i18n.ShippingView.priceLabel:$scope.deliveryMethods[i].price;
						if(typeof($scope.deliveryMethods[i].selected) != 'undefined' && $scope.deliveryMethods[i].selected){
							$scope.shipping.deliveryMethod = $scope.deliveryMethods[i].ship_method;
						}
					}
				}

				// If no delivery methods or only one delivery method and it's M redirect to billing.
				if(userInfoService.getDeliveryRouting() == $rootScope.routes.BILLING){
					$scope.routeTo($scope.routes.BILLING);
				}

				// Store delivery methods in cookies as fallback to local storage.
				userInfoService.set('deliveryMethods', $scope.deliveryMethods)
				// Set default country.
				if( !$scope.shipping.country ){
					$scope.shipping.country = appDataService.defaults.country;
					$scope.setTelephone( $scope.shipping.country );
				}

				// Dev mode only.
				if($scope.autofill && typeof( $scope.shipping.firstName ) == 'undefined' ){
					//set the userinfo for dev from userInfoService
					$scope.shipping = utilitiesService.copyObject(userInfoService.userInfo)

					$scope.shipping.deliveryMethod = $scope.deliveryMethods[ 0 ].ship_method

					// Fake some optins on shipping and billing form.
					if(false && appDataService.optins.length == 0 ){
						appDataService.optins =
							[
								{
									"section":"ShippingView",
									"target":"optin1",
									"checked":1
								},
								{
									"section":"ShippingView",
									"target":"optin2",
									"checked":1
								},
								{
									"section":"ShippingView",
									"target":"optin3",
									"checked":1
								}
							];
						}
				}

				// Get the description of the selected delivery method.
				handleDeliveryMethodChanged();
				
				for( var i in appDataService.optins ){
					if(appDataService.optins[i].section == 'ShippingView'){
						$scope.optins.push( appDataService.optins[i] );
					}
				}


				// Store the localized label as part of the optin object.
				for( var i = 0; i < $scope.optins.length; i++ ){
					var section = $scope.optins[i].section;
					var target = $scope.optins[i].target;
					$scope.optins[i].label = $scope.i18n[ section ][ target ];
				}
				
				$scope.dataForm.submitted = false;
				$scope.formInited = true;

			}
			
			function verifyInternationDelivery(){
				var selCountry = $scope.shipping.country
				  , selMethod = $scope.shipping.deliveryMethod
				  , nonPhysDeliMeth = false;

				if( !( typeof( selMethod ) != 'undefined' && selMethod ) )
					return;
				
				if( !( typeof( selCountry ) != 'undefined' && selCountry ) )
					return;
				
				nonPhysDeliMeth = isNonPhysicalDeliveryMethod( selMethod );
				if( selCountry != 'US' && !nonPhysDeliMeth ){
					modalService.displayModal
						(
							{ title: $scope.i18n.Alert.internationalShippingErrorTitle
							, headerFlag: modalService.DANGER
							, description: $scope.i18n.Alert.internationalShippingErrorMsg
							, buttonFlags: [$scope.buttonFlags.OK]
							}
						)
				}
				
				// Enable/disable the form based on delivery method selected if any.
				$scope.dataFormEnabled = ( typeof( $scope.shipping.deliveryMethod ) != 'undefined' && !nonPhysDeliMeth );
				
				return ( selCountry != 'US' && !nonPhysDeliMeth );
			}
			
			function isNonPhysicalDeliveryMethod( type ){
				for( var i = 0; i < nonPhysicalDeliveryMethods.length; i++ ){
					if( type.toLowerCase() == nonPhysicalDeliveryMethods[ i ].toLowerCase() )
						return true;
				}
				
				return false;
			}
			
			function getDeliveryAmountByDeliveryType( type ){
				for( var i = 0; i < $scope.deliveryMethods.length; i++ ){
					if( typeof( $scope.deliveryMethods[i].selected ) != 'undefined' && $scope.deliveryMethods[i].selected ){
						$scope.shipping.deliveryMethod = $scope.deliveryMethods[ i ].ship_method
						return Number( $scope.deliveryMethods[ i ].price );
					}
				}
				
				return 0;
			}
			
			function getDeliveryDescriptionByType( type ){
				var desc = ''
				for( var i = 0; i < $scope.deliveryMethods.length; i++ ){
					if( type.toLowerCase() == $scope.deliveryMethods[ i ].ship_method.toLowerCase() ){
						desc = i18n.ShippingView[ type.toLowerCase() + 'LongDescription' ];
						desc += ' - ' + $filter('currencyFormatter')( Number( $scope.deliveryMethods[ i ].price ), appDataService.currencyCode );
					}
				}
				
				return desc;
			}

			$scope.$on( "$routeChangeSuccess", function( event ){
				// Set valid to false in order to force user to click continue button.
				var locPath = location.pathname.toLowerCase()
				if( locPath.indexOf( enums.routes.SHIPPING.toLowerCase() ) > -1 ){
					if( appDataService.shipping )
						appDataService.shipping.valid = false;

					initializeController();
				}
			})
		 }
	  ]
)
