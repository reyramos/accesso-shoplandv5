/**
 * # KnowledgeBaseController
 *
 * Controller that manages knowledge base screen

 @todo: DO NOT USE LOADING SERVICE FOR KNOWLEDGEBASE UNTIL THE API CODE IS UP TO THE 3.5 IMPLEMENTATION.
 @DONE @todo: IncrementReadCount on show of articles.
 @todo: implement a client side search.
 */
angular.module('app').controller
	( 'KnowledgeBaseController',
		[ '$scope'
		, '$log'
		, 'appDataService'
		, 'connectionService'
		, 'storageService'
		, 'navigationService'
		, 'clientInfoService'
		, 'socketService'
		, 'analyticService'
		, 'languageService'
		, 'modalService'
	, function
		( $scope
		, $log
		, appDataService
		, connectionService
		, storageService
		, navigationService
		, clientInfoService
		, socketService
		, analyticService
		, languageService
		, modalService
		)
		{

	window.scrollTo( 0, 0 );

	$scope.hideSpinner();
	$scope.tabSelection = 'FAQs'

	$scope.faqObject = {
		  currentArticle : -1
		, articles       : []

	}
	$scope.articleIndexStatus = []

	// Used for counting the statistics/views of an article. 
	$scope.incrementReadCount = function($event, currentIndex, articleId){

		// Only call if it's true.
		if( currentIndex > -1 ) {

			$scope.articleIndexStatus[currentIndex] = !$scope.articleIndexStatus[currentIndex]

			//commented out but this will close the any open article when clicking on another
//			for(i in $scope.articleIndexStatus){
//				if(currentIndex != i)
//					$scope.articleIndexStatus[i] = false
//			}

			$log.log('$scope.articleIndexStatus',$scope.articleIndexStatus)


			/*if( $scope.faqObject.currentArticle == currentIndex ) {

				// Looks like we're attempting to close it.
				$scope.faqObject.currentArticle = -1

			} else if( $scope.faqObject.currentArticle != currentIndex ) {

				$scope.faqObject.currentArticle = currentIndex;

				// Using anchor technique., BAD< doesn't work, it reloads the page.
				// window.location.hash = 'articleDetailsIcon' + articleId
				// going to try scrolling.
				var targetArticle = document.getElementById( 'articleDetailsIcon' + articleId);
				//targetArticle.scrollIntoView()

				appDataService.fetch(
					$scope.services.INCREMENT_READ_COUNT
					, false
					, {article_id: articleId}
				).then(
					function( data ) {
						if( data.status == $scope.status.OK ) {
						} else if( data.status == $scope.status.FAILED ) {
						} else {
						}
					}
				)
			}*/
		} else {
		}
	}

	function initializeController() {
		// Show the spinner.
		$scope.showSpinner( $scope.i18n.ApplicationLabels.gettingHelpInfoLoadMsg );
		
		appDataService.fetch(
			  $scope.services.GET_ARTICLES_BY_DISPLAY_ORDER
			, true
			, {}
		).then(
				
			function( data ) {
				
				$scope.hideSpinner();
				
				if( data.status == $scope.status.OK ) {

					if( typeof data.ARTICLES.A != 'undefined' ) {
						
						$scope.faqObject.articles = data.ARTICLES.A;
						$scope.faqObject.articles = angular.isArray($scope.faqObject.articles) 
							? $scope.faqObject.articles : [$scope.faqObject.articles];
						
						// Replace @@supportPhoneNumber from the FAQ with value from settings.
						for( var i = 0; i < $scope.faqObject.articles.length; i++ ) {
							
							$scope.faqObject.articles[i].faq_text = $scope.i18n.replaceAllMultiple(
								  ['@@supportPhoneNumber']
								, [appDataService.GetApplicationConsolidated.SETTINGS.support_phone_number]
								, $scope.faqObject.articles[i].faq_text
							);
							
						}
					} 

				} else if( data.status == $scope.status.FAILED ) {
					
					modalService.displayModal({
						title: $scope.i18n.Alert.loadFAQErrorTitle
						, headerFlag: modalService.DANGER
						, description: $scope.i18n.Alert.loadFAQErrorMsg
						, buttonFlags: [$scope.buttonFlags.YES, $scope.buttonFlags.NO]
						, callback: function( detail ){
							if( detail == $scope.buttonFlags.YES ) {
								// initialize();
							}
						}
					})
						
				} else {
					
					$log.info('KnowledgeBaseController.js >> initializeController() // else');
					
				}
			}
		)
	}

	$scope.$on("$routeChangeSuccess", function (event) {
		initializeController();
	});		
}]);