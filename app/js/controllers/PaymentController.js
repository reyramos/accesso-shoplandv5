/**
 * # PaymentController
 *
 * Controller that manages the payment form.
 */
	angular.module('app').controller
	( 'PaymentController',[
		'$scope'
		, '$rootScope'
		, '$log'
		, '$location'
		, '$routeParams'
		, '$filter'
		, 'cartService'
		, 'connectionService'
		, 'enumsService'
		, 'languageService'
		, 'appDataService'
		, 'merchantDetailsData'
		, 'analyticService'
		, 'shippingService'
		, 'userInfoService'
	, function
		( $scope
		, $rootScope
		, $log
		, $location
		, $routeParams
		, $filter
		, cartService
		, connectionService
		, enumsService
		, languageService
		, appDataService
		, merchantDetailsData
		, analyticService
		, shippingService
		, userInfoService
		){
			window.scrollTo( 0, 0 );
			$scope.hideSpinner();

			$scope.currentYear = languageService.currentYear;
			$scope.currentMonth = languageService.currentMonth;
			$scope.range = $filter('range')([],$scope.currentYear,$scope.currentYear + 10)
			$scope.shipping = userInfoService.getShipping();
			$scope.billing = userInfoService.getBilling();

			$scope.showBackToShipping =
				(
					typeof( $scope.shipping ) != 'undefined'
					&& typeof( $scope.shipping.shippingAvailable ) != 'undefined'
					&& $scope.shipping.shippingAvailable == true
				);

			$scope.shippingAddressAvailable =
				(
					$scope.showBackToShipping
					&& (
							typeof( $scope.shipping.deliveryMethod ) != 'undefined'
							&& $scope.shipping.deliveryMethod == enumsService.deliveryMethods.FIRST_CLASS_MAIL
					   )
				)

			$rootScope.showPayPalCheckout = false;
			$rootScope.useDevice = 'desktop';
			$rootScope.useEnvironment = 'production';

			// this can be used by any function
			$scope.gotoPayPalCheckout = function(){

				$rootScope.useDevice = initializeData.device;
				$rootScope.useEnvironment = initializeData.environment;

				$rootScope.showPayPalCheckout = showPayPalCheckoutOption( consolidatedData, merchantDetailsData );

				cartService.gotoPayPalCheckout();
			}

			$scope.gotoBilling = function() {
				var container = utilitiesService.getObject('#payment-method');
				container.addClass('animate-leave');

				// Ensure there are items in the cart before proceeding.
				if( !$scope.hasItemsInCart() ) {

					return;
				}

				$timeout
					(
						function(){
							$scope.routeTo( $rootScope.routes.BILLING );
						}
					, 600
					)
			}

			$scope.months = function(){
				var months = []
				for(var i in languageService.months){
					value = $filter('leadingZeros')(parseInt(i)+1,2)
					months.push
						( { value: value
						  , text: value + ' - ' + languageService.months[i]
						  }
						)
				}

				return months
			}()

			$scope.years = function(){
				var years = []
				for(var i in $scope.range){
					years.push
						( { value: $filter('rightSubstring')($scope.range[i], 2)
						  , text: '' + $scope.range[i]
						  }
						)
				}
				return years
			}()

			$scope.cardTypes = function(){
				var cardOverride =
					(function(){
						// Get the card override if it exist in the CHARACS of any of the items in the cart.
						for( var i = 0; i < cartService.cartItems.length; i++ ){
							var item = cartService.cartItems[ i ];
							if( typeof( item.CHARACS ) != 'undefined' ){
								for( var j = 0; j < item.CHARACS.length; j++ ){
									for( var key in item.CHARACS[ j ] ){
										if( key == 'card_type_required' )
											return item.CHARACS[ j ][ key ];
									}

								}
							}
						}

						return ''
					})()

				cardTypes = [];

				merchantDetailsData.BILLINGTYPE = angular.isArray( merchantDetailsData.BILLINGTYPE ) ? merchantDetailsData.BILLINGTYPE : [ merchantDetailsData.BILLINGTYPE ];
				for( var i = 0; i < merchantDetailsData.BILLINGTYPE.length; i++ ){
					// Including only the credit cards. Excludes paypal from list.
					if ( merchantDetailsData.BILLINGTYPE[ i ].auth_type == 'EC' ) {
						if( cardOverride == '' || merchantDetailsData.BILLINGTYPE[ i ].card_type_code == cardOverride ){
							cardTypes.push( merchantDetailsData.BILLINGTYPE[ i ].name );
						}
					}
				}

				return cardTypes
			}()


			var getBillingTypeByCardType = function(cardType){
				for(var i = 0; i < merchantDetailsData.BILLINGTYPE.length; i++){
					if( merchantDetailsData.BILLINGTYPE[i].name == cardType){
						return merchantDetailsData.BILLINGTYPE[i]
					}
				}
			}

			$scope.save = function(data){
				// Ensure there are items in the cart before proceeding.
				if( $scope.hasItemsInCart()){
					$scope.paymentForm.submitted = true

					if( !getBillingTypeByCardType()){
						if( $scope.paymentForm.$valid){
							// Reset the payment information.
							// @TODO: Ensure payment info is cleared as user navigates away
							// from completing an order.
							appDataService.payment = {}

							// Determine card type and set relevant information on payment object.
							// Get billing type information.
							var billTypeObj = getBillingTypeByCardType( $scope.payment.ccType )

							// @TODO: Validate the card type based on the billTypeObj data
							// (i.e. ensure that this merchant accepts the type of card entered).

							$scope.payment.billingId = billTypeObj.billing_id
							$scope.payment.billingType = billTypeObj.billing_type
							$scope.payment.authType = billTypeObj.auth_type
							$scope.payment.cepayMerchantId = billTypeObj.cepay_merchant_id

							// Format the expiration date as expected by the server.
							$scope.payment.cardExpirationDate = $scope.payment.year + $scope.payment.month
							$scope.payment.valid = true

							appDataService.payment = {
								valid : true
							}

							// Save this data in the shipping form when we implement it.
							if( typeof( $scope.shipping ) != 'undefined' ){
								$scope.payment.shipMethod = ( typeof( $scope.shipping.deliveryMethod ) != 'undefined' ) ? $scope.shipping.deliveryMethod : enumsService.deliveryMethods.MOBILE;
								$scope.payment.shipAmount = ( typeof( $scope.shipping.deliveryAmount ) != 'undefined' ) ? $scope.shipping.deliveryAmount : 0;
							} else {
								$scope.payment.shipMethod = enumsService.deliveryMethods.MOBILE;
								$scope.payment.shipAmount = 0;
							}

							userInfoService.payment = $scope.payment

							// @TODO: need to figure out a way to pass in the data I need for the updateCartCheckoutInfo.
							/*
							 * * TEMPORARY SETUP
							 * // @TODO: Hardcode shipping method until we have shipping form implemented.
							 * This should come in from the selection by the user, or the defaulted value.
							 *
							 */
							var myData =
							{ shippingMethod: $scope.payment.shipMethod
								, shippingAmount: $scope.payment.shipAmount
								, billingId: $scope.payment.billingId
							}

							// Add reference of request in pendingRequests.
							$rootScope.pendingRequests.push($scope.services.UPDATE_CART_CHECKOUT_INFO)

							// Show the spinner.
							$scope.showSpinner($scope.i18n.ApplicationLabels.updatingPaymentInfoLoadMsg)
							appDataService.fetch
								( $scope.services.UPDATE_CART_CHECKOUT_INFO
									, false
									, myData
								).then
							( function(data){
								  // Remove reference of request in pendingRequests.
								  $rootScope.removePendingRequest($scope.services.UPDATE_CART_CHECKOUT_INFO)

									if( data.status == $scope.status.OK ) {
										function routeToReview(){
											$scope.routeTo($scope.routes.REVIEW);

											// Hide the spinner.
											$scope.hideSpinner();
										}

										shippingService.getCartSummary( false, routeToReview );
									} else if( data.status == $scope.status.FAILED){
										// The request to updateCartItem failed.
										modalService.displayModal
											(
												{ title: $scope.i18n.Alert.failedUpdateCheckoutInfoTitle
												, headerFlag: modalService.DANGER
												, description: $scope.i18n.Alert.failedUpdateCheckoutInfoMsg
												, buttonFlags: [$scope.buttonFlags.OK]
												}
											)
									}
								}
							)
						}
					}
				}
			}

			$scope.showSecurityCode = function(){
				// Show image from config if available othersie use the default.
				var imgPath = 'img/'
				// TODO: check to see if settings is still used
				if( appDataService &&
						appDataService.settings &&
						appDataService.settings.security_code_image
					)
				{
					imgPath += appDataService.settings.security_code_image
				} else {
					imgPath += 'cvv.gif"'
				}

				// @TODO: Is this a good place for this. Adding HTML markup to modal?
				modalService.displayModal
					(
						{ title: $scope.i18n.Alert.whatIsCVVTitle
						, headerFlag: modalService.PRIMARY
						, description: '<span class="sprite cvv"></span>'
						, buttonFlags: [$scope.buttonFlags.OK]
						}
					)
			}

			$scope.ccMin = 12
			$scope.ccMax = 19
			$scope.setCardType = function(ccType){
				// TODO: Move this to enumsService.
				if ( typeof ccType != 'undefined' ) {
					ccType = ccType.toLowerCase()
					switch (ccType){
						case "mastercard":
						case "visa":
						case "bankcard":
						case "discover":
						case "discover card":
						case "jcb":
							$scope.ccMax = 16
							break;
						case "amex":
						case "american express":
						case "enroute":
							$scope.ccMax = 15
							break;
						case "dinersclub":
							$scope.ccMax = 14
							break;
						default :
							$scope.ccMax = 19
							break;
					}
				} else {
					// reset the values;.
					$scope.ccMin = 12;
					$scope.ccMax = 19;
				}

				if( typeof (($scope).payment || {}).number != 'undefined' && $scope.payment.number.length > $scope.ccMax){
					$scope.payment.number = $scope.payment.number.substring(0, $scope.ccMax)
				}
			}

			/**
			 * Need to verify that two conditions are met before we show The PayPal checkout option.
			 * @returns {boolean}
			 */
			function showPayPalCheckoutOption(){
				//TODO: turn this into a service with the rest of the paypal stuff in cartService
				var inMerchantDetails = false;
				var inConsolidated = false;
				var hasPayPalToken = false;

				if( typeof consolidatedData != 'undefined' ){
					if( typeof consolidatedData.SETTINGS != 'undefined' ){
						// @todo, make sure we have a functional paypal token.
						if( consolidatedData.SETTINGS.billing_types.indexOf('PPA') > -1 ){
							inConsolidated = true;
						} else {
							return false;
						}
					}
				}

				if( typeof merchantDetailsData != 'undefined' ){
					if( typeof merchantDetailsData.BILLINGTYPE != 'undefined' ){
						for ( var idx in merchantDetailsData.BILLINGTYPE ){
							if( merchantDetailsData.BILLINGTYPE[idx].auth_type == 'PPA'
								&& merchantDetailsData.BILLINGTYPE[idx].visible == 'true'
								){
								inMerchantDetails = true;
							}
						}
					}
				}

				// Check that we have a working token.
				hasPayPalToken = ( cartService.paypal.token ) ? true: false;

				return ( inMerchantDetails && inConsolidated && hasPayPalToken ) ? true: false;
			}

			function initializeController(){
				var shipReq = shippingService.shippingRequired( { skipRequests : 'SetExpressCheckout' }, initializeController );
				if( shipReq == 'gettingCartSummary' ){
					return;
				}

				$log.log('PaymentController >> initializeController > ',userInfoService)

				if( shipReq && ( !$scope.shipping || !$scope.shipping.valid ) ){
					// No shipping information. Alert and take user back to the shipping screen. if not in developer mode
					modalService.displayModal
						(
							{ title: $scope.i18n.Alert.shippingInfoNotSetTitle
							, headerFlag: modalService.DANGER
							, description: $scope.i18n.Alert.shippingInfoNotSetMsg
							, buttonFlags: [$scope.buttonFlags.OK]
							}
						)

					$scope.routeTo( $scope.routes.SHIPPING );
					return;
				} else if( !$scope.billing || !$scope.billing.valid ){
					// No billing information. Alert and take user back to the billing screen. if not in developer mode
					modalService.displayModal
						(
							{ title: $scope.i18n.Alert.billingInfoNotSetTitle
							, headerFlag: modalService.DANGER
							, description: $scope.i18n.Alert.billingInfoNotSetMsg
							, buttonFlags: [$scope.buttonFlags.OK]
							}
						)

					$scope.routeTo( $scope.routes.BILLING );
					return;
				}

				// Dev mode only.
				if($scope.autofill && !$scope.payment){
					$scope.payment = userInfoService.payment
				}
			}
			
			$scope.$on
			( "$routeChangeSuccess"
			, function(event){
					var locPath = location.pathname.toLowerCase()
					if( locPath.indexOf($scope.routes.PAYMENT.toLowerCase()) > -1){
						initializeController()
					}
				}
			)
	}
  ]
)
