angular.module('app').controller
	( 'MainMenuController',
	[
		'$scope'
		, '$rootScope'
		, '$routeParams'
		, '$route'
		, '$log'
		, '$location'
		, '$timeout'
		, 'storageService'
		, 'enumsService'
		, 'appDataService'
		, 'utilitiesService'
		, 'connectionService'
		, 'analyticService'
		, 'clientInfoService'
		, 'loggingService'
		, 'extraFlowService'
		, 'modalService'
		, 'languageService'
		, function
			( $scope
				, $rootScope
				, $routeParams
				, $route
				, $log
				, $location
				, $timeout
				, storageService
				, enumsService
				, appDataService
				, utilitiesService
				, connectionService
				, analyticService
				, clientInfoService
				, loggingService
				, extraFlowService
				, modalService
				, languageService
				){

		window.scrollTo( 0, 0 );
		var socket = enumsService.socket
		var status = enumsService.dataStatus
		var services = enumsService.services

		$scope.menuLinks = []
		$scope.securityProvider = {}
		$scope.showSecurityProvider = false
		$scope.menuItems = []
		$scope.subMenuItems = []
		$scope.showAllItems = false
		$scope.promoCode = ''

		// Show or hide the navigation theme.
		$rootScope.haloTheme = false

		// Controls the view of the top navigation in application.
		$rootScope.showMenuBtn = true
		$rootScope.showTitle = true
		$rootScope.showCartBtn = true

		$scope.clearTextField = function(){
			// Clears the promobox field.
			$scope.promoCode = ''
		}


		$scope.submitPromo = function(){
			if($scope.promoCode.indexOf(':') > -1){
				switch($scope.promoCode){
					case ":debug":
					case ":d":
						loggingService.enabled = true
						return true;
						break;

					case ":stop":
					case ":s":
						loggingService.enabled = false
						return true;
						break;

					case ":clearlog":
					case ":cl":
						console.clear()
						return true;
						break;

					case ":clearall":
					case ":ca":
					case ":c":
					case ":clear":
						$rootScope.resetApp()
						return true;
						break;

					case ":lso":
					case ":ls":
						$rootScope.state.debugMenu = true
						return true;
						break;

					case ":version":
					case ":v":
						$rootScope.state.debugMenu = true
						return true;
						break;

					case ":devmode":
					case ":dm":
					case ":p":
						$routeParams.d = true
						$rootScope.develop = true
						loggingService.enabled = true
						$scope.autofill = true
						return true;
						break;

					default:
						break;

				}
			} else if($scope.promoCode.indexOf('=') > -1){
				var keyval = $scope.promoCode.split('=')
				$routeParams[keyval[0]] = keyval[1]
				if(keyval[0]='d'){
					$rootScope.develop = eval(keyval[1])
					loggingService.enabled = $rootScope.develop
				}
			} else if($scope.promoForm.$valid){
				utilitiesService.toggleMenu()
				$timeout
				( function(){
						// Route to package list with promocode param.
						$scope.routeTo(enumsService.routes.LIST + '/promocode/' + $scope.promoCode)
						// Reset the promo code field.
						$scope.promoCode = ''
					}

					, $rootScope.speed
				)
			} else {
				modalService.displayModal
					(
						{ title: languageService.Alert.missingPromoErrorTitle
						, description: languageService.Alert.missingPromoErrorMsg
						}
					)
			}
		}

		$scope.toggleMenu = function(){
			utilitiesService.toggleMenu()
		}

		$scope.showSSLModal = function(){
			utilitiesService.toggleMenu()

			var desc = $scope.securityProvider.message

			modalService.displayModal
			(
				{   title: '<span class="sprite ssl-modal-img ' + $scope.securityProvider.provider + '2x"></span>'
					, description: desc
					, headerFlag:modalService.DEFAULT
					, buttonFlags:[ $scope.buttonFlags.CANCEL, $scope.buttonFlags.VERIFY]
					, callback: function(detail){
					if(detail == $scope.buttonFlags.VERIFY){
						window.open($scope.securityProvider.link, 'SSL')
					}
				}
				}
			);

		}

		$scope.topNavItemClick = function ($event, data){
			// Menu item click event handler.
			// Don't let event bubble up.
			stopEventPropagation($event)
			// Ensure subMenu is closed.
			$scope.showAllItems = false

			for (var i = 0; i < $scope.menuItems.length; i++){
				if(data.id != $scope.menuItems[i].id){
					$scope.menuItems[i].selected = false;
				}else{
					$scope.menuItems[i].selected = true;
				}

			}

			if(typeof(data.MENUITEM) != 'undefined'){
				$scope.showAllItems = true
				data.showSubMenu = (data.showSubMenu) ? false : true
				//Don't make all the submenu active
				for (var i = 0; i < data.MENUITEM; i++){
						data.MENUITEM[i].selected = false;
				}

				// If item clicked has MENUITEM, set subMenuItems as MENUITEM
				// otherwise set it to the selected item.
				// NOTE: Updating service data directly.
				appDataService.keywordMenuItems = data.MENUITEM
			}else{
				appDataService.keywordMenuItems = [data]
			}

			utilitiesService.scrollPosition()

			if( data.alternateModule ){
				// There's an alternateFlowModule and it's enabled.
				var view = extraFlowService.getAlternateFlowView( data.alternateModule );
				if( view ){
					$scope.routeTo(view + '/' + data.keywords)
					return;
				}
			}

			// TODO: Use enums for packagelist.
			$scope.routeTo('packageList/keyword/' + data.keywords)
			$log.log($scope.menuItems)

		}



		$scope.onMenuItemClick = function($event, data){
			// Menu item click event handler.
			// Don't let event bubble up.
			stopEventPropagation($event)

			// Ensure subMenu is closed.
			$scope.showAllItems = false

			for (var i = 0; i < $scope.menuItems.length; i++){
				// Close any sub menu list (set showSubMenu = false).
				if($scope.menuItems[i].MENUITEM && data.menuItem != $scope.menuItems[i]){
					$scope.menuItems[i].showSubMenu = false;
				}

				// Reset any previously selected item (set selected = false).
				$scope.menuItems[i].selected = false;
			}

			if(data.menuItem.MENUITEM){
				data.menuItem.showSubMenu = (data.menuItem.showSubMenu) ? false : true
				data.menuItem.selected = data.menuItem.showSubMenu

				// If item clicked has MENUITEM, set subMenuItems as MENUITEM
				// otherwise set it to the selected item.
				// NOTE: Updating service data directly.
				appDataService.keywordMenuItems = data.menuItem.MENUITEM
			} else {

				utilitiesService.toggleMenu()
				utilitiesService.scrollPosition()

				// This menu item doesn't have sub menus. Make it (the menu item clicked) the selected item.
				data.menuItem.selected = true

				// NOTE: Updating service data directly.
				appDataService.keywordMenuItems = [data.menuItem]
				if( data.menuItem.alternateModule ){
					// There's an alternateFlowModule and it's enabled.
					var view = extraFlowService.getAlternateFlowView( data.menuItem.alternateModule );
					if( view ){
						$scope.routeTo(view + '/' + data.menuItem.keywords)
						return;
					}
				}

				// TODO: Use enums for packagelist.
				$scope.routeTo('packageList/keyword/' + data.menuItem.keywords)

				// Toggle the menu - handled in service since we're manipulating the DOM.
				//$scope.state.showMainMenu = !$scope.state.showMainMenu
			}
		}

		/*
		 Sub-menu item click event handler.
		 */
		$scope.onSubMenuItemClicked = function($event, data){
			// Don't let event bubble up.
			stopEventPropagation($event)

			// Toggle the menu - handled in service since we're manipulating the DOM.
			//$scope.state.showMainMenu = !$scope.state.showMainMenu
			utilitiesService.toggleMenu()
			utilitiesService.scrollPosition()

			// Iterate through each subMenuItems and set selected to false.
			for (var i = 0; i < appDataService.keywordMenuItems.length; i++){
				appDataService.keywordMenuItems[i].selected = false
			}

			// Select the sub menu that was clicked.
			data.subMenuItem.selected = true

			if( data.menuItem.alternateModule ){
				// There's an alternateFlowModule and it's enabled.
				var view = extraFlowService.getAlternateFlowView( data.menuItem.alternateModule );
				if( view ){
					$scope.routeTo(view + '/' + data.menuItem.keywords)
					return;
				}
			}

			// TODO: Use enums for packagelist.
			$scope.routeTo('packageList/keyword/' + data.subMenuItem.keywords)


		}

		/*
		 TODO: Add this to application scope (in a service) and reference it from there.
		 Helper to prevent default event and stopping propagation of the event.
		 */
		function stopEventPropagation(event){
			event.preventDefault()

			if(event && event.stopPropagation){
				event.stopPropagation()
			} else {
				e = window.event
				e.cancelBubble = true
			}
		}

		appDataService.fetch(services.GET_APPLICATION_CONSOLIDATED, true)
			.then(function(data){
						appDataResult = appDataService.setData(data)
						if(typeof ((appDataService).securityProvider || {}).title != 'undefined'){
							$scope.securityProvider = appDataService.securityProvider
							$scope.showSecurityProvider = true
						} else {
							$scope.securityProvider = {}
							$scope.showSecurityProvider = false
						}

						$scope.menuLinks = appDataService.links
						$scope.menuItems = appDataService.menuItems
					}
		)

		$scope.$on("$routeChangeSuccess", function( event ){

		});

	}])

