angular.module( 'app' )
.controller
( 'CrossSellsController'
, [   '$scope'
	, 'cartSummaryData'
	, 'appDataService'
	, 'extraFlowService'
	, 'consolidatedData'
	, 'enumsService'
	, 'cartService'
	, function
		( $scope
		, cartSummaryData
		, appDataService
		, extraFlowService
		, consolidatedData
		, enumsService
		, cartService
			){
			window.scrollTo( 0, 0 );

			function getKeywordsItems( obj ){
				var handler = []
				  , packages = appDataService.GetMerchantPackageList.PS.P;

				for( var i in packages ){
					if( packages[ i ].keyword == obj.keyword ){
						handler.push( Number( packages[ i ].min_retail_amount ) );
					}
				}

				handler.sort();
				obj.min_retail_amount = handler[ 0 ];
				if( handler.length > 1 ){
					obj.startRate = handler[ 0 ];
				}
			}

			if( cartSummaryData.CHECKOUT_KEYWORDS &&
				cartSummaryData.CHECKOUT_KEYWORDS.K &&
				appDataService.crossSells ){
				if( angular.isArray( cartSummaryData.CHECKOUT_KEYWORDS.K ) ){
					$scope.keywords = cartSummaryData.CHECKOUT_KEYWORDS.K;
				} else {
					$scope.keywords = [ cartSummaryData.CHECKOUT_KEYWORDS.K ];
				}

				for( var i in $scope.keywords ){
					getKeywordsItems( $scope.keywords[ i ] );
				}
				
				$scope.hideSpinner();
			} else {
				$scope.routeTo( $scope.routes.CART );
			}
			
			var hasItems =
				cartSummaryData &&
				cartSummaryData.CART &&
				cartSummaryData.CART.ITEMS &&
				cartSummaryData.CART.ITEMS.ITEM &&
				( angular.isArray( cartSummaryData.CART.ITEMS.ITEM ) ?
						cartSummaryData.CART.ITEMS.ITEM.length > 0 :
						true
				);
			
//			if( cartService && !hasItems ){
//				$scope.routeTo( $scope.routes.LIST );
//				return;
//			}
			
			$scope.handleItemClick = function( keyword ){
				$scope.state.showCrossSells = false;
				
				// Route to alternate flow if applicable.
				var alternateFlow = extraFlowService.isAlternateFlow( keyword.keyword, consolidatedData.TREE.MENUITEM );
				if( alternateFlow != '' ){
					var view = extraFlowService.getAlternateFlowView( alternateFlow );
					if( view ){
						$scope.routeTo( view + '/' + keyword.keyword );
						return;
					}
				}
				
				$scope.routeTo( enumsService.routes.LIST + '/crosssells/' + keyword.keyword );
			}
			
			appDataService.crossSells = false;
		 }
	 ]
)