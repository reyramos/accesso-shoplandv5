/**
 * @author Bryan Olson <bolson@accesso.com>
 *
 * Manages cart screen and related functions
 * Getting the file ready.
 */
angular.module('app').controller
	( 'ReviewController', [
		'$scope'
		, '$log'
		, '$routeParams'
		, 'storageService'
		, 'merchantService'
		, 'navigationService'
		, 'clientInfoService'
		, 'socketService'
		, 'languageService'
		, 'packageService'
		, 'cartService'
		, 'analyticService'
	, function (
		$scope
		, $log
		, $routeParams
		, storageService
		, merchantService
		, navigationService
		, clientInfoService
		, socketService
		, languageService
		, packageService
		, cartService
		, analyticService
		){

		window.scrollTo( 0, 0 );
		
		$scope.hideSpinner()

		$scope.pageViewCart = false;
		$scope.pageViewOrder = false;
		$scope.pageViewReceipt = false;
		$scope.pageView = false;

		$scope.showDiscounts = false;

		$scope.checkOut = function() {
		}

		$scope.completeOrder = function() {
		}

		$scope.checkOut = function() {
		}

		$scope.removeCartItem = function (row) {
			// call the backend and call UpdateCartItems;
			cartService.removeCartItem( row );
		}
		/**
		 * Perform the default view of the cart.
		 */
		$scope.viewCart = function () {
		 	// UNDER DEVELOPMENT
		 	if ( $scope.cartItems.length > 0 ) {
			} else {
			}
		};

		if ( $location.path().indexOf('/cartView') !== -1 ) {

			$scope.pageViewCart = true;
			$scope.pageViewOrder = false;
			$scope.pageViewReceipt = false;
			$scope.showDiscounts = false;
		$scope.cartItems   = $scope.cartService.getCartItems();
		$scope.cartDetails = $scope.cartService.getCartDetails();

		} else {
			$scope.showDiscounts = true;
		}

/*
		$scope.$on
			( enumsSocket.receive + enumsRequestTypes.GetCartSummary
			, function(event,data){
				// good response.
				if ( data.status == enumsDataStatus.success ) {
					cartDetails = cartService.getCartDetails();
					cartItems   = cartService.getCartItems();
				} else {
					// error.
				}
			}
		);
*/

	}]);
