angular.module( 'app' )
.controller
( 'TicketController'
, [   '$routeParams'
	, '$route'
	, '$scope'
	, '$log'
	, '$location'
	, '$filter'
	, '$timeout'
	, '$rootScope'
	, 'clientInfoService'
	, 'connectionService'
	, 'appDataService'
	, 'consolidatedData'
	, 'analyticService'
	, 'languageService'
	, 'utilitiesService'
	, 'enumsService'
	, function
		( $routeParams
		, $route
		, $scope
		, $log
		, $location
		, $filter
		, $timeout
		, $rootScope
		, clientInfoService
		, connectionService
		, appDataService
		, consolidatedData
		, analyticService
		, languageService
		, utilitiesService
		, enumsService
		){
			window.scrollTo( 0, 0 );
			appDataResult = appDataService.setData( consolidatedData );
			
			var showPayPalPayment  = false;
			var showSpritePaymentType  = false;
			var paypalAccount  = '';

			
			$scope.hideSpinner();
			$scope.receiptView = false;
			$rootScope.hammer_event = true;
			
			// Delivery methods selected.
			$scope.isSpecialDelivery  = false;
			$scope.isWillCall  = false;
			$scope.isFirstClassMail = false;
			$scope.isInternationalMail = false;
			$scope.isPrintAtHome = false;
			$scope.specialDeliveryTitle = '';
			$scope.specialDeliveryMessage = '';
			
			$scope.ticketDisplayIndex =
				{ display:[ true ]
				, front:[ true ]
				, back:[ false ]
				}
				
			// TODO: Does this need to live in $rootScope or can it be moved to $scope.
			$rootScope.ticketObject =
				{ switchPage: ''
				, selectedOrderID: $routeParams.orderId || ''
				, selectedOrderL4: $routeParams.lastFour
				, selectedOrderPN: $routeParams.phoneNumber
				, fromOrderLookup: false
				};
			
			$scope.tryAgain = function( receiptView ){
				$scope.receiptView = receiptView;
				initializeController();
			}
			
			$scope.continueShopping = function(){
				var lastKeyword = appDataService.lastKeyword;
				
				if( lastKeyword ){
					$scope.routeTo( 'packageList/keyword/' + lastKeyword );
				} else {
					$scope.routeTo();
				}
			}
			
			$scope.gotoOrderResults = function(){
				$scope.routeTo( $scope.routes.ORDER_HISTORY );
			}
			
			$scope.gotoOrderLookup = function( last4, phoneNumber, orderId ){
				$scope.routeTo( $scope.routes.ORDER_SEARCH );
			}
			
			$scope.viewReceipt = function(){
				$rootScope.ticketObject.switchPage = 'receipt';
			}
			
			$scope.viewTickets = function(){
				$rootScope.paginationPosition();
				$rootScope.ticketObject.switchPage = 'tickets';
				setTicketContainer();
			}

			$scope.toggleTicketDisplay = function( index ){
				if( !$scope.ticketDisplayIndex.display[ index ] ){
					$scope.ticketDisplayIndex.display[ index ] = true
					$scope.ticketDisplayIndex.front[ index ] = true
					$scope.ticketDisplayIndex.back[ index ] = false
					
					for( i in $scope.ticketDisplayIndex.display ){
						if( index != i ){
							$scope.ticketDisplayIndex.display[ i ] = false
							$scope.ticketDisplayIndex.front[ i ] = false
							$scope.ticketDisplayIndex.back[ i ] = false
						}
					}
				}
			}
			
			$scope.flip = function( index ){
				$scope.ticketDisplayIndex.front[ index ] = !$scope.ticketDisplayIndex.front[ index ]
				$scope.ticketDisplayIndex.back[ index ] = !$scope.ticketDisplayIndex.back[ index ]
			}
			
			$scope.nameSubmit = function(ticket){
				ticket.submit_attempt = true
				if( ticket.customerFirstName && ticket.customerLastName ){
					ticket.ticket_view = true
					appDataService.fetch
						( $scope.services.ADD_TICKET_CHARACTERISTIC
						, false
						, { ticket_id: ticket.ticket_id
						  , key: 'ticket_customer_name'
						  , value: ticket.customerFirstName + ' ' + ticket.customerLastName
						  }
						).then
							( function(data){
								if(data.status == $scope.status.OK){
									ticket.CHARACS = ticket.CHARACS || {}
									ticket.CHARACS.ticket_customer_name = ticket.customerFirstName + ' ' + ticket.customerLastName
									ticket.submitted = true
								}
							  }
							)
				}
			}
			
			$scope.copyLastName = function( lastName ){
				angular.forEach
					( $scope.tickets
					, function( ticket ){
						ticket.customerLastName = lastName
					  }
					)
			}
			
			function initializeController(){
				// Reset the data.
				resetProperties()
				
				setMerchantDetails();
				
				// TODO: Refactor as the routes.js object -- Staged init??
				$scope.showSpinner( $scope.i18n.ApplicationLabels.loadingTicketsLoadMsg );
				appDataService.fetch
					(
						  $scope.services.VIEW_ORDER
						, false
						, { order_id: $routeParams.orderId }
					).then
						(
							function( data ){
								if( data.status == $scope.status.OK ){
									var viewOrderReply = data.VIEW_ORDER_REPLY;
									var paymentDetails = viewOrderReply.PAYMENT_DETAILS;
									var customerDetailsReply = viewOrderReply.CUSTOMER_DETAILS_REPLY;
									var orderItemsReply = viewOrderReply.ORDER_ITEMS_REPLY;
									var orderStatusReply = viewOrderReply.ORDER_STATUS_REPLY;
									var ticketDetailsReply = viewOrderReply.TICKET_DETAILS_REPLY;
									
									// Make payments/PAYMENT an array.
									var payments = paymentDetails.PAYMENT =
										( angular.isArray( paymentDetails.PAYMENT ) ) 
											? paymentDetails.PAYMENT
											: [ paymentDetails.PAYMENT ];
									
									// Verify we have a phone number and either email address or the last four digits of CC.
									var phoneNumber = getPhoneNumber( customerDetailsReply.CUSTOMER.PHONE );
									var emailAddress = emailAddressMatch( customerDetailsReply.CUSTOMER.EMAIL.address );
									var lastFour = lastFourCCDigitsMatch( payments );
									
									// Reset all the previous info.
									resetCartObject();
									
									// Set the flags for the various delivery methods.
									determineDeliveryMessage( orderStatusReply );
									
									if( ( emailAddress || lastFour ) && phoneNumber ){
										// Start of Visible Ticket Block.
										$scope.state.parkName = orderStatusReply.merchant_name;
										$rootScope.ticketObject.selectedOrderID = orderStatusReply.order_id;
										
										if( lastFour ){
											$rootScope.ticketObject.selectedOrderL4 = lastFour;
										} else if( !lastFour && angular.isDefined( payments[ 0 ].auth_type ) && payments[ 0 ].auth_type == 'PPA' ){
											// Paypal Detection.
											$scope.showPayPalPayment  = true;
											$scope.showSpritePaymentType  = true;
											$scope.paypalAccount = customerDetailsReply.CUSTOMER.EMAIL.address;
											
											$rootScope.ticketObject.selectedOrderL4 = false;
										}
										
										// Store reference to phone number in rootScope. -- ??
										$rootScope.ticketObject.selectedOrderPN = phoneNumber;
										
										// Add the ticket collection to scope.
										$scope.tickets =
											angular.isArray( ticketDetailsReply.TICKET )
												? ticketDetailsReply.TICKET
												: [ ticketDetailsReply.TICKET ];
										
										// Update all the necessary info on the tickets.
										setAllTicketInfo( $scope.tickets )
										
										// Set the confirmation number.
										$scope.confirmationNumber = getConfirmationNumber( paymentDetails );
										
										// Set the taxes and fees amount.
										$scope.cartObject.cartTaxesAndFeesAmount = getTaxesAndFeesTotal( orderItemsReply.TOTALS );
										
										// Get and store all other fees.
										storeOtherFees( orderItemsReply )
									} else {
										// Email or last four and phone number validation failed.
										$scope.showNoTicketError = true;
									}
								} else if( data.status == $scope.status.FAILED ){
									$scope.showNoTicketError = true;
								}
								
								// Hide the spinner.
								$scope.hideSpinner();
							}
						)
			}
			
			function resetProperties(){
				$scope.showPayPalPayment  = false;
				$scope.showSpritePaymentType  = false;
				$scope.paypalAccount  = '';
				
				
				if( !$scope.receiptView ){
					$rootScope.ticketObject.switchPage = 'tickets';
				}
				
				$scope.orderId = $routeParams.orderId;
				$rootScope.ticketIndex = 0;
				$rootScope.ticketFront = true;
				$rootScope.currentTicket = 1;
				$scope.clientInfoService = clientInfoService;
				$scope.tickets = [];
				$scope.ticketNames = false;
				
				// Reset flag.
				$scope.showNoTicketError = false;
			}
			
			function setMerchantDetails(){
				$scope.merchant = {};
				
				if( angular.isDefined( appDataService.merchant ) ){
					$scope.merchant.alias = appDataService.merchant;
				}
				
				if( angular.isDefined( appDataService.environment ) ){
					$scope.merchant.environment = appDataService.environment;
				}
				
				if( angular.isDefined( appDataService.island ) ){
					$scope.merchant.island = appDataService.island;
				}
				
				if( angular.isDefined( appDataService.userId ) ){
					$scope.merchant.userId = appDataService.userId;
				}
				
				if( angular.isDefined( appDataService.passbook ) &&
					angular.isDefined( appDataService.passbook.url ) ){
					$scope.merchant.passbook = appDataService.passbook.url;
				}
			}
			
			function setAllTicketInfo( tickets ){
				if( tickets.length > 0 ){
					setTicketContainer();
					
					// Check to see if name registration is required for at least one ticket.
					var isRegReqForOne = isRegReqForAtLeastOne( tickets );
					
					for( var i in tickets ){
						var ticket = tickets[ i ];
						
						ticket.CHARACS = ticket.CHARACS || []
						setTicketViewFlag( isRegReqForOne, ticket );
						
						// Update the ticket status.
						ticket.ticket_status = setTicketStatus( ticket );
						ticket.printable = ( ticket.printable == '1' ) ? 1 : 0;
						ticket.isValid = ( ticket.ticket_status == 1 && ticket.printable == 1 ) ? 1 : 0;
						updateTicketInfo( ticket );
						renderDateTimeInfo( ticket );
						renderAdvancedTicketInfo( ticket );
						replaceValidFromInfo( ticket );
						
						if( doAutoSubmit( ticket ) ){
							$scope.nameSubmit( ticket );
						}
					}
				}
			}
			
			function setTicketStatus( ticket ){
				var ticketStatus = ticket.ticket_status.toLowerCase();
				if( ticketStatus == 'cancelled' ||
					ticketStatus == 'unknown' ||
					ticketStatus == 'upgraded' ||
					ticketStatus == 'deposit received' ||
					ticketStatus == 'used' ){
					return 0;
				} else {
					return 1;
				}
			}
			
			function isRegReqForAtLeastOne( tickets ){
				for( var i in tickets ){
					var ticket = tickets[ i ];
					if( utilitiesService.getBoolean( ticket.name_required_to_print ) ){
						return true;
					}
				}
				
				return false
			}
			
			function getTicketRegistrationStatus( ticket ){
				try{
					// Will hit catch if CHARACS or ticket_customer_name is not defined.
					var nameIsEmpty = ticket.CHARACS.ticket_customer_name == '';
					return ( !nameIsEmpty );
				} catch( e ){
					return false;
				}
			}
			
			function setTicketViewFlag( isRegReqForOne, ticket ){
				var isRegistrationReq = utilitiesService.getBoolean( ticket.name_required_to_print );
				var isTicketRegistered = getTicketRegistrationStatus();
				
				if( isRegistrationReq && isTicketRegistered ){
					ticket.ticket_view = true;
				} else if( isRegistrationReq && !isTicketRegistered ){
					ticket.ticket_view = false
				} else if( !isRegistrationReq && !isTicketRegistered && isRegReqForOne ){
					ticket.ticket_view = false;
					ticket.customerFirstName = "Guest";
					ticket.customerLastName = "Guest";
				} else {
					ticket.ticket_view = true;
				}
			}
			
			function doAutoSubmit( ticket ){
				// Copy reservation first name with last name to customer name.
				// Verify that the  `ticket.CHARACS` exists before proceeding.
				var submitTicket = false;
				if( angular.isDefined( ticket.CHARACS ) ){
					if(
						angular.isDefined( ticket.CHARACS.rv_first_name ) 
						&&
						(
							angular.isUndefined( ticket.CHARACS.ticket_customer_name ) 
							|| ticket.CHARACS.ticket_customer_name == '' 
						)
					){
						ticket.customerFirstName = ticket.CHARACS.rv_first_name;
						ticket.CHARACS.ticket_customer_name = ticket.CHARACS.rv_first_name;
						
						if( typeof( ticket.CHARACS.rv_first_name ) != 'undefined' ){
							ticket.customerLastName = ticket.CHARACS.rv_last_name;
							ticket.CHARACS.ticket_customer_name += ' ' + ticket.CHARACS.rv_last_name;
						}
						
						submitTicket = true;
					}
				}
				
				return submitTicket;
			}
			
			function updateTicketInfo( ticket ){
				if( typeof ticket.PRINT_AT_HOME != 'undefined' ){
					ticket.mobile_header = getTicketInfo( 'mobile_header', 'printMobileHeader' );
					ticket.mobile_footer = getTicketInfo( 'mobile_footer', 'printMobileFooter' );
					ticket.ticket_additional_info = getTicketInfo( 'ticket_additional_info', 'printTicketAdditional_info' );
					ticket.ticket_more_info = getTicketInfo( 'ticket_more_info', 'printTicketMoreInfo' );
					ticket.ticket_instructions = getTicketInfo( 'ticket_instructions', 'printTicketInstructions' );
					ticket.ticket_terms_of_use = getTicketInfo( 'ticket_terms_of_use', 'printTicketTermsOfUse' );
					ticket.ticket_disclaimer = getTicketInfo( 'ticket_disclaimer', 'printTicketDisclaimer' );
					ticket.use_passbook = utilitiesService.getBoolean( ticket.PRINT_AT_HOME.use_passbook );
				}
			}
			
			function getTicketInfo( ticketData, defaultLocaleTarget ){
				// If ticketData is undefined, then return default locale.
				try{
					return ( typeof ticket.PRINT_AT_HOME[ ticketData ] != 'undefined' )
						? ticket.PRINT_AT_HOME[ ticketData ]
						: languageService.TicketView[ defaultLocaleTarget ];
				} catch( e ){
					return '';
				}
			}
			
			function renderDateTimeInfo( ticket ){
				ticket.showDateTime =
					(
						ticket.EVENT &&
						ticket.CHARACS &&
						ticket.CHARACS.client_xfc_view != undefined &&
						ticket.CHARACS.client_xfc_view.toLowerCase().indexOf( 'datetime' ) > -1
					) ? true : false;
			}

			
			function renderAdvancedTicketInfo( ticket ){
				ticket.showAdvancedTicket =
					(
						ticket.EVENT &&
						ticket.CHARACS &&
						ticket.CHARACS.client_is_advanced_ticket != undefined &&
						ticket.CHARACS.client_is_advanced_ticket.toLowerCase() == 'true'
					) ? true : false;
			}
			
			function replaceValidFromInfo( ticket ){
				if( ticket.showAdvancedTicket && ticket.ticket_instructions ){
					ticket.ticket_instructions =
						languageService.replaceAllMultiple
							( [ '@@date' ]
							, [ $filter( 'date' )( ticket.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
							, languageService.replaceAllMultiple
								( [ '@@Date' ]
								, [ $filter( 'date' )( ticket.CHARACS.valid_from, 'MM/dd/yyyy' ) ]
								, ticket.ticket_instructions
								)
							)
				}
			}
			
			function getConfirmationNumber( paymentDetails ){
				if( angular.isArray( paymentDetails.PAYMENT ) ){
					return paymentDetails.PAYMENT[ 0 ].order_id;
				} else {
					return paymentDetails.PAYMENT.order_id;
				}
			}
			
			function getTaxesAndFeesTotal( totalsObj ){
				// TODO: We should not be considering 'free' text in this calculation.
				return parseFloat( totalsObj.cart_adjustment_total )
				+ parseFloat( totalsObj.tax_total )
				+ parseFloat( totalsObj.fee_total )
				+ parseFloat
					(
						( Number( totalsObj.shipping_total ) > 0 )
							? totalsObj.shipping_total 
							: 0
					);
			}
			
			function storeOtherFees( orderItemsReply ){
				var feeItem =
					{ label: 'Taxes' // TODO: Localize.
					, amount: orderItemsReply.TOTALS.tax_total
					};
					
				$scope.cartObject.feeItems.push( feeItem );
				$scope.cartObject.subTotal = orderItemsReply.TOTALS.retail_total;
				$scope.cartObject.total = orderItemsReply.TOTALS.current_total;
				
				// Build the cart Items and store fees in collections.
				if( angular.isDefined( orderItemsReply.ITEM ) ){
					if( !angular.isArray( orderItemsReply.ITEM ) ){
						orderItemsReply.ITEM = [ orderItemsReply.ITEM ];
					}
					
					for( var i in orderItemsReply.ITEM ){
						// Iterate through each item and save the fees in the appropriate collection.
						var currentItem = orderItemsReply.ITEM[ i ];
						saveTickets( currentItem, $scope.cartObject.cartItems );
						saveFees( currentItem, $scope.cartObject.feeItems );
						saveAdjustments( currentItem, $scope.cartObject.feeItems );
					}
					
					// Add Shipping if applicable.
					var shipFeeDetails = getShippingFeeDetails( orderItemsReply.TOTALS.shipping_total )
					if( shipFeeDetails ){
						$scope.cartObject.feeItems.push( shipFeeDetails );
					}
				}
			}
			
			function saveTickets( item, collection ){
				// Cart Item:
				if( item.type == 'T' ){
					var itemObject = {};
					var key = item.package_id + '|' + item.action + '|' + item.customer_type;
					
					if( angular.isDefined( itemObject[ key ] ) ){
						itemObject[ key ].qty++;
					} else {
						itemObject[ key ] = {};
						itemObject[ key ].qty = 1;
						itemObject[ key ].package_name = item.package_name;
						itemObject[ key ].retail_price = item.retail_amount;
					}
					
					collection.push( itemObject );
				}
			}
			
			function saveFees( item, collection ){
				// Ticket fees.
				if( item.type == 'F' ){
					var itemObject = {};
					var key = item.type + '|' + item.ticket_id;
					
					if( typeof itemObject[ key ] == 'undefined' ){
						itemObject[ key ] = {};
						itemObject[ key ].label = $scope.i18n.CartView[ 'fee' + item.ticket_id ];
						itemObject[ key ].amount = parseFloat( item.value );
					} else {
						itemObject[ key ].amount += parseFloat( item.value );
					}
					
					collection.push( itemObject );
				}
			}
			
			function saveAdjustments( item, collection ){
				// Cart adjustments.
				if( item.type == 'A' ){
					var itemObject = {};
					var key = item.type + '|' + item.cart_adjustment_id;
					
					if( typeof itemObject[ key ] == 'undefined' ){
						itemObject[ key ] = {};
						itemObject[ key ].label = item.cart_adjustment_name;
						itemObject[ key ].amount = parseFloat( item.retail_amount );
					} else {
						itemObject[ key ].amount += parseFloat( item.retail_amount );
					}
					
					collection.push( itemObject );
				}
			}
			
			function getShippingFeeDetails( shippingTotal ){
				if( shippingTotal ){
					var key = 'S|0';
					var itemObject = {};
					itemObject[ key ] = {};
					itemObject[ key ].label = $scope.i18n.CartView.deliveryFee;
					itemObject[ key ].amount =
						( Number( shippingTotal ) > 0 )
							? shippingTotal
							: $scope.i18n.CartView.free;
					
					return itemObject;
				}
				
				return null;
			}
			
			function resetCartObject(){
				$rootScope.ticketObject.selectedOrder = {};
				$rootScope.ticketObject.selectedOrderID = false;
				$rootScope.ticketObject.selectedOrderL4 = false;
				$rootScope.ticketObject.selectedOrderPN = false;
				$scope.cartObject = {};
				$scope.cartObject.cartItems = [];
				$scope.cartObject.feeItems = [];
				$scope.cartObject.discountSource = [];
				$scope.cartObject.subTotal = 0;
				$scope.cartObject.cartTaxesAndFeesAmount = 0;
				$scope.cartObject.discountTotal = 0;
				$scope.cartObject.total = 0;
			}
			
			function determineDeliveryMessage( orderStatusReply ){
				// Determine delivery type and render view accordingly.
				if( typeof( orderStatusReply ) != 'undefined' &&
					typeof( orderStatusReply.ship_method ) != 'undefined' ){
					var shipMeth = orderStatusReply.ship_method;
					$scope.isWillCall = ( shipMeth == enumsService.deliveryMethods.WILL_CALL );
					$scope.isFirstClassMail = ( shipMeth == enumsService.deliveryMethods.FIRST_CLASS_MAIL );
					$scope.isInternationalMail = ( shipMeth == enumsService.deliveryMethods.INTERNATIONAL_SHIPPING );
					$scope.isPrintAndGo = ( shipMeth == enumsService.deliveryMethods.PRINT_N_GO );
					
					$scope.isSpecialDelivery = ( $scope.isWillCall || $scope.isFirstClassMail );
					
					if( $scope.isWillCall ){
						$scope.specialDeliveryTitle = languageService.TicketView.willCallOrderMessageTitle;
						$scope.specialDeliveryMessage = languageService.TicketView.willCallOrderMessage;
					} else if( $scope.isFirstClassMail ){
						$scope.specialDeliveryTitle = languageService.TicketView.firstClassOrderMessageTitle;
						$scope.specialDeliveryMessage = languageService.TicketView.firstClassOrderMessage;
					}
				}
			}
			
			function getPhoneNumber( phone ){
				var phoneNumber = $routeParams.phoneNumber || '';
				
				// Strip hyphens from phone number.
				phoneNumber = phoneNumber.split( '-' ).join( '' );
				
				if( angular.isArray( phone ) ){
					for ( var i in phone ){
						if( phone[ i ].number == phoneNumber ){
							phoneNumber = phone.number;
						}
					}
				} else if( phone.number == phoneNumber ){
					phoneNumber = phone.number;
				}
				
				return phoneNumber;
			}
			
			function emailAddressMatch( emailAddress ){
				// Verify that the email address used matches that of the URL parameter.
				// NOTE: $routeParams.lastFour could be email or last four digits of the CC.
				var urlParam = $routeParams.lastFour || '';
				if( utilitiesService.isValidEmail( urlParam ) ){
					if( emailAddress.toLowerCase() == urlParam.toLowerCase() ){
						return emailAddress.toLowerCase();
					}
				}
				
				return '';
			}
			
			function lastFourCCDigitsMatch( payments ){
				// TODO: find out if we need to verify against other entries, if there is more than one payment. 
				// Verify that the last four digits of the CC used matches that of the URL parameter.
				// NOTE: $routeParams.lastFour could be email or last four digits of the CC.
				var urlParam = $routeParams.lastFour || '';
				var testLastFour = '';
				var ccNumber = payments[ 0 ].card_number;
				if( typeof ccNumber != 'undefined' ){
					testLastFour = ccNumber.substr( ccNumber.toString().length - 4 );
					if( testLastFour == urlParam ){
						return testLastFour;
					}
				}
				
				return '';
			}
			
			function setTicketContainer(){
				// To allow for swipe functionality the width of the swipe container
				// would need to be 100% by ticket.length once we gather all
				// the tickets we will set the container width
				// using percent so it wont affect orientation change
				var ticketContainer = document.getElementById( "ticket-container" );
				if( ticketContainer ){
					angular.element( ticketContainer ).css( {'width': 100 * $scope.tickets.length + '%'} );
					$timeout
						( function(){ angular.element( ticketContainer ).removeClass( 'preload' ); }
						, 1000
						)
				}
			}
			
			$scope.$on
				(
					"$routeChangeSuccess"
					, function( currentRoute, previousRoute ){
						// If from an order lookup, activate the variable.
						for( var i in clientInfoService.previousPages ){
							// the user came from a search.
							if( clientInfoService.previousPages[ i ].indexOf( enumsService.routes.ORDER_HISTORY ) > -1 ){
								$rootScope.ticketObject.fromOrderLookup = true;
							} else if( clientInfoService.previousPages[ i ].indexOf( enumsService.routes.ORDER_REVIEW ) > -1 ){
								// the user came from a purchase.
								$rootScope.ticketObject.fromOrderLookup = false;
							} else {
								$rootScope.ticketObject.fromOrderLookup = true;
								// Don't reset it for any other views.
							}
						}
					
						initializeController();
					 }
				);

	} ] );



