angular.module('app')
	.controller
	( 'CartController'
		, [   '$scope'
		  , '$rootScope'
		  , '$route'
		  , '$location'
		  , '$log'
		  , '$q'
		  , '$timeout'
		  , '$routeParams'
		  , 'analyticService'
		  , 'appDataService'
		  , 'enumsService'
		  , 'cartService'
		  , 'clientInfoService'
		  , 'packageService'
		  , 'storageService'
		  , 'utilitiesService'
		  , 'consolidatedData'
		  , 'merchantDetailsData'
		  , 'merchantPackageListData'
		  , 'initializeData'
		  , 'modalService'
		  , 'userInfoService'

		  , function
				  ( $scope
					  , $rootScope
					  , $route
					  , $location
					  , $log
					  , $q
					  , $timeout
					  , $routeParams
					  , analyticService
					  , appDataService
					  , enumsService
					  , cartService
					  , clientInfoService
					  , packageService
					  , storageService
					  , utilitiesService
					  , consolidatedData
					  , merchantDetailsData
					  , merchantPackageListData
					  , initializeData
					  , modalService
					  , userInfoService

					  ){
			window.scrollTo( 0, 0 );
			$scope.hideSpinner()
			var undoTrigger = true

			// It will reset everytime the showCartModal is called.
			var onDeckRateChanges = {
				onDeck: null, currentPackageQty: null
			}

			$scope.lastRequest = '';
			$scope.cartObject = cartService;
			$scope.pageView = {};
			$scope.pageView.Cart = false;
			$scope.pageView.Order = false;
			$scope.pageView.Receipt = false;
			$scope.showDiscounts = false;
			$scope.heading = '';

			$scope.shipping = appDataService.shipping

			// @todo leave the following as false, until giftcards are ready.
			$scope.showGiftCardCheckout = false;
			$scope.showBilling = false;
			$scope.showPayment = false;
			$scope.showPayPalCheckout = false;
			$scope.showPayPalPaymentMethod = false;
			$scope.showSpritePaymentType  = false;
			$scope.useDevice = 'desktop';
			$scope.useEnvironment = 'production';

			//display the edit capabilites in cart
			$rootScope.changeQty = false;
			$rootScope.displayChangeQtyOptions = true;

			$scope.updateQTY = function(){
				$rootScope.changeQty = true;
				cartService.updateQTY();
			}

			$scope.cancelUpdate = function(){
				$rootScope.changeQty = false;
				cartService.cancelUpdate();
			}

			$scope.commitUpdateQTY = function(){
				cartService.commitUpdateQTY
				(
					merchantPackageListData.cart_limit
					, function( success ){
						if( success ){
							$rootScope.changeQty = false;
						} else {
							// Problem updating qty. User is alerted by service response.
							modalService.displayModal
							(
								{ replacementKeys: 'cartLimit'
									, replacementValues: '<span class="limit">' + $rootScope.cartLimit + '</span>'
									, title: languageService.Alert.cartLimitTitle
									, description: languageService.Alert.cartLimitMsg
								}
							);
						}
					}
				)
			}

			$scope.decrementQty = function( data ){
				packageService.decrementQty( data, true );

			}

			$scope.incrementQty = function( data ){
				packageService.incrementQty( data, true );
			}

			$scope.onQtyChange = function( data ){
				packageService.onQtyChange( data, true );
			}

			/**
			 *
			 * @param callback ,optional callback return true if OK is pressed
			 * @return {boolean} true if OK is pressed
			 */
			$scope.removeCartItem = function( object, callback ){
				if( typeof object == 'object' ){
					onDeckRateChanges.onDeck = object.id;
				}

				var isDefine = callback === undefined ? false: true;

				modalService.displayModal
				(
					{ title: $scope.i18n.Alert.deleteItemTitle
						, headerFlag: modalService.DANGER
						, description: $scope.i18n.Alert.deleteItemMsg
						, buttonFlags: [$scope.buttonFlags.OK, $scope.buttonFlags.CANCEL]
						, callback: function( detail ){
						if( detail == $scope.buttonFlags.OK ){
							removeItemFromCart( getRowPosition( onDeckRateChanges.onDeck ) );
							if( isDefine ){
								callback.call( this, true );
							}
						} else {
							if( isDefine ){
								// TODO: Why did this change to true?
								callback.call( this, true );
							}
						}
					}
					}
				)
			}


			$scope.removeItem = function( data ){
				if( $scope.pageView.Cart ){
					undoTrigger = true;
					onDeckRateChanges.onDeck = data.id;
					var index = getRowPosition( onDeckRateChanges.onDeck );
					$scope.release( '#cart-item-'+index, false, function(data){} );
				}
			};

			function updateCartItems( itemsToBeUpdated ){
				// Show the spinner.
				$scope.showSpinner( $scope.i18n.ApplicationLabels.updatingCartLoadMsg );

				cartService.updateCartItems(itemsToBeUpdated, function(boolean){
					if( boolean ){
					} else {
					}
				})

				// Hide the spinner.
				$rootScope.hideSpinner()
			}

			function removeItemFromCart(row){
				// User confirmed item should be removed.
				// A bit of redundency.
				if(cartService.cartItems.length){

					var cartItems = utilitiesService.copyObject( cartService.cartItems );
					//close the udpate functioanlity
					$rootScope.changeQty = false;

					// Remove item from the cart array.
					// Find the matching item, and remove from item list.
					if(angular.isDefined(cartItems[row])){
						cartItems.splice(row, 1)

						// NOTE: I don't receive an UpdateCartItems response nor a GetCartSummary response unless we clear the cart.
						// Need to make sure this is not a bug somewhere.
						// Make the UpdateCartItems call.
						$scope.lastRequest = $scope.services.UPDATE_CART_ITEMS

						cartService.updateCartItems(
							cartItems
							, function(boolean){
								if(boolean){
								}else{
								}
							}
						)
					}
				}
			}

			function getRowPosition(id){
				//get the cartItems data
				var cartItems = utilitiesService.copyObject( cartService.cartItems );
				var row = null
				//search for the row position by comparing id
				for (var i = 0; i < cartItems.length; i++){
					cartItems[i].id
					if(cartItems[i].id == id){
						row = i
					}
					continue
				}

				return row
			}

			$scope.cartNotifyUpdate = function( row ){
				alert( row );
			}

			$scope.handleShowTerms = function(){
				modalService.displayModal
				(
					{ title: $scope.i18n.CartView.termsTitle
						, headerFlag: modalService.PRIMARY
						, description: $scope.i18n.CartView.termsMsg
						, buttonFlags: [$scope.buttonFlags.OK]
					}
				)
			}

			$scope.viewReceipt = function(){
				// change the lastFour to emailAddress
				var lastFour = $scope.purchaseReceipt.lastFour;
				// removing the hyphens before we redirect.
				var phoneNumber = $scope.purchaseReceipt.phoneNumber.split('-').join('');
				var orderId = $scope.purchaseReceipt.orderId;
				var emailAddress = $scope.purchaseReceipt.emailAddress;

				if ( emailAddress ){
					$scope.routeTo( $rootScope.routes.ORDER_VIEW + '/' + emailAddress + '/' + phoneNumber + '/' + orderId );
				} else {
					$scope.routeTo( $rootScope.routes.ORDER_VIEW + '/' + lastFour + '/' + phoneNumber + '/' + orderId );
				}
			}

			/**
			 * Perform the default view of the cart.
			 */
			$scope.viewCart = function(){
				// UNDER DEVELOPMENT
				if( $scope.cartItems.length > 0 ){
				} else {
				}
			}

			/**
			 * @todo is this function deprecated?
			 */
			$scope.viewTickets = function( lastFour, phoneNumber, orderId ){
				// UNDER DEVELOPMENT
				// URI:: /:merchant/ticketView/:lastFour/:phoneNumber/:orderId'


				$scope.routeTo( $rootScope.routes.ORDER_VIEW + '/' + lastFour + '/' + phoneNumber + '/' + orderId );
			}

			$scope.continueShopping = function(){
				var lastKeyword = appDataService.lastKeyword;

				$scope.animateIn
				(
					function(){
						if( lastKeyword ) {
							$scope.routeTo('packageList/keyword/' + lastKeyword);
						} else {
							$scope.routeTo();
						}
					}
				)
			}

			$scope.gotoBilling = function(){
				if( lastKeyword ){
					$scope.routeTo('packageList/keyword/' + lastKeyword);
				} else {
					$scope.routeTo();
				}
			}

			$scope.gotoShipping = function(){
				// Ensure there are items in the cart before proceeding.
				if( !$scope.hasItemsInCart() ){
					return;
				}

				$scope.routeTo( $rootScope.routes.SHIPPING );
			}

			$scope.checkOut = function(){
				// Ensure there are items in the cart before proceeding.
				if( !$scope.hasItemsInCart() ){
					return;
				}

				$scope.animateOut
				(
					function(){
						// If this is a paypal enabled merchant, and everything is good, we should present the payment method.
						// NOTE: in the future, when we accept gift cards, we may need to proceed to the payment method screen by default.
						if( $rootScope.showPayPalCheckout ){
							$scope.routeTo($rootScope.routes.PAYMENT_METHOD)
						} else {
							$scope.routeTo( userInfoService.getDeliveryRouting());
						}
					}
				)
			}

			function setOptIns (object ,array){
				if( array.optins ){

					for( var i = 0; i < array.optins.length; i++ ){
						var optProp = 'opt_in';
						if( i > 0 ){
							optProp += String( i + 1 );
						}
						object[optProp] = ( array.optins[i].checked ) ? 1 : 0;
					}
				}
			}

			$scope.completeOrder = function(){

				// Ensure there are items in the cart before proceeding.
				if( !$scope.hasItemsInCart() ){
					return;
				}

				if( $scope.showTerms && $scope.termsChecked == '0' ){
					// Alert user she needs to agree to terms and conditions.
					modalService.displayModal
					(
						{ title: $scope.i18n.Alert.agreeToTermsTitle
							, headerFlag: modalService.PRIMARY
							, description: $scope.i18n.Alert.agreeToTermsMsg
							, hideAllButtons: true
							, callback: function( detail ){
							if( detail == $scope.buttonFlags.YES ){
								$scope.completeOrder();
							}
						}
						}
					)

					// User did not agree to terms.
					return;
				}

				// Show the spinner.
				$scope.showSpinner( $scope.i18n.ApplicationLabels.purchaseLoadMsg );

				var cartDetails   = cartService.cartDetails;
				var savedBilling  = userInfoService.billing;
				var savedShipping = userInfoService.shipping;
				var savedPayment  = userInfoService.payment;

				$log.log('CartController.js // $scope.completeOrder // savedShipping',savedShipping);
				$log.log('CartController.js // $scope.completeOrder // savedPayment',savedPayment);
				$log.log('CartController.js // $scope.completeOrder // savedBilling',savedBilling);
				// The value from here is related to the response from GetExpressCheckoutDetails.
				var paypalPurchase = userInfoService.paypalPurchase || {};

				var customer =
				{
					  f_name: savedBilling.firstName
					, m_name: ''
					, l_name: savedBilling.lastName
					, phone: savedBilling.phone.split('-').join('')
					, mobile_phone: ''
					, email: savedBilling.email
				};

				setOptIns(customer, savedBilling)
				setOptIns(customer, savedShipping)

				// Billing Address.
				var address =
				{
					street1: savedBilling.address
					, street2: ''
					, city: savedBilling.city
					, state: savedBilling.state
					, zip: savedBilling.zip
					, country: savedBilling.country
					, type: $scope.addressTypes.BILLING
				};

				// Save the shipping address information if it's a physical delivery.
				var shippingAddress;
				if( savedShipping && savedShipping.isNonPhysicalDeliveryMethod === false){
					shippingAddress =
					{
						  street1: savedShipping.address
						, street2: savedShipping.address2
						, city: savedShipping.city
						, state: savedShipping.state
						, zip: savedShipping.zip
						, country: savedShipping.country
						, type: $scope.addressTypes.SHIPPING
					};

					customer.ship_f_name = savedShipping.firstName;
					customer.ship_m_name = '';
					customer.ship_l_name = savedShipping.lastName;
				}

				// Note year is first and month has to be two digits e.g Jan 2015 = 1501.
				var payment =
				{
					cvv2: savedPayment.securityCode
					, card_expire_date: savedPayment.cardExpirationDate
					, account_number: savedPayment.credicard
					, billing_id: savedPayment.billingId
					, billing_type: savedPayment.billingType
					, auth_type: savedPayment.authType
					, cepay_merchant_id: savedPayment.cepayMerchantId
					, cardholder_name: savedBilling.lastName + ', ' + savedBilling.firstName
					, amount: cartService.total
				};

				// Get cart adjustments.
				var cartAdj = (cartDetails.CART_ADJUSTMENTS && cartDetails.CART_ADJUSTMENTS.CA) ? cartDetails.CART_ADJUSTMENTS.CA: [];

				// Get CHARACS.
				var characs = [];

				// Nest address in customer object.
				if( shippingAddress ){
					customer.ADDRESS = [ address, shippingAddress ]
				} else {
					customer.ADDRESS = address;
				}

				// Finalize the data and make purchase request.
				// @TODO: Replace hardcoded stuff.
				var myData =
				{
					  ship_method: savedPayment.shipMethod
					, ship_amount: savedPayment.shipAmount
					, tax_amount: cartDetails.tax_total
					, order_amount: cartDetails.total
					, fee_total: cartDetails.fee_total
					, tiny_response: 0
					, software: clientInfoService.software
					, CUSTOMER: customer
					, PAYMENT: payment
					, CHARACS: characs
					, ENVIRONMENT: clientInfoService
				};

				// Paypal
				if( typeof paypalPurchase != 'undefined' && typeof paypalPurchase.valid != 'undefined' && paypalPurchase.valid ){
					// we have a paypal purcahse. let's include the information.
					if(
						( typeof paypalPurchase.paypal_id != 'undefined' && paypalPurchase.paypal_id )
							&&
							( typeof paypalPurchase.token != 'undefined' && paypalPurchase.token )
						){
						myData.PAYPAL_INFO = {
							token: paypalPurchase.token
							, paypal_id: paypalPurchase.paypal_id
						}
					}
				}

				// Delete any unwanted properties from ENVIRONMENT.
				delete myData.ENVIRONMENT.currentPage;
				delete myData.ENVIRONMENT.previousPage;
				delete myData.ENVIRONMENT.previousPages;

				try{
					// Append useful values to the backend call
					if(!utilitiesService.isEmpty(appDataService.storageService.get('_geolocationPosition'))){
						var geo = appDataService.storageService.get('_geolocationPosition');
						var coords = geo.coords;

						delete geo.coords;

						for(key in coords){
							geo[key] = coords[key];
						}

						for(key in geo){
							if(typeof geo[key] != 'string'){
								geo[key] = new String(geo[key]);
							}
						}

						myData.ENVIRONMENT.geolocation = geo;
					} else if(!utilitiesService.isEmpty(appDataService.storageService.get('_geolocationError'))){
						var geo = appDataService.storageService.get('_geolocationError');

						for(key in geo){
							if(typeof geo[key] != 'string'){
								geo[key] = new String(geo[key]);
							}
						}

						myData.ENVIRONMENT.geolocation = geo;
					}

					myData.ENVIRONMENT.host = $window.location.host;
					//myData.ENVIRONMENT.queryString = $window.location.search;
					myData.ENVIRONMENT.environment = appDataService.environment;
					myData.ENVIRONMENT.device = appDataService.device;
					myData.ENVIRONMENT.merchant = appDataService.merchant;
					myData.ENVIRONMENT.merchantId = appDataService.merchantId;
					myData.ENVIRONMENT.langauge = appDataService.langauge;
				} catch(error){}

				myData.CART_ADJUSTMENTS = {};

				if(angular.isArray(cartAdj)){
					myData.CART_ADJUSTMENTS.CA = cartAdj;
				} else {
					myData.CART_ADJUSTMENTS.CA = [cartAdj];
				}

				// Add reference of request in pendingRequests.
				$rootScope.pendingRequests.push($scope.services.PURCHASE);

				appDataService.fetch(
						$scope.services.PURCHASE
						, false
						, myData
					).then(
					function( data ){
						// Remove reference of request in pendingRequests.
						$rootScope.removePendingRequest( $scope.services.PURCHASE );

						// @TODO, if the following line needed? there is similar code below. 
						if( data.status == $scope.status.FAILED && $rootScope.showingPurchaseDiconnectError ){
							return;
						}
						// Hide the spinner.

						if( data.status == $scope.status.OK ){
							// Performing clean up of data in localStorage.
							appDataService.storageService.remove( 'SetExpressCheckout' );
							appDataService.storageService.remove( 'SetPayPalStatus' );
							appDataService.storageService.remove( 'GetExpressCheckoutDetails' );
							appDataService.storageService.remove( 'UpdateCartCheckoutInfo' );
							appDataService.storageService.remove( 'paypalSessionInfo' );
							appDataService.storageService.remove( '_geolocationPosition' );
							appDataService.storageService.remove( '_geolocationError' );
							appDataService.storageService.remove( '_geolocationSwap' );
							appDataService.storageService.remove( '_userInfoService' );

							var savedBilling = userInfoService.billing;
							var purchaseData = data;
							// using the following, `fetch(requestType, true, false, true)` will say to useStorage, and replace the data in storage, but not make a request directly, only listen.

							// I need the lastFour, orderId, and phoneNumber
							$scope.purchaseReceipt.lastFour = data.PAYMENT.account_number.substr( data.PAYMENT.account_number.toString().length - 4 );
							//data.PAYMENT.account_number.substring
							$scope.purchaseReceipt.orderId = data.order_id;
							$scope.purchaseReceipt.phoneNumber = savedBilling.phone;

							// Add reference of request in pendingRequests.
							$rootScope.pendingRequests.push( $scope.services.GET_NEW_CART_ID );

							// Listen for the GetNewCartID
							appDataService.fetch
								( $scope.services.GET_NEW_CART_ID
									, true
									, false
									, true
									, true
								).then
							( function( data ){
								  // Remove reference of request in pendingRequests.
								  $rootScope.removePendingRequest( $scope.services.GET_NEW_CART_ID );
								  cartService.clearInternals();

								  // Add reference of request in pendingRequests.
								  $rootScope.pendingRequests.push($scope.services.VIEW_ORDER)

								  // Listen for the ViewOrder.
								  appDataService.fetch(
										  $scope.services.VIEW_ORDER
										  , false
									  ).then(
									  function( data ){
										  // Remove reference of request in pendingRequests.
										  $rootScope.removePendingRequest( $scope.services.VIEW_ORDER );

										  if( data.status == $scope.status.OK ){
											  var vieworderData = data;

											  //
											  $scope.purchaseReceipt.emailAddress =
												  angular.isDefined(data.VIEW_ORDER_REPLY.CUSTOMER_DETAILS_REPLY.CUSTOMER.EMAIL.address)
													  ? data.VIEW_ORDER_REPLY.CUSTOMER_DETAILS_REPLY.CUSTOMER.EMAIL.address
													  : false;

											  data = { url:$route.current.params.merchant+'/orderComplete' };
											  analyticService.trackPageview( data );

											  data = { 'Purchase': purchaseData, 'ViewOrder': vieworderData };
											  analyticService.trackPurchase( data );
											  // Set a few things locally.
											  // Take them to the Review Page.
											  $scope.hideSpinner();
											  $scope.viewReceipt();
										  } else {
											  $scope.hideSpinner();
											  $scope.viewReceipt();
										  }
									  }
								  )}
							)
						} else if(data.status == $scope.status.FAILED){
							$scope.hideSpinner();

							modalService.displayModal
							(
								{ title: $scope.i18n.Alert.failedPurchaseTitle
									, headerFlag: modalService.DANGER
									, description: $scope.i18n.Alert.failedPurchaseMsg
									, hideAllButtons: true
									, callback: function( detail ){
									$scope.routeTo($scope.routes.SHIPPING)
								}
								}
							)
						}
					}
				)
			}


			function handleUpdateCartItemResponse(data){
				if(data.status == $scope.status.FAILED){
					$scope.i18n.displayErrorCode(data)
				}
			}

			function handleUpdateCartCheckoutInfo(data){
				if(data.status == $scope.status.FAILED){
					$scope.i18n.displayErrorCode(data)
				}
			}

			function initializeController(){
				$rootScope.hideSpinner();
				$scope.useDevice = initializeData.device;
				$scope.useEnvironment = initializeData.environment;
				$scope.showPayPalCheckout = showPayPalCheckoutOption( consolidatedData, merchantDetailsData );

				var pathname = location.pathname.toString();
				// Paypal
				if( pathname.indexOf( $rootScope.routes.PAYPAL_RETURN_URL ) > -1 ){
					// Need the following to disable crossSells from appearing on the return.
					appDataService.crossSells = false;
					processPayPalReturnUrl();
				} else if( pathname.indexOf( $rootScope.routes.PAYPAL_CANCEL_URL ) > -1 ){
					// Need the following to disable crossSells from appearing on the return.
					appDataService.crossSells = false;
					processPayPalCancelUrl();
				} else {
					cartService.fetchSetExpressCheckout(
						function(){ updatePage() }
						, function(){ updatePage() }
					);

					// moving the code lower down.
					appDataService.fetch
						( enumsService.services["GET_CART_SUMMARY"]
							, true    // use localStorage
							, false   // do not make a request to the backend. 
							, false	  // 
							, true    // use cookies
						).then(
						function(data){
							if( data.status == enumsService.status.OK ){
								cartService.setEditFlags( data );
							}

							if( data.CHECKOUT_KEYWORDS
								&& data.CHECKOUT_KEYWORDS.K
								&& appDataService.crossSells
								)
							{
								$location.path('/' + $route.current.params.merchant + '/crossSells');
							}
						}
					);

					// Terms related properties. If @@parkName is in the terms, replace it with the merchant_name.
					$scope.terms = $scope.i18n.replaceAll('@@parkName', $scope.state.parkName, $scope.i18n.CartView.agreement);
					if(typeof $scope.terms == 'string' && $scope.terms != ''){
						$scope.showTerms = ((appDataService[$scope.services.GET_APPLICATION_CONSOLIDATED] || {}).SETTINGS || {}).display_agreement || false;
						$scope.showTerms = utilitiesService.getBoolean($scope.showTerms);
					} else {
						$scope.showTerms = false;
					}

					$scope.termsChecked = 0
					$scope.termsChk = 0
					$scope.purchaseReceipt = appDataService.purchaseReceipt

					// Render view according to route:
					// Cart | Order | Receipt
					var locPath = $location.path().toLowerCase()
					$scope.pageView.Cart = locPath.indexOf($rootScope.routes.CART.toLowerCase()) > -1;
					$scope.pageView.Order = locPath.indexOf($rootScope.routes.REVIEW.toLowerCase()) > -1;
					$scope.pageView.Receipt = locPath.indexOf($rootScope.routes.RECEIPT.toLowerCase()) > -1;

					if( $scope.pageView.Cart ){
						$scope.heading = $scope.i18n.CartView.cartHeading;
					} else if( $scope.pageView.Order ){
						$scope.heading = $scope.i18n.CartView.orderReviewHeading;
						pageViewReview();
					} else if( $scope.pageView.Receipt ){
						$scope.heading = $scope.i18n.CartView.receiptViewHeading;
					}

					$scope.showTerms = ($scope.pageView.Order && $scope.showTerms);

				}



				// Check config for collapsing Taxes and Fees by default.
				if( typeof consolidatedData != 'undefined' ){
					if( typeof consolidatedData.SETTINGS != 'undefined' ){
						// Check config for collapsing Taxes and Fees by default.
						$scope.showFeeDetails = appDataService.settings.expand_prices
						$scope.showDiscountDetails = appDataService.settings.expand_prices
					}
				}
			}

			function pageViewReview (){
				$scope.shipping = userInfoService.shipping || {};
				var billing = userInfoService.billing || {};

				//set the view of the arrow for payment process
				$scope.arrowPositioning = typeof($scope.shipping) != 'undefined' && $scope.shipping !== null? (utilitiesService.getBoolean($scope.shipping.shippingAvailable)?2:1):1

				// PayPal Related: check that we have a paypal session ready for purchase.
				var paypalSessionInfo = appDataService.storageService.get('paypalSessionInfo');

				$log.log('CartController.js // func init // paypalSessionInfo',paypalSessionInfo);
				$log.log('CartController.js // func init // billing', billing);
				$log.log('CartController.js // func init // $scope.shipping',$scope.shipping);
				$log.log('CartController.js // func init // userInfoService.payment', userInfoService.payment);

				if( typeof paypalSessionInfo !== 'undefined' && paypalSessionInfo !== null ){

					if( typeof paypalSessionInfo.billing != 'undefined' ){
						//update billing information stored in localStorage and userInfoService.billing
						userInfoService.set('billing', paypalSessionInfo.billing);
					}

					if( typeof paypalSessionInfo.payment != 'undefined' ){
						userInfoService.payment = paypalSessionInfo.payment;
					}

					if( typeof paypalSessionInfo.paypalPurchase != 'undefined' ){
						userInfoService.paypalPurchase = paypalSessionInfo.paypalPurchase;
					}
				}

				// Verify shipping information is available if applicable.
				var shipReq = userInfoService.shippingRequired( { skipRequests : 'SetExpressCheckout' }, initializeController );
				if( shipReq == 'gettingCartSummary' ){
					return;
				} else if( shipReq && ( !$scope.shipping || !$scope.shipping.valid ) ){
					// No shipping information. Alert and take user back to the shipping screen.
					modalService.displayModal(
						{ title: $scope.i18n.Alert.shippingInfoNotSetTitle
							, description: $scope.i18n.Alert.shippingInfoNotSetMsg
							, hideAllButtons: true
						});

					$scope.routeTo($rootScope.routes.SHIPPING)
					return;
				} else if( !billing || !billing.valid ){
					// No billing information. Alert and take user back to the billing screen.
					modalService.displayModal(
						{ title: $scope.i18n.Alert.billingInfoNotSetTitle
							, description: $scope.i18n.Alert.billingInfoNotSetMsg
							, hideAllButtons: true
						});

					$scope.routeTo($rootScope.routes.BILLING);
					return;
				} else if(!userInfoService.payment || !userInfoService.payment.valid){

					//if you are in development, dont route the user
					if( $scope.autofill && typeof( userInfoService.payment.valid ) == 'undefined' ){
						userInfoService.payment.valid = true
					}else{

						// No payment information. Alert and take user back to the payment screen.
						modalService.displayModal(
							{ title: $scope.i18n.Alert.paymentInfoNotSetTitle
								, description: $scope.i18n.Alert.paymentInfoNotSetMsg
								, hideAllButtons: true
							});
						$scope.routeTo($rootScope.routes.BILLING);
						return;
					}
				} else {

					$log.log('CartController.js // func init // else block verifying paypal data >>>> ');
					// Paypal.
					$scope.showBilling = billing;
					$scope.showPayment = userInfoService.payment;
					$scope.showSpritePaymentType = false;
					$scope.showPayPalPaymentMethod = false;

					$log.log('CartController.js // func init // $scope.showBilling ',$scope.showBilling );
					$log.log('CartController.js // func init // $scope.showPayment ',$scope.showPayment );
					$log.log('CartController.js // func init // billing',billing);
					$log.log('CartController.js // func init // userInfoService.payment',userInfoService.payment);

					var billingDetails = cartService.getBillingDetailsById(
						merchantDetailsData
						, userInfoService.payment.billingId
					);

					if(
						typeof userInfoService.payment.authType != 'undefined'
							&& userInfoService.payment.authType != ''
							&& userInfoService.payment.authType == 'PPA'
						){
						$scope.showPayPalPaymentMethod = true;
						$scope.showSpritePaymentType = userInfoService.payment.authType.toString().toLowerCase();
					} else if(
						typeof userInfoService.payment.billingId != 'undefined'
							&& userInfoService.payment.billingId != ''
						){
						$scope.showSpritePaymentType = billingDetails.card_type_code.toString().toLowerCase();
					} else {
					}
				}
			}

			function updatePage(){
				$log.info('>> CartController.js >> updatePage()');
				$scope.showPayPalCheckout = showPayPalCheckoutOption( consolidatedData, merchantDetailsData );
			}

			$scope.gotoPayPalCheckout = function(){
				// TODO: this will need some more logic in here to verify that the `SetExpressCheckout` is working.
				if( $scope.showPayPalCheckout ){
					if( cartService.paypal.token ){
						var mobile_url = $rootScope.routes.PAYPAL_MOBILE_CHECKOUT + cartService.paypal.token;
						var normal_url = $rootScope.routes.PAYPAL_CHECKOUT + cartService.paypal.token;

						if( $scope.useEnvironment == 'development' || $scope.useEnvironment == 'staging' ){
							mobile_url = $rootScope.routes.PAYPAL_SANDBOX_MOBILE_CHECKOUT + cartService.paypal.token;
							normal_url = $rootScope.routes.PAYPAL_SANDBOX_CHECKOUT + cartService.paypal.token;
						}
						// Using a switch here, in case we wish to specify alternatives in the future.
						switch ( $scope.useDevice ){
							// Use the mobile specific url for mobile devices.
							case 'mobile':
								$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.redirectingToPayPal );
								window.location = mobile_url;
								break;

							case 'tablet':
							case 'desktop':
							default:
								// window.location = mobile_url
								$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.redirectingToPayPal );
								window.location = normal_url;
								break;
						}
					} else {
						modalService.displayModal
						(
							{ title: $scope.i18n.Alert.errorWithPayPalTitle
								, headerFlag: modalService.DANGER
								, description: $scope.i18n.Alert.errorWithPayPalMsg
								, buttonFlags: [$scope.buttonFlags.OK]
								, callback: function( detail ){
								if(detail == $scope.buttonFlags.OK){
									// $scope.completeOrder()
								}
							}
							}
						)
					}
				} else {
					// Not going to redirect to paypal because
					$log.warn('CartController.js::gotoPayPalCheckout() // not allowed, should not see button.');
				}
			}

			/**
			 * Need to verify that two conditions are met before we show The PayPal checkout option.
			 * @returns {boolean}
			 */
			function showPayPalCheckoutOption(){
				var inMerchantDetails = false;
				var inConsolidated = false;
				var hasPayPalToken = false;

				if( typeof consolidatedData != 'undefined' ){
					if( typeof consolidatedData.SETTINGS != 'undefined' ){
						// @todo, make sure we have a functional paypal token.
						if( consolidatedData.SETTINGS.billing_types.indexOf('PPA') > -1 ){
							inConsolidated = true;
						} else {
							return false;
						}
					}
				}

				if( typeof merchantDetailsData != 'undefined' ){
					if( typeof merchantDetailsData.BILLINGTYPE != 'undefined' ){
						for( var idx in merchantDetailsData.BILLINGTYPE ){
							if( merchantDetailsData.BILLINGTYPE[idx].auth_type == 'PPA'
								&& merchantDetailsData.BILLINGTYPE[idx].visible == 'true'
								){
								inMerchantDetails = true;
							}
						}
					}
				}

				// Check that we have a working token. 
				hasPayPalToken = ( cartService.paypal.token ) ? true: false;

				return ( inMerchantDetails && inConsolidated && hasPayPalToken ) ? true: false;
			}

			function processPayPalReturnUrl(){
				// Show the spinner.
				$scope.showSpinner( $scope.i18n.Alert.paypalCompleteUrlMsg );
				/**
				 * Let's verify we have everything we need, the PayerID and token from the URL.
				 * We need to reset the values if they are in the URL.
				 */
				var paypalData = {};
				if( typeof $routeParams.token != 'undefined' && $routeParams.token ){
					paypalData.token = $routeParams.token;
				}

				if( typeof $routeParams.PayerID != 'undefined' && $routeParams.PayerID ){
					paypalData.id = $routeParams.PayerID;
				}

				if( paypalData ){
					cartService.updatePaypal( paypalData );
				}

				// Notify the backend of the status.
				if( cartService.paypal.id && cartService.paypal.token ){
					// I want to strip out the params from the URL. 
					cartService.callSetPayPalStatus(
						cartService.paypal.token
						, 1
						, function(){ processSetPayPalStatusResponse(); }
						, function(){ processPayPalReturnUrlResponseFailed(); }
					);
				} else {
					// Unexpected Problem. @todo code for this condition.
					// route to an error page, or back to the paymentMethod with a modal.
				}

				// TODO: check that it works at this stage.
				$rootScope.hideSpinner();
			}

			function processSetPayPalStatusResponse(){
				var token = cartService.paypal.token;
				// my internals // token: cartService.paypal.token
				cartService.callGetExpressCheckoutDetails(
					token
					, merchantDetailsData
					, function( data ){ processGetExpressCheckoutDetails( data ); }
					, function(){ processPayPalReturnUrlResponseFailed(); }
				);
			};

			function processGetExpressCheckoutDetails( data ){
				storageService.set('PaypalExpressDetails', data);
			
				console.log('ExpressCheckoutDetails // data',data);

				var myData = cartService.updatePurchaseDataWithPayPal(  merchantDetailsData, data );



				// Apply the PayPal information to the necessary data points for purchase.
				appDataService.paypalPurchase = myData.paypalPurchase;
				appDataService.billing  = myData.billing;
				appDataService.payment  = myData.payment;
				appDataService.shipping = myData.shipping;

				// @TODO: need to figure out a way to pass in the data I need for the updateCartCheckoutInfo.
				// @TODO: Hardcode shipping method until we have shipping form implemented.
				// This should come in from the selection by the user, or the defaulted value.
				// Save the final data before redirecting to orderReview.
				// commenting out for now. may not need it.
				// @TODO: Save this data in the shipping form when we implement it.

				var myUpdateCartCheckoutInfoData =
				{ shippingMethod: appDataService.payment.shipMethod
					, shippingAmount: appDataService.payment.shipAmount
					, billingId: appDataService.payment.billingId
					, skip_requests : 'SetExpressCheckout'
				};


				console.log('CartController.js // processGetExpressCheckoutDetails() // myUpdateCartCheckoutInfoData', myUpdateCartCheckoutInfoData);			


				cartService.callUpdateCartCheckoutInfo(
					myUpdateCartCheckoutInfoData
					//, merchantDetailsData
					, function(){ processUpdateCartCheckoutInfo(); }
					, function(){ processPayPalReturnUrlResponseFailed(); }
				);
			}

			function processUpdateCartCheckoutInfo() {
	
				$scope.cartObject
				cartService.updatePayPalSessionInfo();
	
				var routedToShipping = false;
				
				if ( angular.isDefined(merchantDetailsData) ) {
	
					if ( angular.isDefined(merchantDetailsData.SHIPPINGMETHOD) ) {
					
						if ( !angular.isArray(merchantDetailsData.SHIPPINGMETHOD) ) {
							merchantDetailsData.SHIPPINGMETHOD = [merchantDetailsData.SHIPPINGMETHOD];
						}
					
						if ( merchantDetailsData.SHIPPINGMETHOD.length > 0 ) {
							routedToShipping = true;
							$scope.routeTo( $rootScope.routes.SHIPPING );
						}
					}
				} 
				
				if ( routedToShipping == false ) {
					$scope.routeTo( $rootScope.routes.REVIEW );	
				}
				
			}
	
			function processPayPalReturnUrlResponseFailed(){
				// @TODO: disable the paypal option, display modal error message, and route to paymentMethod on click in modal
				modalService.displayModal
				(
					{ title: $scope.i18n.Alert.failedUpdateCheckoutInfoTitle
						, headerFlag: modalService.DANGER
						, description: $scope.i18n.Alert.failedUpdateCheckoutInfoMsg
						, buttonFlags: [$scope.buttonFlags.OK]
						, callback: function( detail ){
						if( detail == $scope.buttonFlags.OK ){
							$scope.routeTo( $rootScope.routes.CART );
						}
					}
					}
				)
			};

			function processPayPalCancelUrl(){
				$rootScope.showSpinner( $scope.i18n.Alert.paypalCancelledUrlMsg );
				// Let's verify we have everything we need, the PayerID and token from the URL.
				// We need to reset the values if they are in the URL.
				if( typeof $routeParams.token != 'undefined' && $routeParams.token ){
					cartService.paypal.token = $routeParams.token;
				}

				if( typeof $routeParams.id != 'undefined' && $routeParams.id ){
					cartService.paypal.id = $routeParams.id;
				}
				// Notify the backend of the issue.
				// NOTES: If you are setting the status to 0, then it is not necessary to call GetExpressCheckoutDetails because there will be no details to checkout on.
				// If you call `GetExpressCheckoutDetails` after you have set the status to 0, you will most likely get an error of\
				//	error_msg: "Phone Number Missing", paypal_id: "", request_type: "GetExpressCheckoutDetails", session_id: "ixww.100518.964", status: "FAILED"�}
				if( cartService.paypal.token ){
					// NOTE, I am not using the $scope.routeTo function because that function has specific code that does something else.  
					cartService.callSetPayPalStatus(
						cartService.paypal.token
						, 0
						, function(){ $location.path('/' + $routeParams.merchant + '/' + $rootScope.routes.CART ) }
						, function(){ $location.path('/' + $routeParams.merchant + '/' + $rootScope.routes.CART ) }
					);

					$rootScope.hideSpinner();

				} else {
					// Unexpected Problem. @todo code for this condition.
					// route to an error page, or back to the paymentMethod with a modal.
					$rootScope.hideSpinner();

					// $rootScope.routeTo($rootScope.routes.ERROR + '/' + '/error/);
					$location.path('/' + $routeParams.merchant + '/' + $rootScope.routes.CART );
					// @potential to have an error here with the spinner.
				}

				$rootScope.hideSpinner();
			}

			$scope.$on("$routeChangeSuccess", function( event ){
				initializeController();
			});

			$scope.$on("$routeChangeStart", function( event ){

				if( $rootScope.changeQty ){
					$scope.cancelUpdate();
				}
				// We want to clear out the current cart, and get a new cartid if necessary.
				if( $scope.pageView.Receipt ){
					var locPath = location.pathname.toLowerCase();
				}
			});

		}])
