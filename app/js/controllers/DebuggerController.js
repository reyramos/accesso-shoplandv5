/**
 * @author Christopher Rivera <crivera@accesso.com>
 *
 * Manages cart screen and related functions
 * Getting the file ready.
 */
angular.module('app').controller
	( 'DebuggerController'
	, function ( $scope
		, $route
		, $location
		, $log
		, $routeParams
		, storageService
		, merchantService
		, navigationService
		, clientInfoService
		, socketService
		, languageService
		, packageService
		, cartService
		, analyticService
		) {
		window.scrollTo( 0, 0 );
		$scope.pageName = 'Cart View (note from cart ctrl)'

		//$scope.cartService = cartService;
		$scope.cartItems   = cartService.cartItems;
		$scope.cartDetails = cartService.cartDetails;
		$scope.cartTotalPrice = cartService.cartTotalPrice;
		$scope.cartTaxesAndFees = cartService.cartTaxesAndFees;
		$scope.cartDetails = cartService.cartDetails;
		$scope.cartAdjustments = cartService.cartAdjustments;


		if ( $location.path().indexOf('/cartView') !== -1 ) {
			$scope.pageViewCart = true;
			$scope.pageViewOrder = false;
			$scope.pageViewReceipt = false;
			$scope.showDiscounts = false;

			$scope.cartItems   = cartService.getCartItems();
			$scope.cartDetails = cartService.getCartDetails();

		} else {
			$scope.showDiscounts = true;
		}

		// @TODO: Refactor with fetch.
		$scope.$on
			( $scope.socket.receive + $scope.services.GET_CART_SUMMARY
			, function(event,data){
				// good response.
				if ( data.status == $scope.status.OK ) {
					$scope.cartDetails = cartService.cartDetails;
					$scope.cartItems   = cartService.cartItems;
				} else {
					// error.
				}
			}
		);


	}
);
