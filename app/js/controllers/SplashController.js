/**
 * # SplashController
 *
 * Manages splash loading screen and related functions
 */
angular.module('app').controller
	( 'SplashController'[
		  '$scope', 'analyticService'
	, function ($scope, analyticService) {
			$scope.pageName = 'Splash View (note from splash ctrl)'
		}])
