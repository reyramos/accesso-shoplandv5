/**
 * # Static File Paths
 *
 * Inject URL paths for fonts/images and potentially other static files
 * This file exists due to the need of dynamically generating absolute
 * paths to all static files for versioning reasons.
 *
 */

angular.module('app').run(
	['$rootScope' , '$log' , '$routeParams', 'utilitiesService'
	, function( $rootScope , $log , $routeParams, utilitiesService ) {

		$rootScope.$on
			( '$routeChangeSuccess'
			, function() {
				var folderName = $routeParams.m ? window.location.pathname.split('/')[1] : utilitiesService.getMerchant();

				var getString = "";
				var fontAwesomePath = "//netdna.bootstrapcdn.com/font-awesome/3.2.1/font";

				var style = document.createElement('style');
				document.getElementsByTagName('head')[0].appendChild(style)

				var sLength = ( document.styleSheets.length > 1 ) ? document.styleSheets.length : 1;
				var s = document.styleSheets[document.styleSheets.length - 1];

				if ($routeParams.v){
					getString = "?v=" + $routeParams.v;
				}

				/**
				 * This is specifically to handle a case where Firefox/Netscape does not have
				 * the addRule() function and fails.
				 */
				function addCSSRule(sheet, selector, rules, index) {
					if( sheet.insertRule ) {
						sheet.insertRule( selector + "{" + rules + "}", index );
					} else {
						sheet.addRule(selector, rules, index);
					}
				}

				function loadCSS( href ) {
					var http = new XMLHttpRequest();
					http.open('HEAD', href, true)
					http.onreadystatechange = function(event){
						if (event.currentTarget.status==200){
							var link=document.createElement("link")
							link.setAttribute("rel", "stylesheet")
							link.setAttribute("type", "text/css")
							link.setAttribute("href", href)
							document.getElementsByTagName("head")[0].appendChild(link)
							http.onreadystatechange = null
						}
					}
					http.send();
				}

				if(typeof folderName != 'undefined'){
					loadCSS('/'+folderName+'/css/merchant/'+utilitiesService.getMerchant()+'.css'+getString);
				}

				//var s = document.styleSheets[0];
				// function addCSSRule(sheet, selector, rules, index);

				addCSSRule
					( s
					, '.sprite'
					, "background-image: url('/"+folderName+"/img/sprite.png"+getString+"')"
					);

				addCSSRule
					(	s
					,	'@font-face'
					,	[	"font-family: font-regular;"
						,	"src: url('/"+folderName+"/fonts/MuseoSans-300.otf"+getString+"');"
						].join('\n')
					);

				addCSSRule
					(	s
						,	'@font-face'
						,	[	"font-family: font-regular-italic;"
							,	"src: url('/"+folderName+"/fonts/MuseoSans-300Italic.otf"+getString+"');"
						].join('\n')
					);

				addCSSRule
					(	s
						,	'@font-face'
						,	[	"font-family: font-semi-bold-italic;"
							 ,	"src: url('/"+folderName+"/fonts/MuseoSans_500_Italic.otf"+getString+"');"
						 ].join('\n')
					);

				addCSSRule
					(	s
						,	'@font-face'
						,	[	"font-family: font-bold-italic;"
							 ,	"src: url('/"+folderName+"/fonts/MuseoSans-700Italic.otf"+getString+"');"
						 ].join('\n')
					);

				addCSSRule
					(	s
					,	'@font-face'
					,	[	"font-family: font-semi-bold;"
						,	"font-weight: bold;"
						,	"src: url('/"+folderName+"/fonts/MuseoSans_500.otf"+getString+"');"
						].join('\n')
					);

				addCSSRule
					(	s
					,	'@font-face'
					,	[	"font-family: font-regular-italic;"
						,	"src: url('/"+folderName+"/fonts/ProximaNova-RegularItalic.otf"+getString+"');"
						].join('\n')
					);

				addCSSRule
					(	s
					,	'@font-face'
					,	[ "font-family: font-bold;"
						, "font-weight: bold;"
						, "src: url('/"+folderName+"/fonts/MuseoSans_700.otf"+getString+"');"
						].join('\n')
					);

				addCSSRule
					(	s
					,	'@font-face'
					,	[	"font-family: Arabella;"
						,	"src: url('/"+folderName+"/fonts/Arabella.otf"+getString+"');"
						].join('\n')
					);

				addCSSRule
				(	s
					,	'@font-face'
					,	[	"font-family: Glyphicons Halflings;"
						,	"src: url('/"+folderName+"/fonts/bootstrap/glyphiconshalflings-regular.eot"+getString+"');"
						,	"src: url('/"+folderName+"/fonts/bootstrap/glyphiconshalflings-regular.eot?#iefix"+getString+"'),"
						,	"url('/"+folderName+"/fonts/bootstrap/glyphiconshalflings-regular.woff"+getString+"')format('woff'),"
						,	"url('/"+folderName+"/fonts/bootstrap/glyphiconshalflings-regular.ttf"+getString+"') format('truetype'),"
						,	"url('/"+folderName+"/fonts/bootstrap/glyphiconshalflings-regular.svg#glyphicons_halflingsregular"+getString+"')format('svg');"
					 ].join('\n')
				);

				addCSSRule
					(	s
						,	'@font-face'
						,	[	"font-family: FontAwesome;"
							 ,	"font-weight: normal;"
							 ,	"font-style: normal;"
							 ,	"src: url('"+fontAwesomePath+"/fontawesome-webfont.eot?v=3.2.1"+getString+"');"
							 ,	"src: url('"+fontAwesomePath+"/fontawesome-webfont.eot?#iefix&v=3.2.1"+getString+"') format('embedded-opentype'),"
							 ,	"url('"+fontAwesomePath+"/fontawesome-webfont.woff?v=3.2.1"+getString+"') format('woff'),"
							 ,	"url('"+fontAwesomePath+"/fontawesome-webfont.ttf?v=3.2.1"+getString+"') format('truetype'),"
							 ,	"url('"+fontAwesomePath+"/fontawesome-webfont.svg#fontawesomeregular?v=3.2.1"+getString+"') format('svg');"
						 ].join('\n')
					);
			  }
			)
	}]
)
