/**
 * # Application Init
 *
 * Core application controller that includes all services needed to
 * kick-start the application.
 */
angular.module('app').run
	( [ '$rootScope'
		, '$log'
		, '$routeParams'
		, '$route'
		, '$location'
		, '$window'
		, '$timeout'
		, 'cartService'
		, 'clientInfoService'
		, 'enumsService'
		, 'socketService'
		, 'languageService'
		, 'appDataService'
		, 'analyticService'
		, 'loggingService'
		, 'utilitiesService'
		, 'templateService'
		, '$http'
		, 'modalService'
		, function
			( $rootScope
			, $log
			, $routeParams
			, $route
			, $location
			, $window
			, $timeout
			, cartService
			, clientInfoService
			, enumsService
			, socketService
			, languageService
			, appDataService
			, analyticService
			, loggingService
			, utilitiesService
			, templateService
			, $http
			, modalService
			){
				
				// Initializes the template service which add any templates defined in it.
				var ts = templateService;
				
				angular.element( $window ).bind
					( "orientationchange load"
					, function (){
						  hideAddressBar()

						}
					, false
					)

			function hideAddressBar(){
				$window.scrollTo(0, 1)
			}


			$rootScope.init = function () {
				hideAddressBar()
			};

				if(clientInfoService.orientation == 'landscape'){
					angular.element( document.getElementById('main-wrapper') )
						.addClass('land-scape-view')
				}
				
				$rootScope.pendingRequests = []
				$rootScope.showingModal = false;
				$rootScope.addressTypes = enumsService.addressTypes
				$rootScope.buttonFlags = enumsService.buttonFlags
				$rootScope.cartMessages = enumsService.cartMessages
				$rootScope.routes =  enumsService.routes
				$rootScope.services = enumsService.services
				$rootScope.socket = enumsService.socket
				$rootScope.status = enumsService.status
				$rootScope.i18n = languageService
				$rootScope.develop = false

				if( ($location.$$host != 'store.accesso.com' && $location.$$host != 'stg-store.accesso.com') && !$location.$$search.d){
					$rootScope.develop = true
				} else if(!$location.$$search.d){
					$rootScope.develop = false
				} else {
					$rootScope.develop = eval($location.$$search.d)
				}
				
				loggingService.enabled = $rootScope.develop
				$rootScope.developMenu = $rootScope.develop
				$rootScope.errorData = {}

				$rootScope.autofill = false
				if( ($location.$$host != 'store.accesso.com' && $location.$$host != 'stg-store.accesso.com') && !$location.$$search.a){
					$rootScope.autofill = true
				} else if(!$location.$$search.a){
					$rootScope.autofill = false
				} else {
					$rootScope.autofill = eval($location.$$search.a)
				}

				$rootScope.appData = appDataService
				$rootScope.routeParams = $routeParams

				$rootScope.hammer_event = true

				$rootScope.clientInfo = clientInfoService
				$rootScope.parkName = ''
				$rootScope.parkLogo = ''
				$rootScope.assetPath = ''
				$rootScope.currencyCode = ''
				$rootScope.displayAccessoBranding = true
				$rootScope.displayKnowledgeBase = true
				$rootScope.displayPromoCode = true

				$rootScope.state = {}
				$rootScope.state.loaded = false
				$rootScope.state.showMainMenu = false
				$rootScope.state.showCrossSells = true
				$rootScope.state.cart = cartService
				$rootScope.state.loadingMessage = ''
				$rootScope.state.debugMenu = false
				$rootScope.state.loadingEllipse = ''
				$rootScope.state.parkName = ''
				$rootScope.state.parkNameTitleTag = ''
				$rootScope.state.parkLogo = ''
				
				$rootScope.$on
					(
					  'log'
					, function(event,logLine){
							$http.post
								( '/debug'
								, 	{ level: logLine.type
									, msg: logLine.message
									, uuid: clientInfoService.uuid
									, ua: clientInfoService.userAgent
									}
								)
						}
					)
				
				socketService.onmessage(
					function(data){
						// TODO: This is a work in progress. Please do not delete.
						// data.data.error_code = 1000
						// Check for error codes. All ECs are abstracted here.
						// Display error_code if it exists and do not propagate message.
						// Hanldle errors codes based on requestTypes
						var msg = data.data
						if( msg.error_code && msg.error_code != '' ){
							var title = ''
							  , errorMsg = ''
							switch( msg.request_type ){
								case $rootScope.services.GET_CART_SUMMARY:
								case $rootScope.services.PURCHASE:
								case $rootScope.services.UPDATE_CART_ITEMS:
								case $rootScope.services.UPDATE_CART_CHECKOUT_INFO:
									//$rootScope.i18n.displayErrorCode(data, 'CheckCartTicketLevelErrors');
									break;

								default:
									//$rootScope.i18n.displayErrorCode(data);
									break;
							}

							// Stop execution.
							// return;
						}

						if(msg.request_type !== undefined){
							$rootScope.$broadcast
								( $rootScope.socket.receive + msg.request_type
								, msg
								)
						}
					}
				)

				$rootScope.removePendingRequest = function(name){
					for(var i = 0; i < $rootScope.pendingRequests.length; i++){
						if($rootScope.pendingRequests[i] == name){
							$rootScope.pendingRequests.splice(i, 1)
							break
						}
					}
				}

				$rootScope.resetApp = function(){
					localStorage.clear();
					appDataService.initializeData( true );
					url = '/' + utilitiesService.getMerchant() + '/';
					if(window.location.search.length > 0){
						url += window.location.search;
					}
					$window.location.href = url;
				}

				$rootScope.showSpinner = function(message){
					if( $rootScope.offline ){
						// Overwrite message if it's offline. This will keep any other showSpinner function calls 
						// from overwritting the offline message.
						$rootScope.state.loadingMessage = $rootScope.offlineMessage;
					} else {
						$rootScope.state.loadingMessage = message
					}
					
					if(!$rootScope.state.loaded)
						return

					// Shows dots under the loading message.
					$rootScope.loadingEllipseTimer = function(){
						$rootScope.state.loadingEllipse += '.'
						if(typeof $rootScope.state.loadingMessage != 'undefined'){
							if( $rootScope.state.loadingEllipse.length >= $rootScope.state.loadingMessage.length + 1 ){
								$rootScope.state.loadingEllipse = ''
							}
						}
						if($rootScope.loadingEllipseTimer){
							loadingEllipseTimer = $timeout($rootScope.loadingEllipseTimer, 100)
						}
					}

					loadingEllipseTimer = $timeout($rootScope.loadingEllipseTimer, 100)
					$rootScope.state.loaded = false

					try{
						if(!$rootScope.$$phase){
							$rootScope.$apply()
						}
					} catch(e){}
				}

				$rootScope.hideSpinner = function(){
					if($rootScope.offline){
						return;
					}
					
					$rootScope.state.loaded = true
					$rootScope.loadingEllipseTimer = null
					$rootScope.state.loadingEllipse = ''

					try{
						if(!$rootScope.$$phase){
							$rootScope.$apply()
						}
					}catch(e){}
				}
				
				/**
				 * Global function for alerting user nothing is in her cart.
				 * @return {boolean} Returns true if there are items false if not.
				 */
				$rootScope.hasItemsInCart = function(){
					if( !cartService.totalQty && !$rootScope.state.cart.totalQty ){
						// Nothing in the cart. Alert user.
						modalService.displayModal
							(
								{ title: $rootScope.i18n.Alert.noItemsTitle
								, description: $rootScope.i18n.Alert.noItemsMsg
								, hideAllButtons: false
								}
							);

						return false;
					}

					return true;
				}

				$rootScope.routeURL = function(url){
					window.open(url,url)
				}
				
				$rootScope.routeTo = function(page, menu){
					utilitiesService.routeTo(page, menu)
				}

				/**
				 * ## Global modal view handler.
				 *
				 * showModal receives option object that specifies behavior.
				 *
				 * Examples:
				 *
				 *     $rootScope.$broadcast(enums.send, somestring)
				 *     $rootScope.$broadcast(enums.send, {'somekey':'somevalue'})
				 *
				 */
				// @TODO: Convert to directive.
				// @TODO: Implement custom templates.

				function swapParkTitle(view){
					// TODO: if TicketView or ReceiptView hide park logo and change
					// park name
					if( false ){
						//RECEIPT
						//TICKET
					} else {
						$rootScope.state.parkName = $rootScope.parkName
						// The following three versions are added to ensure we catch
						// any variation of it.
						$rootScope.state.parkNameTitleTag =
							$rootScope.parkName.split('<br>').join('')
						$rootScope.state.parkNameTitleTag =
							$rootScope.state.parkNameTitleTag.split('<br/>').join('')
						$rootScope.state.parkNameTitleTag =
							$rootScope.state.parkNameTitleTag.split('<br />').join('')
						// TODO: check if the park logo exists
						//appDataService.assets.url
						if( typeof $rootScope.parkLogo == 'string' && $rootScope.parkLogo != '' ){
							$rootScope.state.parkLogo = $rootScope.parkLogo
						} else {
							$rootScope.state.parkLogo = ''
						}

						$rootScope.state.parkLogo = $rootScope.parkLogo
					}
				}

				$rootScope.$on
					( '$routeChangeSuccess'
					, function(angularEvent, current, previous){
							swapParkTitle('view');
						}
					);

				//	Re-Initialize Socket in case of disconnection.
				$rootScope.$on
					( enumsService.socket.open
					, function( data ){
							if( socketService.timesOpened > 1 && socketService.timesOpened <= $rootScope.maxReconnectAttempts ){
								if( window.location.pathname.indexOf( enumsService.routes.ERROR ) == -1 ){
									// The socketService will auto connect to the system,
									// wait one second before we reinitialize.
									setTimeout(
										function(){
											appDataService.initializeData( true );
											if( $rootScope.showingModal ){
												$rootScope.modal.show = true;
											}
											
											// Reset the number of reconnectAttempts after a connection was established.
											socketService.reconnectAttempts = 0;
											socketService.numOpenAttempts = 0;
											$rootScope.hideSpinner();
										}
										, 3000
									);
								}
							}
						}
					);

				$rootScope.maxReconnectAttempts = 10;
				
				$rootScope.$on
					( enumsService.socket.close
					, function(data){
						if( socketService.timesOpened > 0 ){
							var msg = ''
							if( $rootScope.pendingRequests.length ){
								msg = $rootScope.i18n.ApplicationLabels.offlinePreResponseMsg;
								for( var i = 0; i < $rootScope.pendingRequests.length; i++ ){
									if( $rootScope.pendingRequests[i] == $rootScope.services.PURCHASE ){
										$rootScope.showingPurchaseDiconnectError = true;
										msg += '<br /><br />' + $rootScope.i18n.ApplicationLabels.offlinePrePurchaseResponse;
										break;
									}
								}

								msg += '<br /><br />' + $rootScope.i18n.ApplicationLabels.offlineRefreshMsg;

								if($rootScope.showingModal){
									$rootScope.modal.show = false
								}
								
								modalService.displayModal
									(
										{ title: $rootScope.i18n.ApplicationLabels.offlineTitle
										, description: msg
										, callback: function(){
											$rootScope.showingPurchaseDiconnectError = false;
											if( !socketService.open ){
												$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.connectionLost );
											}
										  }
										}
									)

							}

							$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.connectionLost );
						}
					  });
				
				//TODO: Refactor as the routes.js object
				appDataService.fetch($rootScope.services.VIEW_ORDER,true).then(function(data){
					$rootScope.state.parkName = (((data).VIEW_ORDER_REPLY || {}).ORDER_STATUS_REPLY || {}).merchant_name
					if($rootScope.parkName != $rootScope.state.parkName){
						//$rootScope.state.showParkLogo = false
					}
				})
		
				appDataService.fetch($rootScope.services.GET_APPLICATION_CONSOLIDATED,true).then(function(data){
					appDataService.setData(data)
					languageService.setData(data)
					$rootScope.parkName = appDataService.merchantName
					$rootScope.assetPath = appDataService.assets.url
					if(typeof ((appDataService[$rootScope.services.GET_APPLICATION_CONSOLIDATED] || {}).SETTINGS || {}).merchant_logo == 'string' && ((appDataService[$rootScope.services.GET_APPLICATION_CONSOLIDATED] || {}).SETTINGS || {}).merchant_logo != ''){
						$rootScope.parkLogo = $rootScope.assetPath+((appDataService[$rootScope.services.GET_APPLICATION_CONSOLIDATED] || {}).SETTINGS || {}).merchant_logo
					}
		
									$rootScope.currencyCode = appDataService.currencyCode
		
									$rootScope.displayAccessoBranding =
										utilitiesService.getBoolean
											( ( ( appDataService
															[$rootScope.services.GET_APPLICATION_CONSOLIDATED]
														|| {}
													).SETTINGS || {}
												).display_accesso_branding
											)
		
									$rootScope.displayKnowledgeBase =
										utilitiesService.getBoolean
											( ( ( appDataService
															[$rootScope.services.GET_APPLICATION_CONSOLIDATED]
														|| {}
													).SETTINGS || {}
												).display_knowledge_base
											)
		
									$rootScope.displayDisclaimer =
										utilitiesService.getBoolean
											( ( ( appDataService
															[$rootScope.services.GET_APPLICATION_CONSOLIDATED]
														|| {}
													).SETTINGS || {}
												).display_disclaimer
											)
		
									//if the display_promo_code does not exist, then set to true
									if( ( ( appDataService
														[$rootScope.services.GET_APPLICATION_CONSOLIDATED]
													|| {}
													).SETTINGS || {}
											).display_disclaimer === undefined
										)
									{
										$rootScope.displayDisclaimer = false
									}
		
									$rootScope.displayPromoCode =
										utilitiesService.getBoolean
											( ( ( appDataService
															[$rootScope.services.GET_APPLICATION_CONSOLIDATED]
														|| {}
													).SETTINGS || {}
												).display_promo_code
											)
		
									//if the display_promo_code does not exist, then set to true
									if( ( ( appDataService
														[$rootScope.services.GET_APPLICATION_CONSOLIDATED]
													|| {}
													).SETTINGS || {}
											).display_promo_code === undefined
										)
									{
										$rootScope.displayPromoCode = true
									}
									if( ( ( appDataService
														[$rootScope.services.GET_APPLICATION_CONSOLIDATED]
													|| {}
												).SETTINGS || {}
											).display_knowledge_base === undefined
										)
									{
										$rootScope.displayKnowledgeBase = true
									}
		
									if($rootScope.state.parkName == ''){
										$rootScope.state.parkName = $rootScope.parkName
									}
									if($rootScope.state.parkLogo == ''){
										$rootScope.state.parkLogo = $rootScope.parkLogo
									}
									
									$rootScope.state.loadingMessage = ( $rootScope.offline ) ? $rootScope.offlineMessage : languageService.ApplicationLabels.loadingMsg;
									swapParkTitle('view')
								}
							)
			}
		]
	)
