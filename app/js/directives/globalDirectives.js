/**
 * # Global Directives
 *
 * Directives used in the application
 */

/**
 * Ensures text entered is numberical.
 */

angular.module('app').directive
('numbersOnly'
	, function () {
		var directive =
		{ link: function (scope, elm, attrs) {
			elm.bind
			('keyup'
				, function (event) {
					replacedValue = event.target.value.replace(/\D/g, '')
					if (event.target.value != replacedValue) {
						event.target.value = replacedValue
					}
					scope.$apply()
				}
			)
			elm.bind
			('keypress'
				, function (event) {
					event.keyCode = event.charCode || event.keyCode
					if
						(event.shiftKey						 // disallow Shift
						|| ( event.keyCode < 48			 // disallow non-numbers
						|| event.keyCode > 57
						)
						&& event.keyCode != 46			 // allow delete
						&& event.keyCode != 8				 // allow backspace
						&& event.keyCode != 9				 // allow not tab
						&& event.keyCode != 27			 // allow escape
						&& event.keyCode != 13			 // allow enter
						&& event.keyCode != 39			 // allow right arrow
						&& event.keyCode != 40			 // allow left arrow
						&& !( event.keyCode == 65		 // allow CTRL+A
						&& event.ctrlKey === true
						)
						&& !( event.keyCode == 67		 // allow CTRL+C
						&& event.ctrlKey === true
						)
						&& !( event.keyCode == 80		 // allow CTRL+P
						&& event.ctrlKey === true
						)
						) {
						event.preventDefault()
						scope.$apply()
					}
				}
			)
		}
		}
		return directive
	}
)

angular.module('app').directive
('onTap',['$window','utilitiesService'
	, function ($window, utilitiesService) {
		var directive =
		{
			link: function (scope, element) {
				element.bind('touchstart click dblclick', function () {
					element.css('opacity', '0.8')
					setTimeout(function () {
						element.css('opacity', '1')
					}, 250)

					// var object = utilitiesService.getObject('.focus')
					// var application = utilitiesService.getObject('#application-scrollContent')

					// if(object[0]){
					// 	try{
					// 		var offset = (object[0].offsetParent.offsetTop - 60);
					// 		application[0].scrollTop = offset;
					// 	} catch( e ){}
					// }
				});

			}
		}
		return directive
	}])

angular.module('app')
.directive
	('accessoFooter',[
		'$routeParams'
		, 'enumsService'
		, 'utilitiesService'
	    , '$log'
		, function (
		 $routeParams
		 , enumsService
		 , utilitiesService
		 , $log
		 ) {
			var directive =
				{ link: function (scope, element) {
					// @TODO: Year should come from server config.
					var d = new Date();
					var year = d.getFullYear();
					var copyright = scope.i18n.ApplicationLabels.copyright ? ' | ' + scope.i18n.ApplicationLabels.copyright : ''

					// @TODO: Path should come from server config.
					var footer = angular.element('<p class="accesso-footer">&copy; ' + year + ' accesso, LLC |' +
						' <a href=" '+ enumsService.routes.PRIVACY +'" target="_blank">' + scope.i18n.ApplicationLabels.privacyStatement + '</a> |'
						+ ' <a href="/' + $routeParams.merchant + '/'+ enumsService.routes.KNOWLEDGEBASE +'">' + scope.i18n.ApplicationLabels.help + '</a>'
						+ copyright + '</p>');

					element.append(footer)

					var container  = utilitiesService.getObject('.application-body-container-package-list');
					var bodyContainer  = utilitiesService.getObject('.body-container-no-padding-package-list');

					try{
						if(footer[0].clientHeight > 40 && container && bodyContainer){
							container.css({'margin-top':container[0].offsetTop - (footer[0].clientHeight - 40)+'px'});
							$log.log(footer[0].clientHeight)
							bodyContainer.css({'padding-top':(footer[0].clientHeight - 40)+'px'});
						}
					}
					catch(e){}
				}
			}

			return directive;
		}])

angular.module('app')
.directive
	('testdirective',[
		'$routeParams'
		, 'enumsService'
		, 'utilitiesService'
	    , '$log'
		, function (
		 $routeParams
		 , enumsService
		 , utilitiesService
		 , $log
		 ) {
			var directive =
				{ link: function (scope, element) {
					element.html('THIS IS FROM DIRECTIVE @@mollis + @@molli2')
				}
			}

			return directive;
		}])

angular.module('app')
	.directive('verifyImageSrc',['$timeout', '$compile', function($timeout, $compile) {
	var directive = {
		restrict: 'A'
		, link: function(scope, element, attrs) {
			$timeout(function(){
				var image = new Image();
				scope.$watch(function(){
					return attrs.verifyImageSrc
				}, function(verifyImageSrc){
					image.src = verifyImageSrc
				})
				image.onload = function() {
					element[0].src = attrs.verifyImageSrc
					element.removeClass('hide')
					element.addClass('show')
					try{
						if(typeof attrs.ngShow == 'string' && attrs.ngShow != ''){
							scope[attrs.ngShow] = true
							$compile(element.contents())(scope)
						}
					}catch(e){}
					try{
						if(!scope.$$phase){
							scope.$apply()
						}
					}catch(e){}
				}
				image.onerror = function() {
					element[0].src = ''
					element.removeClass('show')
					element.addClass('hide')
					try{
						if(typeof attrs.ngShow == 'string' && attrs.ngShow != ''){
							scope[attrs.ngShow] = false
							$compile(element.contents())(scope)
						}
					}catch(e){}
					try{
						if(!scope.$$phase){
							scope.$apply()
						}
					}catch(e){}
				}
			}, 0)
		}
	}
	return directive;
}])

angular.module('app')
	.directive
	( 'menuListIcon',function(){
			var directive =
			{
				link: function(scope, element, attrs){
				var menuListIcon = angular.element('<span class="menu-list-icon"></span>')
					scope.$watch(attrs.menuListIcon, function(value) {
						if(typeof value == 'undefined'){
							scope.$watch(attrs.menuListKeywords, function(value){
								element.parent().prepend(menuListIcon.addClass('icon-'+value.toLowerCase().replace(/ /g, '-')))
							})
						}else{
							element.parent().prepend(menuListIcon.addClass('icon-'+value.toLowerCase().replace(/ /g, '-')))
						}
					});

			}
			}

			return directive
		})

angular.module('app')
	.directive
( 'animate',function(){
	var directive =
	{
		link: function(scope, element, attrs){
			element.addClass('animate-element')

			if(scope.animationStart){
				element.addClass('animate-start')
				scope.animationStart = false

				scope.$on("$routeChangeSuccess", function(event){
					element.css({'-webkit-transform':'translateX(0%)'})
					element.bind("webkitTransitionEnd transitionend", function (){
						element.removeClass('animate-start');
						element.removeAttr('style');
					}, false)
				})
			}
		}
	}

	return directive
})

angular.module('app').directive
	('customAnimate',['$window','utilitiesService','$timeout'
		, function ($window, utilitiesService,$timeout) {
			var directive =
			{
				link: function (scope, element, attrs) {
					scope.animationStart = false

					scope.speed = 600;
					scope.animateOut = function(callback){
						scope.animationStart = true
						var animateFrame = utilitiesService.getObject('.animate-element')

						if(!animateFrame){
							var container = element.children()
							container.css({'-webkit-transition':scope.speed+'ms','-webkit-transform':'translateX(-100%)'})
						}

						// Ensure there are items in the cart before proceeding.
						$timeout(function(){
							callback()
						}, scope.speed)

					}

					scope.animateIn = function(callback){
						scope.animationStart = true

						var container = element.children()
						container.addClass('animate-start')

						// Ensure there are items in the cart before proceeding.
						$timeout(function(){
							callback()
							container.removeClass('animate-start');

						}, 600)

						scope.$on("$routeChangeSuccess", function(event){
//							var container = element.children()
//							container.css({'display':'none'});
//							container.addClass('animate-out');
//							$timeout(function(){
//								container.removeAttr('style');
//								$timeout(function(){
//									container.css({'-webkit-transform':'translateX(0%)'});
//								}, 600)
//							}, 150)
//
//							container.bind("webkitTransitionEnd transitionend", function (){
//								container.removeClass('animate-start');
//								container.removeClass('animate-out');
//								container.removeAttr('style');
//							}, false)

						})

					}
				}
			}
			return directive
		}])


angular.module('app').directive
( 'autofillMyLocation'
	, function($http, $routeParams){
	  var directive =
	  { require: 'ngModel'
		  , link: function(scope, elm, attrs, ctrl){
		  ctrl.$parsers.unshift(function(value){

			  if(typeof(value) != 'undefined' && value.length == 5 && angular.isNumber(Number(value))){
				  $http.get('http://zip.elevenbasetwo.com/?zip='+value).success(function(data) {
					  scope.Ziptastic = data
					  scope.$broadcast('ZIPTASTIC',data)
				 });
			  }
			  return value
		  })
	  }
	  }
	  return directive
  }
)

angular.module('app').directive
('webKitTransitions'
	, function () {
	 var directive =
	 {
		 link: function (scope, element) {
			 element.removeClass('preload');
		 }
	 }
	 return directive
 })

