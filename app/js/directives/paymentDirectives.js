/**
 * # Payment Directives
 *
 * Directives used in the payment/billing screens
 */
angular.module('app').directive
	( 'creditCardType'
	, function(){
			var directive =
				{ require: 'ngModel'
				, link: function(scope, elm, attrs, ctrl){
						ctrl.$parsers.unshift(function(value){
							// Disabled because CC field is being deselected when typing credit card.
							scope.payment.ccType =
								(/^5[1-5]/.test(value)) ? "mastercard"
								: (/^4/.test(value)) ? "visa"
								: (/^3[47]/.test(value)) ? 'amex'
								: (/^6011|65|64[4-9]|622(1(2[6-9]|[3-9]\d)|[2-8]\d{2}|9([01]\d|2[0-5]))/.test(value)) ? 'discover'
								: undefined
							ctrl.$setValidity('invalid',!!scope.payment.ccType)


							return value
						})
					}
				}
			return directive
			}
		)

angular.module('app').directive
	( 'expirationMonth',['$filter'
	, function($filter){
			var directive =
				{ require: 'ngModel'
				, link: function(scope, elm, attrs, ctrl){
						scope.$watch('[payment.month,payment.year]',function(value){
							ctrl.$setValidity('invalid',true)
							if (scope.payment && scope.payment.year == $filter('rightSubstring')(scope.currentYear, 2)
									 && scope.payment.month < $filter('leadingZeros')(scope.currentMonth, 2)
								 ) {
								ctrl.$setValidity('invalid', false)
							}
							return value
						},true)
					}
				}
			return directive
			}])
