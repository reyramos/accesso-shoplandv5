angular.module('app')
	.directive
('gesturesController',[
	'$timeout'
	,'$log'
	,'$window'
	,'$rootScope'
	,'utilitiesService'
	,function(
		$timeout
		,$log
		,$window
		,$rootScope
		, utilitiesService
		){
		var directive =
		{
			link:function(scope,element){
				//css3 elements
				var g = ["webkit","moz","o","ms"]
					, css = ['transition','perspective','transform']
				/* Set Navigation Width */
				var navigationWidth = 60
				/*Set the speed of the transition */
				$rootScope.speed = 250
				var drag_offset = 0;
				// Listen for orientation changes and load
				angular.element($window).bind("orientationchange resize",function(){
					if($window.orientation == 90 || $window.orientation == -90){
						utilitiesService.getObject('#main-wrapper').addClass('land-scape-view')
						if($window.location.href.indexOf("orderView") > -1){
							if(scope.ticketObject.switchPage == "tickets"){
								utilitiesService.getObject('#application-view').addClass('ticket-scope')
							}else{
								utilitiesService.getObject('#application-view').removeClass('ticket-scope')
							}
						}
					}else{
						utilitiesService.getObject('#main-wrapper').removeClass('land-scape-view')
						utilitiesService.getObject('#application-view').removeClass('ticket-scope')
					}
					utilitiesService.getObject('#application-view').removeAttr('style')
				},false)

				/**
				 *
				 * @param {object} object reference to the element by its ID or Class
				 * @param {integer} position position where to animate
				 * @param {integer} speed  to set animate class
				 * https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Using_CSS_transitions
				 * @param {String} timing  options are ease, linear, step-end, setps()
				 * @param {object} addCSS, add additional css element, JQUERY object
				 */
				scope.animate = function(object,position,speed,timing,addCSS){

					$log.log('ANIMATE');
					speed = speed === undefined ? false : (speed == true) ? $rootScope.speed : speed
					timing = timing === undefined ? 'linear' : (timing == false) ? 'linear' : timing
					//CSS3 animation
					var transition = {}, perspective = {}, transform = {}
					for(var i in g){
						transition['-' + g[i] + '-' + css[0]] = 'all ' + speed + 'ms ' + timing
						perspective['-' + g[i] + '-' + css[1]] = $window.innerWidth + 'px'
						transform['-' + g[i] + '-' + css[2]] = 'translateX(' + position + 'px)'

					}
					object.css(transition).css(perspective).css(transform)
					if(typeof addCSS == 'object' && !(addCSS === undefined))
						object.css(addCSS)

				}
				scope.previousTicket = function(){
					if($window.location.href.indexOf("orderView") > -1){
						if(scope.ticketObject.switchPage == "tickets"){
							$rootScope.previous()
						}
					}
				}


				/**
				 * Set the dragRight function with element ID or Class use JQuery naming convention
				 * @param name reference to the element by its ID or Class use Jquery's format.
				 * @param isMenu boolean, if this is set for menu view
				 */
				scope.dragRight = function(name){
					$window.event.gesture.stopPropagation();
					$window.event.gesture.preventDefault();

					var menuWidth = $window.innerWidth - navigationWidth
					var offSet = (arguments[1] == undefined || arguments[1] == false ? $window.innerWidth : menuWidth) - $window.Math.abs($window.event.gesture.deltaX)

					if(scope.state.showMainMenu == false && offSet > 0){
						if(drag_offset != $window.event.gesture.deltaX){
							drag_offset = $window.event.gesture.deltaX
							scope.animate(utilitiesService.getObject(name),drag_offset + 2)
						}
					}
				}
				scope.swipeRight = function(name){

					$window.event.gesture.stopPropagation();
					$window.event.gesture.preventDefault();

					scope.animate(utilitiesService.getObject(name),$window.innerWidth)

				}

			}
		}
		return directive
	}])


angular.module('app')
	.directive
('scrollWindow',[
	'$routeParams'
	,'enumsService'
	,'$window'
	,'$timeout'
	,function(
	 $routeParams
	 ,enumsService
	 ,$window
	 ,$timeout
	 ){
		var directive =
		{ link:function(scope,element){
			scope.coffeeTime =  function(){
				scope.flipMe = typeof scope.hammer_event == 'undefined'?true:scope.hammer_event
				scope.allowSwipe = false
				if($window.location.href.indexOf("orderView") > -1){
					scope.allowSwipe = (scope.ticketObject.switchPage == "tickets") ? true : false

				}

				event.stopPropagation()
				switch($window.event.gesture.direction){
					case 'up':
						break;
					case 'down':
//						if( scope.allowSwipe){
//							$window.event.stopPropagation();
//							$window.event.gesture.preventDefault();
//						}
						break;
					case 'left':
						if(scope.hammer_event && scope.allowSwipe && scope.flipMe){
							scope.next()
						}
						$window.event.gesture.stopDetect();
						break;
					case 'right':

						if(scope.hammer_event && scope.allowSwipe && scope.flipMe){
							scope.previous()
						}

						$window.event.gesture.stopDetect();
						break;
				}
			};
			}
		}

		return directive
	}])
