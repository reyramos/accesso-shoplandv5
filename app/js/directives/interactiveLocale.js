/**
 * # interactiveLocale Directives
 *
 * Directive that takes locale data (or any string) with HTML markup, compiles and renders it.
 * The purpose was specifically because (but not limited to) the fact that we can get a locale
 * like this 'I agree to the <a href="event:showTerms">terms and conditions</a>.' We need to handle
 * that interaction (i.e. showTerms). It's expected that scope defines showTerms()
 */

angular.module( 'app' )
.directive
(  'interactiveLocale'
, [   '$compile'
	, function( $compile ){
		return function( scope, elm, attrs ){
			scope.$watch
				( attrs.interactiveLocale
				, function( newValue, oldValue ){
						var val = newValue;

						if( val ){
							var i = val.indexOf( '<a href="event:' ) // This is the expected format and should not deviate or we should handle other expected formats.
							  , j
							  , funcName = ''
							  , newVal = '';

							if( i > -1 ){
								j = val.indexOf( '">', i + 6 );

								// Extract the function name.
								funcName = val.substring( i + 15, j );

								// Add Angular specific code.
								newVal = val.substring( 0, i ) + '<a data-ng-click="' + funcName + '()"' + val.substring( j + 1 );
							
								// Update the val string which will be used to set the HTML content of the element.
								val = newVal;
							}
						}

						elm.html( val );
						$compile( elm.contents() )( scope );
					}
				)
		}
	  }
  ]
)
