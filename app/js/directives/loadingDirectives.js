/**
 * # Loading Directives
 *
 * Controls loadinging screen based on state of loadingService.
 */

angular.module('app').directive
	( 'loading',[
		'$timeout'
		, 'spinnerService'
	, function
			( $timeout
			, spinnerService
			)
		{
			var directive =
				{ link: function(scope, element, attrs){
						scope.$watch
							( 'state.loaded'
							, function(){
									if(scope.state.loaded){
										spinnerService.stop()
									} else {
										spinnerService.spin()
										angular.element(element).append(spinnerService.el)
									}
								}
							, true
							)
					}
				}

			return directive
		}])
