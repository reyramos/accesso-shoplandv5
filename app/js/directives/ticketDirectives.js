/**
 * # Ticket Directives
 *
 * Generates barcodes for TicketView
 */

angular.module('app').directive
('barcode',[
	'clientInfoService', '$timeout'
	, function (clientInfoService, $timeout) {
		var directive =
		{
			link: function (scope, element, attrs) {
				// TODO: MAke this a config entry.
				var ticket =
				{ barcode: scope.ticket.barcode.toString(), barcodeType: 'code128', location: 'canvas' + (scope.$index)
				}

				function renderBarcode(barcode, barcodeType, location) {
					var validBarcode = false;
					var rotation = ['N', 'L', 'R', 'I'];
					var barcodeConfig = [
						{type: 'code128', name: 'Code 128', opts: 'includetext parsefnc', scale: 1, rotate: 0},
						{type: 'hibccode128', name: 'HIBC Code 128', opts: 'includetext', scale: 1, rotate: 0},

						{type: 'hibcmicropdf417', name: 'HIBC MicroPDF417', opts: '', scale: 2, rotate: 0},
						{type: 'hibcpdf417', name: 'HIBC PDF417', opts: '', scale: 1, rotate: 0},
						{type: 'micropdf417', name: 'MicroPDF417', opts: '', scale: 1, rotate: 0},
						{type: 'pdf417', name: 'PDF417', opts: '', scale: 2, rotate: 0},

						{type: 'azteccode', name: 'Aztec Code', opts: 'format=full', scale: 2, rotate: 0},

						{type: 'hibcqrcode', name: 'HIBC QR Code', opts: '', scale: 2, rotate: 0},
						{type: 'qrcode', name: 'QR Code', opts: 'eclevel=M', scale: 2, rotate: 0}
					];
					if (typeof barcodeType == 'string') {
						for (var i in barcodeConfig) {
							if (barcodeType == barcodeConfig[i].type) {
								var type = barcodeConfig[i].type;
								var opts = barcodeConfig[i].opts;
								var scale = barcodeConfig[i].scale;
								var rotate = rotation[barcodeConfig[i].rotate];

								var text = barcode.replace(/^\s+/, '').replace(/\s+$/, ''); // Barcode Value
								var altx = barcode.replace(/^\s+/, '').replace(/\s+$/, ''); // Barcode Text

								validBarcode = true;
								break;
							}
						}
					}
					if (validBarcode) {
						var bw = new Baren;
						// Convert the options to a dictionary object, so we can pass alttext with
						// spaces.
						var tmp = opts.split(' ');
						opts = {};
						for (var i = 0; i < tmp.length; i++) {
							if (!tmp[i]) {
								continue;
							}
							var eq = tmp[i].indexOf('=');
							if (eq == -1) {
								opts[tmp[i]] = bw.value(true);
							}
							else {
								opts[tmp[i].substr(0, eq)] = bw.value(tmp[i].substr(eq + 1));
							}
						}
						// Add the alternate text
						if (altx) {
							opts.alttext = bw.value(altx);
						}
						// Add any hard-coded options required to fix problems in the javascript
						// emulation.
						opts.inkspread = bw.value(0);
						if (Baren.needyoffset[type] && !opts.textxalign && !opts.textyalign && !opts.alttext && opts.textyoffset === undefined) {
							opts.textyoffset = bw.value(-10);
						}
						bw.bitmap(new Baren.Bitmap);
						var scl = parseInt(scale, 10) || 5;
						bw.scale(scl, scl);
						bw.push(text);
						bw.push(opts);
						try {
							bw.call(type);
							bw.bitmap().show(location, rotate);
							bw = null;
						}
						catch (e) {
						}
					}
					else {
						// TODO: Handle Invalid Barcode Info
					}
				}

				$timeout(function () {
					renderBarcode(ticket.barcode, ticket.barcodeType, ticket.location)
				}, 0)
			}
		}
		return directive
	}])


angular.module('app').directive
('viewTicket'
	, function () {
		var directive =
		{
			link: function (scope, element) {
				element.css('width', 100 / scope.tickets.length + '%')
			}
		}

		return directive
	}
)

angular.module('app').directive
('ticketSwipeContainer',[
	'$log'
	, '$timeout'
	, '$window'
	, '$rootScope'
	, function (
		$log
		, $timeout
		, $window
		, $rootScope
		) {
		var directive =
		{
			link: function (scope) {

				$rootScope.ticketContainer = angular.element(document.getElementById("ticket-container"))
				var g = ["webkit","moz","o","ms"]
					, css = ['transition','perspective','transform']

				$rootScope.previous = function () {
					if(!$rootScope.hammer_event)
						return false

					if (scope.ticketIndex > 0) {
						scope.ticketIndex = scope.ticketIndex - 1;
						animate(scope.ticketIndex)

					}
				}

				$rootScope.next = function () {

					if(!$rootScope.hammer_event)
						return false

					var request = scope.ticketIndex + 1;
					if (request < scope.tickets.length) {
						scope.ticketIndex = request;

						animate(request)
					} else {
						scope.ticketIndex = scope.tickets.length - 1;
					}
				}

				$rootScope.getTicket = function (index) {
					scope.ticketIndex = index
					scope.currentTicket = scope.ticketIndex + 1
					animate(index)
				}

				var animate = function (position) {
					var transition = {}
					for(var i in g){
						transition['-' + g[i] + '-' + css[0]] = 'all ' + '600ms ' + 'ease'
					}

					$rootScope.paginationPosition(position)

//					$rootScope.ticketContainer.css(transition)
					$rootScope.ticketContainer.css({left:"-" + 100 * position + "%"})
					scope.currentTicket = scope.ticketIndex + 1

				}

				$rootScope.paginationPosition = function(position){

					position = typeof position == 'undefined'?0:position

					//remove the previous class
					angular.element(document.getElementsByClassName('button-secondary-' + (scope.currentTicket - 1))).removeClass('button-secondary')
					//add class to next element
					angular.element(document.getElementsByClassName('button-secondary-' + position)).addClass('button-secondary')
				}

			}
		}
		return directive
	}])

angular.module('hmTouchEvents').directive
('ticketContainer'
	, function () {
		var directive =
		{
			link: function (scope) {

				if (scope.tickets) {
					angular.element(document.getElementById("ticket-container")).css({width: 100 * scope.tickets.length + '%'})
				}
			}
		}
		return directive
	}
)



angular.module('app').directive
('noNext'
	, function () {
	 var directive =
	 {
		 link: function (scope, element) {

			 element.bind('keydown focus', function(e) {
				 // if we pressed the tab

				 if (e.keyCode == 9 || e.which == 9) {
					 // prevent default tab action
					e.preventDefault();
					}
			 });

		 }
	 }

	 return directive
 }
)