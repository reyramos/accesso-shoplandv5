angular.module( 'app' )
.directive
(  'clearInputField'
, [   '$compile'
	, '$timeout'
	, function( $compile, $timeout ){
		var directive =
			{ require: 'ngModel'
			, link: function( scope, element, attrs, control ){
				// Wraps the input element with div and adds a div with close-button class.
				var wrapper = angular.element( '<div>' );
				var button = angular.element( '<span>' ).addClass( 'icon-remove-sign' );

				button.addClass( 'hide' );

				function mouseDownHandler(){
					// Reset the input field and the data stored for it.
					control.$setViewValue( '' );
					element.val( '' );

					// Hide the button.
					button.addClass( 'hide' );

					// Set focus back on the button.
					$timeout
						(  function(){
								try{
									element[ 0 ].focus()
								} catch( e ){}
							}
							, 100
						)

					// Apply the changes.
					scope.$apply();
				}

				function keyUpHandler(){
					// We don't want to show the clear button if there is no text. Wait for keyup.
					if( element.val() != '' ){
						// Unbind keyup event.
						try{
							element.unbind( 'keyup', keyUpHandler );
						} catch( e ){}

						// Show the button.
						button.removeClass( 'hide' );

						// Bind the button mousedown event.
						button.bind( 'mousedown', mouseDownHandler );
					}
				}

				function focusOut(){
					// Hide the button when focus is lost.
					button.addClass( 'hide' );

					// Unbind the mousedown event on the button.
					try{
						button.unbind( 'mousedown', mouseDownHandler );
					} catch( e ){}

					// Unbind the focus out event on the input field.
					try{
						element.unbind( 'focusout', focusOut );
					} catch( e ){}
				}

				element.bind( 'focusin', function(){
					// If focused in and there is text, show clear button. Otherwise, don't show it and just bind the other events.
					if( element.val() == '' ){
						element.bind( 'keyup', keyUpHandler );
					} else {
						try{
							element.unbind( 'keyup', keyUpHandler );
						} catch( e ){}

						button.removeClass( 'hide' );
						button.bind( 'mousedown', mouseDownHandler );
					}

					// Always bind focus out when focus is gained.
					element.bind( 'focusout', focusOut );
				} )

				element.wrap( wrapper );
				element.parent().append( button );
			  }
			}

		return directive;
	  }
  ]
)
