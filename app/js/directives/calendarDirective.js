angular.module( 'app' )
.directive
( 'calendarDirective'
, [   'packageService'
	, 'appDataService'
	, 'enumsService'
	, function
		( packageService
		, appDataService
		, enumsService
		){
			var dir =
				{ template:
					'<div'
						+ ' id="datePickerCtrl"'
						+ ' datepicker'
						+ ' ng-model="selectedDate"'
						+ ' show-weeks="showWeeks"'
						+ ' starting-day="1"'
						+ ' date-disabled="disabled( date, mode )"'
						+ ' min="minDate"'
						+ ' max="maxDate">'
					+'</div>'
				, scope:
					{ callbacks: '=callbacks'
					, calOptions: '=calOptions'
					, selectedDate: '=selectedDate'
					, minCapacity: '=minCapacity'
					, customerTypeList: '=customerTypeList'
					}
				, link: function( scope, elm, attrs ){
						// Inherits angularjs datepicker directive and ammends functionality below.
						
						// Determine if date or date time.
						// Determine whether to show the time limit message (config: enable_time_limit; locale: timeLimitMessage)
						// Determine if resources are needed and make GetResourceUsage request.
						// Fetch new date on month change.
						// Load new time on date change if applicable.
						// On date and time change, update the label.
						// Get merchant package dates.
						// Iterate through calendar month and set enable/disable dates.
						// Highlight first available date.
						// If applicable, highlight first available time.
						// On save() validate selection.
						// If valid, save model in packageService.
						// If invalid, alert user.
						// Execute packageService.next() when complete.
						
						// If options are not defined, chances are the user refreshed the page on the calendar.
						// Controller has not initialized it self and will emit all kinds of errors.
						if( !scope.calOptions ){
							return;
						}
						
						var  loadedDates
						   , eventID
						   , settings
						   , packageDetails
						   , firstRun = true
						   , fetching = false
						   , type = -1 // Date or Date/Time.
						   , requestMonth = -1 // Current month. Default or current month = -1.
						   , monthsRequested = []
						   , months = [ 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december' ];

						// Just a reference to the current date to avoid creating new Date() all the time.
						var todaysDate = new Date()
						  , todaysYear = todaysDate.getFullYear()
						  , todaysMonth = todaysDate.getMonth()
						  , todaysDay = todaysDate.getDate();
						
						/***-----------------------------------------------------------------------
						 * PUBLIC
						 *------------------------------------------------------------------------**/
						
						// TODO: This is for reservations. Look at this when working on that.
						// Show/hide time limit.
						var enableTimeLimit = ( ( scope.calOptions ) || {} ).enableTimeLimit;

						// Are resources needed? How many?
						var resourcesNeeded = scope.calOptions.resourcesNeeded;
						
						// Contains the list of times available.
						var timeList = []
						  , timeSelection = ''
						  , calData = null;
						
						// Don't show weeks in the calendar.
						scope.showWeeks = scope.calOptions.showWeeks;
						
						// Set the min and max dates. NOTE: These are based on users date and time setting.
						scope.minDate = new Date();
						scope.maxDate = new Date();

						// TODO: Change 12 to config value.
						scope.maxDate.setMonth( scope.maxDate.getMonth() + 12 );

						// Selected date.
						scope.selectedDate = null;
						
						var getCalData = scope.$watch
							( function(){
								return scope.$$childHead;
							  }
							, function( childHead ){
								if( !childHead ){
									return;
								}
								
								// Set a local reference to the calendar's data.
								calData = childHead;
								// Get a reference to datepicker move function. It will be overridden later.
								scope._move = calData.move;
								calData.move = scope.moveFunction;
								
								// Remove watch now that we have what we need.
								getCalData();
							  }
							);
						
						// Hijacked datePicker move function.
						scope.moveFunction = function( step ){
							if( step == -1 || step == 1 ){
								scope._move( step );
							}
							
							// Select a date in the middle of the selected month and use that for requesting new dates.
							var midDate = calData.rows[ 1 ][ 6 ].date;
							if( midDate ){
								scope.selectedMonthName = months[ midDate.getMonth() ] + ' ' + midDate.getFullYear();
								
								if( scope.selectedMonthName && !firstRun ){
									requestDateBySelectedMonth( scope.selectedMonthName );
								}
							}
						}
						
						// Watch changes in the selected date and month.
						scope.$watch
							( function(){
								return scope.selectedDate;
							  }
							, function( date ){
								// Select the first available time when the date changes.
								if( date )
									scope.calOptions.selectedDateString = gateDateStringByDate( date );
								
								setTimeList( getTimeListByDate( scope.selectedDate ) );
								
								if( timeList ){
									setTimeSelection( timeList[ 0 ] );
								}
							  }
							);
						
						// Watch changes in the request in the option object.
						scope.$watch
							( function(){
								return scope.calOptions.requestObj;
							  }
							, function( requestObj ){
								if( requestObj ){
									requestDates();
								}
							  }
							);
						
						scope.disabled = function( date, mode ){
							// Eanbles/disables days based on result from the server.

							// Return true = disabled
							// Return false = enabled
							var currentDateString 
							  , monthNameStr 
							  , loadedYearObject 
							  , loadedMonthObject;

							if( mode != 'day' ){
								return true;
							}

							if( !loadedDates ){
								return true;
							}
							
							// If the date is not in the month being viewed, then disable it.
							// User will have to navigate to the specific month in order to select a date there.
							if( scope.selectedMonthName ){
								if( getMonthNumberByName( scope.selectedMonthName.split( ' ' )[ 0 ].toLowerCase() ) + 1 != date.getMonth() + 1 ){
									return true;
								}
							}
							
							monthNameStr = months[ date.getMonth() ].toLowerCase();
							loadedYearObject = loadedDates[ 'year' + date.getFullYear() ];
							
							if( !loadedYearObject ){
								return true;
							}
							
							loadedMonthObject = loadedYearObject[ months[ date.getMonth() ] ];

							if( !loadedMonthObject ){
								return true;
							}

							// Enable the date if the date exists in loadedDates.
							currentDateString = gateDateStringByDate( date );
							if( loadedYearObject[ monthNameStr ] && loadedYearObject[ monthNameStr ][ currentDateString ] ){
								return false;
							}

							return true;
						}
						
						/***-----------------------------------------------------------------------
						 * PRIVATE
						 *------------------------------------------------------------------------**/

						function init(){
							// Just routed here. Look at the package details in packageService.
							// If it has the appropriate CHARACs set for this XFC, set the UI accordingly.
							// Gather details required for request.
							if( scope.calOptions.autoRequest ){
								packageDetails = packageService.packageDetails;

								if( packageDetails.E ){
									if( angular.isArray(packageDetails.E ) ){
										setType( packageDetails.E[ 0 ].date );
										setEventID( packageDetails.E[ 0 ].id );
									} else {
										setType( packageDetails.E.date );
										setEventID( packageDetails.E.id );
									}
								}
								
								requestDates();
							}
						}

						function requestDates(){
							// Verify the request was not previously made for this date.
							// If so, load the dates from memory.
							if( fetching ){
								return;
							}

							for( var i = 0; i < monthsRequested.length; i++ ){
								if( monthsRequested[ i ].month == requestMonth && monthsRequested[ i ].served ){
									return;
								}
							}
							
							// Remember the months requested.
							monthsRequested.push( { month: requestMonth, served: false } );
							
							var reqObj = {};
							if( scope.calOptions.requestObj != null ){
								reqObj = scope.calOptions.requestObj;
								reqObj.month = requestMonth;
								reqObj.end_date = ( requestMonth > -1 ) ? requestMonth + 1 : todaysMonth + 2;
							} else {
								reqObj.minCapacity = scope.minCapacity;
								reqObj.customerType = scope.customerTypeList;
								reqObj.version = '2'; // TODO: Hardcoded. Verify this is OK.
								reqObj.eventID = eventID;
								reqObj.packageID = packageService.packageDetails.id;
								reqObj.extraMovie = packageService.settings.name;
								reqObj.month = requestMonth;
								reqObj.end_date = ( requestMonth > -1 ) ? requestMonth + 1 : todaysMonth + 2;
							}

							// Notify controller we're making request.
							cb( 'gettingDates' );

							fetching = true;
							appDataService.fetch
								( enumsService.services.GET_MERCHANT_PACKAGE_EVENT_DATES
								, false
								, reqObj
								).then
									( function(data){
										// Notify controller we've got dates.
										cb( 'gotDates', data );

										// Update the firstRun flag.
										firstRun = false;
										if( data.status == enumsService.status.OK ){
											// Great! We have dates. Update UI.
											for( var i = 0; i < monthsRequested.length; i++ ){
												if( monthsRequested[ i ].month == requestMonth && !monthsRequested[ i ].served ){
													// Ensure the UI is only updated once. I noticed the response was handled more than once.
													monthsRequested[ i ].served = true;
													cleanData( data );
													updateUI( data );
													cb( 'cleanedData', data );
												}
											}
										} else {
											// No dates.
											scope.selectedDate = null;
											setTimeSelection( null );
											setTimeList( [] );
										}
										
										// If there is not at least one date, show the message.
										cb( 'showOrHideNoDateMsg', ( !data.D || data.D.length == 0 ) );
										fetching = false;
									  }
									)
						}

						function cleanData( data ){
							if( !angular.isArray( data.D ) ){
								data.D = [ data.D ];
							}

							for( var i = 0; i < data.D.length; i++ ){
								if( !angular.isArray( data.D[ i ].T ) ){
									data.D[ i ].T = [ data.D[ i ].T ];
									
									// Resources are needed. Ensure we have minimum availability.
									if( scope.calOptions.resourcesNeeded ){
										var dateObj = data.D[ i ].T[ 0 ];
										if( Number( dateObj.available ) < resourcesNeeded ){
											data.D.splice( i, 1 );
											i--;
										}
									}
								}
							}
						}

						function updateUI( data ){
							/* Structure of loadedDates.
							var loadedDates =
							{ 
								year2013:
								{
									july:
									{
										...
									}
								}
							}
							*/

							// Store a reference to the loaded dates. This will allow us to cache the dates to avoid service requests.
							// Note: We don't want to store the dates in localStorage. Dates should pull everytime this controller is initialized.
							var i
							  , day
							  , week
							  , month
							  , year
							  , currentDay
							  , monthsObj
							  , loadedYearObject
							  , loadedMonthObject
							  , monthNameStr
							  , currentDateString
							  , childHead = calData; // Reference to scope in datePicker directive. Probably not the right way to do this.
							
							scope.selectedDate = null;
							for( i = 0; i < data.D.length; i++ ){
								year = data.D[ i ].date.substring( 0, 4 );
								month = Number( data.D[ i ].date.substring( 5, 7 ) );
								day = data.D[ i ].date.substring( 8, 10 );

								// Initialize the main object.
								loadedDates = loadedDates || {};
								
								// Initialize the year object.
								loadedDates[ 'year' + year ] = loadedDates[ 'year' + year ] || {};

								// Initialize the month object.
								loadedDates[ 'year' + year ][ months[ month - 1 ] ] = loadedDates[ 'year' + year ][ months[month - 1 ] ] || {};

								// Get a reference to the month object in loadedDates.
								monthsObj = loadedDates[ 'year' + year ][ months[ month - 1 ] ];
								
								// Store the date as a string right from the service.
								monthsObj[ data.D[ i ].date ] = data.D[ i ];

								// Store a reference to the requested month.
								monthsObj[ data.D[ i ].date ].requestMonth = requestMonth;

								// Store the date as an object as well.
								monthsObj[ data.D[ i ].date ].dateObj = new Date( year, month - 1, day );
								if( !scope.selectedDate )
									scope.selectedDate = new Date( year, month - 1, day );
							}
							
							cb( 'calendarDataUpdated', loadedDates );
							
							// Select the first available date and time.
							selectFirstAvailableDateTime( monthsObj );
						}
						
						function getFirstAvailableDate( monthsObj ){
							// Iterate through all the days in the month and select the earliest date.
							// Return null if there is no reference.
							if( !monthsObj ){
								return null;
							}

							var day = 99999999999 // Start off with a really high number as we will be getting the lesser.
							  , nextDay;
							
							for( var key in monthsObj ){
								if( !key || key == undefined ){
									continue;
								}

								nextDay = Number( key.split( '-' ).join( '' ) );
								day = ( day < nextDay ) ? day : nextDay;
							}

							day = String( day );
							day = day.substring( 0, 4 ) + '-' + day.substring( 4, 6 ) + '-' + day.substring( 6, 8 );
							
							return day;
						}

						function getTimeListByDate( date ){
							if( !loadedDates || !date ){
								return;
							}
							
							var tl
							  , monthsObj = loadedDates[ 'year' + date.getFullYear() ][ months[ date.getMonth() ] ];
							
							try{
								tl = monthsObj[ gateDateStringByDate( date ) ].T;
							} catch( e ){
								tl = [];
							}
							
							return tl;
						}
						
						function gateDateStringByDate( date ){
							var dayStr
							  , monthStr
							  , currentDateString;
							
							dayStr = String( date.getDate() );
							dayStr = ( dayStr.length == 1 ) ? String( '0' + dayStr ) : dayStr;
							
							monthStr = String( date.getMonth() + 1 );
							monthStr = ( monthStr.length == 1 ) ? String( '0' + monthStr ) : monthStr;

							currentDateString = date.getFullYear() + '-' + monthStr + '-' + dayStr;
							
							return currentDateString;
						}

						function requestDateBySelectedMonth( title ){
							var titleArr = title.toLowerCase().split( ' ' );
							var monthName = titleArr[ 0 ]; // Ex. june
							var year = titleArr[ 1 ]; // Ex. 2013
							
							var monthIndex = getMonthNumberByName( monthName );
							
							// Determine the requestMonth value based on the month and year.
							if( monthIndex > -1 ){
								requestMonth = datesPreviouslyLoaded( monthIndex, year );
								
								if( requestMonth != -1 && requestMonth != -2 && requestMonth >= todaysMonth + 1 ){
									requestDates();
								} else if( requestMonth == -2 ){
									// The selected month was previously loaded.
									// Don't do anything here - but don't remove this condition.
								} else {
									// The selected month is earlier than the current month.
									scope.selectedDate = null;
									setTimeSelection( null );
									setTimeList( [] );
								}
							}
						}
						
						function getMonthNumberByName( monthName ){
							for( var i = 0; i < months.length; i++ ){
								if( months[ i ].toLowerCase() == monthName ){
									return i;
								}
							}
							
							return -1;
						}
						
						function datesPreviouslyLoaded( monthIndex, year ){
							// Check if the requestedMonth has been previously requested.
							// Ex: Today is 06/19/2013 
							// requestedMonth = -1 returns the first available month.
							// Assuming first availbale month is 06/2013, to load July/2013 requestedMonth should be 7; August/2-13 8, etc.
							// Following same assumption, to load 01/2014 requestedMonth should be 13, 02/2014 14, etc.
							// Determine what the month should be and see if it exist in the collection. 
							
							var cy = todaysYear;
							var dm = monthIndex + 1; // The month displayed on the calendar. dts.displayedMonth
							var dy = year; // The year displayed on the calendar. dts.displayedYear
							var cm = todaysMonth + 1;
							var requestedMonth = ( ( dy - cy ) * 12 ) + dm;
							
							if( !loadedDates ){
								return requestedMonth;
							}
							
							// If the current year is in loadedDates.
							if( loadedDates[ 'year' + dy ] ){
								// If the display month is in loadedDates[ displayYear ]
								if( loadedDates[ 'year' + dy ][ months[ monthIndex ] ] ){
									// Data was previously loaded for this month.
									selectFirstAvailableDateTime( loadedDates[ 'year' + dy ][ months[ monthIndex ] ] );
									
									// Count at least one date in the selected year-month.
									var c = 0;
									for( var key in loadedDates[ 'year' + dy ][ months[ monthIndex ] ] ){
										if( loadedDates[ 'year' + dy ][ months[ monthIndex ] ].hasOwnProperty( key ) ){
											c++;
											break;
										}
									}
									
									// If there is not at least one date, show the message.
									cb( 'showOrHideNoDateMsg', ( c == 0 ) );
									
									return -2;
								} else {
									// If there is not at least one date, show the message.
									cb( 'showOrHideNoDateMsg', true );
								}
							}
							
							return requestedMonth;
						}
						
						function isCurrentOrLaterMonth( requestedMonth ){
							// Returns tru if the requestedMonth is either the current or a later month.
							if( todaysMonth >= requestedMonth ){
								return true;
							} else if( requestedMonth == -1 ){
								return true;
							}
							
							return false;
						}
						
						function selectFirstAvailableDateTime( monthsObj ){
							var firstDay = getFirstAvailableDate( monthsObj );
							scope.selectedDate = ( monthsObj[ firstDay ] ) ? monthsObj[ firstDay ].dateObj : null;
							
							// List time options.
							if( scope.selectedDate ){
								setTimeList( monthsObj[ firstDay ].T );
								setTimeSelection(timeList[ 0 ] );
							} else {
								setTimeSelection( null );
								setTimeList( [] );
							}
						}
						
						// Setters
						function setTimeList( val ){
							if( timeList != val ){
								timeList = val;
								cb( 'timeListChanged', timeList );
							}
						}
						
						function setTimeSelection( val ){
							if( timeSelection != val ){
								timeSelection = val;
								cb( 'timeSelectionChanged', timeSelection );
							}
						}
						
						function setEventID( val ){
							if( eventID != val ){
								eventID = val;
								cb( 'eventIdChanged', eventID );
							}
						}
						
						function setType( val ){
							if( type != val ){
								type = val;
								cb( 'typeChanged', type );
							}
						}
						
						function cb( callback, data ){
							// Executes the callbacks defined in the controller
							// while safely failing if callback is not defined.
							try{
								scope.callbacks[ callback ]( data )
							} catch( e ){
								// Callback not defined in the controller.
							}
						}

						init()
					}
				}
			
			return dir;
		 }
  ]
)
