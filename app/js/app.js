/**
 * # Main application declaration file
 *
 * Allows main application to be declared. This seperate file is required in
 * order to properly isolate angular logic from requirejs module loading
 */
angular.module('app',['ngMobile','ngCookies','hmTouchEvents', 'logging', 'ui.bootstrap.datepicker'])
