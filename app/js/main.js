/**
 * # Module dependency and load-order manager
 *
 * Uses RequireJS to specify all modules used in the application, and what
 * their dependencies are. RequireJS uses this information to load all modules
 * in the correct order.
 *
 * Also informs RequireJS of all used files so it can compile/minify them for
 * production use
 */
require(
	{ paths:
		{ 'ua-parser': '../lib/ua-parser-js/src/ua-parser'
		, 'angular': '../lib/angular/index'
		, 'angular-mobile': '../lib/angular-mobile/index'
		, 'angular-cookies': '../lib/angular-cookies/index'
		, 'angular-resource': '../lib/angular-resource/index'
		, 'angular-hammer': 'modules/angular-hammer'
		, 'angular-datepicker': '../lib/angular-bootstrap/src/datepicker/datepicker'
		, 'sockjs': '../lib/sockjs/index'
		, 'hammer': '../lib/hammerjs/index'
		, 'spinjs': '../lib/spin.js/spin'
		, 'baren': '../lib/baren/baren'
		, 'renlinear': '../lib/baren/lib/render/renlinear'
		//, 'renmatrix': '../lib/baren/lib/render/renmatrix'
		//, 'renmaximatrix': '../lib/baren/lib/render/renmaximatrix'
		, 'code128': '../lib/baren/lib/barcodes/code128'
		//, 'hibccode128': '../lib/baren/lib/barcodes/hibccode128'
		//, 'hibcmicropdf417': '../lib/baren/lib/barcodes/hibcmicropdf417'
		//, 'hibcpdf417': '../lib/baren/lib/barcodes/hibcpdf417'
		//, 'micropdf417': '../lib/baren/lib/barcodes/micropdf417'
		//, 'pdf417': '../lib/baren/lib/barcodes/pdf417'
		//, 'azteccode': '../lib/baren/lib/barcodes/azteccode'
		//, 'hibcqrcode': '../lib/baren/lib/barcodes/hibcqrcode'
		//, 'qrcode': '../lib/baren/lib/barcodes/qrcode'
		}
	, shim:
		{ 'app':
				{ 'deps':
						[ 'angular'
						, 'angular-resource'
						, 'angular-mobile'
						, 'angular-cookies'
						, 'angular-hammer'
						, 'angular-datepicker'
						]
				}
		, 'angular-resource': { 'deps': ['angular'] }
		, 'angular-mobile': { 'deps': ['angular'] }
		, 'angular-cookies': { 'deps': ['angular'] }
		, 'angular-hammer': { 'deps': ['angular'] }
		, 'modules/logging': { 'deps': ['angular'] }
		, 'routes':
			{ 'deps':
				['app'
				 ,'services/storageService'
				 ]
			}
		, 'paths':
			{ 'deps':
				[ 'app'
				, 'services/utilitiesService'
				]
			}
		, 'bootstrap': { 'deps': ['app'] }
		, 'directives/ticketDirectives':
			{ 'deps':
				[ 'app'
				, 'baren'
				, 'code128'
				//, 'hibccode128'
				//, 'hibcmicropdf417'
				//, 'hibcpdf417'
				//, 'micropdf417'
				//, 'pdf417'
				//, 'azteccode'
				//, 'hibcqrcode'
				//, 'qrcode'
				]
			}
		, 'directives/loadingDirectives':
			{ 'deps':
				[ 'app'
				, 'services/spinnerService'
				]
			}
		, 'directives/globalDirectives':
			{ 'deps':
				[ 'app'
				, 'hammer'
				, 'angular-hammer'
				, 'services/utilitiesService'
				]
			}
		, 'directives/gesturesDirective':
			{ 'deps':
				[ 'app'
				, 'hammer'
				, 'angular-hammer'
				, 'services/utilitiesService'
				]
			}
		, 'directives/clearInputField':
			{ 'deps':
				[ 'app'
				]
			}
		, 'directives/interactiveLocale':
			{ 'deps':
				[ 'app'
				]
			}
		, 'directives/calendarDirective':
			{ 'deps':
				[ 'app'
				]
			}
		, 'filters/globalFilters': { 'deps': ['app'] }
		, 'filters/currencyFormatter':
			{ 'deps':
				[ 'app'
				]
			}
		, 'filters/telephoneNumber':
			{ 'deps':
				[ 'app'
				]
			}
		, 'directives/billingDirectives' : { 'deps': ['app'] }
		, 'directives/paymentDirectives' : { 'deps': ['app'] }
		, 'services/utilitiesService' : { 'deps': ['app'] }
		, 'services/storageService' :
			{ 'deps':
				[ 'app'
				 , 'services/utilitiesService'
				]
			}
		, 'services/socketService' : { 'deps': ['app','sockjs']}
		, 'services/enumsService' : { 'deps': ['app']}
		, 'services/modalService' :
			{ 'deps':
				[   'app'
				]
			}
		, 'services/templateService' : { 'deps': ['app']}
		, 'services/flashpassService' : { 'deps': ['app']}
		, 'services/extraFlowService' : { 'deps': ['app']}
		, 'services/cartService' :
			{ 'deps':
				[	'app'
				 	, 'services/connectionService'
					, 'services/enumsService'
					, 'services/socketService'
					, 'services/storageService'
					, 'services/utilitiesService'
					, 'services/modalService'
					, 'services/extraFlowService'
				]
			}
		, 'enums/countriesJSon' :
			{ 'deps':
				[	'app'
				]
			}
		, 'enums/iso4217' :
			{ 'deps':
				[	'app'
				]
			}
		, 'services/connectionService' :
			{ 'deps':
				[
				 	'app'
				 	,'sockjs'
				 	, 'services/enumsService'
				 	,'services/socketService'
					, 'services/utilitiesService'
				 ]
			}
			, 'services/appDataService' :
			{ 'deps':
				[
				 	'app'
				 	,'sockjs'
				 	,'services/enumsService'
				 	,'services/socketService'
				 	,'services/connectionService'
				 	,'services/storageService'
				 	,'services/clientInfoService'
					,'services/utilitiesService'
				]
			}

			, 'services/analyticService' :
			{ 'deps':
				[
				 	'app'
					, 'services/appDataService'
					, 'services/utilitiesService'
				 ]
			}
		, 'services/navigationService' : { 'deps': ['app']}
		, 'services/orderHistoryService' : { 'deps': ['app']}
		, 'services/merchantService' : {
			'deps':
				[	'app'
					, 'services/enumsService'
					, 'services/socketService'
					, 'services/storageService'
					, 'services/utilitiesService'
				]
			}

		, 'services/packageService' :
			{ 'deps':
				[	'app'
					, 'services/enumsService'
					, 'services/socketService'
					, 'services/storageService'
					, 'services/cartService'
					, 'services/utilitiesService'
					, 'services/modalService'
				]
			}
		, 'services/languageService' :
			{ 'deps':
				[ 'app',
				, 'services/enumsService'
				, 'services/socketService'
				, 'services/storageService'
				, 'services/appDataService'
				]
			}
		, 'services/clientInfoService' : { 'deps': ['app','ua-parser'] }
		, 'run' :
			{ 'deps':
				[ 'app'
				, 'paths'
				, 'services/utilitiesService'
				, 'services/analyticService'
				, 'services/storageService'
				, 'services/socketService'
				, 'services/languageService'
				, 'services/clientInfoService'
				, 'services/cartService'
				, 'services/merchantService'
				, 'directives/globalDirectives'
				, 'filters/globalFilters'
				, 'modules/logging'
				]
			}
		, 'services/spinnerService' :
			{ 'deps':
				[
				 	'app'
				 	, 'spinjs'
				 ]
			, 'init':
					function(app,spinnerService){
						return this.Spinner = spinnerService
					}
			}
		, 'services/userInfoService' :
			{ 'deps':
				[
					'app'
					, 'services/cartService'
					, 'services/appDataService'
					, 'services/enumsService'
					, 'services/utilitiesService'
					, 'services/storageService'
					, 'services/cartService'
				]
			}

		, 'controllers/MainMenuController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/navigationService'
				, 'services/utilitiesService'
				, 'directives/globalDirectives'
				]
			}
		, 'controllers/extraFlow/DateTimeController' :
			{ 'deps':
				[ 'app'
				, 'services/extraFlowService'
				, 'services/packageService'
				, 'services/utilitiesService'
				, 'directives/calendarDirective'
				]
			}
		, 'controllers/extraFlow/ReservationsController' :
			{ 'deps':
				[ 'app'
				, 'services/extraFlowService'
				, 'services/packageService'
				, 'services/utilitiesService'
				]
			}
		, 'controllers/ErrorPageController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/socketService'
				, 'services/languageService'
				, 'services/clientInfoService'
				, 'services/merchantService'
				, 'directives/globalDirectives'
				, 'filters/globalFilters'
				, 'services/utilitiesService'
				]
			}
		, 'controllers/FlashPassController' :
			{ 'deps':
				[ 'app'
				, 'services/enumsService'
				, 'services/flashpassService'
				]
			}
		, 'controllers/TicketController' :
			{ 'deps':
				[ 'app'
				, 'services/enumsService'
				, 'hammer'
				, 'services/utilitiesService'
				]
			}
		, 'controllers/OrderLookupController' :
			{ 'deps':
				[ 'app'
				, 'services/enumsService'
				, 'services/languageService'
				, 'services/orderHistoryService'
				, 'services/utilitiesService'
				, 'services/modalService'
				]
			}
		, 'controllers/BillingController' :
			{ 'deps':
				[ 'app'
				, 'filters/globalFilters'
				, 'services/enumsService'
				, 'services/languageService'
				, 'services/utilitiesService'
				, 'services/modalService'
				, 'services/userInfoService'
				]
			}
		, 'controllers/PaymentController' :
			{ 'deps':
				[ 'app'
				, 'filters/globalFilters'
				, 'services/enumsService'
				, 'services/languageService'
				, 'services/utilitiesService'
				, 'services/modalService'
				, 'services/userInfoService'
				]
			}
		, 'controllers/CartController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/cartService'
				, 'services/utilitiesService'
				, 'services/userInfoService'
				, 'services/extraFlowService'
				, 'services/modalService'

				]
			}
		, 'controllers/CrossSellsController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/cartService'
				, 'services/utilitiesService'
				]
			}
		, 'controllers/DebuggerController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/cartService'
				, 'services/utilitiesService'
				]
			}
		, 'controllers/KnowledgeBaseController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/clientInfoService'
				, 'services/utilitiesService'
				, 'services/modalService'
				]
			}
		, 'controllers/PackageDetailsController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/clientInfoService'
				, 'services/packageService'
				, 'services/cartService'
				, 'directives/globalDirectives'
				, 'services/utilitiesService'
				, 'services/modalService'
				]
			}
		, 'controllers/PackageListController' :
			{ 'deps':
				[ 'app'
				, 'filters/globalFilters'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/clientInfoService'
				, 'services/packageService'
				, 'services/utilitiesService'
				]
			}
		, 'controllers/SplashController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/utilitiesService'
				]
			}
		, 'controllers/ShippingController' :
			{ 'deps':
				[ 'app'
				, 'services/storageService'
				, 'services/enumsService'
				, 'services/utilitiesService'
				, 'services/userInfoService'
				, 'services/modalService'
				, 'services/userInfoService'

				]
			}
		//,'renmatrix': { 'deps': [ 'baren' ] }
		//,'renmaximatrix': { 'deps': [ 'baren' ] }
		,'code128': { 'deps': [ 'baren', 'renlinear' ] }
		//,'hibccode128': { 'deps': [ 'code128' ] }
		, 'angular-datepicker': { 'deps': ['angular'] }
		//,'micropdf417': { 'deps': [ 'renmatrix' ] }
		//,'hibcmicropdf417': { 'deps': [ 'micropdf417' ] }
		//,'pdf417': { 'deps': [ 'renmatrix' ] }
		//,'hibcpdf417': { 'deps': [ 'pdf417' ] }
		//,'azteccode': { 'deps': [ 'renmatrix' ] }
		//,'qrcode': { 'deps': [ 'renmatrix' ] }
		//,'hibcqrcode': { 'deps': [ 'qrcode' ] }
		}
	}
	, [ 'require'
		, 'filters/globalFilters'
		, 'routes'
		, 'paths'
		, 'modules/logging'
		, 'run'
		, 'directives/loadingDirectives'
		, 'directives/globalDirectives'
		, 'directives/gesturesDirective'
		, 'directives/ticketDirectives'
		, 'directives/billingDirectives'
		, 'directives/paymentDirectives'
		, 'directives/clearInputField'
		, 'directives/interactiveLocale'
		, 'directives/calendarDirective'
		, 'services/languageService'
		, 'services/enumsService'
		, 'services/connectionService'
		, 'services/analyticService'
		, 'services/appDataService'
		, 'services/clientInfoService'
		, 'services/cartService'
		, 'services/merchantService'
		, 'services/navigationService'
		, 'services/orderHistoryService'
		, 'services/packageService'
		, 'services/socketService'
		, 'services/storageService'
		, 'services/utilitiesService'
		, 'services/modalService'
		, 'services/templateService'
		, 'services/flashpassService'
		, 'services/userInfoService'
		, 'enums/countriesJSon'
		, 'enums/iso4217'
		, 'filters/currencyFormatter'
		, 'filters/telephoneNumber'
		, 'controllers/BillingController'
		, 'controllers/CartController'
		, 'controllers/CrossSellsController'
		, 'controllers/DebuggerController'
		, 'controllers/ErrorPageController'
		, 'controllers/KnowledgeBaseController'
		, 'controllers/PackageDetailsController'
		, 'controllers/PackageListController'
		, 'controllers/SplashController'
		, 'controllers/TicketController'
		, 'controllers/ShippingController'
		, 'controllers/MainMenuController'
		, 'controllers/OrderLookupController'
		, 'controllers/PaymentController'
		, 'controllers/FlashPassController'
		, 'controllers/extraFlow/DateTimeController'
		, 'controllers/extraFlow/ReservationsController'
		]
	, function(require) {
		return require(['bootstrap'])
	}
)
