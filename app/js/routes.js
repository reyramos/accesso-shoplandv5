/**
 * # User-facing URL Router
 *
 * Specified what URLs are available to a user, and what controllers should be
 * assigned to each.
 */
angular.module('app').config(['$routeProvider' , '$locationProvider', function( $routeProvider , $locationProvider ) {

	var unsupportedBrowser = false;

	var quickDebugRE = /(webkit)\/([\d\.]+)/ig
	var quickDebug   = quickDebugRE.exec(navigator.userAgent)

	if ( quickDebug && quickDebug[2] ) {
		var quickDebugVers = quickDebug[2].toString().split('.');
		if ( quickDebugVers[0] < 534 ) {
			unsupportedBrowser = true;
		}
	}
	// THE FOLLOWING IS FOR HTML5 CAPABLE BROWSERS.
	
	if ( !!(window.history && history.pushState) && unsupportedBrowser === false ) {
	
		$locationProvider.html5Mode(true);
		//$locationProvider.html5Mode(false);

//	=================================================================================
//	Merchant URL Matching Code
//	=================================================================================

		try {
			storageInfo = JSON.parse(window.localStorage.getItem("_initializeSocket"))
			storageMerchant = storageInfo.merchant
			urlMerchant = window.location.href.toString().split(window.location.host)[1].split('/')[1].split('?')[0]
			if( typeof storageMerchant != "undefined" && storageMerchant.toLowerCase() != urlMerchant.toLowerCase()) {
				window.localStorage.clear()
			} else {

			}
		} catch(e) {

		}
/**
 * ## Fetch Data
 *
 * A local dependency-injected proxy of appDataService.fetch in order to allow
 * for brevity in the scope of this file, which is loaded too early for
 * normal dependency injection to be possible
 *
 * This does also side-step the usual use of enumsService for enumerations
 * however enumsService is not directly accessible at this point in the
 * application.
 *
 * If the string provided to fetch does not match a key in enumsService
 * it will be passed in as undefined which will trigger an error in
 * the proxied fetch function
 *
 * @param {String} key Data source to request
 * @param {Boolean} useStorage set/get from localStorage
 * @param {Object} request JSON formatted request for server, If you set this to false, no request will be made.
 *
 *
 * @return {Object} an angular dependency wrapper around appDataService.fetch
 *
 */
		var _fetch = function(key, useStorage, request, force, useCookie, callbackOk, callbackFailed) {

			var wrapper =
				[ 'appDataService'
				, 'enumsService'
				, function(appDataService, enumsService) {
					var promise = appDataService.fetch(
							enumsService.services[key]
							, useStorage
							, request
							, force
							, useCookie
						).then(  
                            function (data) {
                                if ( typeof data.status != 'undefined' && data.status == 'OK' ) {
                                    if ( typeof callbackOk == 'function' ) {
                                        callbackOk(data);
                                    } 
                                } else {
                                    if ( typeof callbackFailed == 'function' ) {
                                        callbackFailed(data);
                                    }
                                }
                                return data;
                            }
                        )
						return promise;
					}
				]
			return wrapper;
		}

/**
 * ## _redirectToError
 *
 * Move
 * @param {Object} appDataService, access to fetch() and other functions.
 * @param {Object} enumsService, access to various elements used in the application.
 * @param {Object} response JSON from server.
 *
 * @return {Object} an angular dependency wrapper around appDataService.fetch
 */


		var _redirectToError = function ( errorNumber ) {
			var locationParts = location.pathname.split('/');
			window.location = '/' + locationParts[1] + '/error/' + errorNumber
			return false;
		}

		var initializeData = function() {

			var wrapper =
				[ 'socketService'
				, 'appDataService'
				, 'enumsService'
				, 'cartService'
				, function(
					  socketService
					, appDataService
					, enumsService
					, cartService
					) {
						return appDataService.initializeData();
					}
				];

			return wrapper;
		}

//	=================================================================================
//	Global Dependencies
//	=================================================================================

		var globalDependencies =
			{ 'initializeData': initializeData()
			, 'consolidatedData':
					_fetch( 'GET_APPLICATION_CONSOLIDATED', true, false, false, false, false, function() { _redirectToError(1001) } )
			, 'merchantDetailsData':
					_fetch( 'GET_MERCHANT_DETAILS',         true, false, false, false, false, function() { _redirectToError(1001) } )
			, 'merchantPackageListData':
					_fetch( 'GET_MERCHANT_PACKAGE_LIST',    true, false, false, false, false, function() { _redirectToError(1001) } )
			, 'merchantKeywordsData':
					_fetch( 'GET_MERCHANT_KEYWORDS',        true, false, false, false, false, function() { _redirectToError(1001) } )
			, 'getNewCartIdData':
					_fetch( 'GET_NEW_CART_ID',              true, false, false, true,  false, function() { _redirectToError(1001) } )
			}
		
		$routeProvider
			.when
			(	"/:merchant/splash"
			,	{ templateUrl: "/views/splash.html"
				, controller: "SplashController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/error/:errorCode"
			,	{ templateUrl: "/views/errorView.html"
				, controller: "ErrorPageController"
				}
			)
			.when
			(	"/error/:errorCode"
			,	{ templateUrl: "/views/errorView.html"
				, controller: "ErrorPageController"
				}
			)
			.when
			(	"/:merchant/packageList"
			,	{ templateUrl: "/views/packageList.html"
				, controller: "PackageListController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/packageList/keyword/:keyword"
			,	{ templateUrl: "/views/packageList.html"
				, controller: "PackageListController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/packageList/promocode/:promocode"
			,	{ templateUrl: "/views/packageList.html"
				, controller: "PackageListController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/packageList/crosssells/:crosssell"
			,	{ templateUrl: "/views/packageList.html"
				, controller: "PackageListController"
				, resolve: globalDependencies
				}
			)

			.when
			(	"/:merchant/packageDetails"
			,	{ templateUrl: "/views/packageDetails.html"
				, controller: "PackageDetailsController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/packageDetails/:packageId"
			,	{ templateUrl: "/views/packageDetails.html"
				, controller: "PackageDetailsController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/cartView"
			,	{ templateUrl: "/views/cartView.html"
				, controller: "CartController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/paymentMethod"
			,	{ templateUrl: "/views/paymentMethod.html"
				, controller: "PaymentController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/paypalCancelled"
			,	{ templateUrl: "/views/paypalCancelled.html"
				, controller: "CartController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/paypalComplete"
			,	{ templateUrl: "/views/paypalComplete.html"
				, controller: "CartController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/crossSells"
			,	{ templateUrl: "/views/crossSells.html"
				, controller: "CrossSellsController"
				, resolve: globalDependencies
				, resolve:
						angular.extend
							( { "cartSummaryData":
									_fetch
										( "GET_CART_SUMMARY"
										, true
										, false
										)
								}
							, globalDependencies
							)
				}
			)
			.when
			(	"/:merchant/shippingView"
			,	{ templateUrl: "/views/shippingView.html"
				, controller: "ShippingController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/billingView"
			,	{ templateUrl: "/views/billingView.html"
				, controller: "BillingController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/paymentView"
			,	{ templateUrl: "/views/paymentView.html"
				, controller: "PaymentController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/orderReview"
			,	{ templateUrl: "/views/cartView.html"
				, controller: "CartController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/receiptView/:lastFour/:phoneNumber/:orderId"
			,	{ templateUrl: "/views/receiptView.html"
				, controller: "TicketController"
				, resolve: globalDependencies
				, reloadOnSearch: false
				}
			)
			.when
			(	"/:merchant/receiptView"
			,	{ templateUrl: "/views/cartView.html"
				, controller: "TicketController"
				, resolve: globalDependencies
				, reloadOnSearch: false
				}
			)			
			.when
			(	"/:merchant/orderLookup"
			,	{ templateUrl: "/views/orderLookup.html"
				, controller: "OrderLookupController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/orderView"
			,	{ redirectTo: 
					function(var1, var2, var3) {
						// using this method to strip out the additional 
						return var1.merchant + '/orderLookup?err=badLink'
					} 
				}
/**
 * qwkejlqkwjel kqjwelk qlwe
 */



			)
			.when
			(	"/:merchant/orderView/:lastFour"
			,	{ redirectTo: 
					function(var1, var2, var3) {
						// using this method to strip out the additional 
						return var1.merchant + '/orderLookup?err=badLink'  
					} 
				}
			)
			.when
			(	"/:merchant/orderView/:lastFour/:phoneNumber"
			,	{ redirectTo: 
					function(var1, var2, var3) {
						return var1.merchant + '/orderLookup?err=badLink'  
					} 
				}
			)
			.when
			(	"/:merchant/orderView/:lastFour/:phoneNumber/:orderId"
			,	{ templateUrl: "/views/ticketSwitch.html"
				, controller: "TicketController"
				, resolve: globalDependencies
				, reloadOnSearch: false
				}
			)
			.when
			(	"/:merchant/orderView/:lastFour/:phoneNumber/:orderId/?tickets"
			,	{ templateUrl: "/views/ticketSwitch.html"
				, controller: "TicketController"
				, resolve: globalDependencies
				, reloadOnSearch: false
				}
			)
			.when
			(	"/:merchant/orderView/:lastFour/:phoneNumber/:orderId/?receipt"
			,	{ templateUrl: "/views/ticketSwitch.html"
				, controller: "TicketController"
				, resolve: globalDependencies
				, reloadOnSearch: false
				}
			)
			.when
			(	"/:merchant/orderView/:lastFour/:phoneNumber/:orderId?tickets"
			,	{ templateUrl: "/views/ticketSwitch.html"
				, controller: "TicketController"
				, resolve: globalDependencies
				, reloadOnSearch: false
				}
			)
			.when
			(	"/:merchant/orderView/:lastFour/:phoneNumber/:orderId?receipt"
			,	{ templateUrl: "/views/ticketSwitch.html"
				, controller: "TicketController"
				, resolve: globalDependencies
				, reloadOnSearch: false
				}
			)
			.when
			(	"/:merchant/orderLookup"
			,	{ templateUrl: "/views/orderLookup.html"
				//, controller: "OrderLookupController"
				, resolve: globalDependencies
				}
			)
			.when

			(	"/:merchant/orderHistory"
			,	{ templateUrl: "/views/orderHistory.html"
				, controller: "OrderLookupController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/knowledgeBaseView"
			,	{ templateUrl: "/views/knowledgeBaseView.html"
				, controller: "KnowledgeBaseController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/help"
			,{ templateUrl: "/views/help.html"
				 , controller: "KnowledgeBaseController"
				 , resolve: globalDependencies
				 }
			)
			.when
			(	"/:merchant/dateTime/:packageId"
			,	{ templateUrl: "/views/extraFlow/dateTime.html"
				, controller: "DateTimeController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/reservations/:packageId"
			,	{ templateUrl: "/views/extraFlow/reservations.html"
				, controller: "ReservationsController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/debug"
			,	{ templateUrl: "/views/_debugView.html"
				, controller: "DebuggerController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant"
			,	{ templateUrl: "/views/packageList.html"
				, controller: "PackageListController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/flashPassList/"
			,	{ templateUrl: "/views/flashPass/flashPassList.html"
				, controller: "FlashPassController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/flashPassList/:keyword"
			,	{ templateUrl: "/views/flashPass/flashPassList.html"
				, controller: "FlashPassController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/flashPass-step-1/:keyword"
			,	{ templateUrl: "/views/flashPass/flashPassStep1.html"
				, controller: "FlashPassController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/flashPass-step-2/:keyword"
			,	{ templateUrl: "/views/flashPass/flashPassStep2.html"
				, controller: "FlashPassController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/flashPass-step-3/:keyword"
			,	{ templateUrl: "/views/flashPass/flashPassStep3.html"
				, controller: "FlashPassController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/flashPass-step-4/:keyword"
			,	{ templateUrl: "/views/flashPass/flashPassStep4.html"
				, controller: "FlashPassController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/:merchant/flashPass-step-5/:keyword"
			,	{ templateUrl: "/views/flashPass/flashPassStep5.html"
				, controller: "FlashPassController"
				, resolve: globalDependencies
				}
			)
			.when
			(	"/"
			,	{ redirectTo: "/error/1004/" // #TODO correct error
				}
			)
			.when
			(	"/:merchant/:unknownPage"
			,	{ redirectTo: "/:merchant/error/1004/"
				}
			)
			.otherwise
			(
				"/:merchant/error/1004"
			)

			/**
			 * ## HTML5 pushState support
			 *
			 * This enables urls to be routed with HTML5 pushState so they appear in a
			 * '/someurl' format without a page refresh
			 *
			 * The server must support routing all urls to index.html as a catch-all for
			 * this to function properly,
			 *
			 * The alternative is to disable this which reverts to '#!/someurl'
			 * anchor-style urls.
			 */

	} else {
		// The following is for browsers without all the HTML5 features we need.
		alert('unsupported browser, setting html5Mode=false');
		$locationProvider.html5Mode(false);

		$routeProvider
			.when
			(	"/error/:errorCode"
			,	{ templateUrl: "/views/errorView.html"
				, controller: "ErrorPageController"
				}
			)
			.when
			(	"/"
			,	{ redirectTo: "/error/1000/" // #TODO correct error
				}
			)
			.otherwise
			(
				"/error/1000"
			)
	}

}]);
