/**
 * # Global string enumerations file
 *
 * Any strings that are used as a part of application logic should be defined
 * here, and their object key be referenced instead.
 *
 * This is especially useful in cases like $on event listeners where a typo in
 * the name would not ever error, but would simply never catch.
 *
 * By the extra work of defining enumerations for all strings and using those
 * enumerations avoids this by at least throwing undefined argument errors.
 *
 */

// @TODO: Move this to enums directory and rename to globalEnums.
angular.module('app').factory
	( 'enumsService'
	, function() {
			var enums = {
				socket:
					{ send: 'socket:send'
					, receive: 'socket:receive:'
					, DATA_INITIALIZED: 'dataInitialized'
					, open: 'socket:open'
					}
				, services:
					{ ADD_CART_ITEM: 'AddCartItem'
					, ADD_TICKET_CHARACTERISTIC: 'AddTicketCharacteristic'
					, CHECK_CART: 'CheckCart'
					, CONTINUATION: 'Continuation'
					, CREATE_DEPENDENT: 'CP_CreateDependent'
					, GUIDE_SERVICE: 'GuideService'
					, IDENTITY_VERIFICATION: 'IdentityVerification'
					, INCREMENT_READ_COUNT: 'IncrementReadCount'
					, INITIALIZE_SOCKET: '_initializeSocket'
					, LOG_IN: 'CP_Login'
					, MAKE_NEW_LOGIN: 'CP_CreateProfile'
					, PRINT_AT_HOME_ORDER: 'PrintAtHomeOrder'
					, PURCHASE: 'Purchase'
					, ORDER_SEARCH: 'OrderSearch'
					, REQUEST_EVENT_RESOURCE: 'RequestEventResource'
					, RELEASE_EVENT_RESOURCE: 'ReleaseEventResource'
					, SEARCH_ARTICLES: 'SearchArticles'
					, SEASON_PASS_NUMBER_SEARCH: 'SeasonPassNumberSearch'
					, SEASON_PASS_LOAD: 'SeasonPassLoad'
					, SET_EXPRESS_CHECKOUT: 'SetExpressCheckout'
					, VALIDATE_EMAIL: 'ValidateEmail'
					, VALIDATE_SEASON_PASS: 'ValidateSeasonPassID'
					, VERIFY_DEFERRED_PACKAGES: 'VerifyDeferredPackages'
					, VERIFY_B2B_LOGIN: 'verifyB2BUserLogin'
					, VIEW_ORDER: 'ViewOrder'
					, ADD_TICKET_CHARACTERISTIC: 'AddTicketCharacteristic'
					, UPDATE_CART_CHECKOUT_INFO: 'UpdateCartCheckoutInfo'
					, UPDATE_CART_ITEMS: 'UpdateCartItems'
					, UPDATE_DEPENDENT: 'CP_UpdateDependent'
					, UPDATE_LOGIN: 'CP_UpdateProfile'
					, GET_ACTIVATION_SEASON_PASS_INFO: 'GetActivationSeasonPassInfo'
					, GET_APPLICATION_CONSOLIDATED: 'GetApplicationConsolidated'
					, GET_APPLICATION_CONFIG: 'GetApplicationConfig'
					, GET_APPLICATION_CONFIG_SERVICE: 'GetApplicationConfigService'
					, GET_APPLICATION_NAVIGATION_TREE: 'GetApplicationNavigationTree'
					, GET_APPLICATION_LOCALE: 'GetApplicationLocale'
					, GET_ARTICLES_BY_DISPLAY_ORDER: 'GetArticlesByDisplayOrder'
					, GET_ARTICLES_BY_TOPIC: 'GetArticlesByTopic'
					, GET_CART_ID: 'GetCartID'
					, GET_CART_SUMMARY: 'GetCartSummary'
					, GET_EVENT_SEATS: 'GetEventSeats'
					, GET_EXPRESS_CHECKOUT_DETAILS: 'GetExpressCheckoutDetails'
					, GET_FORM: 'GetForm'
					, GET_GIFT_CARD_BALANCE: 'GetGiftCardBalance'
					, GET_GIFT_CODE_REMAINING_COUNT: 'GetGiftCodeRemainingCount'
					, GET_GROUP_CATEGORY: 'GetGroupCategory'
					, GET_GUIDES_BY_TOPIC: 'GetGuidesByTopic'
					, GET_KEYWORD_CALENDAR: 'GetKeywordCalendar'
					, GET_MERCHANT_DETAILS: 'GetMerchantDetails'
					, GET_MERCHANT_KEYWORDS: 'GetMerchantKeywords'
					, GET_MERCHANT_PACKAGE_DETAILS: 'GetMerchantPackageDetails'
					, GET_MERCHANT_PACKAGE_EVENT_DATES: 'GetMerchantPackageEventDates'
					, GET_MERCHANT_PACKAGE_LIST: 'GetMerchantPackageList'
					, GET_NEW_CART_ID: 'GetNewCartID'
					, GET_NOTHING: 'GetNothing'
					, GET_OFFERS: 'GetOffers'
					, GET_ORDER_ID_BY_CART_ID: 'GetOrderIDByCartID'
					, GET_PARK_HOURS: 'GetParkHours'
					, GET_PASSWORD: 'CP_LostPasswordEmail'
					, GET_PAYMENT_SCHEDULE: 'GetPaymentSchedule'
					, GET_PROMO_CODE: 'GetPromoCode'
					, GET_PROFILE: 'CP_GetProfile'
					, GET_REDEEM_OFFERS: 'RedeemOffer'
					, GET_RELATIONSHIP_CONSTANTS: 'CP_GetRelationshipConstants'
					, GET_RENEWAL_INFO: 'GetRenewalInfo'
					, GET_RENEWAL_SEARCH: 'GetRenewalSearch'
					, GET_RENEWAL_SEASON_PASS_INFO: 'GetRenewalSeasonPassInfo'
					, GET_RESOURCE_USAGE: 'GetResourceUsage'
					, GET_SEASON_PASS_COUNT: 'GetSeasonPassCount'
					, SET_PAYPAL_STATUS: 'SetPayPalStatus'
					}
				, routes:
					{ LIST: 'packageList'
					, LISTKEYWORD: 'packageList/keyword'
					, DETAILS: 'packageDetails'
					, CART: 'cartView'
					, SHIPPING: 'shippingView'
					, BILLING: 'billingView'
					, PAYMENT: 'paymentView'
					, ERROR: 'error'
					, REVIEW: 'orderReview'
					, RECEIPT: 'receiptView'
					, TICKET: 'ticketView'
					, ORDER_SEARCH: 'orderLookup'
					, ORDER_HISTORY: 'orderHistory'
					, ORDER_VIEW: 'orderView'
					, ORDER_REVIEW: 'orderReview'
					, KNOWLEDGEBASE: 'knowledgeBaseView'
					, UNKNOWN_PAGE : 'unknownPage'
					, UNKNOWN_MERCHANT : 'unknownMerchant'
					, PRIVACY : 'http://accesso.com/privacy-statement/'
					, PAYMENT_METHOD: 'paymentMethod'
					, PAYPAL_CHECKOUT : 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='
					, PAYPAL_MOBILE_CHECKOUT : 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout-mobile&token='
					, PAYPAL_SANDBOX_CHECKOUT : 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='
					, PAYPAL_SANDBOX_MOBILE_CHECKOUT : 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout-mobile&token='
					, PAYPAL_RETURN_URL : 'paypalComplete'
					, PAYPAL_CANCEL_URL : 'paypalCancelled'
					}
				, errorRoutes:
					{ UNSUPPORTED_BROWSER: '1000'
					, FAILED_TO_INIT: '1001'
					, MISSING_MERCHANT: '1002'
					, UNKNOWN_MERCHANT: '1003'
					, UNKNOWN_PAGE: '1004'
					}
				, buttonFlags:
					{ YES: '0'
					, NO: '10'
					, OK: '20'
					, CANCEL: '30'
					, I_AGREE: '40'
					, VERIFY: '50'
					, CONTINUE_SHOPPING: '60'
					, MAKE_CHANGES: '70'
					, YES_CONTINUE: '80'
					, PROCEED_TO_CHECKOUT: '90'
					, NONE: '100'
					, CLOSE: '200'
					}
				, status:
					{ FAILED: 'FAILED'
					, OK: 'OK'
					}
				, events:
					{ blur: 'blur'
					, change: 'change'
					, click: 'click'
					, contextmenu: 'contextmenu'
					, copy: 'copy'
					, cut: 'cut'
					, dblclick: 'dblclick'
					, error: 'error'
					, focus: 'focus'
					, focusin: 'focusin'
					, focusout: 'focusout'
					, hashchange: 'hashchange'
					, keydown: 'keydown'
					, keypress: 'keypress'
					, keyup: 'keyup'
					, load: 'load'
					, mousedown: 'mousedown'
					, mouseenter: 'mouseenter'
					, mouseleave: 'mouseleave'
					, mousemove: 'mousemove'
					, mouseout: 'mouseout'
					, mouseover: 'mouseover'
					, mouseup: 'mouseup'
					, mousewheel: 'mousewheel'
					, paste: 'paste'
					, reset: 'reset'
					, resize: 'resize'
					, scroll: 'scroll'
					, select: 'select'
					, submit: 'submit'
					, textinput: 'textinput'
					, unload: 'unload'
					, wheel: 'wheel'
					}
				, addressTypes:
					{ BILLING: 'B'
					, SHIPPING: 'S'
					}
				, deliveryMethods:
					{ CESTATION: 'A'
					, ENTERPRISE_SHIPPING: 'B'
					, CANADA_POST: 'C'
					, FIRST_CLASS_MAIL_AND_PRINT_AT_HOME: 'D'
					, ENTERPRISE_NO_SHIPPING: 'E'
					, FED_EX_OVERNIGHT: 'F'
					, UPS_EXPEDITED_AND_PRINT_N_GO: 'G'
					, WDW_AND_USO: 'H'
					, INTERNATIONAL_SHIPPING: 'I'
					, PRINT_AT_HOME_AND_USO: 'J'
					, UPS_GROUND: 'K'
					, UPS_EXPEDITED: 'L'
					, MOBILE: 'M'
					, UPS_GROUND_AND_PRINT_N_GO: 'N'
					, PRINT_AT_HOME_AND_WDW: 'O'
					, PRINT_N_GO: 'P'
					, PRINT_AT_HOME_AND_WDW_AND_USO: 'Q'
					, USO: 'R'
					, UPS_EXPEDITED: 'S'
					, WDW: 'T'
					, FIRST_CLASS_MAIL: 'U'
					, FIRST_CLASS_MAIL_AND_PRINT_N_GO: 'V'
					, NO_SHIPPING: 'W'
					, WILL_CALL: 'Y'
					, WILL_CALL_AND_PRINT_AT_HOME: 'Z'
					}
			}

			return enums
		}
	)



