/**
 * # Client Information Service
 *
 * Provides various details that describe the browser being used to render the
 * application as well as global variables specific to this instance needed to
 * properly render the correct templates/data
 *
 */
angular.module('app')
	.factory(
		'clientInfoService',[
			'$rootScope'
			, '$log'
			, '$location'
			, '$rootScope'
			, 'enumsService'
			, 'storageService'
		, function(
			$rootScope
			, $log
			, $location
			, $rootScope
			, enumsService
			, storageService
			){

			var socket = enumsService.socket
			var status = enumsService.status
			var services = enumsService.services

			var ua = window.navigator ? window.navigator.userAgent: window.request ? window.request.headers['user-agent']: 'No User Agent'
			var parser = new UAParser();
			var info =
			{ userAgent: ua
			, deviceType: // tv, tablet, desktop, mobile
					ua.match(/GoogleTV|SmartTV|Internet.TV|NetCast|NETTV|AppleTV|boxee|Kylo|Roku|DLNADOC|CE\-HTML/i) ? 'tv'
					: ua.match(/Xbox|PLAYSTATION.3|Wii/i) ? 'tv'
					: ua.match(/iPad/i) || ua.match(/tablet/i) && !ua.match(/RX-34/i) || ua.match(/FOLIO/i) ? 'tablet'
					: ua.match(/Linux/i) && ua.match(/Android/i) && !ua.match(/Fennec|mobi|HTC.Magic|HTCX06HT|Nexus.One|SC-02B|fone.945/i) ? 'tablet'
					: ua.match(/Kindle/i) || ua.match(/Mac.OS/i) && ua.match(/Silk/i) ? 'tablet'
					: ua.match(/GT-P10|SC-01C|SHW-M180S|SGH-T849|SCH-I800|SHW-M180L|SPH-P100|SGH-I987|zt180|HTC(.Flyer|\_Flyer)|Sprint.ATP51|ViewPad7|pandigital(sprnova|nova)|Ideos.S7|Dell.Streak.7|Advent.Vega|A101IT|A70BHT|MID7015|Next2|nook/i) || ua.match(/MB511/i) && ua.match(/RUTEM/i) ? 'tablet'
					: ua.match(/BOLT|Fennec|Iris|Maemo|Minimo|Mobi|mowser|NetFront|Novarra|Prism|RX-34|Skyfire|Tear|XV6875|XV6975|Google.Wireless.Transcoder/i) ? 'mobile'
					: ua.match(/Opera/i) && ua.match(/Windows.NT.5/i) && ua.match(/HTC|Xda|Mini|Vario|SAMSUNG\-GT\-i8000|SAMSUNG\-SGH\-i9/i) ? 'mobile'
					: ua.match(/Windows.(NT|XP|ME|9)/) && !ua.match(/Phone/i) || ua.match(/Win(9|.9|NT)/i) ? 'desktop'
					: ua.match(/Macintosh|PowerPC/i) && !ua.match(/Silk/i) ? 'desktop'
					: ua.match(/Linux/i) && ua.match(/X11/i) ? 'desktop'
					: ua.match(/Solaris|SunOS|BSD/i) ? 'desktop'
					: ua.match(/Bot|Crawler|Spider|Yahoo|ia_archiver|Covario-IDS|findlinks|DataparkSearch|larbin|Mediapartners-Google|NG-Search|Snappy|Teoma|Jeeves|TinEye/i) && !ua.match(/Mobile/i) ? 'desktop'
					: 'mobile'
			, browserName: parser.getBrowser().name
			, browserVersion: parser.getBrowser().version
			, osName: parser.getOS().name
			, osVersion: parser.getOS().version
			, engineName: parser.getEngine().name
			, engineVersion: parser.getEngine().version
			, deviceName: parser.getDevice().name
			, deviceVersion: parser.getDevice().version
			, viewport_size:
					function(){
						return { height: window.innerHeight, width: window.innerWidth }
					}()
			, document_size:
					function(){
						var height = Math.max
						( document.body.scrollHeight
						, document.body.offsetHeight
						, document.documentElement.clientHeight
						, document.documentElement.scrollHeight
						, document.documentElement.offsetHeight
						)
						var width = Math.max
						( document.body.scrollWidth
						, document.body.offsetWidth
						, document.documentElement.clientWidth
						, document.documentElement.scrollWidth
						, document.documentElement.offsetWidth
						)
						return { height: height, width: width }
					}()
			, orientation:
				function(){
						return window.innerHeight > window.innerWidth ? 'portrait'
						: (window.orientation || 1) == 0 ? 'portrait'
						: 'landscape'
				}()
			, software: 'ShopV5' // @TODO: Append mobile, tablet, or desktop.
			, animations: true
			, cartKey: storageService.get('cartId') || 0
			, cartId: storageService.get('cartKey') || 0
			, sessionId: storageService.get('sessionId') || 0
			, currentPage : ''
			, previousPage : ''
			, previousPages : []
			, uuid:
				function(){
					var uuid, cookies_enabled = false
					document.cookie = 'cookies_enabled=true'
					var cookies = document.cookie.split('; ')
					for (i = 0; i < cookies.length; i++) {
						ca = cookies[i].split('=');
							if (ca[0] == 'uuid') {
								uuid = ca[1]
							}
						if (ca[0] == 'cookies_enabled') {
								cookies_enabled = true
						}
					}
					if (cookies_enabled) {
						if (!uuid && !$location.$$search.u) {
							uuid = ''
							for (var i = 0; i < 32; i++) {
								uuid += Math.floor(Math.random()*16).toString(16).toUpperCase()
							}
							document.cookie = 'uuid'+'='+uuid+
										'; expires=Wed, 15 Dec 2038 19:49:32 GMT; path=/';
						} else if (!uuid){
							uuid = $location.$$search.u
							document.cookie = 'uuid'+'='+uuid+
										'; expires=Wed, 15 Dec 2038 19:49:32 GMT; path=/';
						}
						return uuid
					} else {
						return null
					}
				}()
		}

				/**
				 * TODO: hostname: change the hostname reference to value of the server you are connecting to..
				 */



			$rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute) {
				/**
				 * The following code is used for elements we want to show an hide based on the user history page.
				 * I can't use javascript's window.history due to security and privacy, so this is my solution.
				 * NOTE, $route.current does not work with this because if I use an ng-switch, and loadedTemplateUrl,
				 * something falls out of wack.
				 */

				if(info.previousPages){
					// @TODO: This is causing an error. previousPages is sometimes undefined.
					if(info.previousPages.length >= 3){
						info.previousPages.shift()
					}

					info.previousPages.push( info.currentPage )
					info.previousPage = info.currentPage
					info.currentPage  = $location.path()
				}
			})

			return info

	}])
