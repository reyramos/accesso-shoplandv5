angular.module( 'app' )
.factory
( 'flashpassService'
, [ function(){
		var service =
			{ steps:
				[ 'FlashPassModule'
				, 'flashPass-step-1'
				, 'flashPass-step-2'
				, 'flashPass-step-3'
				, 'flashPass-step-4'
				, 'flashPass-step-5'
				]
			, reset: function(){
				service.numGuests = 0;
				service.stayingTogether = '';
				service.groups = [];
				service.calendarData = {};
				service.products = [];
				service.currentStep = 0;
				service.keyword = '';
				service.selectedDate = null;
				service.selectedDateString = '';
			  }
			};
		
		service.reset();
		
		return service;
	 }
  ]
)

