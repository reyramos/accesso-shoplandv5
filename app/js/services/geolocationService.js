angular.module('app').service( 'geolocationService', function(
	$rootScope
	, $log
	, appDataService
	){
		var locationData = {}
		
		locationData.gatherGeolocation = function(){
			if( 'geolocation' in navigator ){
				navigator.geolocation.getCurrentPosition( geolocationSuccess, geolocationError, appDataService.geolocationOptions );
			} else {
				//$scope.hideSpinner();
			}
		}
		
		function geolocationSuccess(position){
			appDataService.storageService.set( '_geolocationPosition' , position );
			appDataService.storageService.set( '_geolocationError' , {} );
			appDataService.storageService.set( '_geolocationSwap' , [] );
			fetchGeolocationPackage(position);
		}
		
		function geolocationError(error){
			try{
				error.timestamp = new Date().getTime();
			} catch(e){}
			appDataService.storageService.set( '_geolocationPosition' , {} );
			appDataService.storageService.set( '_geolocationError' , error );
			appDataService.storageService.set( '_geolocationSwap' , [] );
			//$scope.hideSpinner();
		}
		
		function fetchGeolocationPackage(position){
			
		}
		
		return locationData
	}
)