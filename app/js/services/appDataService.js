angular.module('app').service
	( 'appDataService',[
		'$rootScope'
		, '$log'
		, '$location'
		, '$route'
		, '$q'
		, '$timeout'
		, 'clientInfoService'
		, 'enumsService'
		, 'socketService'
		, 'connectionService'
		, 'storageService'
		, 'countriesJSon'
		, 'utilitiesService'
	, function
		( $rootScope
		, $log
		, $location
		, $route
		, $q
		, $timeout
		, clientInfoService
		, enumsService
		, socketService
		, connectionService
		, storageService
		, countriesJSon
		, utilitiesService
		){

	var service =
		{ dataLoaded: false
		, flags: {}
		, defaults: {}
		, settings: {}
		, billingTypes: []
		, shippingMethods: []
		, filteredShippingMethods: []
		, billing: {}
		, payment: {}
		, purchaseReceipt : {}
		, optins : []
		, links : []
		, securityProvider: {}
		, countries : countriesJSon.countries
		, regions : countriesJSon.regions
		, lastKeyword : false
		, environment : ''
		, device : ''
		, langauge : ''
		, alias : ''
		, island : ''
		, userId : ''
		, merchant : ''
		, merchantId : ''
		, currencyCode : 'USD'
		, previousPage : ''
		, currentPage : ''
		, assets : {}
		, passbook : {}
		, menuItems : []
		, promoCodes : []
		, merchantName : ''
		, paypalPurchase: {}
		, crossSells : true
		, useLocation : false
		, geolocationOptions : {
			enableHighAccuracy:true
			, maximumAge:0
			, timeout:1000
		}
		, localstorageTimeout : 900000
		}


/**
 * ## Set Data
 *
 * @param {String} key Data source to request
 * @param {Boolean} useStorage set/get from localStorage, NOTE, there is a 3rd option, we can append to it.
 * @param {Object} request JSON formatted request for server
 * @param {Boolean} force true to force the request and bypass cache
 *
 * @return {Object} a $q.defer().promise for the data requested
 *
 */

	service.setData = function(data) {

		if( !service.flags[data.request_type] ) {
			service.flags[data.request_type] = true;

			//
			//	@request_type = '_initializeSocket'
			//

			if( data.request_type == '_initializeSocket' ) {
				if( typeof data.merchant != 'undefined' ) {
					service.merchant = data.merchant;
				}

				if( typeof data.merchantId != 'undefined' ) {
					service.merchantId = data.merchantId;
				}

				if( typeof data.version != 'undefined' && data.version != '' ) {
					service.alias = data.version;
				}

				if( typeof data.environment != 'undefined' ) {
					service.environment = data.environment;
				}

				if( typeof data.island != 'undefined' ) {
					service.island = data.island;
				}

				if( typeof data.device != 'undefined' ) {
					service.device = data.device;
				}

				if( typeof data.langauge != 'undefined' ) {
					service.langauge = data.langauge;
				}
			}

			//
			//	@request_type = 'GetApplicationConsolidated'
			//
			if( data.request_type == enumsService.services.GET_APPLICATION_CONSOLIDATED ) {

				if( typeof ((data).SETTINGS || {}).agent_id != 'undefined' ) {
					service.userId = data.SETTINGS.agent_id;
				}
				if( typeof ((data).SETTINGS || {}).currency_code != 'undefined' ) {
					service.currencyCode = data.SETTINGS.currency_code;
				}
				if( typeof ((data).SETTINGS || {}).merchant_name != 'undefined' ) {
					service.merchantName = data.SETTINGS.merchant_name;
				}
				
				try{
					// TODO: Need to re-introduce application controller (or some other method) to set these
					// application wide configurations coming from a service response.
					$rootScope.offline = utilitiesService.getBoolean( data.SETTINGS.offline );
					$rootScope.offlineMessage = data.LANGUAGE.ApplicationLabels.offlineMsg || 'Application is Offline.<br />Please visit again shortly.'
					$rootScope.showSpinner();
				} catch( e ){}
				
				if( typeof ((data).TREE || {}).MENUITEM != 'undefined') {
					_menuItems = [];
					_menu = angular.isArray(data.TREE.MENUITEM) ? data.TREE.MENUITEM : [data.TREE.MENUITEM];
					// Logic for removing the inactive Menu Items
					for(var i in _menu) {
						if( utilitiesService.getBoolean(_menu[i].isactive) ) {
							_isactive = true;
							if( typeof _menu[i].MENUITEM != 'undefined' ) {
								_submenuItems = [];
								_submenu = angular.isArray(_menu[i].MENUITEM) ? _menu[i].MENUITEM : [_menu[i].MENUITEM];
								for( var j in _submenu ) {
									if( utilitiesService.getBoolean( _submenu[j].isactive ) ) {
										_submenu[j].isactive = true;
										_submenuItems.push( _submenu[j] );
									}
								}
								// Remove Menu Item if there are no active Sub Menu Items
								if( _submenuItems.length == 0 ) {
									_isactive = false;
								} else {
									_menu[i].MENUITEM = _submenuItems;
								}
							}

							if ( utilitiesService.getBoolean(_isactive) ) {
								_menu[i].isactive = true;
								_menuItems.push(_menu[i]);
							}
						}
					}

					var swapArray = storageService.get('_geolocationSwap');
					if(angular.isArray(swapArray) && swapArray.length > 0){
						for(var i in swapArray){
							if(swapArray[i].keyword && swapArray[i].swap_keyword){
								for(var j in _menuItems){
									if(_menuItems[j].keywords == swapArray[i].keyword){
										_menuItems[j]._keywords = _menuItems[j].keywords;
										_menuItems[j].keywords = swapArray[i].swap_keyword;
									}
									if(typeof _menuItems[j].MENUITEM != 'undefined'){
										for(var k in _menuItems[j].MENUITEM){
											if(_menuItems[j].MENUITEM[k].keywords == swapArray[i].keyword){
												_menuItems[j].MENUITEM[k]._keywords = _menuItems[j].MENUITEM[k].keywords;
												_menuItems[j].MENUITEM[k].keywords = swapArray[i].swap_keyword;
											}
										}
									}
								}
							}
						}
					}
					service.menuItems = _menuItems;
				}

				//	Get the assets path from settings.
				if( typeof ( (data).SETTINGS || {} ).assets_path != 'undefined' ) {
					try {
						service.assets.config = data.SETTINGS.assets_path;
						service.assets.json = JSON.parse( data.SETTINGS.assets_path );
						if( typeof service.environment != 'undefined' && service.environment != '' ) {
							service.assets.url = service.assets.json[service.environment];
						} else{
							service.assets.url = service.assets.json['production']
						}
					} catch(e) {
						service.assets.error = e;
					}
				}

				// Get the passbook path from settings.
				if( typeof ( (data).SETTINGS || {} ).passbook_path != 'undefined' ) {
					try {
						service.passbook.config = data.SETTINGS.passbook_path;
						service.passbook.json = JSON.parse( data.SETTINGS.passbook_path );
						if(typeof service.environment != 'undefined' && service.environment != ''){
							service.passbook.url = service.passbook.json[service.environment];
						} else{
							service.passbook.url = service.passbook.json['production'];
						}
					} catch(e) {
						service.passbook.error = e;
					}
				}

				// Get the optins from the settings.
				if( typeof ( (data).SETTINGS || {} ).optins != 'undefined' ) {
					try {
						_optins = JSON.parse( data.SETTINGS.optins );
						service.optins = ( angular.isArray( _optins ) ) ? _optins : [_optins];
					} catch(e) {
						//
					}
				}

				// Get the links from the settings.
				if( typeof ( (data).SETTINGS || {} ).links != 'undefined' ) {
					try {
						_links = JSON.parse( data.SETTINGS.links );
						for( var i  in _links) {
							service.links.push( {'label':i,'url': _links[i]} );
						}
					} catch(e) {

					}
				}

				// Get the provider from the settings.
				if( typeof ( (data).SETTINGS || {} ).security_provider != 'undefined' ) {
					if( typeof ( (data).LANGUAGE[data.SETTINGS.security_provider] || {} ).LT != 'undefined' ){
						try {
							_securityProvider = angular.isArray(data.LANGUAGE[data.SETTINGS.security_provider].LT)
								? data.LANGUAGE[data.SETTINGS.security_provider].LT
								: [data.LANGUAGE[data.SETTINGS.security_provider].LT];
							for(var i in _securityProvider){
								service.securityProvider[_securityProvider[i].target] = _securityProvider[i].value;
							}
							service.securityProvider.provider = data.SETTINGS.security_provider.toLowerCase();
						} catch(e) {
							service.securityProvider.error = e;
						}
					}
				}

				// Get the default keyword from settings.
				if( typeof ( (data).SETTINGS || {} ).default_keyword != 'undefined' ){
					service.defaults.keyword = data.SETTINGS.default_keyword;
				}

				// Get the default country from settings.
				if( typeof ( (data).SETTINGS || {} ).default_country != 'undefined'){
					service.defaults.country = data.SETTINGS.default_country;
				}

				// Get the default language from settings.
				if( typeof ( (data).SETTINGS || {} ).default_language != 'undefined'){
					service.defaults.language = data.SETTINGS.default_language;
				}

				// Get the default over view from settings.
				if( typeof ( (data).SETTINGS || {} ).default_order_view != 'undefined'){
					service.defaults.orderView = data.SETTINGS.default_order_view;
				}

				if( typeof ((data).SETTINGS || {}).use_location != 'undefined' ){
					service.useLocation = utilitiesService.getBoolean(data.SETTINGS.use_location);
					if( service.useLocation && typeof ((data).SETTINGS || {}).geolocation_options != 'undefined' ){
						try{
							service.geolocationOptions = JSON.parse( data.SETTINGS.geolocation_options );
						} catch( e ){

						}
					}
				}

				if( typeof ( (data).SETTINGS || {} ).localstorage_timeout != 'undefined' && !isNaN(data.SETTINGS.localstorage_timeout) ){
					service.defaults.localstorageTimeout = data.SETTINGS.localstorage_timeout;
				}
			}
		}
		return data;
	}

	service.storageService = storageService;


/**
 * ## Get Service Name
 *
 * @param {String} key Data source to request
 *
 * @return {Object||Boolean} if true, you get the object/string, else, you get the boolean
 *
 */
	service.getServiceName = function( requestType ) {
		for( var i in enumsService.services ) {
			if( enumsService.services[i] == requestType ) {
				return i;
			}
		}
		return false;
	}

/**
 * ## Fetch Data
 *
 * @param {String} key Data source to request
 * @param {Boolean} useStorage set/get from localStorage, NOTE, there is a 3rd option, we can append to it.
 * @param {Object} request JSON formatted request for server
 * @param {Boolean} force true to force the request and bypass cache
 *
 * @return {Object} a $q.defer().promise for the data requested
 *
 */
	service.fetch = function( key, useStorage, request, force, useCookie ) {
		// Define a 2nd method for accessing the variables from within the event listener below.
		var self = {};
		// Define the variables internal to function.
		self.key = key;
		self.useStorage = ( angular.isDefined(useStorage) && useStorage ) ? true : false;
		self.useCookie  = ( angular.isDefined(useCookie)  && useCookie  ) ? true : false;
		self.request    = ( angular.isDefined(request)    && request    ) ? request : false;
		self.force      = ( angular.isDefined(force)      && force      ) ? true : false;

		// Define a 2nd method for accessing the variables from within the event listener below.

		// Using Angular.JS promise/defer system.
		var defer = $q.defer();

		// fail if no key is provided
		if ( !self.key ) {
			defer.reject('fetch: ** invalid key');
			return defer.promise;
		}

		// use force to force the request and bypass cache
		var data;
		var dataSource = '';
		if( !self.force && self.useStorage ) {
			data = service[self.key] ? service[self.key] : false;
			if( data ) {
				dataSource = 'Memory';
			}
		}

		// set data from stored object appDataService if it exists
		if( !self.force && !data && self.useStorage ) {
			data = storageService.get( self.key );

			service[self.key] = data;
			if( data ) {
				dataSource = 'LocalStorage';
			} else if( self.useCookie ) {
				data = storageService.get( self.key, true );
				if( data ) {
					dataSource = 'Cookie';
				}
			}
		}

		/**
		 * if we have data already, return right away, using defer.resolve,
		 * otherwise, get data.
		 */
		if( data ) {
			$log.info('appDataService: >>', dataSource, data);
			defer.resolve( service.setData( data) );
		} else { // no stored data, so we must depend on the server for it
			// if a request was defined, we should process it
			if ( self.request ) {
				self.request = ( typeof self.request == 'object' ) ? self.request : {};
				// let connectionService format this request as needed
				self.request = connectionService.format( self.key, self.request );
				// attach a randomly generated token to the request
				self.request.request_token = ''
				for( var i = 0; i < 32; i++ ) {
					self.request.request_token +=
						Math.floor( Math.random() * 16 ).toString( 16 ).toUpperCase();
				}

				$log.info('appDataService: <<', self.request.request_type, self.request);

				if( socketService.open ) { // if socket is open, send away
					socketService.send( self.request);
				} else { // if socket is not open, wait for it to open, then send
					var removeListener = $rootScope.$on
						( enumsService.socket.open
						, function() {
								socketService.send( self.request );
								removeListener(); // ensure this only runs once
							}
						)
				}
				// send failure response if the server never responds at all
				var timeoutError = $timeout
					( function() {
						// @todo, message is not an attribute sent from the server.
						// @todo, error_msg is an attibute used by the backend.
							var data =
								{ status: enumsService.status.FAILED
								, message: 'server timeout'
								, error_msg: 'server timeout'
								, request_type: self.request.request_type
								};

							var datasource = 'Timeout';
							$log.info( 'appDataService: >>', dataSource, data );
							defer.resolve( service.setData(data) );
						}
					, 60000
					)
			} else {

			}

			// Wait for data to come back from server matching our key.
			var fetchListener =
				$rootScope.$on
					( enumsService.socket.receive + self.key
					, function(event, data){
						// if a request was provided, ensure this is the matching response
						// based on the request_token otherwise just blindly assume this
						// is the correct response
						if( ( (self.request || {}).request_token
								   && (self.request || {}).request_token == data.request_token
								 )
								 || !(self.request || {}).request_token
						 ){
							// we actually got back real data so disable the $timeout error
							$timeout.cancel( timeoutError );

							if( self.useStorage ) {
								// save data to appDataService and localStorage
								service[self.key] = data;
								storageService.set( self.key, data, self.useCookie );
							}

							dataSource = 'Socket';
							$log.info('appDataService: >>', dataSource, data)
							defer.resolve( service.setData( data ) );
						}

						// Executing function removes the event listener.
						fetchListener();
					  }
					)
		}

		return defer.promise;
	}

	/**
	 * ## -START :: INITIALIZATION CODE
	 *
	 * @param {Object} appDataService, access to fetch() and other functions.
	 * @param {Object} enumsService, access to various elements used in the application.
	 *
	 * @return {Object} an angular dependency wrapper around appDataService.fetch
	 */

	service.initializeSocket = function ( forceRequest ) {

		var forceRequest = ( typeof forceRequest != 'undefined' && forceRequest ) ? true : false;

		// delete localStorage[enumsService.services.INITIALIZE_SOCKET];
		// setting storage to false will not work. it causes it to reload it.

		// delete localStorage[$scope.services.GET_NEW_CART_ID];
		// appDataService.fetch( $scope.services.GET_NEW_CART_ID , true, false ).then(
		/* _initializeSocket, true, true */
		var promise = service.fetch(
				  enumsService.services.INITIALIZE_SOCKET
				, true
				, true
				, forceRequest
				, true
			).then(
				function (data) {
					// Resetting just in case.
					if ( data.status != enumsService.status.OK ) {
						// Redirect // Failed to initialize.
						if ( location.pathname.indexOf('/error/') === -1 ) {
							$rootScope.showMenuBtn = false;
							$rootScope.showCartBtn = false;

							if ( data.error_msg.indexOf('Failed to load') > -1 ) {
								routeToErrorPage( false, enumsService.errorRoutes.UNKNOWN_MERCHANT );
							} else {
								_data = {}
								_data.error = 1
								_data.data = data
								routeToErrorPage( _data, enumsService.errorRoutes.FAILED_TO_INIT );
							}
							//alert('not redirecting');
						}
					}
					return data;
				}
			);
		return promise;
	}

	/**
	 * _reinitSocketChecksums
	 * used for checking the values of each of the checksums from a reinitialization.
	 * if the response for a value is:
	 * - false, then the server will send me new data.
	 * - an MD5 value, compare it to the local copy, and see if it matches. -
	 *
	 * @param {Object} appDataService, access to fetch() and other functions.
	 * @param {Object} enumsService, access to various elements used in the application.
	 * @param {Object} response JSON from server.
	 *
	 * @return {Object} an angular dependency wrapper around appDataService.fetch
	 */

	service._reinitSocketChecksums = function ( data ) {

		if ( data.status == enumsService.status.OK ) {
			if ( typeof data.checksums != 'undefined' ) {

				for ( var i in data.checksums ) {
					var csRequestType = data.checksums[i].request_type
					var csValue = data.checksums[i].checksum
					var localStorageData = storageService.get(csRequestType)
					// we should be getting this inbound. it should resolve itself
					if ( csValue == 'false' ) {
					} else if ( csValue ){
					// do a comparison of the data.
						if ( localStorageData && typeof localStorageData.checksum != "undefined" && localStorageData.checksum != '' ) {
							// If the values don't match, get a new copy.
							if ( localStorageData.checksum != csValue ) {
								// TODO: GetMerchantPackageList needs GetMerchantKeywords in order to pass the Keywords in the call
								service.fetch( csRequestType, true, {} , true);
							} else {
							}
						} else {
							// I don't have a reliable version of data in storage. I need to request it.
							service.fetch( csRequestType, true, {}, true);
						}
					} else {
						// UNKNOWN CONDITION
					}
				} // end for loop.
			} else {
				var logheader = '>> appDataService.js :: service._reinitSocketChecksums () :: ';
				console.warn(logheader + 'checksums missing when expected')
				// @todo, figure out condition here.
			}
		// Handling for Failure
		} else {
			// @TODO: App can not be initialized, throw an error Route to Error & offer a Try Again then do a clean INIT SOCKET
			if ( location.pathname.indexOf('/error/') == -1 ) {
				_data = {}
				_data.error = 2
				_data.data = data
				routeToErrorPage( _data, enumsService.errorRoutes.FAILED_TO_INIT );
			}
		}
	}
/**
 * ## -
 *
 * @param {Object} appDataService, access to fetch() and other functions.
 * @param {Object} enumsService, access to various elements used in the application.
 * @param {Object} response JSON from server.
 *
 * @return {Object} an angular dependency wrapper around appDataService.fetch
 */
	service.initializeData = function( forceRequest ) {

		var logheader = '>> appDataService.js :: service.initializeData = function() { :: ';

		var forceRequest = ( typeof forceRequest != 'undefined' && forceRequest ) ? true : false;
		var promise = '';


		var urlMerchant = window.location.href.toString().split(window.location.host)[1].split('/')[1].split('?')[0];
		$log.info('>>>> urlMerchant' , urlMerchant);

		// Let's check that we are dealing with an actual merchant string.
		if ( urlMerchant === '' || urlMerchant === 'undefined') {
			$rootScope.showMenuBtn = false;
			$rootScope.showCartBtn = false;
			routeToErrorPage( false, enumsService.errorRoutes.MISSING_MERCHANT );
			return;
		}

		cartData = service.storageService.get(enumsService.services.GET_NEW_CART_ID,true);

		if ( !socketService.open || forceRequest ) {

			if (cartData) {

			/**
			 * @skip_requests, {string} this is used to tell the backend to ignore some follow up calls.
			 *
			 *  ie AddCartItem calls GetCartSummary
			 *  ie UpdateCartItems calls GetCartSummary
			 *  ie UpdateCartCheckoutInfo calls GetCartSummary
			 *  ie GetCartSummary with a bad status and a message of
			 *      "invalid cart id or cart key", calls GetNewCartID
			 *  ie GetCartSummary will call SetExpressCheckout IF the client
			 *      supports paypal.
			 *
			 * Check if the url is a paypal related url. If it's a RETURN_URL,
			 *     SKIP the SetExpressCheckout call in the backend. We are
			 *     doing this to block the SetExpressCheckout call that gets
			 *     auto-generated by GetCartSummary. If it were to run, it will
			 *     cause the system to get a new PayPal token, that would NOT
			 *     allow the user to proceed through the checkout process
			 *     correctly. Trust me, leave this in.
			 *
			 */

				var mySkipRequests = '';
				if (
					window.location.pathname.indexOf( enumsService.routes.PAYPAL_RETURN_URL ) > -1
					|| window.location.pathname.indexOf( enumsService.routes.REVIEW ) > -1
				) {
					if ( mySkipRequests ) {
						mySkipRequests += ',';
					}
					mySkipRequests += enumsService.services.SET_EXPRESS_CHECKOUT;
				}

			/**
			 * Promo Codes:
			 * The following lines of code support promo codes previously used
			 * in any additions made to the cart. If they are not included in
			 * the initialize request to the server, then we can not properly
			 * make some cart calls.
			 *
			 */

				var myPromoCodes = ""
				var promoCodesData = service.storageService.get('promo_codes', true)
				if ( ( typeof promoCodesData == "object" ||  typeof promoCodesData == 'array' ) && promoCodesData != null ) {
					myPromoCodes = promoCodesData.join(',');
				}

			/**
			 * Proceeding with `_initializeSocket` call.
			 */
				promise = service.fetch(
					  enumsService.services.INITIALIZE_SOCKET
					, true
					, {   "cart_id"     : cartData.cart_id
						, "cart_key"    : cartData.cart_key
						, "session_id"  : cartData.session_id
						, "promo_codes" : myPromoCodes
						, "skip_requests" : mySkipRequests
					  }
					, true
					, true
				).then(
					function(data) {
						if ( data.status == enumsService.status.OK ) {
							service._reinitSocketChecksums( data )
						} else {
							// Redirect // Failed to initialize.
							if ( location.pathname.indexOf('/error/') === -1 ) {
								_data = {}
								_data.error = 3
								_data.data = data
								routeToErrorPage( _data, enumsService.errorRoutes.FAILED_TO_INIT );
							} else {
								/*
								 * if the path is already in an error state,
								 * there may be another error that takes precidence
								 * over this one.
								 *
								 */
							}
						}
						// Time to check the response // Handling for Success
						return data
					}
				)
			} else {
				// Working as a fresh init.
				promise = service.initializeSocket( forceRequest );
			}
		} else {
			// Working as a fresh init.
			promise = service.initializeSocket( forceRequest );
		}

		return promise;
	}
/**
 * ###########################################################################
 * END :: INITIALIZATION CODE
 * ###########################################################################
 */

	/**
	 * The purpose of the url is to have a generalized way to go to error pages,
	 * but to not cause other problems down the road. ie: storage getting cleared
	 * because we rerouted to an error page, without a merchant, causing a localStorage.clear();
	 */

	function routeToErrorPage ( data , errorPage ) {
		// Let's determine if we have a merchant string in the url
		var urlMerchant = window.location.href.toString().split(window.location.host)[1].split('/')[1].split('?')[0];
		$log.info('>>>> appDataService::routeToErrorPage( data , ' + errorPage + ') :: urlMerchant' , urlMerchant);

		// whether to include the merchant or not.
		var urlPrefix = ( typeof urlMerchant != 'undefined' && urlMerchant !== '' ) ? '/' + urlMerchant : '';

		var data = data || {};
		var errorPage = errorPage || '';
		$rootScope.errorData = data

		$log.info('appDataService.routeToErrorPage() errorPage = ' + errorPage);
		$log.info('appDataService.routeToErrorPage() data = ' + data);

		if ( errorPage !== '' ) {
			$location.path( urlPrefix + '/' + enumsService.routes.ERROR + '/' + errorPage );
		} else if ( data ) {
			if ( typeof data.error_msg != 'undefined' && data.error_msg.indexOf('Failed to load') > -1 ) {
				$location.path( urlPrefix + '/' + enumsService.routes.ERROR + '/' + enumsService.errorRoutes.UNKNOWN_MERCHANT );
			} else {
				$location.path( urlPrefix + '/' + enumsService.routes.ERROR + '/' + enumsService.errorRoutes.FAILED_TO_INIT );
			}
		}
	}

	return service;
}])
