angular.module( 'app' )
.service
	( 'cartService',
	[ '$log'
	, '$q'
	, '$rootScope'
	, 'enumsService'
	, 'appDataService'
	, 'languageService'
	, '$timeout'
	, '$filter'
	, 'packageService'
	, 'extraFlowService'
	, 'modalService'
	, function
		( $log
		, $q
		, $rootScope
		, enumsService
		, appDataService
		, languageService
		, $timeout
		, $filter
		, packageService
		, extraFlowService
		, modalService
		) {

		var socket = enumsService.socket;
		var status = enumsService.status;
		var services = enumsService.services;
		var cartMessages = enumsService.cartMessages;
		var i18n = languageService;
		var lastRequest = '';
		var cartService =
			{ cartItems: []
			, paypal:
				{ token: ''
				, id: ''
				, show: false
				}
			, paypalPurchase: {}
			};
		
		var qtyAdded = 0
		  , retainCharacs = [ 'card_type_required' ]; // Copy these CHARACS over to items being added.
		
		// Update cart quantity functionality.
		cartService.updateQTY = function(){
			for( var i = 0; i < cartService.cartItems.length; i++ ){
				// Flag used to determine if the CHARACs should be copied.
				// Checks if currently, the length of the CHARACs = quantity.
				cartService.cartItems[ i ].copyCharacs =
					( cartService.cartItems[ i ].CHARACS && 
					  Number( cartService.cartItems[ i ].qty ) == cartService.cartItems[ i ].CHARACS.length
					);
				
				// Save a reference to the original quantity.
				cartService.cartItems[ i ].prevQty = cartService.cartItems[ i ].qty;
			}
		}
		
		cartService.cancelUpdate = function(){
			// Clean up the added properties when udateQTY was executed.
			for( var i = 0; i < cartService.cartItems.length; i++ ){
				cartService.cartItems[ i ].qty = cartService.cartItems[ i ].prevQty;
				delete cartService.cartItems[ i ].prevQty;
				delete cartService.cartItems[ i ].copyCharacs;
			}
		}

		cartService.commitUpdateQTY = function( cartLimit, callback ){
			// Extract the difference of the items being added.
			// Verify adding more does not exceed cart limit.
			// Handle less qty then previously added.
			
			var numPrevQty = 0
			  , originalQty = 0
			  , diff = 0
			  , moreItems = false
			  , lessItems = false
			  , noChange = false
			  , itemsUpdated = 0;
				
			for( var i = 0; i < cartService.cartItems.length; i++ ){
				var item = cartService.cartItems[ i ];
				
				// This will be true if the user deletes the qty but does not enter a value.
				// Assume the user meant to delete the item.
				if( item.qty == '' ){
					item.qty = '0';
				}
				
				// For some reason, this property is failing the addToCart functionality.
				// Delete it.
				delete item.$$hashKey;
				
				numPrevQty = Number( item.prevQty );
				originalQty = Number( item.qty );
				moreItems = numPrevQty < originalQty;
				lessItems = numPrevQty > originalQty;
				noChange = numPrevQty == originalQty;
				
				if( noChange ){
					continue;
				}
				
				if( moreItems ){
					// Ensure we are not exceeding cart limit.
					if( cartService.getTotalLessOverride() <= Number( cartLimit ) ){
						diff = originalQty - numPrevQty;
						item.qty = String( item.qty );
						
						if( item.copyCharacs ){
							// Copy the CHARACS.
							item.CHARACS = item.CHARACS || [];
							
							var characLen = item.CHARACS.length;
							for( var j = characLen; j < diff + characLen; j++ ){
								item.CHARACS[ j ] = item.CHARACS[ 0 ] || {};
							}
						}
						
						itemsUpdated++;
					} else {
						// Alert User... Cart limit will be exceeded.
						showCartLimitModal();
						return;
					}
				} else if( lessItems ){
					diff = numPrevQty - originalQty;
					if( item.CHARACS && ( item.CHARACS.length > 0 ) && item.copyCharacs ){
						while( item.CHARACS.length != originalQty ){
							item.CHARACS.splice( item.CHARACS.length -1, 1 );
						}
					}
					
					itemsUpdated++;
				}
				
				delete item.prevQty;
				delete item.copyCharacs;
			}
			
			if( itemsUpdated ){
				// Update Cart Items.
				cartService.updateCartItems
					(
						cartService.cartItems
					,	function( success ){
							if( callback ){
								callback( success );
							}
						}
					);
			}
		}
		
		cartService.addToCart = function( customerTypes, packageDetails ){
			$log.log('cartService.addToCart')
			// Reset the qty after message has been displayed.
			qtyAdded = 0;
			var totalLessOverideBeingAdded = 0;
			var itemsToBeAdded = [];
			
			if( exceededPackageLimit( customerTypes, packageDetails ) ){
				return;
			}
			
			for( var i in customerTypes ){
				var currentItem = customerTypes[ i ];

				if( currentItem.qty <= 0 ){
					// Skip rates with 0 qty selected.
					continue;
				}
				
				if( !validateMinQty( currentItem ) ){
					return;
				}
				
				if( !validateMaxQty( currentItem ) ){
					return;
				}
				
				// Do not consider packages with cart limit overrides.
				if(  getCartLimitOverride( currentItem ) != '1' ){
					// If cart limit override is undefined, count this qty towards total.
					totalLessOverideBeingAdded += Number( currentItem.qty );
				}
				
				itemsToBeAdded.push( cartService.setItemsToBeAdded( currentItem ) );
				addPackageCharacs( itemsToBeAdded, packageDetails );
				qtyAdded += currentItem.qty;
			}

			if( itemsToBeAdded.length > 0 ){
				if( cartService.getTotalLessOverideBeingAdded( totalLessOverideBeingAdded, packageService.cartLimit ) ){
					// If an advanced ticket, prompt user before adding to cart.
					setAdvancedTicketNotification
						(
							function( isAdvanced ){
								// If it's advanced, add client charac to each item.
								if( isAdvanced ){
									for( var i = 0; i < itemsToBeAdded.length; i++ ){
										itemsToBeAdded[ i ].CHARACS = itemsToBeAdded[ i ].CHARACS || [];

										for( var j = 0; j < itemsToBeAdded[ i ].qty; j++ ){
											itemsToBeAdded[ i ].CHARACS[ j ] = itemsToBeAdded[ i ].CHARACS[ j ] || {};
											itemsToBeAdded[ i ].CHARACS[ j ].client_is_advanced_ticket = 'true';
											itemsToBeAdded[ i ].CHARACS[ j ].valid_from = packageService.advanceTicketDate;
										}
									}
								}

								// Advanced ticket check complete and user agreed to terms.
								// Check for XFCs and delegate to extraFlowService if any.
								packageService.doXFC
									(
										packageDetails
										, itemsToBeAdded
										, customerTypes
										, function(){
											addCartItems( itemsToBeAdded );
										  }
									)
							}
						)
				} else {
					qtyAdded = 0;
				}
			} else {
				// User didn't select a qty to add.
				modalService.displayModal
					(
						{ title: languageService.Alert.cartNoQtySelectedTitle
						, description: languageService.Alert.cartNoQtySelectedMsg
						, hideAllButtons: true
						}
					);
			}
		}
		
		/**
		 *
		 * @param item
		 * @returns {{}}
		 */
		cartService.setItemsToBeAdded = function( item ){
			var cartItem = {};
			cartItem.package_id = item.package_id;
			cartItem.customer_type = item.customer_type;
			cartItem.qty = Number( item.qty );
			cartItem.client_rate_name = item.cusTypeName;

			if( item.cart_limit_override )
				cartItem.cart_limit_override = item.cart_limit_override;

			if( item.extra_movie )
				cartItem.extra_movie = item.extra_movie;
			
			if( item.date )
				cartItem.date = item.date;
			
			if( item.time )
				cartItem.time = item.time;
			
			if( item.promo_code )
				cartItem.promo_code = item.promo_code;
			
			cartItem.CHARACS = item.CHARACS || [];
			
			return cartItem;
		}
		
		cartService.getTotalLessOverideBeingAdded = function( totalLessOverideBeingAdded, cartLimit ){
			var status =  false

			if( Number( totalLessOverideBeingAdded ) > Number( cartLimit ) ||
				( Number( totalLessOverideBeingAdded ) + cartService.getTotalLessOverride() ) > Number( cartLimit ) ){
					showCartLimitModal();

			} else {
				status = true;
			}

			return status;
		}
		
		cartService.gotoPayPalCheckout = function(){
			// @todo, this will need some more logic in here to verify that the `SetExpressCheckout` is working.
			/*
			 $log.info('>> CartController.js >> $scope.gotoPayPalCheckout >> $scope.showPayPalCheckout', $scope.showPayPalCheckout);
			 $log.info('>> CartController.js >> $scope.gotoPayPalCheckout >> $scope.cartService.paypal', $scope.cartService.paypal);
			 $log.info('>> CartController.js >> $scope.gotoPayPalCheckout >> $scope.useEnvironment', $scope.useEnvironment);
			 $log.info('>> CartController.js >> $scope.gotoPayPalCheckout >> $scope.useDevice', $scope.useDevice);
			 */
			$log.log('cartService.gotoPayPalCheckout',$rootScope.showPayPalCheckout)

			if( $rootScope.showPayPalCheckout ){

				if( cartService.paypal.token ){


					var mobile_url = $rootScope.routes.PAYPAL_MOBILE_CHECKOUT + cartService.paypal.token;
					var normal_url = $rootScope.routes.PAYPAL_CHECKOUT + cartService.paypal.token;

					if( $rootScope.useEnvironment == 'development' || $rootScope.useEnvironment == 'staging' ){
						mobile_url = $rootScope.routes.PAYPAL_SANDBOX_MOBILE_CHECKOUT + cartService.paypal.token;
						normal_url = $rootScope.routes.PAYPAL_SANDBOX_CHECKOUT + cartService.paypal.token;
					}
					// Using a switch here, in case we wish to specify alternatives in the future.
					switch ( $rootScope.useDevice ){
						// Use the mobile specific url for mobile devices.
						case 'mobile':
							$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.redirectingToPayPal );
							window.location = mobile_url;
							break;

						case 'tablet':
						case 'desktop':
						default:
							// window.location = mobile_url
							$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.redirectingToPayPal );
							window.location = normal_url;
							break;
					}
				} else {
					modalService.displayModal
						(
							{ title: $rootScope.i18n.Alert.errorWithPayPalTitle
							, description: $rootScope.i18n.Alert.errorWithPayPalMsg
							, callback: function (detail){
								if(detail == $rootScope.buttonFlags.OK){
									// $scope.completeOrder()
								}
							  }
							}
						);
				}
			} else {
				// Not going to redirect to paypal because
				$log.warn('CartController.js::gotoPayPalCheckout() // not allowed, should not see button.');
			}
		};


		/**
		 * Update the cart with items to be added, return true if successfull
		 * @param {array} itemsToBeAdded
		 * @param {boolean} callback
		 *
		 */
		cartService.addCartItems = function(itemsToBeAdded, callback){
			$rootScope.pendingRequests.push($rootScope.services.ADD_CART_ITEM)
			appDataService.fetch(
				  enumsService.services.ADD_CART_ITEM
				, false
				, itemsToBeAdded
			).then(
				function( data ){
					$rootScope.removePendingRequest($rootScope.services.ADD_CART_ITEM)
					if( data.status == enumsService.status.OK ){
						// ADD_CART_ITEM Response.
						$rootScope.pendingRequests.push($rootScope.services.GET_CART_SUMMARY)
						appDataService.fetch(
								  enumsService.services.GET_CART_SUMMARY
								, true
								, false
								, true
						).then(
							function( data ){
								$rootScope.removePendingRequest($rootScope.services.GET_CART_SUMMARY)
								cleanUpCartData( data );

								if( !cartService.checkForItemErrors( data ) ){
									if( data
										&& data.CART
										&& data.CART.ITEMS
										&& data.CART.ITEMS.ITEM
										&& !angular.isArray( data.CART.ITEMS.ITEM )
										){
										data.CART.ITEMS.ITEM = [data.CART.ITEMS.ITEM];
									}

									if( data.status == enumsService.status.OK ){

										// Update the cart object.
										cartService.updateInternals( data );

										// PAYPAL RELATED.
										cartService.fetchSetExpressCheckout();

										callback.call(this, data);

									} else if( data.status == enumsService.status.FAILED ){
										// @TODO: Do we update internals when there is a problem?

										// If we get an error, expect an inbound get new cart id.


										callback.call(this, data);
									}
								}
							}
						)
					} else if( data.status == enumsService.status.FAILED ){
						// The request to updateCartItem failed.
						modalService.displayModal
							(
								{ title: i18n.Alert.cartErrorAddingItemTitle
								, description: i18n.Alert.cartErrorAddingItemMsg
								, hideAllButtons: true
								}
							);

						i18n.displayErrorCode(data);
						// Hide the spinner.
						$rootScope.hideSpinner();
					}
				}
			)
		};

		cartService.updateCartItems = function( itemsToBeUpdated, callback ){
			// Item has been removed. Update the cart.
			$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.updatingCartLoadMsg );
			$rootScope.pendingRequests.push( $rootScope.services.UPDATE_CART_ITEMS )
			appDataService.fetch
				(
					  enumsService.services.UPDATE_CART_ITEMS
					, false
					, itemsToBeUpdated
				).then
					(
						function( data ){
							$rootScope.removePendingRequest( $rootScope.services.UPDATE_CART_ITEMS );
							if( data.status == enumsService.status.OK ){
								// Do nothing for now.
								// GET_CART_SUMMARY Response.
								if( data
									&& data.CART
									&& data.CART.ITEMS
									&& data.CART.ITEMS.ITEM
									&& !angular.isArray( data.CART.ITEMS.ITEM ) ){
									data.CART.ITEMS.ITEM = [ data.CART.ITEMS.ITEM ];
								}

								$rootScope.pendingRequests.push( enumsService.services.GET_CART_SUMMARY )
								appDataService.fetch
									(
										  enumsService.services.GET_CART_SUMMARY
										, false
									).then
										(
											function( data ){
												$rootScope.removePendingRequest( enumsService.services.GET_CART_SUMMARY )
												if( !cartService.checkForItemErrors( data ) ){
													if( data.status == enumsService.status.OK ){
														// Update the cart object.
														cartService.updateInternals( data );

														// PAYPAL RELATED.
														cartService.fetchSetExpressCheckout();

														callback.call( this, true );
													} else if( data.status == enumsService.status.FAILED ){
														// @TODO: Do we update internals when there is a problem?
														callback.call( this, false );
													}
												}

												$rootScope.hideSpinner();
											}
										)
							} else if( data.status == enumsService.status.FAILED ){
								// The request to updateCartItem failed.
								modalService.displayModal
									(
										{ title: i18n.Alert.cartErrorUpdatingItemTitle
										, description: i18n.Alert.cartErrorUpdatingItemMsg
										, hideAllButtons: true
										}
									);

								callback.call( this, false );
								
								// Hide the spinner.
								$rootScope.hideSpinner();
							}
						}
					)
		};

		/**
		 * Return the total of items that does not include cart Override
		 * @return {[ integer ]} return integer cartLimit,
		 */
		cartService.getTotalLessOverride = function(){
			var qtySum = 0;
			if( cartService.cartItems.length ){
				for( var i = 0; i < cartService.cartItems.length; i++ ){
					var item = cartService.cartItems[ i ];
					if( getCartLimitOverride( item ) != '1' ){
						qtySum += Number( item.qty );
					}
				}
			}

			return qtySum;
		}
		
		cartService.updateCartCheckoutInfo = function( data ){
			// TODO: Deprecated. This is not referenced anywhere. Delete if unused.
			var myData = {};
			myData.shippingMethod = appDataService.shipping.deliveryMethod;
			myData.shippingAmount = appDataService.shipping.deliveryAmount;
			myData.billingId = '0';

			lastRequest = services.UPDATE_CART_CHECKOUT_INFO;
		}

		cartService.getNewCartID = function(){
			// TODO: Deprecated. This is not referenced anywhere. Delete if unused.
			// Reset total qty.
			cartService.totalQty = 0;

			// Reset cart details.
			cartService.cartDetails = {};

			// Get new CART ID.
			appDataService.fetch( enumsService.services.GET_NEW_CART_ID, true, true, false, true );
		}

		cartService.fetchGetNewCartID = function(){
			// TODO: Deprecated. This is not referenced anywhere except in a fetch tht needs to be removed. Delete if unused.
			// Reset total qty.
			cartService.totalQty = 0;

			// Reset cart details.
			cartService.cartDetails = {};

			// Get new CART ID. // use localStorage, don't generate the request, and overwrite existing.
			appDataService.fetch( enumsService.services.GET_NEW_CART_ID, true, false, true, true );
		}

		cartService.clearInternals = function(){
			// TODO: This is called from cartController after getCartSummary.
			// Maybe cartController should not be making getCartSummary request.
			// Move that functionality to the service and make this function private.
			cartService.cartDetails = {};
			cartService.cartItems = [];
			cartService.cartTaxesAndFeesAmount = 0;
			cartService.discountSource = 0;
			cartService.discountTotal = 0;
			cartService.feeSource = 0;
			cartService.potentialCartAdj = 0;
			cartService.potentialCartAdjTotal = 0;
			cartService.shippingSource = 0;
			cartService.showDiscounts = 0;
			cartService.subTotal = 0;
			cartService.total = 0;
			cartService.totalQty = 0;
		}

		/**
		 * [ updateInternals description ]
		 * @param  {[ type ]} data [ description ]
		 * @return {[ type ]}      [ description ]
		 */
		cartService.updateInternals = function( data ){
			// @TODO: Break this into functions.
			var i
			  , feeSource = []
			  , feeTotal = 0
			  , potentialCartAdj = []
			  , potentialCartAdjTotal = 0
			  , discountsSource = []
			  , discountTotal = 0
			  , shippingSource = {};

			cartService.cartItems = [];
			
			if( data.CART.ITEMS && data.CART.ITEMS.ITEM ){
				cartService.cartItems = ( angular.isArray( data.CART.ITEMS.ITEM ) ) ? data.CART.ITEMS.ITEM: [ data.CART.ITEMS.ITEM ];
				cartService.setEditFlags( data );
			}

			// @TODO: Request that cartTaxesAndFeesAmount be calculated on the backend.
			// Update all relevant cart information.
			// Taxes first.
			if( data.tax_total ){
				feeSource.push
					(
						{ label: i18n.CartView.taxLabel
						, amount: data.tax_total
						}
					);
				feeTotal += Number( data.tax_total );
			}

			// Followed by cart adjustments.
			if( data.CART_ADJUSTMENTS && data.CART_ADJUSTMENTS.CA ){
				var cartAdj = ( angular.isArray( data.CART_ADJUSTMENTS.CA ) ) ? data.CART_ADJUSTMENTS.CA: [ data.CART_ADJUSTMENTS.CA ];

				// Update the collection on data as well.
				data.CART_ADJUSTMENTS.CA = cartAdj;

				for( i = 0; i < cartAdj.length; i++ ){
					if( cartAdj[ i ].amount > 0 ){
						// Keep sum of fees.
						feeTotal += Number( cartAdj[ i ].amount );
						feeSource.push
							(
								{ label: i18n.CartView[ 'cartAdjustment' + cartAdj[ i ].id ]
								, amount: cartAdj[ i ].amount
								}
							);
					} else {
						// Amount is less than 0. Apply to discounts. Keep sum of discounts.
						discountTotal += Number( cartAdj[ i ].amount )
						discountsSource.push
							(
								{ label: i18n.CartView[ 'cartAdjustment' + cartAdj[ i ].id ]
								, amount: cartAdj[ i ].amount
								}
							);
					}
				}
			}

			// @TODO: Investigate how POTENTIAL_CART_ADJUSTMENTS is used in Shop V4.
			if( data.POTENTIAL_CART_ADJUSTMENTS && data.POTENTIAL_CART_ADJUSTMENTS.CA ){
				var potCartAdj = ( angular.isArray( data.POTENTIAL_CART_ADJUSTMENTS.CA ) ) ? data.POTENTIAL_CART_ADJUSTMENTS.CA: [ data.POTENTIAL_CART_ADJUSTMENTS.CA ];

				// Update the collection on data as well.
				data.POTENTIAL_CART_ADJUSTMENTS.CA = potCartAdj;

				for( i = 0; i < potCartAdj.length; i++ ){
					// Keep sum of fees.
					potentialCartAdjTotal += Number( potCartAdj[ i ].amount );
					potentialCartAdj.push
						(
							{ label: i18n.CartView[ 'potentialCartAdjustment' + potCartAdj[ i ].id ]
							, amount: potCartAdj[ i ].amount
							}
						);
				}
			}

			// Followed by fees.
			if( data.FEES && data.FEES.F ){
				var fees = ( angular.isArray( data.FEES.F ) ) ? data.FEES.F: [ data.FEES.F ]

				// Update the collection on data as well.
				data.FEES.F = fees

				for( i = 0; i < fees.length; i++ ){
					feeTotal += Number( fees[ i ].amount )
					feeSource.push
					(
						{ label: i18n.CartView[ 'fee' + fees[ i ].id ]
						, amount: fees[ i ].amount
						}
					)
				}
			}

			// Shipping if applicable.
			if( data.ship_total ){
				shippingSource =
					{ label: i18n.CartView.deliveryFee
					, amount: ( Number( data.ship_total ) > 0 ) ? data.ship_total: i18n.CartView.free
					};
				feeTotal += Number( data.ship_total );
				feeSource.push( shippingSource )
			}

			// Store all references to relevant information.
			cartService.subTotal = data.payment_due
			cartService.shippingSource = shippingSource
			cartService.cartTaxesAndFeesAmount = Number( feeTotal )
			cartService.feeSource = feeSource
			cartService.potentialCartAdj = potentialCartAdj
			cartService.potentialCartAdjTotal = potentialCartAdjTotal
			cartService.discountSource = discountsSource
			cartService.discountTotal = discountTotal
			cartService.showDiscounts = discountTotal > 0
			cartService.total = data.total

			// Total Qty.
			cartService.totalQty = getTotalQty()
			// Keep a reference to all cart details.
			cartService.cartDetails = data
		}

		cartService.checkForItemErrors = function( data ){
			var currentItem
				, i
				, deleteItemsArr = []
				, allMsgs = ''
				, errorCodeMsg = ''
				, mssgs = {}
				, appendedDefaultError = false
				, codeList = []
				, numErrorItms = 1;

			if( data.CART && data.CART.ITEMS && data.CART.ITEMS.ITEM && angular.isDefined( data.CART.ITEMS.ITEM ) ){
				for( i = 0; i < data.CART.ITEMS.ITEM.length; i++ ){
					currentItem = data.CART.ITEMS.ITEM[ i ];

					if( !currentItem.action ||
						 currentItem.action.toLowerCase() != "error" &&
						 currentItem.action.toLowerCase() != "warning" )
						continue;

					// Add default message... by default.
					if( !appendedDefaultError ){
						appendedDefaultError = true;
						errorCodeMsg = "<b>" + String( numErrorItms ) + "</b> " + i18n.CheckCartTicketLevelErrors.defaultError + "<br /><br />";
					}
					
					if( currentItem.action.toLowerCase() == "error" ){
						
						//Get the index of the item to be deleted.
						deleteItemsArr.push( currentItem );

						errorTitle = currentItem.package_name;
						errorCode = currentItem.error_code;

						if( errorCode != '' ){
							codeList.push( errorCode );
							if( !mssgs[ 'msg' + errorCode ] ){
								numErrorItms++;
								mssgs[ 'msg' + errorCode ] = i18n.CheckCartTicketLevelErrors[ errorCode ];
								errorCodeMsg += "<b>" + String( numErrorItms ) + "</b> " + mssgs[ 'msg' + errorCode ] + "<br /><br />";
							}
						}

						// Remove the offending item.
						data.CART.ITEMS.ITEM.splice( i, 1 );
						i--;
					}

					/*
					 if( currentItem.action.toLowerCase() == "warning" )
					 {
					 //Get the index of the item to be deleted
					 errorTitle = currentItem.@package_name.toString()
					 errorCode = currentItem.@error_code.toString()
					 errorCodeMsg = _i18n.getErrorMessageById( int( errorCode ), FaultHandler.CHECK_CART_TICKET_LEVEL_ERRORS )
					 errorCodeMsg = StringUtils.replace( errorCodeMsg, "@@minimum", currentItem.@associated_group_minimum )
					 errorCodeMsg = StringUtils.replace( errorCodeMsg, "@@found", currentItem.@associated_group_found )
					 warningKeyword = currentItem.CHARACS[ 0 ].@keyword_used
					 _pm.show( errorCodeMsg, errorTitle, AccAlert.LEVEL_HINT, AccAlert.OK | AccAlert.FIX_ISSUES, fixWarnings, AccAlert.OK )
					 }
					 */
				}
			}

			if( deleteItemsArr.length > 0 ){
				cartService.updateInternals( data );
			}

			if( errorCodeMsg.length > 0 ){
				appDataService.fetch
					(enumsService.services[ 'GET_APPLICATION_CONSOLIDATED' ]
					, true
					, false
					).then
					(function( data ){
						errorCodeMsg = i18n.replaceAll( '@@supportPhoneNumber', data.SETTINGS.support_phone_number, errorCodeMsg );

						modalService.displayModal
							(
								{ replacementKeys: 'errorCode'
								, replacementValues: codeList.join(', ')
								, title: languageService.CheckCartTicketLevelErrors.errorCodeTitle
								, description: '<div class="text-left">' + errorCodeMsg + '</div>'
								, hideAllButtons: true
								}
							);

						// @TODO: Delete items from cart and call getCartSummary.
						return true;
					  }
					)
			}

			return false;
		}
		
		cartService.setEditFlags = function( data ){
			var hasItems =
				data.CART &&
				data.CART.ITEMS &&
				data.CART.ITEMS.ITEM &&
				(angular.isArray( data.CART.ITEMS.ITEM ) ?
						data.CART.ITEMS.ITEM.length > 0:
						true
				);
			
			if( hasItems ){
				$rootScope.displayChangeQtyOptions = false;
				data.CART.ITEMS.ITEM = angular.isArray( data.CART.ITEMS.ITEM ) ? data.CART.ITEMS.ITEM: [ data.CART.ITEMS.ITEM ];
				// Toggle edit buttons.
				for( var i = 0; i < data.CART.ITEMS.ITEM.length; i++ ){
					var cartItem = data.CART.ITEMS.ITEM[ i ];
					
					// Ensure CHARACS are stored as array.
					if( cartItem.CHARACS ){
						cartItem.CHARACS = ( angular.isArray( cartItem.CHARACS ) ) ? cartItem.CHARACS: [ cartItem.CHARACS ]
					}
					
					// Add editable flag.
					cartItem.showChangeBtns = !extraFlowService.getXFCEditMdodeStatusForItem( cartItem );
					if( cartItem.showChangeBtns ){
						$rootScope.displayChangeQtyOptions = true;
					}
				}
			}
		}

		/**
		 * fetchSetExpressCheckout
		 * This will collect the expected inbound response for SetExpressCheckout.
		 *
		 * @param  {[ type ]} data [ description ]
		 * @return {[ type ]}      [ description ]
		 */
		cartService.fetchSetExpressCheckout = function( callbackOk, callbackFailure ){
				// We should get the SetExpressCheckout on a GetCartSummary @status='OK'
			appDataService.fetch
			(
				  enumsService.services.SET_EXPRESS_CHECKOUT
				, true
				, false
				, true
			).then
			(
				function( data ){
					if ( data.status == enumsService.status.OK ){
						// Update the cart object.
						cartService.paypal.token = data.token;
						if ( typeof callbackOk == 'function' ){
								callbackOk( data.token );
						} else {

						}
					} else {
						cartService.paypal.token = false;
						if ( typeof callbackFailure == 'function' ){
							callbackFailure();
						} else {

						}
					}
				}
			)
		}

		
		/* fetchGetCartSummary
		 * This will collect the expected inbound response for SetExpressCheckout.
		 *
		 * @param  {[ type ]} data [ description ]
		 * @return {[ type ]}      [ description ]
		 */
		cartService.fetchGetCartSummary = function( callbackOk, callbackFailure ){
			// We should get the SetExpressCheckout on a GetCartSummary @status='OK'
			/** FETCH CART SUMMARY ***/
			appDataService.fetch
			(
				  enumsService.services.GET_CART_SUMMARY
				, true
				, false
				, true
			).then
			(
				function( data ){
					cleanUpCartData( data );
					if( !cartService.checkForItemErrors( data ) ){
						if( data.status == status.OK ){
							// Update the cart object.
							cartService.updateInternals( data );
							// Listen for SetExpressCheckout.
							cartService.fetchSetExpressCheckout();
							
							if ( typeof callbackOk == 'function' ){
								callbackOk( data.token );
							} else {
	
							}
							
						} else if( data.status == status.FAILED ){
							// @TODO: Do we update internals when there is a problem?
							// listen for a new cart_id/cart_key.
							if ( typeof callbackFailure == 'function' ){
								callbackFailure();
							} else {
	
							}
							cartService.fetchGetNewCartID();
						}
					}
				}
			);
		}
		
		
		cartService.updatePayPalSessionInfo = function() {
		
			var paypalSessionInfo = 
				{ paypalPurchase: appDataService.paypalPurchase
				, billing: appDataService.billing
				, payment: appDataService.payment
				};		
			
			return appDataService.storageService.set( 'paypalSessionInfo', paypalSessionInfo );
		}
		
		/**
		 * fetchSetExpressCheckout
		 * This will collect the expected inbound response for SetExpressCheckout.
		 *
		 *
		 * @param  {[ type ]} data [ description ]
		 * @return {[ type ]}      [ description ]
		 */
		cartService.callSetPayPalStatus = function( token, status, callbackOk, callbackFailed ){
			// We should get the SetExpressCheckout on a GetCartSummary @status='OK'
			var requestStatus =
				{ token: token
				, status: status
				};

			appDataService.fetch
			(
				  enumsService.services.SET_PAYPAL_STATUS
				, false
				, requestStatus
				, true
			).then
			(
				function( data ){

					if ( data.status == enumsService.status.OK ){
						if ( typeof callbackOk == 'function' ){
							callbackOk( token );
						} else {
						}
					} else if ( data.status == enumsService.status.FAILED ){
						// @TODO: disable the paypal option, display modal error message, and route to paymentMethod on click in modal
						if ( typeof callbackFailed == 'function' ){
							callbackFailed();
						} else {
						}
					} else {
					}
				}
			);
		}
		
		/**
		 * PAYPAL SPECIFIC.
		 */
		cartService.callGetExpressCheckoutDetails = function( token, merchantDetailsData, callbackResponseOk, callbackResponseFailed ){
			appDataService.fetch
			(
				  services.GET_EXPRESS_CHECKOUT_DETAILS
				, false
				, { token: token }
				, true
			).then
			(
				function( data ){
					if ( data.status == status.OK ){
						callbackResponseOk( data );
					} else {
						if ( typeof callbackResponseFailed == 'function' ){
							callbackResponseFailed();
						} else {
						}
					}
				}
			);
		}

		/**
		 * Take the response from GetExpressCheckoutDetails and update the billing
		 * in appDataService.
		 */
		cartService.updatePurchaseDataWithPayPal = function(  merchantDetailsData, data ){
			// Create the data object we will return.
			var myData = {};
			
			// Create the paypal object.
			var myPayPal =
				{ payer_id: cartService.paypal.payer_id
				, paypal_id: data.paypal_id
				, token : cartService.paypal.token
				, valid : true
				};
			
			myData.paypalPurchase = myPayPal;

			 // Define the shipping information.
			 // based on appDataService.shipping
			 // TODO: determine the better solution. for now, we'll define mobile as the selected option for now until the next iteration.

			var myShipping =
				{ deliveryMethod: 'M'
				, deliveryAmount: '0.00'
				, valid      : true
				};

			myData.shipping = myShipping;

			 // Define the billing information from PayPal.
			 // based on appDataService.billing;
			var myBilling =
				{ firstName: data.BUYER_INFO.first_name
				, lastName: data.BUYER_INFO.last_name
				, email   : data.BUYER_INFO.email
				, emailVerify: data.BUYER_INFO.email
				, country : data.SHIPPING_INFO.country
				, address : data.SHIPPING_INFO.street
				, address2: data.SHIPPING_INFO.street2
				, city    : data.SHIPPING_INFO.city
				, state   : data.SHIPPING_INFO.state
				, zip     : data.SHIPPING_INFO.zip
				, phone   : data.BUYER_INFO.phone_number
				, mobile  : data.SHIPPING_INFO.phone
				, valid   : true
				};

			myData.billing = myBilling;

			// Get the billing details for the payment.
			// Specific to paypal
			var paymentBillingDetails = cartService.getBillingDetailsByName( merchantDetailsData, 'PayPal' );
			//  cvv2="null" card_expire_date="" account_number="null" billing_id="201" billing_type="PPA" auth_type="PPA" cepay_merchant_id="null" cardholder_name="Dev, Accesso " amount="32.98"/>
			/**
			 * @todo, shipMethod will need to change when we go to tablet and desktop.
			 * @todo, shipAmount will need to change when we go to tablet and desktop.
			 * @todo, valid may need to change when shipMethod and shipAmount work with tablet and desktop.
			 */

			/**
			 * Define the Payment Information.
			 * based on appDataService.payment
			 */
			var myPayment =
				{ securityCode   : 'null'
				, cardExpirationDate: ''
				, number         : 'null'
				, billingId      : paymentBillingDetails.billing_id
				, billingType    : paymentBillingDetails.billing_type
				, authType       : paymentBillingDetails.auth_type
				, cepayMerchantId: paymentBillingDetails.cepay_merchant_id
				, shipMethod     : myData.shipping.deliveryMethod
				, shipAmount     : myData.shipping.deliveryAmount
				, valid          : true
				};

			myData.payment = myPayment;

			/**
			 * Return the data object.
			 */
			return myData;
		}

		/**
		 * We now need to call UpdateCartCheckoutInfo, to tell the backend we have a PayPal order.
		 * YOU NEED TO ADD IN THE skip_requests='SetExpressCheckout'!!!, otherwise, a new token will
		 * be generated, replacing our existing token, something we DO NOT WANT at this stage.
		 * The current token is the last token we want, unless the user cancels and starts over again.
		 *
		 * If SetExpressCheckout gets called, you will LOSE the working token on the server side
		 * associated to the cart. It will get replaced with a new token, which we don't want at this time.
		 *
		 * UpdateCartCheckoutInfo calls GetCartSummary, which in turn calls SetExpressCheckout.
		 * that is why we will add the attribute of `skip_requests` to our call.
		 */
		cartService.callUpdateCartCheckoutInfo = function( requestData, callbackResponseOk, callbackResponseFailed  ){
			appDataService.fetch
			(
				  services.UPDATE_CART_CHECKOUT_INFO
				, false
				, requestData
			).then
			(
				function( data ){
					if( data.status == status.OK ){
						//$scope.routeTo( $rootScope.routes.REVIEW );
						if ( typeof callbackResponseOk == 'function' ){
							callbackResponseOk();
						} else {
						}
					} else if( data.status == status.FAILED ){
						// The request to updateCartItem failed.
						if ( typeof callbackResponseFailed == 'function' ){
							callbackResponseFailed();
						}
					}
				}
			);
		}
			
		cartService.updatePaypal = function( data ){
			if ( typeof data.token != 'undefined' && data.token != '' ){
				cartService.paypal.token = data.token
			}
			if ( typeof data.id != 'undefined' && data.id != '' ){
				cartService.paypal.id = data.id
			}
		}

		cartService.getBillingDetailsByName = function( merchantDetailsData , name ){
			if ( typeof merchantDetailsData.BILLINGTYPE != 'undefined' && merchantDetailsData.BILLINGTYPE ){
				for ( var i in merchantDetailsData.BILLINGTYPE ){
					if ( merchantDetailsData.BILLINGTYPE[ i ].name == name ){
						return merchantDetailsData.BILLINGTYPE[ i ];
					}
				}
			} else {
				return false;
			}
		}
			
		cartService.getBillingDetailsById = function( merchantDetailsData , billingId ){
			if ( typeof merchantDetailsData.BILLINGTYPE != 'undefined' && merchantDetailsData.BILLINGTYPE ){
				for ( var i in merchantDetailsData.BILLINGTYPE ){
					if ( merchantDetailsData.BILLINGTYPE[ i ].billing_id == billingId ){
						return merchantDetailsData.BILLINGTYPE[ i ];
					}
				}
			} else {
				return false;
			}
		}
		
		function addCartItem(itemsToBeAdded){
			// Show the spinner.
			$rootScope.showSpinner(languageService.ApplicationLabels.updatingCartLoadMsg)

			//Make the call and update cart with items.
			cartService.addCartItems(itemsToBeAdded, function(data){
				// Hide the spinner.
				$rootScope.hideSpinner()

				if(data.status == enumsService.status.OK){
					modalService.displayModal
						(
							{ replacementKeys: 'qtyAdded'
							, replacementValues: '<span class="limit">' + qtyAdded + '</span>'
							, title: languageService.Alert.cartOrContinueShoppingTitle
							, headingFlag: modalService.SUCCESS
							, flags: [ enumsService.buttonFlags.CONTINUE_SHOPPING, enumsService.buttonFlags.PROCEED_TO_CHECKOUT ]
							, callback: function( detail ){
								if( detail == enumsService.buttonFlags.CONTINUE_SHOPPING ){
									$rootScope.routeToPackageList();
								} else if( detail == enumsService.buttonFlags.PROCEED_TO_CHECKOUT ){
									$rootScope.routeTo( $rootScope.routes.CART )
								}
							  }
							}
						);

					// Prompt for cross-sells after each successful add to cart.
					appDataService.crossSells = true;
				} else {
					// Problem adding the item to cart.
					languageService.displayErrorCode(data, 'CheckCartTicketLevelErrors')
				}
			})
		}
		
		function exceededPackageLimit( customerTypes, packageDetails ){
			var totalQtyByRate = getTotalQtyForRatesBeingAdded( customerTypes );
			
			if( totalQtyByRate > Number( packageDetails.max_quantity ) ){
				modalService.displayModal
					(
						{ replacementKeys: 'maxQty'
						, replacementValues: packageDetails.max_quantity
						, title: languageService.Alert.maxQtyTitle
						, description: languageService.Alert.maxQtyMsg
						, callback: function( detail ){
							if(detail == $rootScope.buttonFlags.YES){
								callback.call(this, true)
							}
						  }
						, flags: [ enumsService.buttonFlags.YES, enumsService.buttonFlags.NO ]
						}
					)
				
				return true;
			}
			
			return false;
		}
		
		function getTotalQtyForRatesBeingAdded( customerTypes ){
			var sum = 0;
			for(var i in customerTypes){
				var currentItem = customerTypes[ i ];
				sum += Number( currentItem.qty );
			}
			
			return sum;
		}
		
		function getItemQtyFromCart( item ){
			var qtyInCart = 0;
			for( var i = 0; i < cartService.cartItems.length; i++ ){
				var cartItem = cartService.cartItems[ i ]
				  , pkgId = cartItem.package_id
				  , pkgQty = Number( cartItem.qty )
				  , ctId = cartItem.customer_type
				  , ctName = ( cartItem.customer_type_name ) ? cartItem.customer_type_name.toLowerCase(): '';
				
				//if( getCartLimitOverride( cartItem ) != '1' && ctName != 'group' ){
					if( item.package_id == pkgId &&
						item.customer_type == ctId &&
						item.cusTypeName.toLowerCase() == ctName ){
						qtyInCart += pkgQty;
					}
				//}
			}
			
			return qtyInCart;
		}

		function showCartLimitModal(){
			// Alert user is adding a total qty that will exceed the cart limit.
			// OR... Alert user is adding a total qty that combined with cart
			// total will exceed the cart limit.
			modalService.displayModal
				(
					{ replacementKeys: 'cartLimit'
					, replacementValues: '<span class="limit">' + $rootScope.cartLimit + '</span>'
					, title: languageService.Alert.cartLimitTitle
					, description: languageService.Alert.cartLimitMsg
					}
				);
		}
		
		function cleanUpCartData( data ){
			if( data.CART && data.CART.ITEMS && data.CART.ITEMS.ITEM ){
				data.CART.ITEMS.ITEM = angular.isArray( data.CART.ITEMS.ITEM ) ? data.CART.ITEMS.ITEM: [ data.CART.ITEMS.ITEM ];
			}
		}
		
		/**
		 * Compares the items to be added with the cart Limit, making sure that this does not pass the limit of the
		 * cart not including those that are override Items
		 * @param {integer} totalLessOverideBeingAdded
		 * @param {object} cartService
		 * @returns {boolean}
		 */
		
		function getCartLimitOverride( cartItem ){
			if( typeof( cartItem.cart_limit_override ) == 'undefined' ){
				return 0;
			} else {
				return Number( cartItem.cart_limit_override );
			}
		}
		
		function addPackageCharacs( itemsToBeAdded, packageDetails ){
			var lastItem = itemsToBeAdded[ itemsToBeAdded.length - 1 ];
			
			for( var i = 0; i < lastItem.qty; i++ ){
				// Add a CHARACs object for each qty if not already there.
				lastItem.CHARACS = lastItem.CHARACS || [];
				lastItem.CHARACS[ i ] = lastItem.CHARACS[ i ] || {};
				
				// Copy retained CHARACs from the package to the item being added to cart - if they exist in the package.
				if( typeof( packageDetails.CHARACS ) != 'undefined' ){
					for( var j = 0; j < retainCharacs.length; j++ ){
						for( var key in packageDetails.CHARACS ){
							if( key == retainCharacs[ j ] ){
								lastItem.CHARACS[ i ][ key ] = packageDetails.CHARACS[ key ];
							}
						}
					}
				}
			}
		}
		
		function validateMinQty( currentItem ){
			// Ensure min qty or greater is selected for each rate type.
			if( Number( currentItem.qty ) < Number( currentItem.min_quantity ) ){
				// Alert user has less than min qty.
				modalService.displayModal
					(
						{ replacementKeys: 'minQty'
						, replacementValues: currentItem.min_quantity
						, title: languageService.Alert.minQtyTitle
						, description: languageService.Alert.minQtyMsg
						}
					);

				return false;
			}
			
			return true;
		}
		
		function validateMaxQty( currentItem ){
			// Ensure max qty is not exceeded for each rate type.
			// Account for the quantity in this cart.
			var qtyInCart = getItemQtyFromCart( currentItem );
			if( Number( currentItem.qty ) + qtyInCart > Number( currentItem.max_quantity ) ){
				// Alert user has more than max qty.
				modalService.displayModal
					(
						{ replacementKeys: 'maxQty'
						, replacementValues: currentItem.max_quantity
						, title: languageService.Alert.maxQtyTitle
						, description: languageService.Alert.maxQtyMsg
						}
					);

				return false;
			}
			
			return true;
		}
		
		function setAdvancedTicketNotification( callback ){
			if(packageService.isAdvancedTicket){
				modalService.displayModal
					(
						{ replacementKeys: 'date'
						, replacementValues: $filter( 'date' )( packageService.advanceTicketDate, 'MM/dd/yyyy' )
						, title: languageService.Alert.advancePackageTitle
						, description: languageService.Alert.advancePackageMsg
						, allback: function( detail ){
							if(detail == $rootScope.buttonFlags.YES){
								callback.call(this, true)
							}
						  }
						, flags: [ enumsService.buttonFlags.YES, enumsService.buttonFlags.NO ]
						}
					);
			} else {
				callback.call(this, false)
			}
		}
		
		/**
		 * Update the cart with items to be added, return true if successfull
		 * @param {array} itemsToBeAdded
		 * @param {boolean} callback
		 *
		 */

		function addCartItems( itemsToBeAdded ){

			$log.log('cartService >> addCartItems')

			// Show the spinner.
			$rootScope.showSpinner( languageService.ApplicationLabels.updatingCartLoadMsg )
			$rootScope.pendingRequests.push( $rootScope.services.ADD_CART_ITEM )
			appDataService.fetch
				(
					  enumsService.services.ADD_CART_ITEM
					, false
					, itemsToBeAdded
				).then
				(
					function( data ){
						$rootScope.removePendingRequest( $rootScope.services.ADD_CART_ITEM )
						if( data.status == enumsService.status.OK ){
							// ADD_CART_ITEM Response.
							$rootScope.pendingRequests.push( $rootScope.services.GET_CART_SUMMARY )
							appDataService.fetch
							(
									  enumsService.services.GET_CART_SUMMARY
									, true
									, false
									, true
							).then
							(
								function( data ){
									// Hide the spinner.
									$rootScope.hideSpinner()
									
									$rootScope.removePendingRequest( $rootScope.services.GET_CART_SUMMARY )
									cleanUpCartData( data );

									if( !cartService.checkForItemErrors( data ) ){
										if( data
											&& data.CART
											&& data.CART.ITEMS
											&& data.CART.ITEMS.ITEM
											&& !angular.isArray( data.CART.ITEMS.ITEM )
											){
											data.CART.ITEMS.ITEM = [ data.CART.ITEMS.ITEM ];
										}

										if( data.status == enumsService.status.OK ){
											// Update the cart object.
											cartService.updateInternals( data );

											// PAYPAL RELATED.
											cartService.fetchSetExpressCheckout();

											handleAddCartItemResponse( data );

										} else if( data.status == enumsService.status.FAILED ){
											// If we get an error, expect an inbound get new cart id.
											handleAddCartItemResponse( data );
										}
									}
								}
							)

						} else if( data.status == enumsService.status.FAILED ){
							// The request to updateCartItem failed.
							modalService.displayModal
								(
									{ title: languageService.Alert.cartErrorAddingItemTitle
									, description: languageService.Alert.cartErrorAddingItemMsg
									, hideAllButtons: true
									}
								);

							i18n.displayErrorCode( data );
							// Hide the spinner.
							$rootScope.hideSpinner();
						}
					}
				)
		}
		
		function handleAddCartItemResponse( data ){
			if( data.status == enumsService.status.OK ){
				modalService.displayModal
					(
						{ replacementKeys: 'qtyAdded'
						, replacementValues: '<span class="limit">'+qtyAdded+'</span>'
						, replaceInTitle: true
						, title: languageService.Alert.cartAddedItemTitle
						, description: languageService.Alert.cartOrContinueShoppingTitle
						, headerFlag: modalService.SUCCESS
						, buttonFlags: [ enumsService.buttonFlags.CONTINUE_SHOPPING, enumsService.buttonFlags.PROCEED_TO_CHECKOUT ]
						, callback: function( detail ){
							if( detail == enumsService.buttonFlags.CONTINUE_SHOPPING ){
								// Route back to package list.
								if( appDataService && appDataService.lastKeyword ){
									$rootScope.routeTo( enumsService.routes.LISTKEYWORD + '/' + appDataService.lastKeyword )
								} else {
									$rootScope.routeTo( enumsService.routes.LIST )
								}
							} else if( detail == enumsService.buttonFlags.PROCEED_TO_CHECKOUT ){
								$rootScope.routeTo( enumsService.routes.CART )
							}
						  }
						}
					);
					
				// Prompt for cross-sells after each successful add to cart.
				appDataService.crossSells = true;
			} else {
				// Problem adding the item to cart.
				languageService.displayErrorCode( data, 'CheckCartTicketLevelErrors' )
			}
		}
		
		function exceededPackageLimit( customerTypes, packageDetails ){
			var totalQtyByRate = getTotalQtyForRatesBeingAdded( customerTypes );
			
			if( totalQtyByRate > Number( packageDetails.max_quantity ) ){
				modalService.displayModal
					(
						{ replacementKeys: 'maxQty'
						, replacementValues: packageDetails.max_quantity
						, title: languageService.Alert.maxQtyTitle
						, description: languageService.Alert.maxQtyMsg
						, hideAllButtons: true
						}
					);
				
				return true;
			}
			
			return false;
		}
		
		function getTotalQtyForRatesBeingAdded( customerTypes ){
			var sum = 0;
			for( var i in customerTypes ){
				var currentItem = customerTypes[ i ];
				sum += Number( currentItem.qty );
			}
			
			return sum;
		}
		
		function getItemQtyFromCart( item ){
			var qtyInCart = 0;
			for( var i = 0; i < cartService.cartItems.length; i++ ){
				var cartItem = cartService.cartItems[ i ]
				  , pkgId = cartItem.package_id
				  , pkgQty = Number( cartItem.qty )
				  , ctId = cartItem.customer_type
				  , ctName = ( cartItem.customer_type_name ) ? cartItem.customer_type_name.toLowerCase(): '';
				
				if( item.package_id == pkgId &&
					item.customer_type == ctId &&
					item.cusTypeName.toLowerCase() == ctName ){
					qtyInCart += pkgQty;
				}
			}
			
			return qtyInCart;
		}

		function showCartLimitModal(){
			// Alert user is adding a total qty that will exceed the cart limit.
			// OR... Alert user is adding a total qty that combined with cart
			// total will exceed the cart limit.
			modalService.displayModal
				(
					{replacementKeys: 'cartLimit'
					, replacementValues: packageService.cartLimit
					, title: languageService.Alert.cartLimitTitle
					, description: languageService.Alert.cartLimitMsg
					, hideAllButtons: true
					}
				);
		}
		
		function getTotalQty(){
			var qtySum = 0
			if( cartService.cartItems && typeof( cartService.cartItems ) != 'undefined' && cartService.cartItems.length ){
				for( var i = 0; i < cartService.cartItems.length; i++ ){
					var item = cartService.cartItems[ i ]
					qtySum += Number( item.qty )
				}
			}

			return qtySum
		}
		
		// TODO: Deprecated. Remove this from here.
		// Fetch cart summary.
		appDataService.fetch
		(
			  enumsService.services.GET_CART_SUMMARY
			, true
			, false
			, true
		).then
		(
			function( data ){
				cleanUpCartData( data );
				if( !cartService.checkForItemErrors( data ) ){
					if( data.status == status.OK ){
						// Update the cart object.
						cartService.updateInternals( data );
						// Listen for SetExpressCheckout.
						cartService.fetchSetExpressCheckout();
					} else if( data.status == status.FAILED ){
						// @TODO: Do we update internals when there is a problem?
						// listen for a new cart_id/cart_key.
						cartService.fetchGetNewCartID();
					}
				}
			}
		);

		return cartService
	} ] )
