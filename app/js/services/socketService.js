/**
 * # SockJS socket management service
 *
 * Creates SockJS socket connection to server, re-connects on disconnection,
 * and exports hooks to map handlers for various data interactions.
 *
 */
angular.module('app').factory
	( 'socketService'
	, [ '$rootScope'
	  , '$log'
	  , 'enumsService'
	, function
		( $rootScope
		, $log
		, enumsService
		){
			var connectTimeStamps = [];
			var maxConnectsInTime = 10;
			var connectTimeThreshold = 600000; // 10 Minutes.
			$rootScope.maxReconnectAttempts = 10;
			
			function exceededConnectionLimits(){
				// If numOpenAttempts exceeds maxReconnectAttempts, do not proceed to attempt to connect.
				var threshLimitExceeded = false;
				if( connectTimeStamps.length >= maxConnectsInTime ){
					var timeDiff = connectTimeStamps[ connectTimeStamps.length - 1 ] - connectTimeStamps[ 0 ];
					if( timeDiff < connectTimeThreshold ){
						threshLimitExceeded = true
					} else if( connectTimeStamps.length >= maxConnectsInTime && timeDiff > connectTimeThreshold){
						// User attempted to connect >= maxConnectsInTime but in more time than connectTimeThreshold.
						// We're still good. Reset everything.
						connectTimeStamps = [];
						service.numOpenAttempts = 0;
						service.timesOpened = 0;
					}
				}
				
//				if( threshLimitExceeded || (service.numOpenAttempts > $rootScope.maxReconnectAttempts + 1) ){
//
//					modalService.displayModal
//					(
//						{ title: $rootScope.i18n.ApplicationLabels.reconnectAttemptsExceededTitle
//							, description: $rootScope.i18n.Alert.reconnectAttemptsExceededMsg
//							, callback: function(){
//							try{
//								localStorage.clear()
//							} catch(e){}
//
//							location.reload();
//							}
//						}
//					)
//
//					return true;
//				}
				
				// Keeep track of number of times the socket is attempting to open.
				service.numOpenAttempts++;
				
				return false;
			}
			
			var createSocket = function(){
				if( exceededConnectionLimits() ) {
					return;
				}
				
				// Get reference to port.
				var port = (location.port != 80) ? ':' + location.port : '';

				socket = new SockJS
					( '//' + document.domain + '' + port + '/api'
					, ''
					, { 'debug':true
						, 'devel' : true
						, 'protocols_whitelist':
							[ 'xdr-streaming'
							, 'xdr-polling'
							, 'xhr-streaming'
							, 'iframe-eventsource'
							, 'iframe-htmlfile'
							, 'xhr-polling'
							, 'websocket'
							]
					  }
					);
				
/**
* ## Data interaction hooks
*
* Passes off core SockJS data interaction hooks to rest of application so
* callbacks can be cleanly defined externally for each event.
*/
				socket.onopen = function(){
					var args = arguments;
					service.open = true;
					service.timesOpened++;
					// Attempted to connect. Note timestamp.
					connectTimeStamps.push( new Date().getTime() );
					
					$rootScope.$broadcast( enumsService.socket.open );

					if( service.handlers.onopen ){
						$rootScope.$apply
							( function(){
								service.handlers.onopen.apply( socket, args )
							  }
							)
					}
				}
				
				socket.onmessage = function( data ){
					var args = arguments;
					try{
						args[0].data = JSON.parse(args[0].data);
					} catch(e){
						// 
					}

					if( service.handlers.onmessage ){
						$rootScope.$apply(
							function(){
								service.handlers.onmessage.apply(socket, args);
							}
						)
					}
				}
				
				socket.onclose = function(){
					service.open = false;
					setTimeout( function(){ socket = createSocket(service); } , 3000 );
					var args = arguments;
					$rootScope.$broadcast(enumsService.socket.close);
					
					if( service.handlers.onclose ){
						$rootScope.$apply(
							function(){
								service.handlers.onclose.apply(socket,args);
							}
						)
					}
				}
				
				return socket;
			}
			
			var service =
				{ handlers : {}
				, onopen:
					function( callback ){
						this.handlers.onopen = callback;
					}
				, onmessage:
					function( callback ){
						this.handlers.onmessage = callback;
					}
				, onclose:
					function( callback ){
						this.handlers.onclose = callback;
					}
				, send:
					function( data ){
						var msg = typeof(data) == "object" ? JSON.stringify(data) : data;
						var status = socket.send(msg);
					}
				, open: false
				, timesOpened: 0
				, numOpenAttempts: 0
				};
			
			var socket = createSocket();
			return service;
		}
	]
);