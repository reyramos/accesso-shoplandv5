/**
 * # Language definitions service
 *
 * All user-facing text should reference a key provided by this service. That
 * key is then set to corresponding text in the language defined in
 * clientInfoService.
 *
 */
angular.module('app').factory
	( 'languageService',[
		'$rootScope'
		, '$locale'
		, '$log'
		, 'enumsService'
		, 'appDataService'
	, function
		( $rootScope
		, $locale
		, $log
		, enumsService
		, appDataService
		){

			var socket = enumsService.socket
			var status = enumsService.status
			var services = enumsService.services
			var languageData = {};

			languageData.currentYear = new Date().getFullYear();
			languageData.currentMonth = new Date().getMonth() + 1;
			languageData.months = $locale.DATETIME_FORMATS.MONTH;
			
			if(true){
				// Default Error Codes Locale Entries.
				languageData.ErrorCodes = languageData.ErrorCodes || {};
				languageData.ErrorCodes.errorCodeTitle = "Error in Items Added";
				languageData.ErrorCodes[401] = 'Cart exceeded max number of tickets.';
	
				languageData.ErrorCodes[1000] = 'Sorry, your browser is unsupported. We recommend one of the following:' // errors/unsupportedBrowser.html // links to app store.;
				languageData.ErrorCodes[1001] = 'Sorry, the application failed to load. Either your connection to the internet is not working, or the store is unavailable at this time.' // errors/failedToStart.html  // initializeSocket failed.;
				languageData.ErrorCodes[1002] = 'Sorry, you did not specify a merchant.'    // errors/missingMerchant.html // a merchant string was not supplied. unknownMerchant;
				languageData.ErrorCodes[1003] = 'Sorry, the merchant you specified is unknown.' // errors/unknownMerchant.html // the merchant specified could not be found.;
				languageData.ErrorCodes[1004] = 'Sorry, the page you requested does not exist.' // errors/unknownPage.html   // the page requested does not exist.;
	
				// Default Error Codes Locale Entries.
				languageData.CheckCartTicketLevelErrors = languageData.CheckCartTicketLevelErrors || {};
				languageData.CheckCartTicketLevelErrors.errorCodeTitle = 'Errors With Items Added';
				languageData.CheckCartTicketLevelErrors.errorCodeLabel = 'Error Code @@errorCode';
				languageData.CheckCartTicketLevelErrors[1] = 'There was a problem reserving the selected date. Please try again. If you continue to experience problems, please contact our support line at @@supportPhoneNumber.';
				languageData.CheckCartTicketLevelErrors[375] = 'Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again. If you continue to have issues, please contact Online Ticketing Technical Support at @@supportPhoneNumber and inform the Agent that the Comp Parent is not valid.';
				languageData.CheckCartTicketLevelErrors[451] = 'Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again. If you continue to have issues, please contact Online Ticketing Technical Support at @@supportPhoneNumber and inform the Agent that the Package can\'t be found.';
				languageData.CheckCartTicketLevelErrors[452] = 'Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again. If you continue to have issues, please contact Online Ticketing Technical Support at @@supportPhoneNumber and inform the Agent that the Customer Type can\'t be found.';
				languageData.CheckCartTicketLevelErrors[460] = 'Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again. If you continue to have issues, please contact Online Ticketing Technical Support at @@supportPhoneNumber and inform the Agent that the Addon is not valid.';
				languageData.CheckCartTicketLevelErrors[465] = 'We\'re sorry, there was an issue with the quantity of items in your cart. Please check your cart to verify there are not duplicate items or a quantity that exceeds the maximum items allowed per order. Please contact our Online Technical Support at @@supportPhoneNumber.';
				languageData.CheckCartTicketLevelErrors[470] = 'Could not add to cart. Code Missing.';
				languageData.CheckCartTicketLevelErrors[471] = 'Could not add to cart. Code Invalid.';
				languageData.CheckCartTicketLevelErrors[472] = 'Could not add to cart. Code usage limit exceeded.';
				languageData.CheckCartTicketLevelErrors[476] = 'We\'re sorry, there was a problem adding an item to your cart. You have entered an invalid quantity. Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again.';
				languageData.CheckCartTicketLevelErrors[477] = 'Comp No dates found on base ticket for comp 154275.';
				languageData.CheckCartTicketLevelErrors[480] = 'Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again. If you continue to have issues, please contact Online Ticketing Technical Support at @@supportPhoneNumber and inform the Agent that the Event is missing capacity information.';
				languageData.CheckCartTicketLevelErrors[481] = 'Could not add to cart. Item is Sold Out.';
				languageData.CheckCartTicketLevelErrors[482] = 'This pass has already been renewed for this season. Please process your order as a new pass.';
				languageData.CheckCartTicketLevelErrors[484] = 'This group package has a minimum quantity of <b>@@minimum</b>, but you\'ve only selected <b>@@found</b>. <br/><br/><b>Adult and Jr/Sr tickets can be combined to reach the minimum quantity of @@minimum. </b><br/><br/> Please add additional tickets to complete your order.';
				languageData.CheckCartTicketLevelErrors[485] = 'This item cannot be added to your cart at this time. Only items from the same park vendor can be purchased with each order.';
				languageData.CheckCartTicketLevelErrors[486] = 'Package is no longer Valid for Purchase.';
				languageData.CheckCartTicketLevelErrors.defaultError = 'We\'re sorry, there was a problem adding an item to your cart. Please check the Shopping Cart in the top right corner of your screen and ensure that the cart has been emptied. Please try your purchase again.';
	
				// Default Application Labels Locale Entries.
				languageData.ApplicationLabels = languageData.ApplicationLabels || {};
				languageData.ApplicationLabels.loadingMsg = 'Loading Please Wait';
				languageData.ApplicationLabels.purchaseLoadMsg = 'Processing Order';
				languageData.ApplicationLabels.orderSearchLoadMsg = 'Searching for Order';
				languageData.ApplicationLabels.loadingTicketsLoadMsg = 'Loading Ticket Information';
				languageData.ApplicationLabels.searchingForPromoMsg = 'Searching for Promo Code';
				languageData.ApplicationLabels.updatingPaymentInfoLoadMsg = 'Updating Payment Information';
				languageData.ApplicationLabels.updatingCartLoadMsg = 'Updating Cart';
				languageData.ApplicationLabels.gettingHelpInfoLoadMsg = 'Fetching Help Information';
				languageData.ApplicationLabels.gettingEventDates = 'Getting Event Dates';
				languageData.ApplicationLabels.privacyStatement = 'Privacy Statement';
				languageData.ApplicationLabels.connectionLost = 'Application Offline. Reconnecting';
				languageData.ApplicationLabels.help = 'Help';
				languageData.ApplicationLabels.offlineTitle = 'Application Offline';
				languageData.ApplicationLabels.offlinePreResponseMsg = 'The application went off line while a request was made to our servers.';
				languageData.ApplicationLabels.offlinePrePurchaseResponse = 'If you attempted a purchase, please check your email for your receipt before attmepting to purchase again otherwise you may be charged twice.';
				languageData.ApplicationLabels.offlineRefreshMsg = 'Please refresh the application before attempting to proceed.';
				languageData.ApplicationLabels.redirectingToPayPal = 'Redirecting to PayPal';
	
				// Default Alert Locale Entries.
				languageData.Alert = languageData.Alert || {};
				languageData.Alert.advancePackageTitle = 'Advance Purchase Notice';
				languageData.Alert.advancePackageMsg = 'You have chosen to purchase an Advance Purchase Ticket. The barcode will be valid from @@date. Please be sure to review the park\'s hours of operations. Would you like to proceed with this ticket?';
				//languageData.Alert.advancedPurchaseTitle = 'To continue your purchase, you must agree to the following:';
				//languageData.Alert.advancedPurchaseMsg = 'You have chosen to purchase an Advance Purchase Ticket. The barcode(s) will be activated @@numDaysInAdavanced @@[numDaysInAdavanced, day, days] post purchase. The first day the ticket can be used and the park is open is @@firstAvailableDate. Click the box to agree.';
				languageData.Alert.cartHeaderString = 'Items';
				languageData.Alert.cartEmptyTitle = '';
				languageData.Alert.cartEmptyMsg = 'Your Cart is Empty.';
				languageData.Alert.cartLimitTitle = 'Cart Limit Error';
				languageData.Alert.cartLimitMsg = 'There is a limit of <span class="limit">@@cartLimit</span> @@[cartLimit, item, items] per transaction. Please adjust the quantity you have selected.';
				languageData.Alert.cartAddedItemTitle = 'Items Added to Cart';
				languageData.Alert.cartOrContinueShoppingTitle = '<span class="limit">@@qtyAdded</span> @@[qtyAdded, Item, Items] have been successfully added to your cart';
				//languageData.Alert.cartOrContinueShoppingMsg = 'Please click Continue Shopping or View Cart to proceed.';
				languageData.Alert.cartErrorAddingItemTitle = 'Add to Cart Error';
				languageData.Alert.cartErrorAddingItemMsg = 'We apologize for the inconvenience but there was a problem adding the item to the cart. Please try again.';
				languageData.Alert.cartErrorUpdatingItemTitle = 'Update Cart Error';
				languageData.Alert.cartErrorUpdatingItemMsg = 'We apologize for the inconvenience but there was a problem updating your cart. Please try again.';
				languageData.Alert.cartNoQtySelectedTitle = 'Select Quantity';
				languageData.Alert.cartNoQtySelectedMsg = 'Please select a quantity before proceeding.';
				languageData.Alert.deleteItemTitle = 'Confirm Delete Item';
				languageData.Alert.deleteItemMsg = 'Would you like to remove the item from your cart?';
				languageData.Alert.failedPurchaseTitle = 'Order Request Failed';
				languageData.Alert.failedPurchaseMsg = 'There was a problem completing your order. Please verify your billing and payment information is correct.';
				languageData.Alert.failedUpdateCheckoutInfoTitle = 'Error Updating Information';
				languageData.Alert.failedUpdateCheckoutInfoMsg = 'There was a problem updating your billing information. Please check your data and try resubmitting the form.';
				languageData.Alert.generalErrorTitle = 'Error';
				languageData.Alert.generalErrorMsg = 'There was an error processing your request. Please try again.';
				//languageData.Alert.itemsAddedTitle = '@@qty @@[qty, item, items] added to cart.';
				//languageData.Alert.itemsAddedMsg = '';
				languageData.Alert.minQtyTitle = 'Minimum Quantity Error';
				languageData.Alert.minQtyMsg = 'This package has a minimum quantity of @@minQty. Please ensure you have selected at least @@minQty @@[minQty, item, items] before proceeding.';
				languageData.Alert.maxQtyTitle = 'Maximum Quantity Error';
				languageData.Alert.maxQtyMsg = 'This package has a maximum quantity of @@maxQty. Please ensure you have selected a total (items in your cart plus items you are adding) less than or equal to @@maxQty @@[maxQty, item, items] before proceeding.';
				languageData.Alert.noOrdersFoundTitle = 'No Orders Found';
				languageData.Alert.noOrdersFoundMsg = 'There were no orders found with the information provided. Please check your information and try again.';
				languageData.Alert.whatIsCVVTitle = 'What is the Card Security Code?';
				//languageData.Alert.whatIsCVVMsg = '';
				languageData.Alert.noItemsTitle = 'Your Cart is Empty';
				languageData.Alert.noItemsMsg = 'Your Cart is Empty';
				languageData.Alert.billingInfoNotSetTitle = 'Missing Information';
				languageData.Alert.billingInfoNotSetMsg = 'You\'re missing the billing information required to process your order. Please enter required data.';
				languageData.Alert.shippingInfoNotSetTitle = 'Missing Information';
				languageData.Alert.shippingInfoNotSetMsg = 'You\'re missing the delivery information required to process your order. Please enter required data.';
				languageData.Alert.paymentInfoNotSetTitle = 'Missing Information';
				languageData.Alert.paymentInfoNotSetMsg = 'You\'re missing the payment information required to process your order. Please enter required data.';
				languageData.Alert.loadFAQErrorTitle = 'Failed to Load FAQ';
				languageData.Alert.loadFAQErrorMsg = 'There was a problem loading the help and FAQ\'s. Please try again.';
				languageData.Alert.loadTicketErrorTitle = 'Failed to Get Ticket Information';
				languageData.Alert.loadTicketErrorMsg = 'There was a problem retrieving your tickets. Please try again.';
				languageData.Alert.loadReceiptErrorTitle = 'Failed to Get Receipt Details';
				languageData.Alert.loadReceiptErrorMsg = 'There was a problem retrieving your receipts. Please try again.';
				languageData.Alert.missingPromoErrorTitle = 'Missing Promotional Code';
				languageData.Alert.missingPromoErrorMsg = 'Please enter a promotional code before proceeding.';
				languageData.Alert.errorWithPayPalTitle = 'An Error with PayPal';
				languageData.Alert.errorWithPayPalMsg = 'A problem with creating link, PayPal Checkout is unavailable at this time.';
				languageData.Alert.paypalCompleteUrlMsg = 'Updating your cart to include your PayPal information.';
				languageData.Alert.paypalCancelledUrlMsg = 'Cancelling Your PayPal Method, Returning you to the Store.';
				languageData.Alert.reconnectAttemptsExceededTitle = 'Exceeded Reconnect Attempts';
				languageData.Alert.reconnectAttemptsExceededMsg = 'The application attempted to reconnect more than the allowed times. For a better user experience, please visit us again when you have a more consistent Internet connection.<br /><br /> Click OK to refresh your browser.';
				languageData.Alert.internationalShippingErrorTitle = 'No International Delivery';
				languageData.Alert.internationalShippingErrorMsg = 'We currently do not support international delivery. Please chose an alternate delivery method.';
					
				// Default Form Labels  Locale Entries.
				languageData.FormLabels = languageData.FormLabels || {};
				languageData.FormLabels.firstName = 'First Name';
				languageData.FormLabels.lastName = 'Last Name';
				languageData.FormLabels.country = 'Country';
				languageData.FormLabels.streetAddress = 'Street Address';
				languageData.FormLabels.streetAddress2 = 'Apt/Suite';
				languageData.FormLabels.city = 'City/Town';
				languageData.FormLabels.state = 'State/Province';
				languageData.FormLabels.zip = 'Zip/Postal Code';
				languageData.FormLabels.phone = 'Phone Number';
				languageData.FormLabels.mobile = 'Mobile Number';
				languageData.FormLabels.email = 'Email Address';
				languageData.FormLabels.confirmEmail = 'Confirm Email';
				languageData.FormLabels.paymentType = 'Payment Type';
				languageData.FormLabels.cardNumber = 'Card Number';
				languageData.FormLabels.securityCode = 'CVC';
				languageData.FormLabels.expiration = 'Expiration Date';
				languageData.FormLabels.expMonth = 'Month';
				languageData.FormLabels.expYear = 'Year';
				languageData.FormLabels.issueNumber = 'Issue Number';
				languageData.FormLabels.startDate = 'Start Date';
				languageData.FormLabels.startMonth = 'Month';
				languageData.FormLabels.startYear = 'Year';
				languageData.FormLabels.last4Digits = 'Last 4 digits of Card Number';
				languageData.FormLabels.selectTime = 'Select Time';
				languageData.FormLabels.at = 'at';
				languageData.FormLabels.deliveryMethod = 'Delivery Method';
				languageData.FormLabels.yes = 'Yes';
				languageData.FormLabels.no = 'No';
	
				// Billing Form Errors.
				languageData.FormValidation = languageData.FormValidation || {};
				languageData.FormValidation.nameRequiredError = 'Name must be at least 1 character.';
				languageData.FormValidation.firstNameRequiredError = 'First name must be at least 1 character.';
				languageData.FormValidation.firstNamePatternError = 'First name can only contain letters.';
				languageData.FormValidation.lastNameRequiredEerror = 'Last name must be at least 1 character.';
				languageData.FormValidation.lastNamePatternError = 'Last name can only contain letters.';
				languageData.FormValidation.countryRequiredError = 'You must specify a country.';
				languageData.FormValidation.addressRequiredError = 'Address is required.';
				languageData.FormValidation.cityRequiredError = 'City is required.';
				languageData.FormValidation.stateRequiredError = 'You must specify a state/province.';
				languageData.FormValidation.zipRequiredError = 'Zip/Postal code is required.';
				languageData.FormValidation.zipPatternEerror = 'Zip/Postal code must contain at least 5 characters.';
				languageData.FormValidation.phoneRequiredError = 'Phone number is required.';
				languageData.FormValidation.phonePatternError = 'Phone number must contain only numbers.';
				languageData.FormValidation.phoneMinLengthError = 'Phone number must contain at least 10 digits.';
				languageData.FormValidation.emailRequiredError = 'Email address is required.';
				languageData.FormValidation.emailPatternError = 'Email address is not valid.';
				languageData.FormValidation.emailConfirmationMatchError = 'Email addresses must match.';
				languageData.FormValidation.deliveryMethodRequiredError = 'Delivery method is required.';
	
				// Payment Form Errors.
				languageData.FormValidation.paymentTypeRequiredError = 'Select a payment type.';
				languageData.FormValidation.ccMinMaxLengthError = 'Credit card must be 12-19 digits.';
				languageData.FormValidation.ccMustBeValidType = 'Credit card must be a valid Amex, Visa, Discover, or Master Card.';
				languageData.FormValidation.ccRequiredError = 'Credit card required.';
				languageData.FormValidation.cvvMinMaxError = 'Security code must be 3-4 digits.';
				languageData.FormValidation.cvvRequiredError = 'Security code required.';
				languageData.FormValidation.invalidExpDateError = 'Provided expiration date is invalid.';
				languageData.FormValidation.expMonthRequiredError = 'Expiration month required.';
				languageData.FormValidation.expYearRequiredError = 'Expiration year required.';
	
				// Order Lookup Form Errors.
				languageData.FormValidation.last4DigitsNumberOnly = 'Last 4 digits of card number can only contain digits.';
				languageData.FormValidation.last4DigitsLength = 'Last 4 digits of card number must be 4 digits.';
				languageData.FormValidation.last4DigitsRequired = 'Last 4 digits of card number required.';
				languageData.FormValidation.phoneNumberMinLength = 'Phone number must be at least 10 digits.';
				languageData.FormValidation.phoneNumberRequired = 'Phone Number required.';

				//datetime form validation
				languageData.FormValidation.dateRequired = 'You must specify a time.';
	
				// Default Button Labels Locale Entries.
				languageData.ButtonLabels = languageData.ButtonLabels || {};
				languageData.ButtonLabels.addToCart = 'Add To Cart';
				languageData.ButtonLabels.notEditable = 'Not Editable';
				languageData.ButtonLabels.next = 'Next';
				languageData.ButtonLabels.details = 'Details';
				languageData.ButtonLabels.update = 'Update';
				languageData.ButtonLabels.viewDetails = 'View Details';
				languageData.ButtonLabels.backButton = 'Back';
				languageData.ButtonLabels.ok = 'OK';
				languageData.ButtonLabels.cancel = 'Cancel';
				languageData.ButtonLabels.verify = 'Verify';
				languageData.ButtonLabels.checkout = 'Checkout';
				languageData.ButtonLabels.purchase = 'Complete Your Order';
				languageData.ButtonLabels.continueButton = 'Continue';
				languageData.ButtonLabels.continueCart = 'Proceed to cart';
				languageData.ButtonLabels.continueShopping = 'Continue Shopping';
				languageData.ButtonLabels.edit = 'Edit';
				languageData.ButtonLabels.yes = 'Yes';
				languageData.ButtonLabels.no = 'No';
				languageData.ButtonLabels.info = 'Info';
				languageData.ButtonLabels.rates = 'Rates';
				languageData.ButtonLabels.proceedToCheckout = 'Proceed to Checkout';
				languageData.ButtonLabels.proceedToCart = 'Proceed to Cart';
				languageData.ButtonLabels.searchButton = 'Search Orders';
				//languageData.ButtonLabels.submitButton = 'Submit';
				languageData.ButtonLabels.question = '?';
				languageData.ButtonLabels.viewReceipt = 'View Receipt';
				languageData.ButtonLabels.viewTicket = 'View Tickets';
				languageData.ButtonLabels.passbook = 'Add to Passbook';
				languageData.ButtonLabels.reloadTickets = 'Reload Tickets';
				languageData.ButtonLabels.reloadReceipt = 'Reload Receipt';
				languageData.ButtonLabels.noThanks = 'No Thanks';
				languageData.ButtonLabels.update = 'Update';
				languageData.ButtonLabels.seeAvailableRides = 'See the Available Rides';
				languageData.ButtonLabels.buyFlashPassNow = 'Buy a Flash Pass Now!';
				languageData.ButtonLabels.getStartedWithFlashPass = 'Get Started With Flash Pass';
				languageData.ButtonLabels.addGroup = 'Add a Group';
				languageData.ButtonLabels.placeNewOrder = 'Place a new order';

				// Default Menu Locale Entries.
				languageData.Menu = languageData.Menu || {};
				languageData.Menu.promoPlaceholder = 'Enter Promotional Code';
				languageData.Menu.keywordHeading = 'Products';
				languageData.Menu.linkHeading = 'Social Networking';
				languageData.Menu.settingHeading = 'Settings';
				languageData.Menu.title = 'Menu';
				languageData.Menu.help = 'FAQs';
				languageData.Menu.orderLookup = 'Order Lookup';
	
				// Default Package List Locale Entries.
				languageData.PackageList = languageData.PackageList || {}
				languageData.PackageList.startingLabel = 'Starting At'
				languageData.PackageList.buyLabel = 'Buy Now'
				languageData.PackageList.noPackagesInKeyword = 'No Packages Found'
				languageData.PackageList.disclaimer = 'Please visit our full website for our complete ticket selection.'

				// Default Package Details Locale Entries.
				languageData.PackageDetails = languageData.PackageDetails || {};
				languageData.PackageDetails.rateTypeHeading = 'Available Rates';
				languageData.PackageDetails.packageDetailHeading = 'Details';
				languageData.PackageDetails.passbookBadge = 'Passbook Enabled';
				
				// Default CrossSells Locale Entries.
				languageData.CrossSells = {};
				languageData.CrossSells.heading = 'You may also be interested in the following product(s)';
	
				// Default CartView Locale Entries.
				languageData.CartView = languageData.CartView || {};
				languageData.CartView.changeQuantityLabel = 'Change Quantity';
				languageData.CartView.cartHeading = 'Shopping Cart';
				languageData.CartView.orderReviewHeading = 'Review your order';
				languageData.CartView.receiptViewHeading = 'Your Receipt';
				languageData.CartView.qtyLabel = 'Qty';
				languageData.CartView.qtyXLabel = ': ';
				languageData.CartView.feeLabel = 'Taxes & Fees';
				languageData.CartView.taxLabel = 'Tax';
				languageData.CartView.discountLabel = 'Discounts';
				languageData.CartView.totalLabel = 'Total';
				languageData.CartView.subtotal = 'Subtotal';
				languageData.CartView.notEditable = 'Not Editable';
				languageData.CartView.cartAdjustment1 = 'Processing Fee';
				languageData.CartView.cartAdjustment2 = '5% Discover Card Savings';
				languageData.CartView.fee1 = 'Service Fee';
				languageData.CartView.fee2 = 'Amusement Fee';
				languageData.CartView.fee3 = 'Processing Fee';
				languageData.CartView.fee4 = 'City Fee';
				languageData.CartView.fee5 = 'Transaction Fee';
				languageData.CartView.shipping = 'Delivery';
				languageData.CartView.deliveryFee = 'Delivery Fee';
				languageData.CartView.free = 'Free';
				languageData.CartView.agreement = 'I agree to the <a href="event:handleShowTerms">terms and conditions</a>.';
				languageData.CartView.termsTitle = 'Terms and Conditions';
				languageData.CartView.date = 'Date:';
				languageData.CartView.time = 'Time:';
				languageData.CartView.dateTime = 'Date\\Time:';
				languageData.CartView.termsMsg = '';
				languageData.CartView.paymentMethodHeading = 'Payment Method';
				languageData.CartView.paypalAccountLabel = 'PayPal Account';
				languageData.CartView.changeQuantityLabel = 'Change Quantity';

				// Default PaymentView Locale Entries.
				languageData.PaymentView = languageData.PaymentView || {};
				languageData.PaymentView.paymentHeading = 'Payment Method';
				
				// Default PaymentMethodView Locale Entries.
				languageData.PaymentMethodView = languageData.PaymentMethodView || {};
				languageData.PaymentMethodView.selectPaymentMethodHeading = 'Select a Payment Method';
				languageData.PaymentMethodView.paypalLabel = 'PayPal Checkout';
				languageData.PaymentMethodView.creditCardlabel = 'Credit Card';
				languageData.PaymentMethodView.debitCreditCardlabel = 'Debit / Credit Card';
				languageData.PaymentMethodView.giftCardlabel = 'Gift Card';
				languageData.PaymentMethodView.payWithPayPal = 'Pay with PayPal&#8482;';
				languageData.PaymentMethodView.payWithCC = 'Pay with credit card';

				// Default labels for steps process for Shipping/Billing/Review
				languageData.PaymentProcess = languageData.PaymentProcess || {};
				languageData.PaymentProcess.shippingLabel = 'Shipping';
				languageData.PaymentProcess.billingLabel = 'Billing';
				languageData.PaymentProcess.reviewLabel = 'Review';


				// Default BillingView Locale Entries.
				languageData.BillingView = languageData.BillingView || {};
				languageData.BillingView.billingHeading = 'Billing Information';
				languageData.BillingView.optin1 = 'Sign me up for e-news from my local park.';
				languageData.BillingView.optin2 = 'Please send me information and discounts from your partners and sponsors.';
				languageData.BillingView.addressSameAsShipping = 'Use my Delivery Address.';
				
				// Default ShippingView Locale Entries.
				languageData.ShippingView = languageData.ShippingView || {};
				languageData.ShippingView.shippingHeading = 'Shipping Information';
				languageData.ShippingView.uDescription = 'First Class Mail';
				languageData.ShippingView.uLongDescription = 'Allow 14 Business Days';
				languageData.ShippingView.mDescription = 'Mobile';
				languageData.ShippingView.mLongDescription = 'Shipping Method for Mobile Stores';
				languageData.ShippingView.yDescription = 'Will Call';
				languageData.ShippingView.yLongDescription = 'Pick-up your tickets at the gate';
				languageData.ShippingView.pDescription = 'Print@Home';
				languageData.ShippingView.pLongDescription = 'Print@Home Now';
				languageData.ShippingView.optinLabel = 'Opt-ins';
				languageData.ShippingView.priceLabel = 'Free';
				// languageData.ShippingView.optin1 = 'Yes, I would like to receive the APIMC HTML email newsletter (we respect your privacy).';
				// languageData.ShippingView.optin2 = 'Yes, I would like to receive mobile TEXT messages (tournament week only, no more than 3 per day).';
				// languageData.ShippingView.optin3 = 'Yes, I would like to receive promotional mailings.';
				
				// Default OrderView Locale Entries.
				languageData.OrderView = languageData.OrderView || {};
				languageData.OrderView.reviewHeading = 'Order Review';
	
				// Default ReceiptView Locale Entries.
				languageData.ReceiptView = languageData.ReceiptView || {};
				languageData.ReceiptView.receiptMessage = 'Thank you for your purchase!';
				languageData.ReceiptView.problemLoadingReceipt = 'There was a problem loading your receipts.';
	
				// Default TicketView Locale Entries.
				languageData.TicketView = languageData.TicketView || {};
				languageData.TicketView.orderLabel = 'Order # ';
				languageData.TicketView.linkingLabel = ' of ';
				languageData.TicketView.receiptLabel = 'Receipt';
				languageData.TicketView.ticketLabel = 'Ticket(s)';
				languageData.TicketView.eventLabel = 'Valid Starting: ';
				languageData.TicketView.barcodeLabel = 'ID ';
				languageData.TicketView.instructions = 'Instructions:';
				languageData.TicketView.moreInfo = 'More Info:';
				languageData.TicketView.additionalInfo = 'Additional Info:';
				languageData.TicketView.terms = 'Terms and Conditions:';
				languageData.TicketView.disclaimer = 'Disclaimer:';
				languageData.TicketView.ticketNameHeading = 'Ticket Holder Names';
				languageData.TicketView.ticketNameTitle = 'Tickets Must Have Names to be Valid';
				languageData.TicketView.ticketNameCopy = 'Use last name for all';
				languageData.TicketView.ticketNameSubmit = 'Register Ticket';
				languageData.TicketView.ticketNameDescription = 'Please enter a ticket holder name for each of your purchased tickets below.';
				languageData.TicketView.customerFirstName = 'First Name';
				languageData.TicketView.customerLastName = 'Last Name';
				languageData.TicketView.confirmationNumber = 'Confirmation Number';
				languageData.TicketView.willCallOrderMessageTitle = 'Will Call Order Information';
				languageData.TicketView.willCallOrderMessage = 'This is a Will Call order. Please bring receipt and your ID to the Will Call at Edwin Watts, 7501 Turkey Lake Road, Orlando FL 32819 to pick-up your tickets.';
				languageData.TicketView.firstClassOrderMessageTitle = 'First Class Mail Order Information';
				languageData.TicketView.firstClassOrderMessage = '<b>This is not your ticket.</b> Please note that tickets for the tournament will be mailed out beginning in <b>December 2013</b>. You will receive an email when your tickets have been shipped.';
	
				// moving the language entries from the file to here. 
				languageData.TicketView.printMobileHeader = 'This is your ticket!';
				languageData.TicketView.printMobileFooter = 'Please proceed directly to the gate and scan this barcode.';
				languageData.TicketView.printTicketAdditional_info = '';
				languageData.TicketView.printTicketMoreInfo = '';
				languageData.TicketView.printTicketInstructions = '';
				languageData.TicketView.printTicketTermsOfUse = '';
				languageData.TicketView.printTicketDisclaimer = '';
					
				// Default OrderLookup Locale Entries.
				languageData.OrderLookup = languageData.OrderLookup || {}
				languageData.OrderLookup.searchViewTitle = 'To gain access to your past orders:'
				languageData.OrderLookup.searchViewDescription = 'Enter the email address & the phone number used during the purchase process.'
				languageData.OrderLookup.searchHeading = 'Order Lookup'
				languageData.OrderLookup.resultHeading = 'Order History'
				languageData.OrderLookupErrorCodes = [];
				languageData.OrderLookupErrorCodes.badLink = 'The link used for looking up your order is invalid. Please user the form below to find your order';

				// Default Reservations Locale Entries.
				languageData.Reservations = languageData.Reservations || {};
				languageData.Reservations.heading = 'Reservations';
				languageData.Reservations.pageTitle = 'Names for Ticket Reservation';
				languageData.Reservations.pageDescription = 'Please enter a ticket holder name for each of your tickets below.';
				languageData.Reservations.sameLastName = 'Use the same last name for all tickets.';
				
				// Default KnowledgeBase Locale Entries.
				languageData.KnowledgeBase = languageData.KnowledgeBase || {};
				languageData.KnowledgeBase.pageHeading = 'Frequently Asked Questions';
				languageData.KnowledgeBase.filterInputPlaceHolder = 'Filter Knowledgebase';
				languageData.PackageList.noKnowledgeBaseArticles = 'No Results Found';
				
				// Default DateTime XFC locale entries.
				languageData.DateTime = languageData.DateTime || {};
				languageData.DateTime.dateOnlyHeading = 'Select Date Below';
				languageData.DateTime.dateTimeHeading = 'Select Date and Time';
				languageData.DateTime.timeLimitMessage = 'You have 15 minutes to complete your order.';
				languageData.DateTime.noDatesAvailable = 'No dates available for selected month.';
				languageData.DateTime.selectTimeError = 'Please select a time from the list.';
				
				languageData.FlashPass = languageData.FlashPass || {};
				languageData.FlashPass.good = 'regular';
				languageData.FlashPass.better = 'gold';
				languageData.FlashPass.best = 'platinum';
				languageData.FlashPass.baseTitle = 'Base';
				languageData.FlashPass.baseDescription = 'Reservations equal to current wait time';
				languageData.FlashPass.turboTitle = 'Turbo';
				languageData.FlashPass.turboDescription = 'Reservations 50% FASTER than current time';
				languageData.FlashPass.lightningTitle = 'Lightning <span>+ double down</span>';
				languageData.FlashPass.lightningDescription = 'Reservations 90% FASTER than current time';
				languageData.FlashPass.importantInforamtionTitle = 'Important Information';
				languageData.FlashPass.termsText = '<p>&#8226; Each <i>THE FLASH Pass</i> unit can be used for up to six guests each. You can determine the number of <i>THE FLASH Pass</i> units you will need by using the step-by-step purchasing process that follows this screen.<p>&#8226; After indicating the number of guests in your party, how many guests will be using each of <i>THE FLASH Pass</i> unit(s) and the date of your visit, you will be shown availability and pricing options.</p><p>&#8226; After making your selection and completing the checkout process, print your <i>THE FLASH Pass</i> voucher(s) which you will redeem at the park\'s <i>THE FLASH Pass</i> Sales Center for the unit(s) you reserved.</p><p>&#8226; <i>THE FLASH Pass</i> does not include admission to the park.</p><p>&#8226; <b>Important notice: A photo ID is required for use of <i>THE FLASH Pass</i>. Your ID will be returned to you when you return <i>THE FLASH Pass</i> unit at the end of your visit.</b></p>';
				languageData.FlashPass.noWaitingInLineMsg = '<strong>NO WAITING</strong> IN LINE.<br>ARRIVE AT RESERVED TIME';
				languageData.FlashPass.partySize = 'How many total guest will be in your party?';
				languageData.FlashPass.stayingTogether = 'Will your entire party be staying together and using one Flash Pass?';
				languageData.FlashPass.pleaseSelectQty = 'Please enter the number of guests in your party.';
				languageData.FlashPass.guestSelectionErrorMsg = 'The maximum number of guests is 60 and the maximum number of guests per group is 6. Please be sure to select a value less thatn the maximum and greater than 1. Please ensure you will be breaking up your party if you have more than 6 guest.';
				languageData.FlashPass.exceededNumberOfGuestErrorMsg = 'You have exceeded the number of guests selected. Please adjust the quantity in your groups.';
				languageData.FlashPass.noKeywordErrorMsg = 'No keyword selected. Please select a keyword from the menu.';
				languageData.FlashPass.groupTotalErrorMsg = 'The number of guests selected does not match the sum of each groups quantity.';
				languageData.FlashPass.devideGroupMsg = 'Please divide your party into groups and indicated the number of guests for each Flash Pass.<br>(Maximum six guests per group)';
				languageData.FlashPass.availableGroups = 'Available Groups';
				languageData.FlashPass.groupLbl = 'Group';
				languageData.FlashPass.groupCountHeader = 'Guests/Group Total:';
				languageData.FlashPass.selectDate = '<p>Please selectthe date of your visit.<br>Some day reservations are not available online.<br>Dates shown in gray are not available.</p>';
				languageData.FlashPass.packageSelectorLightningTitle = '<h3>Lightning</h3><span>90% faster<br>+ Double Down</span>';
				languageData.FlashPass.packageSelectorTurboTitle = '<h3>Turbo</h3><span>50% faster</span>';
				languageData.FlashPass.packageSelectorBaseTitle = '<h3>Base</h3><span>less time in line</span>';
				languageData.FlashPass.selectionTotalLbl = 'Selection Total:';
				languageData.FlashPass.step2Image = 'flash.png';
				languageData.FlashPass.step3Image = 'flash.png';
				languageData.FlashPass.step4Image = 'flash.png';
				//languageData.FlashPass = '';
				
			}
			// Uncomment to test your locales.
			/*
			if(true){
				// Ensure all sections are added.
				languageData = {};
				languageData.ErrorCodes = {};
				languageData.CheckCartTicketLevelErrors = {};
				languageData.ApplicationLabels = {};
				languageData.Alert = {};
				languageData.FormLabels = {};
				languageData.FormValidation = {};
				languageData.ButtonLabels = {};
				languageData.Menu = {};
				languageData.PackageList = {};
				languageData.PackageDetails = {};
				languageData.CartView = {};
				languageData.ShippingView = {};
				languageData.BillingView = {};
				languageData.PaymentView = {};
				languageData.OrderView = {};
				languageData.ReceiptView = {};
				languageData.TicketView = {};
				languageData.OrderLookup = {};
				languageData.KnowledgeBase = {};
			}
			*/

			languageData.setData = function( data ) {
				if( data.status == status.OK ){
					for( var section in data.LANGUAGE ){
						if( data.LANGUAGE[section].LT ){
							languageData[section] = languageData[section] || {};
							data.LANGUAGE[section].LT = angular.isArray(data.LANGUAGE[section].LT) ? data.LANGUAGE[section].LT : [data.LANGUAGE[section].LT];
							for( var i in data.LANGUAGE[section].LT ){
								// Replace default locale entries with values from the database.
								if( typeof(data.LANGUAGE[section].LT[i].value) != 'undefined' ){
									// Replace @@supportPhoneNumber with value from config.
									if( data.LANGUAGE[section].LT[i].value.indexOf('@@supportPhoneNumber') > -1 ){
										languageData[section][data.LANGUAGE[section].LT[i].target] =
											languageData.replaceAll
												( '@@supportPhoneNumber'
												, appDataService.GetApplicationConsolidated.SETTINGS.support_phone_number
												, data.LANGUAGE[section].LT[i].value
												);
									} else {
										languageData[section][data.LANGUAGE[section].LT[i].target] = data.LANGUAGE[section].LT[i].value;
									}
								}
							}
						}
					}
				}
				
				try{
					if(!$rootScope.$$phase){
						$rootScope.$apply();
					}
				} catch(e){}
			}

			/**
			 *
			 * Listen for new languageData to come in from socket connection, update model;
			 * with new data
			 */
			appDataService.fetch
				( services.GET_APPLICATION_CONSOLIDATED
				, false
				).then
				( function(data){
					languageData.setData(data);
				  }
				)

			// @TODO: Refactor code below into a filter and update all references accordingly.

			/**
			 * Replaces all occurrences of a particular string in a string.
			 * @param {string} find  The string that will be replaced.
			 * @param {string} replace The string that will be used as the replacement.
			 * @param {string} str   The string that is being searched and updated.
			 * @return {string}     The updated string.
			 */
			languageData.replaceAll = function(find, replace, str){
				if(!str)
					return ''

				if(find.indexOf('@@') == -1){
					find = '@@' + find
				}
				return str.replace(new RegExp(find, 'g'), replace)
			}

			/**
			 * Gives the ability to pass in an array of strings to search, and array of strings to use as replacements.
			 * @param {array} find  Array of strings that will be searched and replaced inside str.
			 * @param {array} replace Array of strings that will be used as the replacement.
			 * @param {string} str  The string that is being searched and updated
			 * @return {string}    The updated string.
			 */
			languageData.replaceAllMultiple = function(find, replace, str){
				if(!str)
					return ''

				if(!(find instanceof Array && replace instanceof Array)){
					// Log Warning
					$log.warn('This function requires array as parameters.')
					return
				}

				if(find.length != replace.length){
					// Log Warning
					$log.warn('This function requires the arrays to be of the same length.')
					return
				}

				// Good to proceed.
				for(var i = 0; i < find.length; i++){
					str = this.replaceAll(find[i], replace[i], str)
				}

				return str
			}

			/**
			 * Takes a localized string (str) with singular/plural variables (i.e. @@[value, singularOption, pluralOption]) and based on value determines which option to replace the string with.
			 * @param {string} find  The key to search for without @@. In example @@[value, singularOption, pluralOption], you want to pass value for find parameter.
			 * @param {int} value   The value that will be checked in order to determine whether to select singular or plural option.
			 * @param {string} str   The string that is being searched and updated
			 * @return {string}     The updated string.
			 */
			languageData.updateLocaleKeyInArray = function(find, value, str){
				if(!str)
					return ''

				// Mark the index of the locale array entry being searched for.
				var indexOfFind = str.indexOf('@@[' + find),
					 indexOfClosingBracket = -1,
					 localeArray,
					 strArr

				// Proceed only if the search term is in the locale entry.
				if(indexOfFind > -1){
					// Increment the index by 3 to compensate for '@@['
					indexOfFind += 3

					// Find the closing bracket for this locale array entry.
					indexOfClosingBracket = str.indexOf(']', indexOfFind)

					// Get the exact entry name i.e. @@[key, value, values].
					localeArray = str.substring(indexOfFind - 3, indexOfClosingBracket + 1)

					// Remove spaces and create an array from the string.
					strArr = this.replaceAll(' ', '', str.substring(indexOfFind, indexOfClosingBracket)).split(',')

					// Determine selection based on value.
					if(value > 1){
						// Plural selection.
						str = str.replace(localeArray, strArr[2])
					} else {
						// Singular selection.
						str = str.replace(localeArray, strArr[1])
					}
				}

				return str
			}

			/**
			 * Gives the ability to pass in an array of strings to search, and array of strings to use as replacements.
			 * @param {array} find  An array of keys to search for without @@. In example @@[value, singularOption, pluralOption], you want to pass an array of value for find parameter.
			 * @param {array} value  An array of values that will be checked in order to determine whether to select singular or plural option.
			 * @param {string} str  The string that is being searched and updated
			 * @return {string}    The updated string.
			 */
			languageData.updateMultipleLocaleKeysInArray = function(find, value, str){
				if(!str)
					return ''

				if(!(find instanceof Array && value instanceof Array)) {
					// Log Warning
					$log.warn('This function requires array as parameters.')
					return
				}

				if(find.length != value.length) {
					// Log Warning
					$log.warn('This function requires the arrays to be of the same length.')
					return
				}

				for(var i = 0; i < find.length; i++){
					str = this.updateLocaleKeyInArray(find[i], value[i], str)
				}

				return str
			}

			languageData.displayErrorCode = function(data, section){
				if(!section || section == undefined)
					section = 'ErrorCodes'

				if(data.error_code && languageData[section][data.error_code]){
					var title = languageData.replaceAll('@@errorCode', data.error_code, languageData[section].errorCodeTitle);
					var desc = languageData.replaceAllMultiple(['@@supportPhoneNumber'], [appDataService.GetApplicationConsolidated.SETTINGS.support_phone_number], languageData[section][data.error_code]);
					desc += '<br /><br />' + languageData.replaceAllMultiple(['@@errorCode'], [data.error_code], languageData.CheckCartTicketLevelErrors.errorCodeLabel);
					

					modalService.displayModal({
						 title: title
						, headerFlag: modalService.DANGER
						, description: desc
						, buttonFlags: [$rootScope.buttonFlags.OK]
					})
				} else {

				modalService.displayModal({
					 title: languageData.Alert.generalErrorTitle
					, headerFlag: modalService.DANGER
					, description: languageData.Alert.generalErrorMsg
					, buttonFlags: [$rootScope.buttonFlags.OK]
					})
				}
			}
			
			return languageData;
		}])
