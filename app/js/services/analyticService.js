angular.module('app').service( 'analyticService',[
	'$rootScope'
	, '$log'
	, '$route'
	, '$window'
	, '$location'
	, '$timeout'
	, 'appDataService'
	, 'utilitiesService'
	, 'enumsService'
	,function(
	$rootScope
	, $log
	, $route
	, $window
	, $location
	, $timeout
	, appDataService
	, utilitiesService
	, enumsService
	){
		var socket = enumsService.socket;
		var services = enumsService.services;
		var status = enumsService.status;
		$window._gaq = $window._gaq || [];
		var analyticsData = {
			_accounts : { production:['UA-42209183-1'], staging:['UA-26514552-22'], development:['UA-26514552-22'] }
			, accounts : []
			, tags : {}
			, accertify : {}
			, config : {
				account : ''
				, conversion : ''
				, accertify : ''
			}
			, cart_id : ''
			, environment : ''
			, device : ''
			, merchant : ''
			, langauge : ''
			, eventComplete : {
				accounts : false
				, getApplicationConsolidated : false
				, initializeSocket : false
				, purchase : false
			}
			// TODO: Use isReady to Manage all the eventComplete
			, isReady : false
			, hasTrackedPurchase : false
			, hasTrackedPurchasePixel : false
			, customVaribles : []
			, queue : []
			, trackPageviewEvent : '$viewContentLoaded'
		};
		
		
		/*
		// TODO: Add automation to handle queued analytics
		$timeout(function queueTimer(){
			_processQueue()
			$timeout(queueTimer, 5000)
		}, 5000)
		
		function _processQueue(){
			if(analyticsData.isReady && analyticsData.queue.length > 0){
				failedQueue = []
				for(var i in analyticsData.queue){
					switch(analyticsData.queue[i].method){
						case 'trackPageview':
							analyticsData.trackPageview(analyticsData.queue[i].data)
						break;
						case 'trackEvent':
							analyticsData.trackEvent(analyticsData.queue[i].data)
						break;
						case 'trackVariables':
							analyticsData.trackVariables(analyticsData.queue[i].data)
						break;
						case 'trackPurchase':
							analyticsData.trackPurchase(analyticsData.queue[i].data)
						break;
						default:
							// TODO: Decide how to handle the failedQueue
							//failedQueue.push(analyticsData.queue[i])
						break;
					}
				}
				analyticsData.queue = failedQueue
			}
		}
		
		analyticsData.reinitialize = function(data) {
			// TODO: Handle the queued messages before reinit
			//_processQueue()
			analyticsData.accounts = []
			analyticsData.config = ''
			analyticsData.environment = ''
			analyticsData.eventComplete.accounts = false
			analyticsData.eventComplete.config = false
			analyticsData.eventComplete.environment = false
			analyticsData.eventComplete.purchase = false
			analyticsData.customVaribles = []
			analyticsData.queue = []
			if(!data.environment){
				_createCustomVariables(data)
				analyticsData.environment = data.environment
				analyticsData.eventComplete.environment = true
			}
			// TODO: Handle no analytics entry
			if(!((data).SETTINGS || {}).analytics){
				analyticsData.config = data.SETTINGS.analytics
				analyticsData.eventComplete.config = true
			}
			if(analyticsData.eventComplete.environment && analyticsData.eventComplete.config){
				_initializeAnalytics()
			}
			else{
				// TODO: Handle
			}
		}
		*/
		
		
		appDataService.fetch(services.INITIALIZE_SOCKET,true,false,false,false).then(function(data){
			if(!analyticsData.eventComplete.initializeSocket){
				if(typeof data.environment != 'undefined'){
					analyticsData.environment = data.environment;
				}
				if(typeof data.device != 'undefined'){
					analyticsData.device = data.device;
				}
				if(typeof data.merchant_id != 'undefined'){
					analyticsData.merchant = data.merchant_id;
				}
				if(typeof data.langauge != 'undefined'){
					analyticsData.langauge = data.langauge;
				}
				analyticsData.eventComplete.environment = true;
				_initializeAnalytics();
			}
		});
		
		appDataService.fetch(services.GET_APPLICATION_CONSOLIDATED,true,false,false,false).then(function(data){
			if(!analyticsData.eventComplete.getApplicationConsolidated){
				appDataService.setData(data);
				if(typeof ((data).SETTINGS || {}).analytics != 'undefined'){
					analyticsData.config.account = data.SETTINGS.analytics;
				}
				if(typeof ((data).SETTINGS || {}).analytics_conversion != 'undefined'){
					analyticsData.config.conversion = data.SETTINGS.analytics_conversion;
				}
				if(typeof ((data).SETTINGS || {}).accertify != 'undefined'){
					analyticsData.config.accertify = data.SETTINGS.accertify;
				}
				analyticsData.eventComplete.config = true;
				_initializeAnalytics();
			}
		});
		
		appDataService.fetch(services.GET_NEW_CART_ID,true,false,false,false).then(function(data){
			analyticsData.cart_id = data.cart_id;
		});
		
		function _initializeAnalytics() {
			if(analyticsData.eventComplete.environment && analyticsData.eventComplete.config && !analyticsData.eventComplete.accounts){
				_accounts = angular.isArray(analyticsData._accounts[analyticsData.environment]) ? analyticsData._accounts[analyticsData.environment] : [analyticsData._accounts[analyticsData.environment]];
				_additionalAccounts = [];
				_tags = {};
				_accertify = {};
				try{
					if(analyticsData.config.account != ''){
						_config = JSON.parse(analyticsData.config.account);
						if(typeof _config[analyticsData.environment] != 'undefined'){
							_additionalAccounts = angular.isArray(_config[analyticsData.environment]) ? _config[analyticsData.environment] : [_config[analyticsData.environment]];
						}
					}
				}
				catch(error){
					$log.info(error);
				}
				try{
					if(analyticsData.config.conversion != ''){
						_config = JSON.parse(analyticsData.config.conversion);
						if(typeof _config[analyticsData.environment] != 'undefined'){
							_tags = _config[analyticsData.environment];
						}
					}
				}
				catch(error){
					$log.info(error);
				}
				try{
					if(analyticsData.config.accertify != ''){
						_config = JSON.parse(analyticsData.config.accertify);
						if(typeof _config[analyticsData.environment] != 'undefined'){
							_accertify = _config[analyticsData.environment];
						}
					}
				}
				catch(error){
					$log.info(error);
				}
				analyticsData.accounts = _accounts.concat(_additionalAccounts);
				analyticsData.tags = _tags;
				analyticsData.accertify = _accertify;
				analyticsData.eventComplete.accounts = true;
				analyticsData.isReady = true;
				//_processQueue()
			}
		}
		
		analyticsData.clearElement = function(element) {
			var node = $window.document.getElementById(element);
			while (node.hasChildNodes()) {
				node.removeChild(node.firstChild);
			}
		}
		
		analyticsData.trackingPixels = function(data) {
			$timeout(function(){
				try{
					analyticsData.clearElement('Quantcast');
					var qc = $window.document.createElement('img');
					qc.src = '//pixel.quantserve.com/pixel/p-ttp70ZK2FD2N3.gif?labels=' + ((($route).current || {}).params || {}).merchant || '';
					qc.style.cssText = 'border:0px;width:1px;height:1px;';
					$window.document.getElementById('Quantcast').appendChild(qc);
				}
				catch(error){
					$log.info(error);
				}
				
			}, 0);
		}
		
		analyticsData.trackAccertify = function(data) {
			/*
				<!-Begin ThreatMetrix profiling tags below -->
				<!- note: replace 'UNIQUE_SESSION_ID' with a uniquely generated handle
					
					UNIQUE_SESSION_ID = brand_id + cart_id
					
					note: 'PAGEID' is only needed for Page Fingerprinting (NOT Device Fingerprinting), replace 'PAGEID' with an unique ID for that page, if omitted, default is 1
					note: for production, replace 'h.online-metrix.net' with a local URL and configure your web server to redirect to 'h.online-metrix.net' -->
				<div style="background:url(https://h.online-metrix.net/fp/clear.png?org_id=q5ua9sl2&amp;session_id=UNIQUE_SESSION_ID&amp;m=1)"></div>
				<img src="https://h.online- metrix.net/fp/clear.png?org_id=q5ua9sl2&amp;session_id=UNIQUE_SESSION_ID&amp;m=2" />
				<script src="https://h.online- metrix.net/fp/check.js?org_id=q5ua9sl2&amp;session_id=UNIQUE_SESSION_ID&amp;pageid=##PAGEID##"></script>
				<object type="application/x-shockwave-flash" data="https://h.online- metrix.net/fp/fp.swf?org_id=q5ua9sl2&amp;session_id=UNIQUE_SESSION_ID" width="1" height="1">
					<param name="movie" value="https://h.online-metrix.net/fp/fp.swf?org_id=q5ua9sl2&amp;session_id=UNIQUE_SESSION_ID" />
					<param name="wmode" value="transparent" />
					<div></div>
				</object>
				<!- End profiling tags -->
			*/
			try{
				if(typeof analyticsData.accertify.brand_id != 'undefined' && typeof analyticsData.cart_id != 'undefined' && typeof analyticsData.accertify.org_id != 'undefined'){
					analyticsData.clearElement('Accertify');
					var session_id = analyticsData.accertify.brand_id.toString() + analyticsData.cart_id.toString();
					var org_id = analyticsData.accertify.org_id.toString();
					var page_id = 1; //utilitiesService.md5($location.$$absUrl);
					var accertify_div = $window.document.createElement('div');
					accertify_div.style.cssText = 'background:url(https://h.online-metrix.net/fp/clear.png?org_id='+org_id+'&session_id='+session_id+'&m=1)';
					$window.document.getElementById('Accertify').appendChild(accertify_div);
					var accertify_img = $window.document.createElement('img');
					accertify_img.src = '//h.online-metrix.net/fp/clear.png?org_id='+org_id+'&session_id='+session_id+'&m=2';
					$window.document.getElementById('Accertify').appendChild(accertify_img);
					var accertify_script = $window.document.createElement('script');
					accertify_script.src = '//h.online-metrix.net/fp/check.js?org_id='+org_id+'&session_id='+session_id+'&pageid='+page_id;
					$window.document.getElementById('Accertify').appendChild(accertify_script);
					var accertify_object = $window.document.createElement('object');
					accertify_object.type = 'application/x-shockwave-flash';
					accertify_object.data = '//h.online-metrix.net/fp/fp.swf?org_id='+org_id+'&session_id='+session_id;
					accertify_object.width = '1';
					accertify_object.height = '1';
					var accertify_param = $window.document.createElement('param');
					accertify_param.name = 'movie';
					accertify_param.value = 'https://h.online-metrix.net/fp/fp.swf?org_id='+org_id+'&session_id='+session_id;
					accertify_object.appendChild(accertify_param);
					accertify_param = $window.document.createElement('param');
					accertify_param.name = 'wmode';
					accertify_param.value = 'transparent';
					accertify_object.appendChild(accertify_param);
					accertify_div = $window.document.createElement('div');
					accertify_object.appendChild(accertify_div);
					$window.document.getElementById('Accertify').appendChild(accertify_object);
				}
			} catch(error){
				$log.info(error);
			}
		}
		
		analyticsData.trackPageview = function(data) {
			if(typeof data.url == 'undefined'){
				// Send the page name as /{MERCHANT_CODE}/{ROUTE_INFO}?QUERY_STRING
				data.url = $window.location.pathname + $window.location.search;
			}
			if(analyticsData.isReady){
				if(!analyticsData.hasTracked){
					analyticsData.hasTracked = !analyticsData.hasTracked;
				}
				for(var i in analyticsData.accounts){
					try{
						$window._gaq.push(
							['tracker'+i+'._setAccount', analyticsData.accounts[i]]
							, ['tracker'+i+'._setDomainName', 'none']
							, ['tracker'+i+'._setCustomVar', 1, 'host', $window.location.host, 3]
							, ['tracker'+i+'._setCustomVar', 2, 'environment', analyticsData.environment, 3]
							, ['tracker'+i+'._setCustomVar', 3, 'device', analyticsData.device, 3]
							, ['tracker'+i+'._setCustomVar', 4, 'merchant', analyticsData.merchant, 3]
							, ['tracker'+i+'._setCustomVar', 5, 'langauge', analyticsData.langauge, 3]
							, ['tracker'+i+'._trackPageview', data.url]
						);
					}
					catch(error){
						$log.info(error);
					}
				}
			}
			else{
				data.queued = true;
				analyticsData.queue.push({ 'method':'trackPageview', 'data':data });
			}
		}
		
		analyticsData.trackEvent = function(data) {
			/*
			 * _trackEvent(category, action, opt_label, opt_value, opt_noninteraction)
			 *
			 * category (required)
			 * The name you supply for the group of objects you want to track.
			 *
			 * action (required)
			 * A string that is uniquely paired with each category, and commonly used to define the type of user interaction for the web object.
			 *
			 * label (optional)
			 * An optional string to provide additional dimensions to the event data.
			 *
			 * value (optional)
			 * An integer that you can use to provide numerical data about the user event.
			 *
			 * non-interaction (optional)
			 * A boolean that when set to true, indicates that the event hit will not be used in bounce-rate calculation.
			 *
			 */
			if(analyticsData.isReady){
				if(!analyticsData.hasTracked){
					analyticsData.hasTracked = !analyticsData.hasTracked;
				}
				for(var i in analyticsData.accounts){
					try{
						$window._gaq.push(
							['tracker'+i+'._setAccount', analyticsData.accounts[i]]
							, ['tracker'+i+'._setDomainName', 'none']
							, ['tracker'+i+'._trackEvent', data.category, data.action, data.label, data.value]
						);
					}
					catch(error){
						$log.info(error);
					}
				}
			}
			else{
				data.queued = true;
				analyticsData.queue.push({ 'method':'trackEvent', 'data':data });
			}
		}
		
		analyticsData.trackVariables = function(data) {
			/*
			 * _setCustomVar(index, name, value, opt_scope)
			 *
			 * index (required)
			 * This is a number whose value can range from 1 - 5, inclusive. A custom variable should be placed in one slot only and not be re-used across different slots.
			 *
			 * name (required)
			 * This is a string that identifies the custom variable and appears in the top-level Custom Variables report of the Analytics reports.
			 *
			 * value (required)
			 * This is a string that is paired with a name. You can pair a number of values with a custom variable name. The value appears in the table list of the UI for a selected variable name. Typically, you will have two or more values for a given name. For example, you might define a custom variable name gender and supply male and female as two possible values.
			 *
			 * opt_scope (optional)
			 * As described above, the scope defines the level of user engagement with your site. It is a number whose possible values are 
			 * 	1 (visitor-level [The current session and all future sessions for the life of the visitor cookie.]), 
			 * 	2 (session-level [The current session of the visitor.]), or 
			 * 	3 (page-level [A single pageview, event, or transaction call.]).
			 * When left undefined, the custom variable scope defaults to page-level interaction.
			 *
			 */
			// TODO: track sfCustomerId which is pulled off six flag's page
			if(typeof data.url == 'undefined'){
				// Send the page name as /{MERCHANT_CODE}/{ROUTE_INFO}?QUERY_STRING
				data.url = $window.location.pathname + $window.location.search;
			}
			if(analyticsData.isReady){
				if(!analyticsData.hasTracked){
					analyticsData.hasTracked = !analyticsData.hasTracked;
				}
				for(var i in analyticsData.accounts){
					try{
						$window._gaq.push(
							['tracker'+i+'._setAccount', analyticsData.accounts[i]]
							, ['tracker'+i+'._setDomainName', 'none']
						);
						_varibles = angular.isArray(data.variables) ? data.variables : [data.variables];
						for(var v in _variables){
							$window._gaq.push(
								['tracker'+i+'._setCustomVar', _variables[v].index, _variables[v].name, _variables[v].value, _variables[v].opt_scope]
							);
						}
						$window._gaq.push(
							['tracker'+i+'._trackPageview', data.url]
						);
					}
					catch(error){
						$log.info(error);
					}
				}
			}
			else{
				data.queued = true;
				analyticsData.queue.push({ 'method':'trackVariables', 'data':data });
			}
		}
		
		analyticsData.trackPurchase = function(data) {
			if(analyticsData.isReady){
				if(!analyticsData.hasTrackedPurchase){
					analyticsData.hasTrackedPurchase = !analyticsData.hasTrackedPurchase;
				}
				//analyticsData.eventComplete.purchase = true
				for(var i in analyticsData.accounts){
					try{
						$window._gaq.push(
							['tracker'+i+'._setAccount', analyticsData.accounts[i]]
							, ['tracker'+i+'._setDomainName', 'none']
							//, ['tracker'+i+'._set', 'currencyCode', appDataService.currencyCode]
							//, ['tracker'+i+'._trackPageview', location.href]
							, ['tracker'+i+'._addTrans'
								, ((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).ORDER_STATUS_REPLY || {}).order_id											// transactionId
								, ((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).ORDER_STATUS_REPLY || {}).merchant_name									// affiliation
								, (((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).ORDER_ITEMS_REPLY || {}).TOTALS || {}).current_total						// total
								, (((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).ORDER_ITEMS_REPLY || {}).TOTALS || {}).tax_total							// tax
								, (((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).ORDER_ITEMS_REPLY || {}).TOTALS || {}).shipping_total						// shipping
								, ((((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).CUSTOMER_DETAILS_REPLY || {}).CUSTOMER || {}).ADDRESS || {}).city		// city
								, ((((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).CUSTOMER_DETAILS_REPLY || {}).CUSTOMER || {}).ADDRESS || {}).state		// state
								, ((((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).CUSTOMER_DETAILS_REPLY || {}).CUSTOMER || {}).ADDRESS || {}).country		// country
							]
						);
						if(typeof ((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).TICKET_DETAILS_REPLY || {}).TICKET != 'undefined'){
							_tickets = angular.isArray(data.ViewOrder.VIEW_ORDER_REPLY.TICKET_DETAILS_REPLY.TICKET) ? data.ViewOrder.VIEW_ORDER_REPLY.TICKET_DETAILS_REPLY.TICKET : [data.ViewOrder.VIEW_ORDER_REPLY.TICKET_DETAILS_REPLY.TICKET];
							_items = [];
							for(var t in _tickets){
								found = false;
								for(var index in _items){
									if(_tickets[t].package_id == _items[index].package_id){
										_items[index].qty++;
										found = true;
										break;
									}
								}
								if(!found){
									_item = {};
									_item.order_id = _tickets[t].order_id;
									_item.package_id = _tickets[t].package_id;
									_item.package_name = _tickets[t].package_name;
									_item.package_class = _tickets[t].package_class;
									_item.retail = _tickets[t].retail;
									_item.qty = 1;
									_items.push(_item);
								}
							}
							for(var t in _items){
								$window._gaq.push(['tracker'+i+'._addItem'
									, _items[t].order_id													// transactionId
									, _items[t].package_id													// sku
									, _items[t].package_name // + ' : ' + _tickets[t].customer_type_name	// name
									, _items[t].package_class												// category
									, _items[t].retail														// price
									, _items[t].qty															// quantity
								]);
							}
						}
						$window._gaq.push(['tracker'+i+'._trackTrans']);
					}
					catch(error){
						$log.info(error);
					}
				}
				analyticsData.purchasingPixels( data );
			}
			else{
				analyticsData.purchasingPixels( data );
				data.queued = true;
				analyticsData.queue.push({ 'method':'trackPurchase', 'data':data });
			}
		}
		
		analyticsData.purchasingPixels = function(data) {
			if(analyticsData.isReady){
				if(!analyticsData.hasTrackedPurchasePixel){
					analyticsData.hasTrackedPurchasePixel = !analyticsData.hasTrackedPurchasePixel;
				}
				//analyticsData.eventComplete.purchase = true;
				$timeout(function(){
					for(var key in analyticsData.tags){
						switch(key){
							case 'Floodlight':
								try{
									analyticsData.clearElement('DFAFloodlight');
									for(var i in analyticsData.tags[key]){
										var src = analyticsData.tags[key][i].src || '';
										var type = analyticsData.tags[key][i].type || '';
										var cat = analyticsData.tags[key][i].cat || '';
										var u1 = analyticsData.tags[key][i].u1 || '';
										var u2 = analyticsData.tags[key][i].u2 || '';
										var qty = 1;
										var _tickets = ((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).TICKET_DETAILS_REPLY || {}).TICKET;
										var _qty = angular.isArray(_tickets) ? _tickets.length : typeof _tickets != 'undefined' ? 1 : 0;
										if(typeof analyticsData.tags[key][i].qty != 'undefined' && analyticsData.tags[key][i].qty == '@@orderQty'){
											qty = i18n.replaceAll('@@orderQty', _qty, analyticsData.tags[key][i].qty);
										}
										else if(typeof analyticsData.tags[key][i].qty == 'string' && analyticsData.tags[key][i].qty.indexOf('@@') < 0){
											qty = analyticsData.tags[key][i].qty;
										}
										var cost = (((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).ORDER_ITEMS_REPLY || {}).TOTALS || {}).current_total;
										var ord = ((((data).ViewOrder || {}).VIEW_ORDER_REPLY || {}).ORDER_STATUS_REPLY || {}).order_id;
										
										var dfa = $window.document.createElement('iframe');
										dfa.src = '//fls.doubleclick.net/activityi;src=' + src + ';type=' + type + ';cat=' + cat + ';u1=' + u1 + ';u2=' + u2 + ';qty=' + qty + ';cost=' + cost + ';ord=' + ord + '?';
										dfa.width = 0;
										dfa.height = 0;
										dfa.frameborder = 0;
										dfa.style.cssText = 'display:none;';
										$window.document.getElementById('DFAFloodlight').appendChild(dfa);
									}
								}
								catch(error){
									$log.info(error);
								}
							break;
						}
					}
				}, 0);
			}
			else{
				data.queued = true;
				analyticsData.queue.push({ 'method':'purchasingPixels', 'data':data });
			}
		}
		
		// Track Analytics for every Page View
		//$rootScope.$on('$routeChangeStart', function(next, current)
		//$rootScope.$on('$routeChangeSuccess', function(angularEvent, current, previous)
		//$rootScope.$on('$routeUpdate', function()
		//$rootScope.$on('$viewContentLoaded', function()
		$rootScope.$on(analyticsData.trackPageviewEvent, function(param1, param2, param3) {
			data = {'event':analyticsData.trackPageviewEvent};
			switch(analyticsData.trackPageviewEvent){
				case '$routeChangeStart':
					data.next = param1;
					data.current = param2;
				break;
				case '$routeChangeSuccess':
					data.angularEvent = param1;
					data.current = param2;
					data.previous = param3;
				break;
			}
			analyticsData.trackPageview(data);
			analyticsData.trackingPixels(data);
		});
		
		return analyticsData;
	}])