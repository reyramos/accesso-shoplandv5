angular.module('app').service
	( 'spinnerService'
	, function() {

	var opts = {
	  lines: 20, // The number of lines to draw
	  length: 20, // The length of each line
	  width: 2, // The line thickness
	  radius: 22, // The radius of the inner circle
	  corners: 0, // Corner roundness (0..1)
	  rotate: 0, // The rotation offset
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  color: '#333', // #rgb or #rrggbb
	  speed: 1.5, // Rounds per second
	  trail: 50, // Afterglow percentage
	  shadow: false, // Whether to render a shadow
	  hwaccel: false, // Whether to use hardware acceleration
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  top: '50%', // Top position relative to parent in px
	  left: '50%' // Left position relative to parent in px
	}

	return new window.Spinner(opts).spin()

})
