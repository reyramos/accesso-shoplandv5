angular.module('app').factory
( 'packageService',[
	'$log'
	, '$q'
	, '$rootScope'
	, '$filter'
	, '$routeParams'
	, 'enumsService'
	, 'appDataService'
	, 'extraFlowService'
	, 'modalService'
, function
	( $log
	, $q
	, $rootScope
	, $filter
	, $routeParams
	, enumsService
	, appDataService
	, extraFlowService
	, modalService
	){
		var packageService =
			{ packagesItems: []
			, isAdvancedTicket: false
			, advanceTicketDate: null
			, customerTypes: []
			, packageDetails: {}
			}

		var pkgXFCs = []
		  , xfcFinalCallback
		  , currentXFC = -1
		  , qtyAdded = 0
		  , retainCharacs = [ 'card_type_required' ] // Copy these CHARACS over to items being added.

		/***-----------------------------------------------------------------------
		 * FOLLOWING ARE CONTROLLER FOR PACKAGE QTY MANIPULATION
		 *------------------------------------------------------------------------**/

		/**
		 * Handles manual user input of the numeric stepper. Ensures that the
		 * value entered is not less than the min quantiy on the package or
		 * greater than the max quantity.
		 *
		 * @param {object} data data passed by the click event.
		 * @return undefined
		 */

		packageService.onQtyChange = function( data, editing ){
			if(data.qty == '' || isNaN(data.qty)){
				return
			}

			var qtyEntered = Number(data.qty)
			if(qtyEntered > data.min_quantity && qtyEntered <= data.max_quantity){
				data.qty = Number(qtyEntered)
			}

			if(Number(packageService.customerTypes.length) <= 1){

				if( editing && ((data.qty < data.min_quantity) || (data.qty <= 0)) ){
				// Otherwise set to zero if less than min qty or some how they set it to less than 0.
					qtyEntered = 0
				} else if(qtyEntered < data.min_quantity){
					qtyEntered = Number(data.min_quantity)
				} else if(qtyEntered > data.max_quantity){
					qtyEntered = Number(data.max_quantity)
				}
			}
			
			if( editing && ((data.qty < data.min_quantity) || (data.qty <= 0)) ){
				// Otherwise set to zero if less than min qty or some how they set it to less than 0.
				qtyEntered = 0
			}

			data.qty = qtyEntered
		}

		/**
		 * Handles the increment button click event on the numeric stepper.
		 * Increments the quantity on the specific rate type data object. If it's
		 * a value greater than the package's max quantity, it sets the value to
		 * max quantity.
		 *
		 * @param {object} data  current data item in the repeater.
		 * @return undefined
		 */

		packageService.incrementQty = function( data, editing ){
			var qtyHandler = data.qty;
			qtyHandler++;
			
			if( isNaN( data.min_quantity ) ){
				data.min_quantity = 1
			}
			
			if( isNaN( data.max_quantity ) ){
				data.max_quantity = 999
			}
			
			if( qtyHandler != 0 && qtyHandler < data.min_quantity )
				qtyHandler = data.min_quantity;
			
			if( data.min_quantity <= qtyHandler && ( qtyHandler <= data.max_quantity ) ){
				data.qty = Number(qtyHandler);
			}
		}

		/**
		 * Handles the decrement button click event on the numeric stepper.
		 * Decrements the quantity on the specific rate type data object. Resets
		 * the new value to 0 if its a negative value. If it's a value less than
		 * the package's min quantity, it jumps down to zero. Also checks against
		 * max quantity incase user manually inserted a quantity > than max
		 * followed by clicking the decrement button.
		 *
		 * @param {object} data data item in the repeater.
		 * @return undefined
		 */

		packageService.decrementQty = function( data, editing ){
			// Decrement the quantity stepper by 1. Ensure qty is never less than
			// min qty set on package level. Also, ensure qty is never less than 0.
			data.qty -= 1;
			
			if( isNaN( data.min_quantity ) ){
				data.min_quantity = 1
			}
			
			if( isNaN( data.max_quantity ) ){
				data.max_quantity = 999
			}

			if( editing && ((data.qty < data.min_quantity) || (data.qty <= 0)) ){
				// Otherwise set to zero if less than min qty or some how they set it to less than 0.
				data.qty = 0
			} else if( data.qty > data.max_quantity ){
				data.qty = data.max_quantity

			} else if( packageService.customerTypes.length == 1 ){
				// Set to pkg min qty if there is only one rate type.
				if( data.qty < data.min_quantity || data.qty < 0 ){
					data.qty = data.min_quantity
				}
			}
		}

		packageService.doXFC = function(packageDetails, itemsToBeAdded, customerTypes, callback){
			$log.log('packageService.doXFC')
			// Check if there are XFCs in the package.
			// Verify control has settings in extraFlowService.

			// Reset the XFCs object.
			pkgXFCs = extraFlowService.getEnabledXFCsFromList( packageDetails.extra_movie );
			currentXFC = -1;
			xfcFinalCallback = callback;

			// Store a refrence to the details and items to be added. this will be used by the XFCs.
			packageService.packageDetails = packageDetails;
			packageService.itemsToBeAdded = itemsToBeAdded;
			packageService.customerTypes = customerTypes;

			// If there are still XFCs left, let's process those. 
			// Route to the first view and wait for response from XFC.
			if(pkgXFCs.length > 0){
				packageService.nextXFC();
			} else {
				$log.log('packageService >> xfcFinalCallback')
				xfcFinalCallback();
			}
		}
		
		packageService.getCTNameByID = function(ctID){
			for( var i = 0; i < packageService.itemsToBeAdded.length; i++ ){
				for( var j = 0; j < packageService.packageDetails.CT.length; j++ ){
					if( ctID == packageService.packageDetails.CT[j].id )
						return packageService.packageDetails.CT[j].name;
				}
			}
			
			return '';
		}
		
		packageService.initXFC = function(){
			// Ensure we have packages to add. This would not be true if user refreshed the browser.
			if( !packageService.itemsToBeAdded ){
				// Re-route back to packageDetails if packageId is defined otherwise to packageList.
				if( $routeParams.packageId ){
					$rootScope.routeTo( $rootScope.routes.DETAILS + '/' + $routeParams.packageId );
				} else {
					$rootScope.routeTo( $rootScope.routes.LIST );
				}

				return false;
			}
			
			return true;
		}


		packageService.nextXFC = function(){
			$log.log('packageService.nextXFC', pkgXFCs)
			// Either load the next XFC or execute the callback from PackageDetailsController.
			currentXFC += 1

			if(currentXFC < pkgXFCs.length){
				packageService.settings = extraFlowService[pkgXFCs[currentXFC].name]
				packageService.xfcName = pkgXFCs[currentXFC].name
				$rootScope.routeTo(extraFlowService[pkgXFCs[currentXFC].name].view + '/' + packageService.packageDetails.id)
			} else {
				// Completed all XFCs.
				xfcFinalCallback()
			}
		}

		packageService.cancelXFC = function(){
			previousXFC()
		}

		function previousXFC(){
			currentXFC -= 1

			if(currentXFC >= 0){
				$rootScope.routeTo(extraFlowService[pkgXFCs[currentXFC].name].view + '/' + packageService.packageDetails.id)
			} else {
				// Canceled all XFCs. go back to packageDetails.
				$rootScope.routeTo(enumsService.routes.DETAILS + '/' + packageService.packageDetails.id)
			}
		}

		/***-----------------------------------------------------------------------
		 * PRIVATE
		 *------------------------------------------------------------------------**/
		/**
		 * The advance data parameters can be found within the packageDetails CHARACS or
		 * within the rates CHARACS
		 * If data is undefined it will check advance data parameter within the packageDetails
		 *
		 * @param {object} pkg
		 *
		 */
		function updateAdvancedTicketDesciption(pkg){
			// Set the advanceTicketDate identifier.
			packageService.advanceTicketDate = pkg.CHARACS.valid_from

			pkg.desc =
				$rootScope.i18n.replaceAllMultiple
					( ['@@date']
						, [$filter('date')(pkg.CHARACS.valid_from, 'MM/dd/yyyy')]
						, $rootScope.i18n.replaceAllMultiple
						( ['@@Date']
							, [$filter('date')(pkg.CHARACS.valid_from, 'MM/dd/yyyy')]
							, pkg.desc
						)
					)

			pkg.headline =
				$rootScope.i18n.replaceAllMultiple
					( ['@@date']
						, [$filter('date')(pkg.CHARACS.valid_from, 'MM/dd/yyyy')]
						, $rootScope.i18n.replaceAllMultiple
						( ['@@Date']
							, [$filter('date')(pkg.CHARACS.valid_from, 'MM/dd/yyyy')]
							, pkg.headline
						)
					)

			pkg.more_info =
				$rootScope.i18n.replaceAllMultiple
					( ['@@date']
						, [$filter('date')(pkg.CHARACS.valid_from, 'MM/dd/yyyy')]
						, $rootScope.i18n.replaceAllMultiple
						( ['@@Date']
							, [$filter('date')(pkg.CHARACS.valid_from, 'MM/dd/yyyy')]
							, pkg.more_info
						)
					)	
		}

		return packageService
}])

