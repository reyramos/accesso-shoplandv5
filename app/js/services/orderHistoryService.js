/**
 * @deprecated
 * candidate for removal
 * 
 * # Storage for navigation items to share accross controllers and in application.
 *
 * Any navigation data consumable by multiple controllers (e.g keyword sub menu shows up in slide out menu
 * but also in package details view).
 *
 */
angular.module('app').factory
	( 'orderHistoryService'
	, function() {

			var data =
				{ orders:[]
				, phoneNumber: ""
				, lastFourDigits: ""
				, emailAddress: ""
				}

			return data
		}
	)
