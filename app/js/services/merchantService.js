/**
 * # Language definitions service
 *
 * All user-facing text should reference a key provided by this service. That
 * key is then set to corresponding text in the language defined in
 * clientInfoService.
 *
 */
angular.module('app').factory
	( 'merchantService',[
		'$rootScope'
		, '$log'
		, 'enumsService'
		, 'storageService'
	, function
		( $rootScope
		, $log
		, enumsService
		, storageService
		) {

			var socket = enumsService.socket
			var status = enumsService.status
			var services = enumsService.services

/**
 * ## Data model
 *
 * Initially populate with localStorage data if exists or fall back to
 * empty object while waiting on new data from socket
 */
			var merchantDetails  = storageService.get('merchantDetails') || {};

			var merchantKeywords = storageService.get('merchantKeywords') || {};

			var parkHours        = storageService.get('parkHours') || {};
/**
 * ## Socket Listener
 *
 */

	/**
	 * ## GetMerchantDetails: returns BILLINGTYPE, SHIPPINGMETHOD, and CART_ADJUSTMENTS
	 */
			$rootScope.$on
				( socket.receive + services.GET_MERCHANT_DETAILS
					, function(event,data){
						if ( data.status == status.OK ) {
							// merchantDetails['_checksum'] = data.['checksum']; // FUTURE TODO
							merchantDetails['BILLINGTYPE'] = data['BILLINGTYPE'];
							merchantDetails['SHIPPINGMETHOD'] = data['SHIPPINGMETHOD'];
							merchantDetails['CART_ADJUSTMENTS'] = data['CART_ADJUSTMENTS'];

							storageService.set('merchantDetails',merchantDetails);
						} else {
						}
					}
				)
	/**
	 * ## GetMerchantKeywords: returns BILLINGTYPE, SHIPPINGMETHOD, and CART_ADJUSTMENTS
	 */
			$rootScope.$on
				( socket.receive + services.GET_MERCHANT_KEYWORDS
					, function(event,data){

						if ( data.status == status.OK ) {
							// merchantKeywords['_checksum'] = data.['checksum']; // FUTURE TODO
							var hold = data.keywords.split(",");

							merchantKeywords['KEYWORDS'] = new Array();
							for ( var idx in hold ) {
								merchantKeywords['KEYWORDS'].push( hold[idx].trim() );
							}

							storageService.set('merchantKeywords',merchantKeywords);

						} else {
						}
					}
				)

	/**
	 * ## GetParkHours:
	 */
			$rootScope.$on
				( socket.receive + services.GET_PARK_HOURS
					, function(event,data){

						if ( data.status == status.OK ) {
							// parkHours['_checksum'] = data.['checksum']; // FUTURE TODO
							parkHours['ClosedText'] = data.Hours.ClosedText;
							parkHours['Days'] = data.Hours.Days.Day;

							storageService.set('parkHours',parkHours);
						} else {
						}
					}
				)

			function getMerchantDetails()  { return this.merchantDetails; };
			function getMerchantKeywords() { return this.merchantKeywords; };
			function getParkHours()        { return this.parkHours; };
		}])
