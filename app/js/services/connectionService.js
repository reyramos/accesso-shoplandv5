angular.module('app').factory
	( 'connectionService',[
		'$rootScope'
		, '$log'
		, '$route'
		, '$location'
		, 'clientInfoService'
		, 'enumsService'
		, 'utilitiesService'
	, function
		( $rootScope
		, $log
		, $route
		, $location
		, clientInfoService
		, enumsService
		, utilitiesService
		) {

	var status = enumsService.status;
	var services = enumsService.services;
	var routes = enumsService.routes;
	var service = {};

	service.format =
		function(key,data){
			request = this[key] ? this[key](data) : data;

			request.request_type = key;
			request._version = window.version || 'local-dev'
			this.cleanObject(request);
			return request;
		}

	service.cleanObject =
		function(object){
			for(var property in object){
				if(object.hasOwnProperty(property)){
					if( object[property] == undefined
							|| object[property] == null ){
						delete object[property];
					}
				}
			}
		}

	service[services.INITIALIZE_SOCKET] =
		function( data ) {
		//	==========================================================
		//		Start: PayPal Support.
		//	----------------------------------------------------------
			// Building the base url for the site.
			var paypal_base_url = $location.$$protocol + '://'
				+ $location.$$host
				+ ( ( $location.$$port != '80') ? ':' + $location.$$port : '' )
				+ '/' + utilitiesService.getMerchant();

			console.log('>>>> paypal_base_url',paypal_base_url);
				
			var paypal_suffix_url = '?';
				paypal_suffix_url += ( $location.$$search.e ) ? 'e=' + $location.$$search.e : ''; // environment override
				paypal_suffix_url += ( $location.$$search.m ) ? 'm=' + $location.$$search.m : ''; // merchant override
				paypal_suffix_url += ( $location.$$search.d ) ? 'd=' + $location.$$search.d : ''; // debug override
				paypal_suffix_url += ( $location.$$search.l ) ? 'l=' + $location.$$search.l : ''; // language override
			
			// Setting the Return and Cancel URLs.
			var paypal_cancel_url = paypal_base_url + '/' + routes.PAYPAL_CANCEL_URL + paypal_suffix_url; 
			var paypal_return_url = paypal_base_url + '/' + routes.PAYPAL_RETURN_URL + paypal_suffix_url;

		//	----------------------------------------------------------
		//		END: PayPal Support.
		//	==========================================================
			
			var request =
				{ merchant   : utilitiesService.getMerchant()
				, language   : $location.$$search.l || ''
				, device     : 'mobile' // to get from url in the future similar to $location.$$search.develop
				// TODO: switch hostname for go live
				, hostname   : $location.$$host
				, cart_key   : data.cart_key   || false
				, cart_id    : data.cart_id    || false
				, session_id : data.session_id || false
				, environment : $location.$$search.e
				, paypal_cancel_url   : paypal_cancel_url || ''
				, paypal_return_url   : paypal_return_url || ''
				, skip_requests : data.skip_requests || ''
				, _ua       : clientInfoService.userAgent
				, _os       : clientInfoService.osName + ' - ' + clientInfoService.osVersion
				, _device   : clientInfoService.deviceType + ' - ' + clientInfoService.deviceName + ' - ' + clientInfoService.deviceVersion
				, _browser  : clientInfoService.browserName + ' - ' + clientInfoService.browserVersion
				, _referrer : document.referrer || ''
				, _location : window.location.href || ''
				};

			console.log('>>>> request',request);
			
			if ( typeof data.skip_requests != 'undefined' ) {
				request.skip_requests = data.skip_requests;
			}

			if ( typeof data.paypal_payer_id != 'undefined' ) {
				request.paypal_payer_id = data.paypal_payer_id;
			}

			if ( typeof data.paypal_paypal_id != 'undefined' ) {
				request.paypal_id = data.paypal_id;
			}

			if ( typeof data.paypal_token != 'undefined' ) {
				request.paypal_token = data.paypal_token;
			}

			return request;
		}

	service[services.GET_APPLICATION_CONSOLIDATED] =
		function( data ) {
			var request =
				{ language: clientInfoService.language
				, section_list: data.sectionList
				};
			return request;
		}

	service[services.GET_APPLICATION_LOCALE] =
		function( data ) {
			var request =
				{ language: clientInfoService.language
				, section_list: data.sections
				};
			return request;
		}

	service[service.GET_MERCHANT_PACKAGE_LIST] =
		function( data ) {
			var request =
				{ include_merchant_tree: data.includeMerchantTree
				, keyword: data.keyword
				, referring_url: data.referringURL
				, promos_only: data.promosOnly
				, promo_codes: data.promoCodes
				, show_details: data.showDetails
				, packages: data.packages
				, long_desc_flag: data.longDescFlag
				, choose_random_package: data.chooseRandomPackage
				, keyword: data.upsellKeyword ? data.upsellKeyword : data.keyword
				, include_comps: data.includeComps
				, zip_code: data.zipCode
				, latitude: data.latitude
				, longitude: data.longitude
				, accuracy: data.accuracy
				, skip_cache: data.skip_cache
				, no_cache: data.no_cache
				};
			return request;
		}

	service[services.GET_MERCHANT_KEYWORDS] =
		function( data ) {
			var request =
				{ version: '2' // @TODO: Why is this hardcoded?
				, show_invisible_flag: '1' // @TODO: Why is this hardcoded?
				};
			return request;
		}

	service[services.GET_MERCHANT_PACKAGE_EVENT_DATES] =
		function( data ) {
			var request =
				{ month: data.month
				, min_capacity: data.minCapacity
				, customer_type: data.customerType
				, version: data.version
				, event_id: data.eventID
				, package_id: data.packageID
				, extra_movie: data.extraMovie
				, identify_customer_types: data.identifyCustomerTypes
				};
			return request;
		}

	service[services.GET_PAYMENT_SCHEDULE] =
		function( data ) {
			var request =
				{ qty: data.qty
				, customer_type: data.customerType
				, package_id: data.packageID
				};
			return request;
		}

	service[services.GET_CART_SUMMARY] =
		function( data ) {
			var request =
				{ check_cart: "1" // @TODO: Why is this hardcoded?
				, include_checkout_keywords: "1" // @TODO: Why is this hardcoded?
				, promo_codes: data.promoCodes
				};
			return request;
		}

	// NOTE: Shop V4 does a bunch of checks before it actually makes
	// this service. It seems like it's related to validating data required
	// by XMC is in place.
	service[services.ADD_CART_ITEM] =
		function( data ) {
			var request =
				{ ITEMS:
					{ ITEM: data
					}
				};
			return request;
		}

	service[services.UPDATE_CART_ITEMS] =
		function( data ) {
			var request =
			{ ITEMS:
				{ ITEM: data
				}
			};
			return request;
		}

	/**
	 * @skipRequests is set to SetExpressCheckout for supporting PayPal and not to overwrite a current token.
	 * I'm hardcoding skip_requests to 'SetExpressCheckout' for this particular call.
	 * called in specific cases, and I need to make sure it doesn't call
	 * a chain reaction of other calls.
	 */
	service[services.UPDATE_CART_CHECKOUT_INFO] =
		function( data ) {
			var request =
				{ ship_method: data.shippingMethod
				, ship_amount: data.shippingAmount
				, billing_type_id: data.billingId
				, skip_requests: 'SetExpressCheckout'
				};
			return request;
		}

//	==========================================================
//		START :: PayPal Support.
//	----------------------------------------------------------

	/**
	 * Used when the user returns to the site from PayPal either
	 * - cancelling their paypal checkout, set status=0
	 * - completeing their paypal checkout, set status=1
	 */
	service[services.SET_PAYPAL_STATUS] =
		function( data ) {
			var request =
				{ token  : data.token
				, status : data.status
				};
			return request;
		}

	service[services.GET_EXPRESS_CHECKOUT_DETAILS] =
		function( data ) {
			var request =
				{ token : data.token
				};
			return request;
		}

//	----------------------------------------------------------
//		END :: PayPal Support.
//	==========================================================

	service[services.ORDER_SEARCH] =
		function( data ) {
			var request = {}
			request.phone_number = data.phoneNumber;

			if ( typeof data.emailAddress != 'undefined' ) {
				request.email = data.emailAddress;
			}

			if ( typeof data.lastFourDigits != 'undefined' ) {
				request.card_number = data.lastFourDigits;
			}

			if ( typeof data.userId != 'undefined' ) {
				request.user_id = data.userId;
			}

			return request;
		}

	service[services.VIEW_ORDER] =
		function( data ) {
			var request =
				{ order_id: data.order_id
				, show_print_at_home_details: 1 //data.showPrintAtHomeDetails
				}
			return request;
		}

	service[services.GUIDE_SERVICE] =
		function( data ) {
			var request =
				{ action: "Read"
				, GUIDES:
					{ id: data.topic
					}
				}
			return request;
		}

	service[services.SEARCH_ARTICLES] =
		function( data ) {
			var request =
				{ search_string: data.searchString
				}
			return request;
		}

	service[services.INCREMENT_READ_COUNT] =
		function( data ) {
			var request =
				{ article_id: data.article_id
				}
			return request;
		}

	service[services.PURCHASE] =
		function( data ) {
			var request = data
			request.tiny_response = '1';
			request.software = clientInfoService.software;
			request.ver_id="";
			request.verify_attempt="1";
			request.version="4";
			return request;
		}

	service[services.GET_NEW_CART_ID] =
		function( data ) {
			return request;
		}


	return service;

}])
