/**
 * # Storage for navigation items to share accross controllers and in application.
 *
 * Any navigation data consumable by multiple controllers (e.g keyword sub menu shows up in slide out menu
 * but also in package details view).
 *
 */
angular.module('app').factory
	( 'navigationService'
	, function() {
			var d = new Date()
			var n = d.getTime()

			var navigationData =
			{
			  showAllKeywordItems: false
			, keywordMenuItems: []
			, serviceId: n
			}

			return navigationData
		}
	)
