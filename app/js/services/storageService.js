/**
 * # HTML5 localStorage management service
 *
 * Allows for storing and recalling data from localStorage prefixed with an
 * application specific key. Also handles object stringification and
 * de-stringification as needed
 *
 * Examples:
 *
 *     storageService.set('somekey','somevalue')
 *     storageService.set('somekey',{'key':'value'})
 *     var storedKey = storageService.get('somekey')
 *
 */
angular.module('app').factory
	( 'storageService'
	, [ '$log'
		, '$cookieStore'
		, 'utilitiesService'
		, '$location'
		, '$rootScope'
		, function
				( $log
				, $cookieStore
				, utilitiesService
				, $location
				, $rootScope
				)
			{


				var methods =
					{ data : {}
					, storage_id:
						utilitiesService.md5(
							$location.$$host
							+ $location.$$url.split('/')[1].split('?')[0]
							+ $location.$$search.device
							+ $location.$$search.e
							+ $location.$$search.l
							+ window.version
						) + '_'
					, timeout: new Date().getTime()
					, manageTimeout:
						function(_key){
							var _session = window.localStorage.getItem('_session');
							if(_session !== null){
								try{
									_session = JSON.parse(_session);
									var _now = new Date().getTime();
									if(_session.timeout < _now){
										$rootScope.resetApp();
										return;
									}
								} catch(e) {
									
								}
							}
						}
					, get:
						function( key, useCookie )	{
							var data
							  , result;

							try{
								data = localStorage.getItem(this.storage_id+key);
							} catch(e){}

							if (useCookie && !data){
								//data = $cookieStore.get(this.storage_id+key);
							}

							try {
								result = JSON.parse(data);
							} catch(e) {
								result = data;
							}

							return result
						}
					, set:
						function( key, data, useCookie ){
							if (typeof data == "object"){
								data = JSON.stringify(data);
							}

							try{
								localStorage.setItem(this.storage_id+key, data);
							} catch(e){}

							if (useCookie){
								//$cookieStore.put(this.storage_id+key,data);
							}
						}
					, remove:
						function( key , useCookie )	{
							if (useCookie){
								$cookieStore.remove(this.storage_id+key);
							}
							try {
								return localStorage.removeItem(this.storage_id+key);
							} catch( e ){
								return false;
							}
						}
					}

				
				$rootScope.$on('$routeChangeStart', function(next, current){
					var hasLocalstorage = false;
					try{
						if('localStorage' in window && window['localStorage'] !== null){
							localStorage.setItem('test', 'test');
							localStorage.removeItem('test');
							hasLocalstorage = true;
						}
					}
					catch(e){
						
					}
					if(!hasLocalstorage){
						return;
					}

					var merchantCount = 0;
					for(var i = 0; i < window.localStorage.length; i++){
						if(window.localStorage.key(i).indexOf('_initializeSocket') > -1){
							_key = window.localStorage.key(i).split('_')[0] + '_';
							merchantCount++;
						}
					}
					
					if(merchantCount != 1 || window.localStorage.length < 3){
						window.localStorage.clear();
					}
					
					var _key = utilitiesService.md5(
								$location.$$host
								+ $location.$$url.split('/')[1].split('?')[0]
								+ $location.$$search.device
								+ $location.$$search.e
								+ $location.$$search.l
								+ window.version
							) + '_';
					var _host = $location.$$host;
					var _merchant = $location.$$url.split('/')[1].split('?')[0];
					var _device = $location.$$search.device; //appDataService.device;
					var _environment = $location.$$search.e; //appDataService.environment;
					var _language = $location.$$search.l; //appDataService.langauge;
					var _version = window.version;

					var _session = window.localStorage.getItem('_session');
					if(_session === null){
						var _now = new Date().getTime();
						try{
							window.localStorage.setItem('_session', JSON.stringify(
								{
									key : _key
									, host : _host
									, merchant : _merchant
									, device : _device
									, environment : _environment
									, language : _language
									, version : _version
									, timeout : _now + _timeout
								}
							));
						} catch(e) {
							
						}
					} else{
						try{
							_session = JSON.parse(_session);
							if( _session.key != _key ){
								window.localStorage.clear();
							}
						} catch(e) {

						}
					}
				});
				
				return methods;
			}
		]
	)
