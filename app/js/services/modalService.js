angular.module( 'app' )
.factory
	( 'modalService'
	, [
	    '$compile'
	  , '$rootScope'
	  , '$timeout'
	  , '$rootScope'
	  , 'enumsService'
	  , 'languageService'
	  , 'utilitiesService'
	  , function
		( $compile
		, $rootScope
		, $timeout
		, $rootScope
		, enumsService
		, languageService
		, utilitiesService
		)
		{

			// Constants.
			var modalService =
				{ DANGER: 'danger'
				, SUCCESS: 'success'
				, PRIMARY: 'primary'
				, DEFAULT: 'default'
				};
			
			// Options.
			modalService.modal =
				{ show: false
				, headerFlag: ''
				, title: ''
				, description: ''
				, tagLine: ''
				, callback: null
				, element: utilitiesService.getObject( 'modal-description-container' )
				, flags: {}
				}
			
			modalService.displayModal = function( options ){
				/*
				Options:
					replacementKeys: Array (or string) of any text to be replaced in message.
					replacementValues: Array (or string) of what the replacementKeys should be replaced with.
					replaceInTitle: Flag that indicates whther the replacement should be in the title.
					replaceInDescription: Flag that indicates whther the replacement should be in the description.
					title: Modal title.
					description: Modal description.
					headerFlag: Determines the color of the modal header.
					callback: Callback function to be executed after button is clicked. Passes button flag item.
					buttonFlags: Array of the buttons to display in the modal.
					tagline: 
					hideAllButtons: If no button flag is passed, it defaulst to OK. Setting this to true shows no buttons.
					footer: Footer markup.
					compile: If passing markup with directive, this tell angular to compile the directive.
					
				Sample Usage:
					modalService.displayModal
						(
							{ replacementKeys: ['mollis','molli2']
							, replacementValues: [65,95]
							, title: '<span testdirective>FROM PACKAGE LIST</span>'
							, description: '<span testdirective>FROM PACKAGE LIST</span>'
							, compile: true
							, scope: $scope
							, headerFlag:'primary'
							, buttonFlags: [ enumsService.buttonFlags.OK, enumsService.buttonFlags.CANCEL ]
							, hideAllButtons: false
							, replaceInTitle: true
							, callback: function (detail){
								if(detail == $rootScope.buttonFlags.OK){
									//alert('OK')
								}
							  }
							}
						);
				*/

				var desc = options.description || ''
				  , title = options.title || '';
				
				// Default values...
				options.compile = options.compile || false;
				options.scope = options.scope || null;
				options.hideAllButtons = options.hideAllButtons || false;
				options.buttonFlags = options.buttonFlags || [ enumsService.buttonFlags.OK ];
				options.buttonFlags = ( options.hideAllButtons ) ? [] : options.buttonFlags;
				options.headerFlag = options.headerFlag || modalService.DANGER;
				options.replacementValues = options.replacementValues || '';
				options.replacementValues = angular.isArray( options.replacementValues ) ? options.replacementValues : [ options.replacementValues ];
				options.replacementKeys = options.replacementKeys || '';
				options.replacementKeys = angular.isArray( options.replacementKeys ) ? options.replacementKeys : [ options.replacementKeys ];
				options.replaceInTitle = options.replaceInTitle || false;
				options.replaceInDescription = options.replaceInDescription || true;
				
				if( options.compile && options.scope ){
					// Replace the description with the compiled directive.
					try{
						desc = ( $compile( desc )( options.scope ) )[0].innerHTML;
					} catch( e ){}
					
					try{
						title = ( $compile( title )( options.scope ) )[0].innerHTML;
					} catch( e ){}
				}
				
				if( options.replacementKeys != '' ){
					// Handles replacement of data in the description and/or title.
					if( options.replaceInTitle ){
						title =
							replaceKeys
								( options.replacementKeys
								, options.replacementValues
								, title
								)
					} else {
						title = options.title;
					}
					
					if( options.replaceInDescription ){
						desc =
							replaceKeys
								( options.replacementKeys
								, options.replacementValues
								, desc
								);
					}
				} else {
					desc = options.description;
					title = options.title;
				}
				
				showModal
					(
						{ title: title
						, description: desc
						, headerFlag: options.headerFlag
						, flags: options.buttonFlags
						, callback: options.callback
						, tagline: options.tagline
						, compile: options.compile
						, scope: options.scope
						}
					);
			}
			
			function replaceKeys( replacementKeys, replacementValues, text ){
				var str =
					languageService.updateMultipleLocaleKeysInArray
						(
							  replacementKeys
							, replacementValues
							, languageService.replaceAllMultiple
								(
									  replacementKeys
									, replacementValues
									, text
								)
						);
						
				return str;
			}
			
			function showModal(options){
				modalService.hideModal('');
				$rootScope.showingModal = true;

				// Hack for ios devices.
				var object = utilitiesService.getObject('#application-scrollContent');
				$timeout
					( function(){
						object.css( { 'pointer-events': 'none' } );
					  }
					, 600
					);

				var modal = modalService.modal;
				modal.headerFlag = options.headerFlag || '';
				modal.title = options.title || '';
				modal.description = options.description || '';
				modal.tagLine = options.tagLine || '';

				if( options.flags ){
					angular.forEach
						( options.flags
						, function( value, key ){
								modal.flags[ value ] = true;
							}
						)
				}

				modal.callback = options.callback;
				modal.show = true;
			}

			modalService.hideModal = function( detail ){
				$rootScope.showingModal = false;
				var object = utilitiesService.getObject( '#application-scrollContent' );
				
				$timeout
					( function(){
						object.css( { 'pointer-events':'all' } );
					  }
					, 600
					);

				var modal = modalService.modal;
				modal.show = false;
				modal.headerFlag = '';
				modal.title = '';
				modal.description = '';
				modal.tagLine = '';
				modal.flags = {};

				if( modal.callback && detail != '' ){
					modal.callback( detail );
					modal.callback = null;
				}
			}
			
			$rootScope.modalService = modalService;
			$rootScope.modal = modalService.modal;
			
			return modalService;
		}
	  ]
	)





