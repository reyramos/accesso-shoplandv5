angular.module('app')
.factory
( 'templateService'
, [
	  '$templateCache'
	, function
		( $templateCache
		){
			var service = {};
			
			$templateCache.put('template/datepicker/datepicker.html', '<table class="calendar-date-time-ui">'
				+'<thead>'
				+'<tr class="calendar-heading">'
				+'<th ng-click="move(-1)" colspan="2" class="previous">' +
				'<button class="btn btn-primary btn-xs"><span class="icon-arrow-left"></span></button>' +
				'</th>'
				+'<th colspan="{{rows[0].length - 4 + showWeekNumbers}}" class="center"><strong ng-bind="title"></strong></th>'
				+'<th ng-click="move(1)" colspan="2"class="next">' +
				'<button class="btn btn-primary btn-xs pull-right"><span class="icon-arrow-right"></span></button>' +
				'</th></tr>'
				+'<tr class="calendar-week-names limit" ng-show="labels.length > 0"><th ng-show="showWeekNumbers">#</th><th ng-repeat="label in labels" class="center" data-ng-bind="label"></th></tr>'
				+'</thead>'
				+'<tbody>'
				+'<tr ng-repeat="row in rows">'
				+'<td ng-show="showWeekNumbers" class="text-center"><em data-ng-bind="getWeekNumber(row)"></em></td>'
				+'<td ng-repeat="dt in row" class="text-center"><button class="btn btn-default cal-btn" ng-class="{\'date-info\': dt.isSelected, \'date-disabled\':dt.disabled}" ng-click="select(dt.date)" ng-disabled="dt.disabled"><span ng-class="{muted: ! dt.isCurrent}" ng-bind="dt.label"></span></button></td>'
				+'</tr></tbody></table>')
			
			return service;
		 }
  ]
)

