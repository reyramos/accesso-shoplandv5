/**
 * Created with JetBrains PhpStorm.
 * User: rramos
 * Date: 7/12/13
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */

angular.module('app').factory
	( 'userInfoService'
		, [ '$timeout'
		  , '$rootScope'
		  , 'utilitiesService'
		  , 'storageService'
		  , '$log'
		  , '$routeParams'
		  , '$location'
		  , 'cartService'
		  , 'appDataService'
		  , 'enumsService'

		  , function(
			$timeout
			, $rootScope
			, utilitiesService
			, storageService
			, $log
			, $routeParams
			, $location
			, cartService
			, appDataService
			, enumsService

			) {

			var userInfoService = {
			userInfo :
				{
					firstName: 'Shopland'
					, lastName: 'v5'
					, country: 'US'
					, address: '1025 Greenwood Boulevard'
					, address2: 'Suite 500'
					, city: 'Lake Mary'
					, state: 'FL'
					, zip: '32746'
					, phone: '1234567890'
					, mobile: ''
					, email: 'frontend-js@accesso.com'
					, emailVerify: 'frontend-js@accesso.com'
				}
			, payment: {
					'ccType': 'Visa'
					, 'credicard': '4111111111111111'
					, 'securityCode': '123'
					, 'month': '05'
					, 'year': '20'
					, 'cardExpirationDate': '2005'
					, 'billingId': '2'
					, 'billingType': 'CCD'
					, 'authType': 'EC'
					, 'cepayMerchantId': '106001'
				}
			, shipping: {}
			, billing: {}
			, paypalPurchase : {}
			, localStorage: {}
			, deliveryMethods: []
			};


			userInfoService.__constructor = function(){

				userInfoService.localStorage = storageService.get('_userInfoService') || {}

				$log.log('userInfoService.__constructor', userInfoService.localStorage)
				//if localStorage is not empty set the values to their keys
				if(!utilitiesService.isEmpty(userInfoService.localStorage)){
					Object.keys(userInfoService.localStorage).forEach(function(key) {
						userInfoService[key] = userInfoService.localStorage[key]
					});
				}
			}()

			userInfoService.set = function (key,data){

				userInfoService[key] = data
				userInfoService.localStorage[key] = data
				storageService.set('_userInfoService',userInfoService.localStorage, true)

			}

			userInfoService.get = function (key){
				return userInfoService.localStorage[key]
			}


			userInfoService.getBilling = function(){
				if(utilitiesService.isEmpty(userInfoService.billing && storageService.get('_userInfoService' ))){
					userInfoService.localStorage = storageService.get('_userInfoService' )
					userInfoService.billing = userInfoService.localStorage.billing
				}

				return userInfoService.billing;
			}

			userInfoService.destroy = function(){
				storageService.remove('_userInfoService', true )
			}

			userInfoService.shippingRequired = function( options, callback ){

				var delMethFromCookie = appDataService.storageService.get( '_deliveryMethods', true );
				userInfoService.deliveryMethods = [];

				if( cartService &&
					cartService.cartDetails &&
					cartService.cartDetails.CART &&
					cartService.cartDetails.CART.SHIPPING_METHODS &&
					cartService.cartDetails.CART.SHIPPING_METHODS.S){
					//Convert to array if needed.
					cartService.cartDetails.CART.SHIPPING_METHODS.S = angular.isArray( cartService.cartDetails.CART.SHIPPING_METHODS.S ) ? cartService.cartDetails.CART.SHIPPING_METHODS.S : [ cartService.cartDetails.CART.SHIPPING_METHODS.S ];
					userInfoService.deliveryMethods = cartService.cartDetails.CART.SHIPPING_METHODS.S;
				} else if( delMethFromCookie != null && typeof( delMethFromCookie ) != 'undefined' && delMethFromCookie != 'undefined' ){
					userInfoService.deliveryMethods = delMethFromCookie;
				} else if( typeof( cartService ) == 'undefined' ||
					typeof( cartService.cartDetails ) == 'undefined' ||
					typeof( cartService.cartDetails.CART ) == 'undefined' ) {
					userInfoService.getCartSummary( options, callback );
					return 'gettingCartSummary';
				}

				// If there are no delivery methods or the only one is Mobile...
				if( userInfoService.deliveryMethods.length > 0 ){
					// Store delivery methods in cookies as fallback to local storage.
					appDataService.storageService.set( '_deliveryMethods', userInfoService.deliveryMethods, true );
					if( userInfoService.deliveryMethods.length == 1 && userInfoService.deliveryMethods[ 0 ].ship_method == 'M' ){
						return false;
					} else if( userInfoService.deliveryMethods.length > 1 ){
						return true;
					}
				}


				return false;
			}

			userInfoService.getCartSummary = function( options, callback ){
				$rootScope.pendingRequests.push( enumsService.services.GET_CART_SUMMARY );
				$rootScope.showSpinner( $rootScope.i18n.ApplicationLabels.updatingCartLoadMsg );

				// TODO: Need to move getCartSummary Request from cart controller to carService
				// and call getCartSummary in cartService instead of calling it from here.
				appDataService.fetch
					( enumsService.services.GET_CART_SUMMARY
						, true
						, options
						, true
					).then
				( function( data ) {
					$rootScope.removePendingRequest( enumsService.services.GET_CART_SUMMARY );
					$rootScope.hideSpinner();
					if( !cartService.checkForItemErrors( data ) ) {
						if( data.status == enumsService.status.OK ) {
							// Update the cart object.
							cartService.updateInternals( data );
							if( callback ){
								callback();
							}
						}
					}

					$rootScope.hideSpinner();
				})
			}

			userInfoService.getDeliveryRouting = function(){
				var shipReq = userInfoService.shippingRequired( { skipRequests : 'SetExpressCheckout' }, userInfoService.getDeliveryRouting );
				if( shipReq == 'gettingCartSummary' ){
					return;
				}

				// If no delivery methods or only one delivery method and it's M redirect to billing.
				if( userInfoService.deliveryMethods.length == 0 ||
					( userInfoService.deliveryMethods.length == 1 && userInfoService.deliveryMethods[0].ship_method == 'M' ) ){
					// If no delivery method, or delivery is N/A, default to Mobile shipping.
					appDataService.shipping = appDataService.shipping || {};
					appDataService.shipping.deliveryMethod = 'M';
					appDataService.shipping.deliveryAmount = 0;
					appDataService.shipping.shippingAvailable = false;
					return $rootScope.routes.BILLING;
				}else{

					return $rootScope.routes.SHIPPING;
				}

			}


			return userInfoService;
		}])
